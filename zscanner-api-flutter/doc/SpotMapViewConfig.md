# zscanner_api.model.SpotMapViewConfig

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parentSpotId** | **String** |  | [optional] 
**viewId** | **String** |  | 
**label** | **String** |  | 
**resourceId** | **String** |  | 
**type** | [**SpotMapViewType**](SpotMapViewType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


