# zscanner_api.model.ZScannerApplicationManifest

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**systemProtection** | **String** |  | [optional] [default to 'optional']
**applicationProtection** | **String** |  | [optional] [default to 'optional']
**skipJailBreakCheck** | **bool** |  | [optional] [default to false]
**hasCustomVibrations** | **bool** |  | [optional] [default to false]
**showCrashButton** | **bool** |  | [optional] [default to false]
**showServerUrl** | **bool** |  | [optional] [default to false]
**localization** | **BuiltMap&lt;String, String&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


