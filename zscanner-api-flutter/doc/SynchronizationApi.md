# zscanner_api.api.SynchronizationApi

## Load the API package
```dart
import 'package:zscanner_api/api.dart';
```

All URIs are relative to *https://zscanner.fnol.dev.bindworks.eu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**synchronize**](SynchronizationApi.md#synchronize) | **POST** /v4/sync | 


# **synchronize**
> ZScannerSynchronizationResponse synchronize(zScannerSynchronizationRequest)



Synchronize all information about the client

### Example
```dart
import 'package:zscanner_api/api.dart';

final api = ZscannerApi().getSynchronizationApi();
final ZScannerSynchronizationRequest zScannerSynchronizationRequest = ; // ZScannerSynchronizationRequest | 

try {
    final response = api.synchronize(zScannerSynchronizationRequest);
    print(response);
} catch on DioError (e) {
    print('Exception when calling SynchronizationApi->synchronize: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zScannerSynchronizationRequest** | [**ZScannerSynchronizationRequest**](ZScannerSynchronizationRequest.md)|  | [optional] 

### Return type

[**ZScannerSynchronizationResponse**](ZScannerSynchronizationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

