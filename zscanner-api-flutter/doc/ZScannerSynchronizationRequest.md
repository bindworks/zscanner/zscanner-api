# zscanner_api.model.ZScannerSynchronizationRequest

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requiredCaps** | **BuiltList&lt;String&gt;** | DEPRECATED, pass it in the X-Required-Caps header | [optional] 
**supportedCaps** | **BuiltList&lt;String&gt;** | DEPRECATED, pass it in the X-Supported-Caps header | [optional] 
**metadata** | [**BuiltList&lt;ZScannerMetadataItem&gt;**](ZScannerMetadataItem.md) |  | 
**state** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


