# zscanner_api.model.ZScannerSubfolderSchema

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**typeId** | **String** |  | 
**label** | **String** |  | 
**moduleId** | **String** |  | [optional] 
**moduleVersion** | **String** |  | [optional] [default to 'any']
**config** | [**ZScannerSubfolderConfigUnion**](ZScannerSubfolderConfigUnion.md) |  | [optional] 
**displaySchema** | [**BuiltList&lt;ZScannerMetadataSchemaItem&gt;**](ZScannerMetadataSchemaItem.md) |  | 
**createSchema** | [**BuiltList&lt;ZScannerMetadataSchemaItem&gt;**](ZScannerMetadataSchemaItem.md) |  | 
**updateSchema** | [**BuiltList&lt;ZScannerMetadataSchemaItem&gt;**](ZScannerMetadataSchemaItem.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


