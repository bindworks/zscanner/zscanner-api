# zscanner_api.api.FoldersApi

## Load the API package
```dart
import 'package:zscanner_api/api.dart';
```

All URIs are relative to *https://zscanner.fnol.dev.bindworks.eu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createFolder**](FoldersApi.md#createfolder) | **PUT** /v5/folders | 
[**findFolders**](FoldersApi.md#findfolders) | **POST** /v5/folders | 
[**findFoldersV4**](FoldersApi.md#findfoldersv4) | **POST** /v4/folders | 
[**loadSubfolderMedia**](FoldersApi.md#loadsubfoldermedia) | **POST** /v4/folders/{folderId}/subfolders/{subfolderTypeId}/{subfolderId}/media | 
[**loadSubfolders**](FoldersApi.md#loadsubfolders) | **POST** /v4/folders/{folderId}/subfolders | 


# **createFolder**
> ZScannerFoundFolder createFolder(zScannerCreateFolderRequest)



Create folder

### Example
```dart
import 'package:zscanner_api/api.dart';

final api = ZscannerApi().getFoldersApi();
final ZScannerCreateFolderRequest zScannerCreateFolderRequest = ; // ZScannerCreateFolderRequest | 

try {
    final response = api.createFolder(zScannerCreateFolderRequest);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FoldersApi->createFolder: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zScannerCreateFolderRequest** | [**ZScannerCreateFolderRequest**](ZScannerCreateFolderRequest.md)|  | [optional] 

### Return type

[**ZScannerFoundFolder**](ZScannerFoundFolder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findFolders**
> BuiltList<ZScannerFoundFolder> findFolders(zScannerFindFoldersRequest)



Find folders by criteria

### Example
```dart
import 'package:zscanner_api/api.dart';

final api = ZscannerApi().getFoldersApi();
final ZScannerFindFoldersRequest zScannerFindFoldersRequest = ; // ZScannerFindFoldersRequest | 

try {
    final response = api.findFolders(zScannerFindFoldersRequest);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FoldersApi->findFolders: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zScannerFindFoldersRequest** | [**ZScannerFindFoldersRequest**](ZScannerFindFoldersRequest.md)|  | [optional] 

### Return type

[**BuiltList&lt;ZScannerFoundFolder&gt;**](ZScannerFoundFolder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findFoldersV4**
> BuiltList<ZScannerFolder> findFoldersV4(zScannerFindFoldersRequest)



Find folders by criteria

### Example
```dart
import 'package:zscanner_api/api.dart';

final api = ZscannerApi().getFoldersApi();
final ZScannerFindFoldersRequest zScannerFindFoldersRequest = ; // ZScannerFindFoldersRequest | 

try {
    final response = api.findFoldersV4(zScannerFindFoldersRequest);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FoldersApi->findFoldersV4: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zScannerFindFoldersRequest** | [**ZScannerFindFoldersRequest**](ZScannerFindFoldersRequest.md)|  | [optional] 

### Return type

[**BuiltList&lt;ZScannerFolder&gt;**](ZScannerFolder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **loadSubfolderMedia**
> BuiltList<ZScannerUploadBatchMedia> loadSubfolderMedia(folderId, subfolderTypeId, subfolderId, zScannerLoadSubfolderMediaRequest)



Load media of a subfolder of a folder

### Example
```dart
import 'package:zscanner_api/api.dart';

final api = ZscannerApi().getFoldersApi();
final String folderId = folderId_example; // String | 
final String subfolderTypeId = subfolderTypeId_example; // String | 
final String subfolderId = subfolderId_example; // String | 
final ZScannerLoadSubfolderMediaRequest zScannerLoadSubfolderMediaRequest = ; // ZScannerLoadSubfolderMediaRequest | 

try {
    final response = api.loadSubfolderMedia(folderId, subfolderTypeId, subfolderId, zScannerLoadSubfolderMediaRequest);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FoldersApi->loadSubfolderMedia: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **folderId** | **String**|  | 
 **subfolderTypeId** | **String**|  | 
 **subfolderId** | **String**|  | 
 **zScannerLoadSubfolderMediaRequest** | [**ZScannerLoadSubfolderMediaRequest**](ZScannerLoadSubfolderMediaRequest.md)|  | [optional] 

### Return type

[**BuiltList&lt;ZScannerUploadBatchMedia&gt;**](ZScannerUploadBatchMedia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **loadSubfolders**
> BuiltList<ZScannerSubfolder> loadSubfolders(folderId, zScannerLoadSubfoldersRequest)



Load subfolders of a folder

### Example
```dart
import 'package:zscanner_api/api.dart';

final api = ZscannerApi().getFoldersApi();
final String folderId = folderId_example; // String | 
final ZScannerLoadSubfoldersRequest zScannerLoadSubfoldersRequest = ; // ZScannerLoadSubfoldersRequest | 

try {
    final response = api.loadSubfolders(folderId, zScannerLoadSubfoldersRequest);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FoldersApi->loadSubfolders: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **folderId** | **String**|  | 
 **zScannerLoadSubfoldersRequest** | [**ZScannerLoadSubfoldersRequest**](ZScannerLoadSubfoldersRequest.md)|  | [optional] 

### Return type

[**BuiltList&lt;ZScannerSubfolder&gt;**](ZScannerSubfolder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

