# zscanner_api.model.ZScannerFindFoldersRequest

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata** | [**BuiltList&lt;ZScannerMetadataItem&gt;**](ZScannerMetadataItem.md) |  | 
**internalId** | **String** |  | [optional] 
**fullText** | **String** |  | [optional] 
**suggestions** | **bool** |  | [optional] 
**barCodeType** | **String** |  | [optional] 
**barCodeValue** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


