# zscanner_api.model.ZScannerConfig

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**folderHistoryDisabled** | **bool** |  | [optional] 
**scannerEnabled** | **bool** |  | [optional] 
**scannerTypes** | **BuiltList&lt;String&gt;** |  | [optional] 
**createFolderEnabled** | **bool** |  | [optional] 
**submissionConfirmationEnabled** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


