# zscanner_api.model.Map2dConfig

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdTs** | [**DateTime**](DateTime.md) |  | 
**updatedTs** | [**DateTime**](DateTime.md) |  | 
**spotMetaId** | **String** |  | 
**views** | [**BuiltList&lt;SpotMapViewConfig&gt;**](SpotMapViewConfig.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


