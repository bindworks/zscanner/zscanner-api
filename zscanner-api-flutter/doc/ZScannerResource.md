# zscanner_api.model.ZScannerResource

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resourceId** | **String** |  | 
**contentType** | **String** |  | 
**contentLength** | **int** |  | 
**createdTs** | [**DateTime**](DateTime.md) |  | 
**updatedTs** | [**DateTime**](DateTime.md) |  | 
**inlineData** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


