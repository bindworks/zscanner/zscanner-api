# zscanner_api.model.ZScannerStickerConfig

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fontFace** | [**ZScannerStickerConfigFontFace**](ZScannerStickerConfigFontFace.md) |  | [optional] 
**textStickers** | **BuiltList&lt;String&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


