# zscanner_api.model.ZScannerUploadBatchMediaRelation

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relatedMediaRole** | **String** | Roles defined so far: SOURCE, STICKERED, WOUND_MASK, ORIGINAL_WOUND_MASK, WOUND_ANALYZED  | 
**relatedMediaId** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


