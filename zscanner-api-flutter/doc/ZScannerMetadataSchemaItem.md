# zscanner_api.model.ZScannerMetadataSchemaItem

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metaId** | **String** |  | 
**label** | **String** |  | 
**hidden** | **bool** |  | [optional] [default to false]
**hint** | **String** | Known hints: - `czBirthNumber` (this is a Czech birth number) - `birthDate` (this is a birth date) - `sex` (this is sex M or F)  | [optional] 
**type** | **String** | Supported types: - string - number - boolean - date - date-time - multi-string  | 
**required_** | **bool** |  | [optional] [default to false]
**enum_** | **String** |  | [optional] 
**default_** | [**ZScannerMetadataDefaultValue**](ZScannerMetadataDefaultValue.md) |  | [optional] 
**widgetId** | **String** |  | [optional] 
**widgetConfig** | [**ZScannerWidgetConfigUnion**](ZScannerWidgetConfigUnion.md) |  | [optional] 
**validators** | [**BuiltList&lt;ZScannerMetadataValidator&gt;**](ZScannerMetadataValidator.md) |  | [optional] [default to ListBuilder()]
**serverOnly** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


