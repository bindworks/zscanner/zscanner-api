# zscanner_api.model.ZScannerAuthenticationManifest

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**openid** | [**ZScannerOpenIDAuthenticationManifest**](ZScannerOpenIDAuthenticationManifest.md) |  | [optional] 
**seacat3** | [**ZScannerSeaCat3AuthenticationManifest**](ZScannerSeaCat3AuthenticationManifest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


