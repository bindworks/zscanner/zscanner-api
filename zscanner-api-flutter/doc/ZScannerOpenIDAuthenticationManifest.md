# zscanner_api.model.ZScannerOpenIDAuthenticationManifest

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manifestUrl** | **String** |  | 
**clientId** | **String** |  | 
**scopes** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


