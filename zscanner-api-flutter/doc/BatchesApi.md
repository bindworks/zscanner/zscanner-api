# zscanner_api.api.BatchesApi

## Load the API package
```dart
import 'package:zscanner_api/api.dart';
```

All URIs are relative to *https://zscanner.fnol.dev.bindworks.eu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBatch**](BatchesApi.md#createbatch) | **POST** /v4/batches | 
[**finishBatch**](BatchesApi.md#finishbatch) | **POST** /v4/batches/{uuid}/finish | 
[**uploadBatchMedia**](BatchesApi.md#uploadbatchmedia) | **POST** /v4/batches/{uuid}/media/{mediaId} | 


# **createBatch**
> createBatch(zScannerUploadBatchRequest)



Create a batch on the server

### Example
```dart
import 'package:zscanner_api/api.dart';

final api = ZscannerApi().getBatchesApi();
final ZScannerUploadBatchRequest zScannerUploadBatchRequest = ; // ZScannerUploadBatchRequest | 

try {
    api.createBatch(zScannerUploadBatchRequest);
} catch on DioError (e) {
    print('Exception when calling BatchesApi->createBatch: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zScannerUploadBatchRequest** | [**ZScannerUploadBatchRequest**](ZScannerUploadBatchRequest.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **finishBatch**
> finishBatch(uuid)



Finish batch

### Example
```dart
import 'package:zscanner_api/api.dart';

final api = ZscannerApi().getBatchesApi();
final String uuid = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.finishBatch(uuid);
} catch on DioError (e) {
    print('Exception when calling BatchesApi->finishBatch: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **String**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **uploadBatchMedia**
> uploadBatchMedia(uuid, mediaId, file)



Upload batch media to the server

### Example
```dart
import 'package:zscanner_api/api.dart';

final api = ZscannerApi().getBatchesApi();
final String uuid = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final String mediaId = mediaId_example; // String | 
final MultipartFile file = BINARY_DATA_HERE; // MultipartFile | 

try {
    api.uploadBatchMedia(uuid, mediaId, file);
} catch on DioError (e) {
    print('Exception when calling BatchesApi->uploadBatchMedia: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | **String**|  | 
 **mediaId** | **String**|  | 
 **file** | **MultipartFile**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

