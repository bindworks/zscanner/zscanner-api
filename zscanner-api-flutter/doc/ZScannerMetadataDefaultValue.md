# zscanner_api.model.ZScannerMetadataDefaultValue

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fromMeta** | **String** |  | [optional] 
**fromTemplate** | **String** |  | [optional] 
**dateValue** | **String** |  | [optional] 
**stringValue** | **String** |  | [optional] 
**numberValue** | **num** |  | [optional] 
**booleanValue** | **bool** |  | [optional] 
**dateTimeValue** | [**DateTime**](DateTime.md) |  | [optional] 
**dateRelativeValue** | **int** |  | [optional] 
**multiStringValue** | **BuiltList&lt;String&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


