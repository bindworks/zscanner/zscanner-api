# zscanner_api.model.ZScannerLoadSubfoldersRequest

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata** | [**BuiltList&lt;ZScannerMetadataItem&gt;**](ZScannerMetadataItem.md) |  | 
**typeId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


