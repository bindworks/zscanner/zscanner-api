# zscanner_api.model.ZScannerFolder

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**internalId** | **String** |  | 
**externalId** | **String** |  | 
**name** | **String** |  | 
**type** | **String** |  | 
**metadata** | [**BuiltList&lt;ZScannerMetadataItem&gt;**](ZScannerMetadataItem.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


