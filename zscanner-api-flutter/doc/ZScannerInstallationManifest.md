# zscanner_api.model.ZScannerInstallationManifest

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**installationId** | **String** |  | 
**name** | **String** |  | 
**iconId** | **String** |  | [optional] 
**environment** | **String** |  | 
**authentication** | [**ZScannerAuthenticationManifest**](ZScannerAuthenticationManifest.md) |  | 
**server** | [**ZScannerServerManifest**](ZScannerServerManifest.md) |  | 
**client** | [**ZScannerClientManifest**](ZScannerClientManifest.md) |  | 
**application** | [**ZScannerApplicationManifest**](ZScannerApplicationManifest.md) |  | [optional] 
**theme** | [**ZScannerThemeManifest**](ZScannerThemeManifest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


