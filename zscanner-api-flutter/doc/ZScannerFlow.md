# zscanner_api.model.ZScannerFlow

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**flowId** | **String** |  | 
**label** | **String** |  | 
**hidden** | **bool** |  | [optional] [default to false]
**icon** | **String** | Icon can take several forms: - `str:...` plain string (takes advantage of emojis) - `material:...` material icon (by Flutter library code) - `res:...` specific resource by UUID  | [optional] 
**moduleId** | **String** |  | 
**moduleVersion** | **String** | Specification according to https://pub.dev/documentation/pub_semver/latest/pub_semver/VersionConstraint/VersionConstraint.parse.html  | [default to 'any']
**moduleConfig** | [**ZScannerModuleConfigUnion**](ZScannerModuleConfigUnion.md) |  | [optional] 
**metadataSchema** | [**BuiltList&lt;ZScannerMetadataSchemaItem&gt;**](ZScannerMetadataSchemaItem.md) |  | 
**mediaMetadataSchema** | [**BuiltList&lt;ZScannerMetadataSchemaItem&gt;**](ZScannerMetadataSchemaItem.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


