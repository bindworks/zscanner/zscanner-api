# zscanner_api.model.ZScannerUploadBatchMedia

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mediaId** | **String** |  | 
**contentType** | **String** |  | 
**contentLength** | **int** |  | 
**photoTs** | [**DateTime**](DateTime.md) |  | [optional] 
**metadata** | [**BuiltList&lt;ZScannerMetadataItem&gt;**](ZScannerMetadataItem.md) |  | [optional] 
**subfolderTypeId** | **String** |  | [optional] 
**subfolderId** | **String** |  | [optional] 
**relations** | [**BuiltList&lt;ZScannerUploadBatchMediaRelation&gt;**](ZScannerUploadBatchMediaRelation.md) |  | [optional] 
**moduleData** | [**ZScannerModuleDataUnion**](ZScannerModuleDataUnion.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


