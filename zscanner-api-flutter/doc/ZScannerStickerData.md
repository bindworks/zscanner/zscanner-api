# zscanner_api.model.ZScannerStickerData

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fontFace** | **String** |  | [optional] 
**text** | **String** |  | [optional] 
**textColor** | **String** |  | [optional] 
**boundingBox** | **BuiltList&lt;int&gt;** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


