# zscanner_api.model.ZScannerMetadataValidator

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**minLength** | **int** |  | [optional] 
**maxLength** | **int** |  | [optional] 
**maxDateRelative** | **int** | Maximum date specified relative to today. I.e. TODAY + maxDateRelative is the maximum date valid | [optional] 
**minDateRelative** | **int** | Minimum date specified relative to today. I.e. TODAY - minDateRelative is the maximum date valid | [optional] 
**czBirthNumber** | **bool** | String is a czech birth number | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


