# zscanner_api.model.SpotMapConfig

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdTs** | [**DateTime**](DateTime.md) |  | 
**updatedTs** | [**DateTime**](DateTime.md) |  | 
**spotMetaId** | **String** |  | 
**views** | [**BuiltList&lt;SpotMapViewConfig&gt;**](SpotMapViewConfig.md) |  | 
**spots** | [**BuiltList&lt;SpotMapSpotConfig&gt;**](SpotMapSpotConfig.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


