# zscanner_api.model.ZScannerUploadBatchRequest

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **String** |  | 
**folderId** | **String** |  | 
**flowId** | **String** |  | 
**moduleId** | **String** |  | 
**moduleVersion** | **String** |  | [default to 'any']
**createdTs** | [**DateTime**](DateTime.md) |  | 
**media** | [**BuiltList&lt;ZScannerUploadBatchMedia&gt;**](ZScannerUploadBatchMedia.md) |  | 
**metadata** | [**BuiltList&lt;ZScannerMetadataItem&gt;**](ZScannerMetadataItem.md) |  | 
**subfolders** | [**BuiltList&lt;ZScannerSubfolder&gt;**](ZScannerSubfolder.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


