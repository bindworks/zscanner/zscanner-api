# zscanner_api.model.SpotMapSpotConfig

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**spotId** | **String** |  | 
**label** | **String** |  | 
**viewId** | **String** |  | 
**coordinates** | [**SpotMapSpotConfigCoordinates**](SpotMapSpotConfigCoordinates.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


