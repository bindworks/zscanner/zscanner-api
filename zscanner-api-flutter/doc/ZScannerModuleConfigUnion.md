# zscanner_api.model.ZScannerModuleConfigUnion

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subfolder** | **String** |  | [optional] 
**subfolderTypeId** | **String** |  | [optional] 
**maxVideoDuration** | **int** | seconds | [optional] [default to 15]
**recordAudio** | **String** |  | [optional] [default to 'ALLOW-DEFAULT-YES']
**allowMimeTypes** | **BuiltList&lt;String&gt;** |  | [optional] [default to ListBuilder()]
**allowShareTo** | **bool** |  | [optional] 
**allowGallery** | **bool** |  | [optional] 
**allowMultiselect** | **bool** |  | [optional] 
**skipToVictory** | **bool** |  | [optional] 
**skipEdgeRecognition** | **bool** |  | [optional] 
**skipStickerBurning** | **bool** |  | [optional] 
**stickers** | [**ZScannerStickerConfig**](ZScannerStickerConfig.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


