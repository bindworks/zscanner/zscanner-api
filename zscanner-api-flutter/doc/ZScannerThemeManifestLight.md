# zscanner_api.model.ZScannerThemeManifestLight

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**textScaleFactor** | **int** |  | [optional] 
**mainFontFamily** | **String** |  | [optional] 
**secondaryFontFamily** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


