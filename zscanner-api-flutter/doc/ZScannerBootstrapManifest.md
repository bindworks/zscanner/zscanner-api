# zscanner_api.model.ZScannerBootstrapManifest

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**global** | [**ZScannerGlobalManifest**](ZScannerGlobalManifest.md) |  | 
**installations** | [**BuiltList&lt;ZScannerInstallationManifest&gt;**](ZScannerInstallationManifest.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


