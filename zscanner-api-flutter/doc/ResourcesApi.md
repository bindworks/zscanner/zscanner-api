# zscanner_api.api.ResourcesApi

## Load the API package
```dart
import 'package:zscanner_api/api.dart';
```

All URIs are relative to *https://zscanner.fnol.dev.bindworks.eu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**loadResource**](ResourcesApi.md#loadresource) | **GET** /v4/resources/{resourceId} | 


# **loadResource**
> Uint8List loadResource(resourceId)



Load resource data

### Example
```dart
import 'package:zscanner_api/api.dart';

final api = ZscannerApi().getResourcesApi();
final String resourceId = resourceId_example; // String | 

try {
    final response = api.loadResource(resourceId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ResourcesApi->loadResource: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceId** | **String**|  | 

### Return type

[**Uint8List**](Uint8List.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

