# zscanner_api.model.ZScannerSynchronizationResponse

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nextState** | **String** |  | 
**metadataValid** | **bool** |  | 
**error** | [**ZScannerError**](ZScannerError.md) |  | [optional] 
**blockApplication** | **bool** |  | [optional] 
**supportedCaps** | **BuiltList&lt;String&gt;** | DEPRECATED, use the value from the X-Supported-Caps: header | [optional] 
**config** | [**ZScannerConfig**](ZScannerConfig.md) |  | [optional] 
**me** | [**ZScannerUser**](ZScannerUser.md) |  | [optional] 
**metadataSchema** | [**BuiltList&lt;ZScannerMetadataSchemaItem&gt;**](ZScannerMetadataSchemaItem.md) | Schema of metadata. If not empty, the whole list is replaced  | [optional] 
**folderSchema** | [**BuiltList&lt;ZScannerMetadataSchemaItem&gt;**](ZScannerMetadataSchemaItem.md) | Schema of folder metadata. If not empty, the whole list is replaced  | [optional] 
**createFolderSchema** | [**BuiltList&lt;ZScannerMetadataSchemaItem&gt;**](ZScannerMetadataSchemaItem.md) | Schema of folder metadata. If not empty, the whole list is replaced  | [optional] 
**subfolderSchemata** | [**BuiltList&lt;ZScannerSubfolderSchema&gt;**](ZScannerSubfolderSchema.md) | Schema of metadata of various types of subfolders  | [optional] 
**flows** | [**BuiltList&lt;ZScannerFlow&gt;**](ZScannerFlow.md) | List of flows. If not empty, the whole list is replaced  | [optional] 
**updatedEnums** | [**BuiltList&lt;ZScannerEnum&gt;**](ZScannerEnum.md) | List of flows. If not empty, enums are upserted to the ones currently on the client  | [optional] 
**updatedResources** | [**BuiltList&lt;ZScannerResource&gt;**](ZScannerResource.md) | List of resources. If not empty, resources are upserted to the ones currently on the client  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


