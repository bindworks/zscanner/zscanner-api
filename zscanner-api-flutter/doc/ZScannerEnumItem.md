# zscanner_api.model.ZScannerEnumItem

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itemId** | **String** |  | 
**code** | **String** |  | [optional] 
**icon** | **String** | Icon can take several forms: - `str:...` plain string (takes advantage of emojis) - `material:...` material icon (by Flutter library code) - `res:...` specific resource by UUID  | [optional] 
**label** | **String** |  | 
**help** | **String** |  | [optional] 
**parentItemId** | **String** |  | [optional] 
**selectable** | **bool** |  | [optional] [default to true]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


