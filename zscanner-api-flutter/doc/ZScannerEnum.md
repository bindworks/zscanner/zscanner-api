# zscanner_api.model.ZScannerEnum

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enumId** | **String** |  | 
**enumVersion** | **String** |  | [optional] 
**enumType** | **String** |  | 
**createdTs** | [**DateTime**](DateTime.md) |  | 
**updatedTs** | [**DateTime**](DateTime.md) |  | 
**label** | **String** |  | 
**items** | [**BuiltList&lt;ZScannerEnumItem&gt;**](ZScannerEnumItem.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


