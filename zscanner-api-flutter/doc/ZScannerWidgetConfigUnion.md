# zscanner_api.model.ZScannerWidgetConfigUnion

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**minLines** | **int** |  | [optional] 
**maxLines** | **int** |  | [optional] 
**searchable** | **bool** |  | [optional] 
**allowOther** | **bool** |  | [optional] 
**maxDecimalPlaces** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


