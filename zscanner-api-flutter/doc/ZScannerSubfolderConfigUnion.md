# zscanner_api.model.ZScannerSubfolderConfigUnion

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**spotMap** | [**SpotMapConfig**](SpotMapConfig.md) |  | [optional] 
**map2d** | [**Map2dConfig**](Map2dConfig.md) |  | [optional] 
**naked** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


