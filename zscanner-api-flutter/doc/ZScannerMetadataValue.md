# zscanner_api.model.ZScannerMetadataValue

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dateValue** | **String** |  | [optional] 
**stringValue** | **String** |  | [optional] 
**numberValue** | **num** |  | [optional] 
**booleanValue** | **bool** |  | [optional] 
**dateTimeValue** | [**DateTime**](DateTime.md) |  | [optional] 
**multiStringValue** | **BuiltList&lt;String&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


