# zscanner_api.api.WoundsApi

## Load the API package
```dart
import 'package:zscanner_api/api.dart';
```

All URIs are relative to *https://zscanner.fnol.dev.bindworks.eu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**woundMarkImage**](WoundsApi.md#woundmarkimage) | **POST** /v4/modules/wounds/mark-wound | 


# **woundMarkImage**
> Uint8List woundMarkImage(outputWidth, outputType, file)



Produce a masking image out of a wound photo. Only implemented if the server supports sm.wound-analysis.0 capability

### Example
```dart
import 'package:zscanner_api/api.dart';

final api = ZscannerApi().getWoundsApi();
final int outputWidth = 56; // int | 
final String outputType = outputType_example; // String | 
final MultipartFile file = BINARY_DATA_HERE; // MultipartFile | 

try {
    final response = api.woundMarkImage(outputWidth, outputType, file);
    print(response);
} catch on DioError (e) {
    print('Exception when calling WoundsApi->woundMarkImage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **outputWidth** | **int**|  | [optional] 
 **outputType** | **String**|  | [optional] [default to 'image']
 **file** | **MultipartFile**|  | [optional] 

### Return type

[**Uint8List**](Uint8List.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: image/jpeg, application/octet-stream, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

