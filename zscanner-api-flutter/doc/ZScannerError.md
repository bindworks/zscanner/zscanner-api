# zscanner_api.model.ZScannerError

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exception** | **String** |  | [optional] 
**error** | **String** |  | [optional] 
**uuid** | **String** |  | [optional] 
**metaErrors** | [**BuiltList&lt;ZScannerMetaError&gt;**](ZScannerMetaError.md) |  | [optional] 
**mediaIds** | **BuiltList&lt;String&gt;** |  | [optional] 
**folderId** | **String** |  | [optional] 
**subfolderId** | **String** |  | [optional] 
**resourceId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


