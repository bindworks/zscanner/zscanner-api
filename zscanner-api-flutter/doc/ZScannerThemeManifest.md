# zscanner_api.model.ZScannerThemeManifest

## Load the model package
```dart
import 'package:zscanner_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logoUrl** | **String** |  | [optional] 
**secondaryLogoUrl** | **String** |  | [optional] 
**mainColor** | **String** |  | [optional] 
**mainLightColor** | **String** |  | [optional] 
**secondaryColor** | **String** |  | [optional] 
**backgroundColor** | **String** |  | [optional] 
**additionalColor** | **String** |  | [optional] 
**textFieldFillColor** | **String** |  | [optional] 
**outlineColor** | **String** |  | [optional] 
**shadowColor** | **String** |  | [optional] 
**textColor** | **String** |  | [optional] 
**fabForegroundColor** | **String** |  | [optional] 
**fabBackgroundColor** | **String** |  | [optional] 
**light** | [**ZScannerThemeManifestLight**](ZScannerThemeManifestLight.md) |  | [optional] 
**dark** | [**ZScannerThemeManifestDark**](ZScannerThemeManifestDark.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


