#!/usr/bin/env bash

set -e

if ! npx --version >/dev/null 2>&1; then
    echo "Please install node and npm. We need npx to run"
    exit 1
fi

if ! flutter --version >/dev/null 2>&1; then
    echo "Could not execute flutter. Is it in the path?"
    exit 1
fi

cd "$( dirname "${BASH_SOURCE[0]}" )"

if [ -e .build.timestamp ] && [ "$(find .build.timestamp -anewer ../openapi.yml -and -anewer ../metadata.yml | wc -l)" = "1" ]; then
  echo "*** Source files have not changed, not rebuilding."
  exit 0
fi

echo "*** Determining version..."

if [ "$1" = "" ]; then
  # shellcheck disable=SC2016
  MVN_VERSION=$(cd .. && ./mvnw -q \
      -Dexec.executable=echo \
      -Dexec.args='${project.version}' \
      --non-recursive \
      exec:exec 2>/dev/null)
else
  MVN_VERSION="$1"
fi

DART_VERSION=${MVN_VERSION%-*}

echo "Maven Version: $MVN_VERSION"
echo "Dart Version: $DART_VERSION"

sed -e "s#__VERSION__#${DART_VERSION}#" < pubspec.yaml.template > pubspec.yaml

echo "*** Generating sources..."

rm -f .build.timestamp
git rm -rf doc lib || true
rm -rf doc lib
rm -rf .packages pubspec.lock
grep -v '^\s*format:\s*date\s*$' ../openapi.yml > ./openapi.yml
npx @openapitools/openapi-generator-cli generate -i ./openapi.yml -g dart-dio -p pubName=zscanner_api -p useEnumExtension=true -p disallowAdditionalPropertiesIfNotPresent=false -p legacyDiscriminatorBehavior=false --type-mappings AnyType=Object -o . -t templates

echo "*** Running flutter build..."

flutter pub get
flutter pub run build_runner build
flutter analyze --no-fatal-infos --no-fatal-warnings
touch .build.timestamp

echo "*** Readding files to git..."

git add lib doc README.md pubspec.yaml

echo "*** DONE. RUN:"

