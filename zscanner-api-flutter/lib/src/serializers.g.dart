// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(Map2dConfig.serializer)
      ..add(SpotMapConfig.serializer)
      ..add(SpotMapSpotConfig.serializer)
      ..add(SpotMapSpotConfigCoordinates.serializer)
      ..add(SpotMapViewConfig.serializer)
      ..add(SpotMapViewType.serializer)
      ..add(ZScannerApplicationManifest.serializer)
      ..add(ZScannerApplicationManifestApplicationProtectionEnum.serializer)
      ..add(ZScannerApplicationManifestSystemProtectionEnum.serializer)
      ..add(ZScannerAuthenticationManifest.serializer)
      ..add(ZScannerAuthenticationManifestTypeEnum.serializer)
      ..add(ZScannerBootstrapManifest.serializer)
      ..add(ZScannerClientManifest.serializer)
      ..add(ZScannerConfig.serializer)
      ..add(ZScannerCreateFolderRequest.serializer)
      ..add(ZScannerEnum.serializer)
      ..add(ZScannerEnumEnumTypeEnum.serializer)
      ..add(ZScannerEnumItem.serializer)
      ..add(ZScannerError.serializer)
      ..add(ZScannerFindFoldersRequest.serializer)
      ..add(ZScannerFlow.serializer)
      ..add(ZScannerFolder.serializer)
      ..add(ZScannerFolderTypeEnum.serializer)
      ..add(ZScannerFoundFolder.serializer)
      ..add(ZScannerGlobalManifest.serializer)
      ..add(ZScannerInstallationManifest.serializer)
      ..add(ZScannerInstallationManifestEnvironmentEnum.serializer)
      ..add(ZScannerLoadSubfolderMediaRequest.serializer)
      ..add(ZScannerLoadSubfoldersRequest.serializer)
      ..add(ZScannerMetaError.serializer)
      ..add(ZScannerMetadataDefaultValue.serializer)
      ..add(ZScannerMetadataItem.serializer)
      ..add(ZScannerMetadataSchemaItem.serializer)
      ..add(ZScannerMetadataValidator.serializer)
      ..add(ZScannerMetadataValue.serializer)
      ..add(ZScannerModuleConfigUnion.serializer)
      ..add(ZScannerModuleConfigUnionRecordAudioEnum.serializer)
      ..add(ZScannerModuleConfigUnionSubfolderEnum.serializer)
      ..add(ZScannerModuleDataUnion.serializer)
      ..add(ZScannerOpenIDAuthenticationManifest.serializer)
      ..add(ZScannerResource.serializer)
      ..add(ZScannerSeaCat3AuthenticationManifest.serializer)
      ..add(ZScannerServerManifest.serializer)
      ..add(ZScannerStickerConfig.serializer)
      ..add(ZScannerStickerConfigFontFace.serializer)
      ..add(ZScannerStickerData.serializer)
      ..add(ZScannerSubfolder.serializer)
      ..add(ZScannerSubfolderConfigUnion.serializer)
      ..add(ZScannerSubfolderSchema.serializer)
      ..add(ZScannerSynchronizationRequest.serializer)
      ..add(ZScannerSynchronizationResponse.serializer)
      ..add(ZScannerThemeManifest.serializer)
      ..add(ZScannerThemeManifestDark.serializer)
      ..add(ZScannerThemeManifestLight.serializer)
      ..add(ZScannerUploadBatchMedia.serializer)
      ..add(ZScannerUploadBatchMediaRelation.serializer)
      ..add(ZScannerUploadBatchRequest.serializer)
      ..add(ZScannerUser.serializer)
      ..add(ZScannerWidgetConfigUnion.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SpotMapViewConfig)]),
          () => new ListBuilder<SpotMapViewConfig>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SpotMapViewConfig)]),
          () => new ListBuilder<SpotMapViewConfig>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SpotMapSpotConfig)]),
          () => new ListBuilder<SpotMapSpotConfig>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataItem)]),
          () => new ListBuilder<ZScannerMetadataItem>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataSchemaItem)]),
          () => new ListBuilder<ZScannerMetadataSchemaItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataSchemaItem)]),
          () => new ListBuilder<ZScannerMetadataSchemaItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataSchemaItem)]),
          () => new ListBuilder<ZScannerMetadataSchemaItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerSubfolderSchema)]),
          () => new ListBuilder<ZScannerSubfolderSchema>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ZScannerFlow)]),
          () => new ListBuilder<ZScannerFlow>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ZScannerEnum)]),
          () => new ListBuilder<ZScannerEnum>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ZScannerResource)]),
          () => new ListBuilder<ZScannerResource>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ZScannerEnumItem)]),
          () => new ListBuilder<ZScannerEnumItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerInstallationManifest)]),
          () => new ListBuilder<ZScannerInstallationManifest>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ZScannerMetaError)]),
          () => new ListBuilder<ZScannerMetaError>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataItem)]),
          () => new ListBuilder<ZScannerMetadataItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataItem)]),
          () => new ListBuilder<ZScannerMetadataItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataItem)]),
          () => new ListBuilder<ZScannerMetadataItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataItem)]),
          () => new ListBuilder<ZScannerMetadataItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataItem)]),
          () => new ListBuilder<ZScannerMetadataItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataItem)]),
          () => new ListBuilder<ZScannerMetadataItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataItem)]),
          () => new ListBuilder<ZScannerMetadataItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataItem)]),
          () => new ListBuilder<ZScannerMetadataItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataItem)]),
          () => new ListBuilder<ZScannerMetadataItem>())
      ..addBuilderFactory(
          const FullType(BuiltList,
              const [const FullType(ZScannerUploadBatchMediaRelation)]),
          () => new ListBuilder<ZScannerUploadBatchMediaRelation>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataSchemaItem)]),
          () => new ListBuilder<ZScannerMetadataSchemaItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataSchemaItem)]),
          () => new ListBuilder<ZScannerMetadataSchemaItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataSchemaItem)]),
          () => new ListBuilder<ZScannerMetadataSchemaItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataSchemaItem)]),
          () => new ListBuilder<ZScannerMetadataSchemaItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataSchemaItem)]),
          () => new ListBuilder<ZScannerMetadataSchemaItem>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataValidator)]),
          () => new ListBuilder<ZScannerMetadataValidator>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerStickerData)]),
          () => new ListBuilder<ZScannerStickerData>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerUploadBatchMedia)]),
          () => new ListBuilder<ZScannerUploadBatchMedia>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ZScannerMetadataItem)]),
          () => new ListBuilder<ZScannerMetadataItem>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ZScannerSubfolder)]),
          () => new ListBuilder<ZScannerSubfolder>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(int)]),
          () => new ListBuilder<int>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>()))
    .build();

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
