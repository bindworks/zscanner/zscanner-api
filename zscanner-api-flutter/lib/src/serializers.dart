//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_import

import 'package:one_of_serializer/any_of_serializer.dart';
import 'package:one_of_serializer/one_of_serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_value/iso_8601_date_time_serializer.dart';
import 'package:zscanner_api/src/date_serializer.dart';
import 'package:zscanner_api/src/model/date.dart';

import 'package:zscanner_api/src/model/map2d_config.dart';
import 'package:zscanner_api/src/model/spot_map_config.dart';
import 'package:zscanner_api/src/model/spot_map_spot_config.dart';
import 'package:zscanner_api/src/model/spot_map_spot_config_coordinates.dart';
import 'package:zscanner_api/src/model/spot_map_view_config.dart';
import 'package:zscanner_api/src/model/spot_map_view_type.dart';
import 'package:zscanner_api/src/model/z_scanner_application_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_authentication_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_bootstrap_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_client_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_config.dart';
import 'package:zscanner_api/src/model/z_scanner_create_folder_request.dart';
import 'package:zscanner_api/src/model/z_scanner_enum.dart';
import 'package:zscanner_api/src/model/z_scanner_enum_item.dart';
import 'package:zscanner_api/src/model/z_scanner_error.dart';
import 'package:zscanner_api/src/model/z_scanner_find_folders_request.dart';
import 'package:zscanner_api/src/model/z_scanner_flow.dart';
import 'package:zscanner_api/src/model/z_scanner_folder.dart';
import 'package:zscanner_api/src/model/z_scanner_found_folder.dart';
import 'package:zscanner_api/src/model/z_scanner_global_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_installation_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_load_subfolder_media_request.dart';
import 'package:zscanner_api/src/model/z_scanner_load_subfolders_request.dart';
import 'package:zscanner_api/src/model/z_scanner_meta_error.dart';
import 'package:zscanner_api/src/model/z_scanner_metadata_default_value.dart';
import 'package:zscanner_api/src/model/z_scanner_metadata_item.dart';
import 'package:zscanner_api/src/model/z_scanner_metadata_schema_item.dart';
import 'package:zscanner_api/src/model/z_scanner_metadata_validator.dart';
import 'package:zscanner_api/src/model/z_scanner_metadata_value.dart';
import 'package:zscanner_api/src/model/z_scanner_module_config_union.dart';
import 'package:zscanner_api/src/model/z_scanner_module_data_union.dart';
import 'package:zscanner_api/src/model/z_scanner_open_id_authentication_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_resource.dart';
import 'package:zscanner_api/src/model/z_scanner_sea_cat3_authentication_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_server_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_sticker_config.dart';
import 'package:zscanner_api/src/model/z_scanner_sticker_config_font_face.dart';
import 'package:zscanner_api/src/model/z_scanner_sticker_data.dart';
import 'package:zscanner_api/src/model/z_scanner_subfolder.dart';
import 'package:zscanner_api/src/model/z_scanner_subfolder_config_union.dart';
import 'package:zscanner_api/src/model/z_scanner_subfolder_schema.dart';
import 'package:zscanner_api/src/model/z_scanner_synchronization_request.dart';
import 'package:zscanner_api/src/model/z_scanner_synchronization_response.dart';
import 'package:zscanner_api/src/model/z_scanner_theme_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_theme_manifest_dark.dart';
import 'package:zscanner_api/src/model/z_scanner_theme_manifest_light.dart';
import 'package:zscanner_api/src/model/z_scanner_upload_batch_media.dart';
import 'package:zscanner_api/src/model/z_scanner_upload_batch_media_relation.dart';
import 'package:zscanner_api/src/model/z_scanner_upload_batch_request.dart';
import 'package:zscanner_api/src/model/z_scanner_user.dart';
import 'package:zscanner_api/src/model/z_scanner_widget_config_union.dart';

part 'serializers.g.dart';

@SerializersFor([
  Map2dConfig,
  SpotMapConfig,
  SpotMapSpotConfig,
  SpotMapSpotConfigCoordinates,
  SpotMapViewConfig,
  SpotMapViewType,
  ZScannerApplicationManifest,
  ZScannerAuthenticationManifest,
  ZScannerBootstrapManifest,
  ZScannerClientManifest,
  ZScannerConfig,
  ZScannerCreateFolderRequest,
  ZScannerEnum,
  ZScannerEnumItem,
  ZScannerError,
  ZScannerFindFoldersRequest,
  ZScannerFlow,
  ZScannerFolder,
  ZScannerFoundFolder,
  ZScannerGlobalManifest,
  ZScannerInstallationManifest,
  ZScannerLoadSubfolderMediaRequest,
  ZScannerLoadSubfoldersRequest,
  ZScannerMetaError,
  ZScannerMetadataDefaultValue,
  ZScannerMetadataItem,
  ZScannerMetadataSchemaItem,
  ZScannerMetadataValidator,
  ZScannerMetadataValue,
  ZScannerModuleConfigUnion,
  ZScannerModuleDataUnion,
  ZScannerOpenIDAuthenticationManifest,
  ZScannerResource,
  ZScannerSeaCat3AuthenticationManifest,
  ZScannerServerManifest,
  ZScannerStickerConfig,
  ZScannerStickerConfigFontFace,
  ZScannerStickerData,
  ZScannerSubfolder,
  ZScannerSubfolderConfigUnion,
  ZScannerSubfolderSchema,
  ZScannerSynchronizationRequest,
  ZScannerSynchronizationResponse,
  ZScannerThemeManifest,
  ZScannerThemeManifestDark,
  ZScannerThemeManifestLight,
  ZScannerUploadBatchMedia,
  ZScannerUploadBatchMediaRelation,
  ZScannerUploadBatchRequest,
  ZScannerUser,
  ZScannerWidgetConfigUnion,
])
Serializers serializers = (_$serializers.toBuilder()
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(ZScannerFoundFolder)]),
        () => ListBuilder<ZScannerFoundFolder>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(ZScannerFolder)]),
        () => ListBuilder<ZScannerFolder>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(ZScannerUploadBatchMedia)]),
        () => ListBuilder<ZScannerUploadBatchMedia>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(ZScannerSubfolder)]),
        () => ListBuilder<ZScannerSubfolder>(),
      )
      ..add(const OneOfSerializer())
      ..add(const AnyOfSerializer())
      ..add(const DateSerializer())
      ..add(Iso8601DateTimeSerializer()))
    .build();

Serializers standardSerializers =
    (serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
