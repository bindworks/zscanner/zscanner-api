//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_subfolder_config_union.dart';
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_metadata_schema_item.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_subfolder_schema.g.dart';

/// ZScannerSubfolderSchema
///
/// Properties:
/// * [typeId] 
/// * [label] 
/// * [moduleId] 
/// * [moduleVersion] 
/// * [config] 
/// * [displaySchema] 
/// * [createSchema] 
/// * [updateSchema] 
@BuiltValue()
abstract class ZScannerSubfolderSchema implements Built<ZScannerSubfolderSchema, ZScannerSubfolderSchemaBuilder> {
  @BuiltValueField(wireName: r'typeId')
  String get typeId;

  @BuiltValueField(wireName: r'label')
  String get label;

  @BuiltValueField(wireName: r'moduleId')
  String? get moduleId;

  @BuiltValueField(wireName: r'moduleVersion')
  String? get moduleVersion;

  @BuiltValueField(wireName: r'config')
  ZScannerSubfolderConfigUnion? get config;

  @BuiltValueField(wireName: r'displaySchema')
  BuiltList<ZScannerMetadataSchemaItem> get displaySchema;

  @BuiltValueField(wireName: r'createSchema')
  BuiltList<ZScannerMetadataSchemaItem> get createSchema;

  @BuiltValueField(wireName: r'updateSchema')
  BuiltList<ZScannerMetadataSchemaItem> get updateSchema;

  ZScannerSubfolderSchema._();

  factory ZScannerSubfolderSchema([void updates(ZScannerSubfolderSchemaBuilder b)]) = _$ZScannerSubfolderSchema;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerSubfolderSchemaBuilder b) => b
      ..moduleVersion = 'any';

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerSubfolderSchema> get serializer => _$ZScannerSubfolderSchemaSerializer();
}

class _$ZScannerSubfolderSchemaSerializer implements PrimitiveSerializer<ZScannerSubfolderSchema> {
  @override
  final Iterable<Type> types = const [ZScannerSubfolderSchema, _$ZScannerSubfolderSchema];

  @override
  final String wireName = r'ZScannerSubfolderSchema';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerSubfolderSchema object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'typeId';
    yield serializers.serialize(
      object.typeId,
      specifiedType: const FullType(String),
    );
    yield r'label';
    yield serializers.serialize(
      object.label,
      specifiedType: const FullType(String),
    );
    if (object.moduleId != null) {
      yield r'moduleId';
      yield serializers.serialize(
        object.moduleId,
        specifiedType: const FullType(String),
      );
    }
    if (object.moduleVersion != null) {
      yield r'moduleVersion';
      yield serializers.serialize(
        object.moduleVersion,
        specifiedType: const FullType(String),
      );
    }
    if (object.config != null) {
      yield r'config';
      yield serializers.serialize(
        object.config,
        specifiedType: const FullType(ZScannerSubfolderConfigUnion),
      );
    }
    yield r'displaySchema';
    yield serializers.serialize(
      object.displaySchema,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
    );
    yield r'createSchema';
    yield serializers.serialize(
      object.createSchema,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
    );
    yield r'updateSchema';
    yield serializers.serialize(
      object.updateSchema,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerSubfolderSchema object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerSubfolderSchemaBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'typeId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.typeId = valueDes;
          break;
        case r'label':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.label = valueDes;
          break;
        case r'moduleId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.moduleId = valueDes;
          break;
        case r'moduleVersion':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.moduleVersion = valueDes;
          break;
        case r'config':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerSubfolderConfigUnion),
          ) as ZScannerSubfolderConfigUnion;
          result.config.replace(valueDes);
          break;
        case r'displaySchema':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
          ) as BuiltList<ZScannerMetadataSchemaItem>;
          result.displaySchema.replace(valueDes);
          break;
        case r'createSchema':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
          ) as BuiltList<ZScannerMetadataSchemaItem>;
          result.createSchema.replace(valueDes);
          break;
        case r'updateSchema':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
          ) as BuiltList<ZScannerMetadataSchemaItem>;
          result.updateSchema.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerSubfolderSchema deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerSubfolderSchemaBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

