//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_upload_batch_media_relation.g.dart';

/// ZScannerUploadBatchMediaRelation
///
/// Properties:
/// * [relatedMediaRole] - Roles defined so far: SOURCE, STICKERED, WOUND_MASK, ORIGINAL_WOUND_MASK, WOUND_ANALYZED 
/// * [relatedMediaId] 
@BuiltValue()
abstract class ZScannerUploadBatchMediaRelation implements Built<ZScannerUploadBatchMediaRelation, ZScannerUploadBatchMediaRelationBuilder> {
  /// Roles defined so far: SOURCE, STICKERED, WOUND_MASK, ORIGINAL_WOUND_MASK, WOUND_ANALYZED 
  @BuiltValueField(wireName: r'relatedMediaRole')
  String get relatedMediaRole;

  @BuiltValueField(wireName: r'relatedMediaId')
  String get relatedMediaId;

  ZScannerUploadBatchMediaRelation._();

  factory ZScannerUploadBatchMediaRelation([void updates(ZScannerUploadBatchMediaRelationBuilder b)]) = _$ZScannerUploadBatchMediaRelation;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerUploadBatchMediaRelationBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerUploadBatchMediaRelation> get serializer => _$ZScannerUploadBatchMediaRelationSerializer();
}

class _$ZScannerUploadBatchMediaRelationSerializer implements PrimitiveSerializer<ZScannerUploadBatchMediaRelation> {
  @override
  final Iterable<Type> types = const [ZScannerUploadBatchMediaRelation, _$ZScannerUploadBatchMediaRelation];

  @override
  final String wireName = r'ZScannerUploadBatchMediaRelation';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerUploadBatchMediaRelation object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'relatedMediaRole';
    yield serializers.serialize(
      object.relatedMediaRole,
      specifiedType: const FullType(String),
    );
    yield r'relatedMediaId';
    yield serializers.serialize(
      object.relatedMediaId,
      specifiedType: const FullType(String),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerUploadBatchMediaRelation object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerUploadBatchMediaRelationBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'relatedMediaRole':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.relatedMediaRole = valueDes;
          break;
        case r'relatedMediaId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.relatedMediaId = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerUploadBatchMediaRelation deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerUploadBatchMediaRelationBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

