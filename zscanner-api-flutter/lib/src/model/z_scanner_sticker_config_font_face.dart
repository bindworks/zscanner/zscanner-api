//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_sticker_config_font_face.g.dart';

/// ZScannerStickerConfigFontFace
///
/// Properties:
/// * [ios] 
/// * [android] 
@BuiltValue()
abstract class ZScannerStickerConfigFontFace implements Built<ZScannerStickerConfigFontFace, ZScannerStickerConfigFontFaceBuilder> {
  @BuiltValueField(wireName: r'ios')
  String? get ios;

  @BuiltValueField(wireName: r'android')
  String? get android;

  ZScannerStickerConfigFontFace._();

  factory ZScannerStickerConfigFontFace([void updates(ZScannerStickerConfigFontFaceBuilder b)]) = _$ZScannerStickerConfigFontFace;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerStickerConfigFontFaceBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerStickerConfigFontFace> get serializer => _$ZScannerStickerConfigFontFaceSerializer();
}

class _$ZScannerStickerConfigFontFaceSerializer implements PrimitiveSerializer<ZScannerStickerConfigFontFace> {
  @override
  final Iterable<Type> types = const [ZScannerStickerConfigFontFace, _$ZScannerStickerConfigFontFace];

  @override
  final String wireName = r'ZScannerStickerConfigFontFace';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerStickerConfigFontFace object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.ios != null) {
      yield r'ios';
      yield serializers.serialize(
        object.ios,
        specifiedType: const FullType(String),
      );
    }
    if (object.android != null) {
      yield r'android';
      yield serializers.serialize(
        object.android,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerStickerConfigFontFace object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerStickerConfigFontFaceBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'ios':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.ios = valueDes;
          break;
        case r'android':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.android = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerStickerConfigFontFace deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerStickerConfigFontFaceBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

