//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_server_manifest.g.dart';

/// ZScannerServerManifest
///
/// Properties:
/// * [rootApiUrl] 
/// * [supportedCapabilities] 
@BuiltValue()
abstract class ZScannerServerManifest implements Built<ZScannerServerManifest, ZScannerServerManifestBuilder> {
  @BuiltValueField(wireName: r'rootApiUrl')
  String get rootApiUrl;

  @BuiltValueField(wireName: r'supportedCapabilities')
  BuiltList<String>? get supportedCapabilities;

  ZScannerServerManifest._();

  factory ZScannerServerManifest([void updates(ZScannerServerManifestBuilder b)]) = _$ZScannerServerManifest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerServerManifestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerServerManifest> get serializer => _$ZScannerServerManifestSerializer();
}

class _$ZScannerServerManifestSerializer implements PrimitiveSerializer<ZScannerServerManifest> {
  @override
  final Iterable<Type> types = const [ZScannerServerManifest, _$ZScannerServerManifest];

  @override
  final String wireName = r'ZScannerServerManifest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerServerManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'rootApiUrl';
    yield serializers.serialize(
      object.rootApiUrl,
      specifiedType: const FullType(String),
    );
    if (object.supportedCapabilities != null) {
      yield r'supportedCapabilities';
      yield serializers.serialize(
        object.supportedCapabilities,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerServerManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerServerManifestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'rootApiUrl':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.rootApiUrl = valueDes;
          break;
        case r'supportedCapabilities':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.supportedCapabilities.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerServerManifest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerServerManifestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

