// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_global_manifest.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerGlobalManifest extends ZScannerGlobalManifest {
  @override
  final ZScannerClientManifest client;

  factory _$ZScannerGlobalManifest(
          [void Function(ZScannerGlobalManifestBuilder)? updates]) =>
      (new ZScannerGlobalManifestBuilder()..update(updates))._build();

  _$ZScannerGlobalManifest._({required this.client}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        client, r'ZScannerGlobalManifest', 'client');
  }

  @override
  ZScannerGlobalManifest rebuild(
          void Function(ZScannerGlobalManifestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerGlobalManifestBuilder toBuilder() =>
      new ZScannerGlobalManifestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerGlobalManifest && client == other.client;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, client.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerGlobalManifest')
          ..add('client', client))
        .toString();
  }
}

class ZScannerGlobalManifestBuilder
    implements Builder<ZScannerGlobalManifest, ZScannerGlobalManifestBuilder> {
  _$ZScannerGlobalManifest? _$v;

  ZScannerClientManifestBuilder? _client;
  ZScannerClientManifestBuilder get client =>
      _$this._client ??= new ZScannerClientManifestBuilder();
  set client(ZScannerClientManifestBuilder? client) => _$this._client = client;

  ZScannerGlobalManifestBuilder() {
    ZScannerGlobalManifest._defaults(this);
  }

  ZScannerGlobalManifestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _client = $v.client.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerGlobalManifest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerGlobalManifest;
  }

  @override
  void update(void Function(ZScannerGlobalManifestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerGlobalManifest build() => _build();

  _$ZScannerGlobalManifest _build() {
    _$ZScannerGlobalManifest _$result;
    try {
      _$result = _$v ?? new _$ZScannerGlobalManifest._(client: client.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'client';
        client.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerGlobalManifest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
