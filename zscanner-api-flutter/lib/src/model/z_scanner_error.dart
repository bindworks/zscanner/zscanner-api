//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_meta_error.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_error.g.dart';

/// ZScannerError
///
/// Properties:
/// * [exception] 
/// * [error] 
/// * [uuid] 
/// * [metaErrors] 
/// * [mediaIds] 
/// * [folderId] 
/// * [subfolderId] 
/// * [resourceId] 
@BuiltValue()
abstract class ZScannerError implements Built<ZScannerError, ZScannerErrorBuilder> {
  @BuiltValueField(wireName: r'exception')
  String? get exception;

  @BuiltValueField(wireName: r'error')
  String? get error;

  @BuiltValueField(wireName: r'uuid')
  String? get uuid;

  @BuiltValueField(wireName: r'metaErrors')
  BuiltList<ZScannerMetaError>? get metaErrors;

  @BuiltValueField(wireName: r'mediaIds')
  BuiltList<String>? get mediaIds;

  @BuiltValueField(wireName: r'folderId')
  String? get folderId;

  @BuiltValueField(wireName: r'subfolderId')
  String? get subfolderId;

  @BuiltValueField(wireName: r'resourceId')
  String? get resourceId;

  ZScannerError._();

  factory ZScannerError([void updates(ZScannerErrorBuilder b)]) = _$ZScannerError;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerErrorBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerError> get serializer => _$ZScannerErrorSerializer();
}

class _$ZScannerErrorSerializer implements PrimitiveSerializer<ZScannerError> {
  @override
  final Iterable<Type> types = const [ZScannerError, _$ZScannerError];

  @override
  final String wireName = r'ZScannerError';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerError object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.exception != null) {
      yield r'exception';
      yield serializers.serialize(
        object.exception,
        specifiedType: const FullType(String),
      );
    }
    if (object.error != null) {
      yield r'error';
      yield serializers.serialize(
        object.error,
        specifiedType: const FullType(String),
      );
    }
    if (object.uuid != null) {
      yield r'uuid';
      yield serializers.serialize(
        object.uuid,
        specifiedType: const FullType(String),
      );
    }
    if (object.metaErrors != null) {
      yield r'metaErrors';
      yield serializers.serialize(
        object.metaErrors,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerMetaError)]),
      );
    }
    if (object.mediaIds != null) {
      yield r'mediaIds';
      yield serializers.serialize(
        object.mediaIds,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
    if (object.folderId != null) {
      yield r'folderId';
      yield serializers.serialize(
        object.folderId,
        specifiedType: const FullType(String),
      );
    }
    if (object.subfolderId != null) {
      yield r'subfolderId';
      yield serializers.serialize(
        object.subfolderId,
        specifiedType: const FullType(String),
      );
    }
    if (object.resourceId != null) {
      yield r'resourceId';
      yield serializers.serialize(
        object.resourceId,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerError object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerErrorBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'exception':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.exception = valueDes;
          break;
        case r'error':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.error = valueDes;
          break;
        case r'uuid':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.uuid = valueDes;
          break;
        case r'metaErrors':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetaError)]),
          ) as BuiltList<ZScannerMetaError>;
          result.metaErrors.replace(valueDes);
          break;
        case r'mediaIds':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.mediaIds.replace(valueDes);
          break;
        case r'folderId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.folderId = valueDes;
          break;
        case r'subfolderId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.subfolderId = valueDes;
          break;
        case r'resourceId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.resourceId = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerError deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerErrorBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

