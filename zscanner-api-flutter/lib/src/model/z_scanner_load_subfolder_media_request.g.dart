// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_load_subfolder_media_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerLoadSubfolderMediaRequest
    extends ZScannerLoadSubfolderMediaRequest {
  @override
  final BuiltList<ZScannerMetadataItem> metadata;

  factory _$ZScannerLoadSubfolderMediaRequest(
          [void Function(ZScannerLoadSubfolderMediaRequestBuilder)? updates]) =>
      (new ZScannerLoadSubfolderMediaRequestBuilder()..update(updates))
          ._build();

  _$ZScannerLoadSubfolderMediaRequest._({required this.metadata}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        metadata, r'ZScannerLoadSubfolderMediaRequest', 'metadata');
  }

  @override
  ZScannerLoadSubfolderMediaRequest rebuild(
          void Function(ZScannerLoadSubfolderMediaRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerLoadSubfolderMediaRequestBuilder toBuilder() =>
      new ZScannerLoadSubfolderMediaRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerLoadSubfolderMediaRequest &&
        metadata == other.metadata;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, metadata.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerLoadSubfolderMediaRequest')
          ..add('metadata', metadata))
        .toString();
  }
}

class ZScannerLoadSubfolderMediaRequestBuilder
    implements
        Builder<ZScannerLoadSubfolderMediaRequest,
            ZScannerLoadSubfolderMediaRequestBuilder> {
  _$ZScannerLoadSubfolderMediaRequest? _$v;

  ListBuilder<ZScannerMetadataItem>? _metadata;
  ListBuilder<ZScannerMetadataItem> get metadata =>
      _$this._metadata ??= new ListBuilder<ZScannerMetadataItem>();
  set metadata(ListBuilder<ZScannerMetadataItem>? metadata) =>
      _$this._metadata = metadata;

  ZScannerLoadSubfolderMediaRequestBuilder() {
    ZScannerLoadSubfolderMediaRequest._defaults(this);
  }

  ZScannerLoadSubfolderMediaRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _metadata = $v.metadata.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerLoadSubfolderMediaRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerLoadSubfolderMediaRequest;
  }

  @override
  void update(
      void Function(ZScannerLoadSubfolderMediaRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerLoadSubfolderMediaRequest build() => _build();

  _$ZScannerLoadSubfolderMediaRequest _build() {
    _$ZScannerLoadSubfolderMediaRequest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerLoadSubfolderMediaRequest._(metadata: metadata.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metadata';
        metadata.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerLoadSubfolderMediaRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
