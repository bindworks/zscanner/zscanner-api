// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_metadata_value.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerMetadataValue extends ZScannerMetadataValue {
  @override
  final String? dateValue;
  @override
  final String? stringValue;
  @override
  final num? numberValue;
  @override
  final bool? booleanValue;
  @override
  final DateTime? dateTimeValue;
  @override
  final BuiltList<String>? multiStringValue;

  factory _$ZScannerMetadataValue(
          [void Function(ZScannerMetadataValueBuilder)? updates]) =>
      (new ZScannerMetadataValueBuilder()..update(updates))._build();

  _$ZScannerMetadataValue._(
      {this.dateValue,
      this.stringValue,
      this.numberValue,
      this.booleanValue,
      this.dateTimeValue,
      this.multiStringValue})
      : super._();

  @override
  ZScannerMetadataValue rebuild(
          void Function(ZScannerMetadataValueBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerMetadataValueBuilder toBuilder() =>
      new ZScannerMetadataValueBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerMetadataValue &&
        dateValue == other.dateValue &&
        stringValue == other.stringValue &&
        numberValue == other.numberValue &&
        booleanValue == other.booleanValue &&
        dateTimeValue == other.dateTimeValue &&
        multiStringValue == other.multiStringValue;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, dateValue.hashCode);
    _$hash = $jc(_$hash, stringValue.hashCode);
    _$hash = $jc(_$hash, numberValue.hashCode);
    _$hash = $jc(_$hash, booleanValue.hashCode);
    _$hash = $jc(_$hash, dateTimeValue.hashCode);
    _$hash = $jc(_$hash, multiStringValue.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerMetadataValue')
          ..add('dateValue', dateValue)
          ..add('stringValue', stringValue)
          ..add('numberValue', numberValue)
          ..add('booleanValue', booleanValue)
          ..add('dateTimeValue', dateTimeValue)
          ..add('multiStringValue', multiStringValue))
        .toString();
  }
}

class ZScannerMetadataValueBuilder
    implements Builder<ZScannerMetadataValue, ZScannerMetadataValueBuilder> {
  _$ZScannerMetadataValue? _$v;

  String? _dateValue;
  String? get dateValue => _$this._dateValue;
  set dateValue(String? dateValue) => _$this._dateValue = dateValue;

  String? _stringValue;
  String? get stringValue => _$this._stringValue;
  set stringValue(String? stringValue) => _$this._stringValue = stringValue;

  num? _numberValue;
  num? get numberValue => _$this._numberValue;
  set numberValue(num? numberValue) => _$this._numberValue = numberValue;

  bool? _booleanValue;
  bool? get booleanValue => _$this._booleanValue;
  set booleanValue(bool? booleanValue) => _$this._booleanValue = booleanValue;

  DateTime? _dateTimeValue;
  DateTime? get dateTimeValue => _$this._dateTimeValue;
  set dateTimeValue(DateTime? dateTimeValue) =>
      _$this._dateTimeValue = dateTimeValue;

  ListBuilder<String>? _multiStringValue;
  ListBuilder<String> get multiStringValue =>
      _$this._multiStringValue ??= new ListBuilder<String>();
  set multiStringValue(ListBuilder<String>? multiStringValue) =>
      _$this._multiStringValue = multiStringValue;

  ZScannerMetadataValueBuilder() {
    ZScannerMetadataValue._defaults(this);
  }

  ZScannerMetadataValueBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _dateValue = $v.dateValue;
      _stringValue = $v.stringValue;
      _numberValue = $v.numberValue;
      _booleanValue = $v.booleanValue;
      _dateTimeValue = $v.dateTimeValue;
      _multiStringValue = $v.multiStringValue?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerMetadataValue other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerMetadataValue;
  }

  @override
  void update(void Function(ZScannerMetadataValueBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerMetadataValue build() => _build();

  _$ZScannerMetadataValue _build() {
    _$ZScannerMetadataValue _$result;
    try {
      _$result = _$v ??
          new _$ZScannerMetadataValue._(
              dateValue: dateValue,
              stringValue: stringValue,
              numberValue: numberValue,
              booleanValue: booleanValue,
              dateTimeValue: dateTimeValue,
              multiStringValue: _multiStringValue?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'multiStringValue';
        _multiStringValue?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerMetadataValue', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
