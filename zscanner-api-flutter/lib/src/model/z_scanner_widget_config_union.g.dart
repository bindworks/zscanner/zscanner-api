// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_widget_config_union.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerWidgetConfigUnion extends ZScannerWidgetConfigUnion {
  @override
  final int? minLines;
  @override
  final int? maxLines;
  @override
  final bool? searchable;
  @override
  final bool? allowOther;
  @override
  final int? maxDecimalPlaces;

  factory _$ZScannerWidgetConfigUnion(
          [void Function(ZScannerWidgetConfigUnionBuilder)? updates]) =>
      (new ZScannerWidgetConfigUnionBuilder()..update(updates))._build();

  _$ZScannerWidgetConfigUnion._(
      {this.minLines,
      this.maxLines,
      this.searchable,
      this.allowOther,
      this.maxDecimalPlaces})
      : super._();

  @override
  ZScannerWidgetConfigUnion rebuild(
          void Function(ZScannerWidgetConfigUnionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerWidgetConfigUnionBuilder toBuilder() =>
      new ZScannerWidgetConfigUnionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerWidgetConfigUnion &&
        minLines == other.minLines &&
        maxLines == other.maxLines &&
        searchable == other.searchable &&
        allowOther == other.allowOther &&
        maxDecimalPlaces == other.maxDecimalPlaces;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, minLines.hashCode);
    _$hash = $jc(_$hash, maxLines.hashCode);
    _$hash = $jc(_$hash, searchable.hashCode);
    _$hash = $jc(_$hash, allowOther.hashCode);
    _$hash = $jc(_$hash, maxDecimalPlaces.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerWidgetConfigUnion')
          ..add('minLines', minLines)
          ..add('maxLines', maxLines)
          ..add('searchable', searchable)
          ..add('allowOther', allowOther)
          ..add('maxDecimalPlaces', maxDecimalPlaces))
        .toString();
  }
}

class ZScannerWidgetConfigUnionBuilder
    implements
        Builder<ZScannerWidgetConfigUnion, ZScannerWidgetConfigUnionBuilder> {
  _$ZScannerWidgetConfigUnion? _$v;

  int? _minLines;
  int? get minLines => _$this._minLines;
  set minLines(int? minLines) => _$this._minLines = minLines;

  int? _maxLines;
  int? get maxLines => _$this._maxLines;
  set maxLines(int? maxLines) => _$this._maxLines = maxLines;

  bool? _searchable;
  bool? get searchable => _$this._searchable;
  set searchable(bool? searchable) => _$this._searchable = searchable;

  bool? _allowOther;
  bool? get allowOther => _$this._allowOther;
  set allowOther(bool? allowOther) => _$this._allowOther = allowOther;

  int? _maxDecimalPlaces;
  int? get maxDecimalPlaces => _$this._maxDecimalPlaces;
  set maxDecimalPlaces(int? maxDecimalPlaces) =>
      _$this._maxDecimalPlaces = maxDecimalPlaces;

  ZScannerWidgetConfigUnionBuilder() {
    ZScannerWidgetConfigUnion._defaults(this);
  }

  ZScannerWidgetConfigUnionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _minLines = $v.minLines;
      _maxLines = $v.maxLines;
      _searchable = $v.searchable;
      _allowOther = $v.allowOther;
      _maxDecimalPlaces = $v.maxDecimalPlaces;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerWidgetConfigUnion other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerWidgetConfigUnion;
  }

  @override
  void update(void Function(ZScannerWidgetConfigUnionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerWidgetConfigUnion build() => _build();

  _$ZScannerWidgetConfigUnion _build() {
    final _$result = _$v ??
        new _$ZScannerWidgetConfigUnion._(
            minLines: minLines,
            maxLines: maxLines,
            searchable: searchable,
            allowOther: allowOther,
            maxDecimalPlaces: maxDecimalPlaces);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
