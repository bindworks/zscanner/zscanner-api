//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_client_manifest.g.dart';

/// ZScannerClientManifest
///
/// Properties:
/// * [requiredCapabilities] 
@BuiltValue()
abstract class ZScannerClientManifest implements Built<ZScannerClientManifest, ZScannerClientManifestBuilder> {
  @BuiltValueField(wireName: r'requiredCapabilities')
  BuiltList<String>? get requiredCapabilities;

  ZScannerClientManifest._();

  factory ZScannerClientManifest([void updates(ZScannerClientManifestBuilder b)]) = _$ZScannerClientManifest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerClientManifestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerClientManifest> get serializer => _$ZScannerClientManifestSerializer();
}

class _$ZScannerClientManifestSerializer implements PrimitiveSerializer<ZScannerClientManifest> {
  @override
  final Iterable<Type> types = const [ZScannerClientManifest, _$ZScannerClientManifest];

  @override
  final String wireName = r'ZScannerClientManifest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerClientManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.requiredCapabilities != null) {
      yield r'requiredCapabilities';
      yield serializers.serialize(
        object.requiredCapabilities,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerClientManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerClientManifestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'requiredCapabilities':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.requiredCapabilities.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerClientManifest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerClientManifestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

