//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_metadata_item.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_folder.g.dart';

/// ZScannerFolder
///
/// Properties:
/// * [internalId] 
/// * [externalId] 
/// * [name] 
/// * [type] 
/// * [metadata] 
@BuiltValue()
abstract class ZScannerFolder implements Built<ZScannerFolder, ZScannerFolderBuilder> {
  @BuiltValueField(wireName: r'internalId')
  String get internalId;

  @BuiltValueField(wireName: r'externalId')
  String get externalId;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'type')
  ZScannerFolderTypeEnum get type;
  // enum typeEnum {  SUGGESTION,  RESULT,  CREATE_TEMPLATE,  };

  @BuiltValueField(wireName: r'metadata')
  BuiltList<ZScannerMetadataItem>? get metadata;

  ZScannerFolder._();

  factory ZScannerFolder([void updates(ZScannerFolderBuilder b)]) = _$ZScannerFolder;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerFolderBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerFolder> get serializer => _$ZScannerFolderSerializer();
}

class _$ZScannerFolderSerializer implements PrimitiveSerializer<ZScannerFolder> {
  @override
  final Iterable<Type> types = const [ZScannerFolder, _$ZScannerFolder];

  @override
  final String wireName = r'ZScannerFolder';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerFolder object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'internalId';
    yield serializers.serialize(
      object.internalId,
      specifiedType: const FullType(String),
    );
    yield r'externalId';
    yield serializers.serialize(
      object.externalId,
      specifiedType: const FullType(String),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(ZScannerFolderTypeEnum),
    );
    if (object.metadata != null) {
      yield r'metadata';
      yield serializers.serialize(
        object.metadata,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerFolder object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerFolderBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'internalId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.internalId = valueDes;
          break;
        case r'externalId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.externalId = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerFolderTypeEnum),
          ) as ZScannerFolderTypeEnum;
          result.type = valueDes;
          break;
        case r'metadata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
          ) as BuiltList<ZScannerMetadataItem>;
          result.metadata.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerFolder deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerFolderBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class ZScannerFolderTypeEnum extends EnumClass {

  @BuiltValueEnumConst(wireName: r'SUGGESTION')
  static const ZScannerFolderTypeEnum SUGGESTION = _$zScannerFolderTypeEnum_SUGGESTION;
  @BuiltValueEnumConst(wireName: r'RESULT')
  static const ZScannerFolderTypeEnum RESULT = _$zScannerFolderTypeEnum_RESULT;
  @BuiltValueEnumConst(wireName: r'CREATE_TEMPLATE')
  static const ZScannerFolderTypeEnum CREATE_TEMPLATE = _$zScannerFolderTypeEnum_CREATE_TEMPLATE;

  @BuiltValueEnumConst(wireName: r'____unknown____', fallback: true)
  static const ZScannerFolderTypeEnum unknown = _$zScannerFolderTypeEnum_$unknown;

  static Serializer<ZScannerFolderTypeEnum> get serializer => _$zScannerFolderTypeEnumSerializer;

  const ZScannerFolderTypeEnum._(String name): super(name);

  static BuiltSet<ZScannerFolderTypeEnum> get values => _$zScannerFolderTypeEnumValues;
  static ZScannerFolderTypeEnum valueOf(String name) => _$zScannerFolderTypeEnumValueOf(name);
}


