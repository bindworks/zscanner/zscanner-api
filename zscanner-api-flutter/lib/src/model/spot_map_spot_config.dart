//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/spot_map_spot_config_coordinates.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'spot_map_spot_config.g.dart';

/// SpotMapSpotConfig
///
/// Properties:
/// * [spotId] 
/// * [label] 
/// * [viewId] 
/// * [coordinates] 
@BuiltValue()
abstract class SpotMapSpotConfig implements Built<SpotMapSpotConfig, SpotMapSpotConfigBuilder> {
  @BuiltValueField(wireName: r'spotId')
  String get spotId;

  @BuiltValueField(wireName: r'label')
  String get label;

  @BuiltValueField(wireName: r'viewId')
  String get viewId;

  @BuiltValueField(wireName: r'coordinates')
  SpotMapSpotConfigCoordinates get coordinates;

  SpotMapSpotConfig._();

  factory SpotMapSpotConfig([void updates(SpotMapSpotConfigBuilder b)]) = _$SpotMapSpotConfig;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(SpotMapSpotConfigBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<SpotMapSpotConfig> get serializer => _$SpotMapSpotConfigSerializer();
}

class _$SpotMapSpotConfigSerializer implements PrimitiveSerializer<SpotMapSpotConfig> {
  @override
  final Iterable<Type> types = const [SpotMapSpotConfig, _$SpotMapSpotConfig];

  @override
  final String wireName = r'SpotMapSpotConfig';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    SpotMapSpotConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'spotId';
    yield serializers.serialize(
      object.spotId,
      specifiedType: const FullType(String),
    );
    yield r'label';
    yield serializers.serialize(
      object.label,
      specifiedType: const FullType(String),
    );
    yield r'viewId';
    yield serializers.serialize(
      object.viewId,
      specifiedType: const FullType(String),
    );
    yield r'coordinates';
    yield serializers.serialize(
      object.coordinates,
      specifiedType: const FullType(SpotMapSpotConfigCoordinates),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    SpotMapSpotConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required SpotMapSpotConfigBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'spotId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.spotId = valueDes;
          break;
        case r'label':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.label = valueDes;
          break;
        case r'viewId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.viewId = valueDes;
          break;
        case r'coordinates':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(SpotMapSpotConfigCoordinates),
          ) as SpotMapSpotConfigCoordinates;
          result.coordinates.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  SpotMapSpotConfig deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = SpotMapSpotConfigBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

