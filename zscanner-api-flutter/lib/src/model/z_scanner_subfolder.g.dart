// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_subfolder.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerSubfolder extends ZScannerSubfolder {
  @override
  final String subfolderId;
  @override
  final String typeId;
  @override
  final BuiltList<ZScannerMetadataItem> metadata;

  factory _$ZScannerSubfolder(
          [void Function(ZScannerSubfolderBuilder)? updates]) =>
      (new ZScannerSubfolderBuilder()..update(updates))._build();

  _$ZScannerSubfolder._(
      {required this.subfolderId, required this.typeId, required this.metadata})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        subfolderId, r'ZScannerSubfolder', 'subfolderId');
    BuiltValueNullFieldError.checkNotNull(
        typeId, r'ZScannerSubfolder', 'typeId');
    BuiltValueNullFieldError.checkNotNull(
        metadata, r'ZScannerSubfolder', 'metadata');
  }

  @override
  ZScannerSubfolder rebuild(void Function(ZScannerSubfolderBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerSubfolderBuilder toBuilder() =>
      new ZScannerSubfolderBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerSubfolder &&
        subfolderId == other.subfolderId &&
        typeId == other.typeId &&
        metadata == other.metadata;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, subfolderId.hashCode);
    _$hash = $jc(_$hash, typeId.hashCode);
    _$hash = $jc(_$hash, metadata.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerSubfolder')
          ..add('subfolderId', subfolderId)
          ..add('typeId', typeId)
          ..add('metadata', metadata))
        .toString();
  }
}

class ZScannerSubfolderBuilder
    implements Builder<ZScannerSubfolder, ZScannerSubfolderBuilder> {
  _$ZScannerSubfolder? _$v;

  String? _subfolderId;
  String? get subfolderId => _$this._subfolderId;
  set subfolderId(String? subfolderId) => _$this._subfolderId = subfolderId;

  String? _typeId;
  String? get typeId => _$this._typeId;
  set typeId(String? typeId) => _$this._typeId = typeId;

  ListBuilder<ZScannerMetadataItem>? _metadata;
  ListBuilder<ZScannerMetadataItem> get metadata =>
      _$this._metadata ??= new ListBuilder<ZScannerMetadataItem>();
  set metadata(ListBuilder<ZScannerMetadataItem>? metadata) =>
      _$this._metadata = metadata;

  ZScannerSubfolderBuilder() {
    ZScannerSubfolder._defaults(this);
  }

  ZScannerSubfolderBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _subfolderId = $v.subfolderId;
      _typeId = $v.typeId;
      _metadata = $v.metadata.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerSubfolder other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerSubfolder;
  }

  @override
  void update(void Function(ZScannerSubfolderBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerSubfolder build() => _build();

  _$ZScannerSubfolder _build() {
    _$ZScannerSubfolder _$result;
    try {
      _$result = _$v ??
          new _$ZScannerSubfolder._(
              subfolderId: BuiltValueNullFieldError.checkNotNull(
                  subfolderId, r'ZScannerSubfolder', 'subfolderId'),
              typeId: BuiltValueNullFieldError.checkNotNull(
                  typeId, r'ZScannerSubfolder', 'typeId'),
              metadata: metadata.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metadata';
        metadata.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerSubfolder', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
