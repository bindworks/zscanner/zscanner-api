//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_sticker_data.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_module_data_union.g.dart';

/// ZScannerModuleDataUnion
///
/// Properties:
/// * [stickers] 
@BuiltValue()
abstract class ZScannerModuleDataUnion implements Built<ZScannerModuleDataUnion, ZScannerModuleDataUnionBuilder> {
  @BuiltValueField(wireName: r'stickers')
  BuiltList<ZScannerStickerData>? get stickers;

  ZScannerModuleDataUnion._();

  factory ZScannerModuleDataUnion([void updates(ZScannerModuleDataUnionBuilder b)]) = _$ZScannerModuleDataUnion;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerModuleDataUnionBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerModuleDataUnion> get serializer => _$ZScannerModuleDataUnionSerializer();
}

class _$ZScannerModuleDataUnionSerializer implements PrimitiveSerializer<ZScannerModuleDataUnion> {
  @override
  final Iterable<Type> types = const [ZScannerModuleDataUnion, _$ZScannerModuleDataUnion];

  @override
  final String wireName = r'ZScannerModuleDataUnion';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerModuleDataUnion object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.stickers != null) {
      yield r'stickers';
      yield serializers.serialize(
        object.stickers,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerStickerData)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerModuleDataUnion object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerModuleDataUnionBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'stickers':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerStickerData)]),
          ) as BuiltList<ZScannerStickerData>;
          result.stickers.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerModuleDataUnion deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerModuleDataUnionBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

