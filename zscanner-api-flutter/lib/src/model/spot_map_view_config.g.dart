// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'spot_map_view_config.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SpotMapViewConfig extends SpotMapViewConfig {
  @override
  final String? parentSpotId;
  @override
  final String viewId;
  @override
  final String label;
  @override
  final String resourceId;
  @override
  final SpotMapViewType? type;

  factory _$SpotMapViewConfig(
          [void Function(SpotMapViewConfigBuilder)? updates]) =>
      (new SpotMapViewConfigBuilder()..update(updates))._build();

  _$SpotMapViewConfig._(
      {this.parentSpotId,
      required this.viewId,
      required this.label,
      required this.resourceId,
      this.type})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        viewId, r'SpotMapViewConfig', 'viewId');
    BuiltValueNullFieldError.checkNotNull(label, r'SpotMapViewConfig', 'label');
    BuiltValueNullFieldError.checkNotNull(
        resourceId, r'SpotMapViewConfig', 'resourceId');
  }

  @override
  SpotMapViewConfig rebuild(void Function(SpotMapViewConfigBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SpotMapViewConfigBuilder toBuilder() =>
      new SpotMapViewConfigBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SpotMapViewConfig &&
        parentSpotId == other.parentSpotId &&
        viewId == other.viewId &&
        label == other.label &&
        resourceId == other.resourceId &&
        type == other.type;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, parentSpotId.hashCode);
    _$hash = $jc(_$hash, viewId.hashCode);
    _$hash = $jc(_$hash, label.hashCode);
    _$hash = $jc(_$hash, resourceId.hashCode);
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SpotMapViewConfig')
          ..add('parentSpotId', parentSpotId)
          ..add('viewId', viewId)
          ..add('label', label)
          ..add('resourceId', resourceId)
          ..add('type', type))
        .toString();
  }
}

class SpotMapViewConfigBuilder
    implements Builder<SpotMapViewConfig, SpotMapViewConfigBuilder> {
  _$SpotMapViewConfig? _$v;

  String? _parentSpotId;
  String? get parentSpotId => _$this._parentSpotId;
  set parentSpotId(String? parentSpotId) => _$this._parentSpotId = parentSpotId;

  String? _viewId;
  String? get viewId => _$this._viewId;
  set viewId(String? viewId) => _$this._viewId = viewId;

  String? _label;
  String? get label => _$this._label;
  set label(String? label) => _$this._label = label;

  String? _resourceId;
  String? get resourceId => _$this._resourceId;
  set resourceId(String? resourceId) => _$this._resourceId = resourceId;

  SpotMapViewType? _type;
  SpotMapViewType? get type => _$this._type;
  set type(SpotMapViewType? type) => _$this._type = type;

  SpotMapViewConfigBuilder() {
    SpotMapViewConfig._defaults(this);
  }

  SpotMapViewConfigBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _parentSpotId = $v.parentSpotId;
      _viewId = $v.viewId;
      _label = $v.label;
      _resourceId = $v.resourceId;
      _type = $v.type;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SpotMapViewConfig other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SpotMapViewConfig;
  }

  @override
  void update(void Function(SpotMapViewConfigBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SpotMapViewConfig build() => _build();

  _$SpotMapViewConfig _build() {
    final _$result = _$v ??
        new _$SpotMapViewConfig._(
            parentSpotId: parentSpotId,
            viewId: BuiltValueNullFieldError.checkNotNull(
                viewId, r'SpotMapViewConfig', 'viewId'),
            label: BuiltValueNullFieldError.checkNotNull(
                label, r'SpotMapViewConfig', 'label'),
            resourceId: BuiltValueNullFieldError.checkNotNull(
                resourceId, r'SpotMapViewConfig', 'resourceId'),
            type: type);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
