// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_sticker_config_font_face.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerStickerConfigFontFace extends ZScannerStickerConfigFontFace {
  @override
  final String? ios;
  @override
  final String? android;

  factory _$ZScannerStickerConfigFontFace(
          [void Function(ZScannerStickerConfigFontFaceBuilder)? updates]) =>
      (new ZScannerStickerConfigFontFaceBuilder()..update(updates))._build();

  _$ZScannerStickerConfigFontFace._({this.ios, this.android}) : super._();

  @override
  ZScannerStickerConfigFontFace rebuild(
          void Function(ZScannerStickerConfigFontFaceBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerStickerConfigFontFaceBuilder toBuilder() =>
      new ZScannerStickerConfigFontFaceBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerStickerConfigFontFace &&
        ios == other.ios &&
        android == other.android;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, ios.hashCode);
    _$hash = $jc(_$hash, android.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerStickerConfigFontFace')
          ..add('ios', ios)
          ..add('android', android))
        .toString();
  }
}

class ZScannerStickerConfigFontFaceBuilder
    implements
        Builder<ZScannerStickerConfigFontFace,
            ZScannerStickerConfigFontFaceBuilder> {
  _$ZScannerStickerConfigFontFace? _$v;

  String? _ios;
  String? get ios => _$this._ios;
  set ios(String? ios) => _$this._ios = ios;

  String? _android;
  String? get android => _$this._android;
  set android(String? android) => _$this._android = android;

  ZScannerStickerConfigFontFaceBuilder() {
    ZScannerStickerConfigFontFace._defaults(this);
  }

  ZScannerStickerConfigFontFaceBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _ios = $v.ios;
      _android = $v.android;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerStickerConfigFontFace other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerStickerConfigFontFace;
  }

  @override
  void update(void Function(ZScannerStickerConfigFontFaceBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerStickerConfigFontFace build() => _build();

  _$ZScannerStickerConfigFontFace _build() {
    final _$result = _$v ??
        new _$ZScannerStickerConfigFontFace._(ios: ios, android: android);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
