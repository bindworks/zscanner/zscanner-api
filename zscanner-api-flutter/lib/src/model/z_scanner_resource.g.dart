// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_resource.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerResource extends ZScannerResource {
  @override
  final String resourceId;
  @override
  final String contentType;
  @override
  final int contentLength;
  @override
  final DateTime createdTs;
  @override
  final DateTime updatedTs;
  @override
  final String? inlineData;

  factory _$ZScannerResource(
          [void Function(ZScannerResourceBuilder)? updates]) =>
      (new ZScannerResourceBuilder()..update(updates))._build();

  _$ZScannerResource._(
      {required this.resourceId,
      required this.contentType,
      required this.contentLength,
      required this.createdTs,
      required this.updatedTs,
      this.inlineData})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        resourceId, r'ZScannerResource', 'resourceId');
    BuiltValueNullFieldError.checkNotNull(
        contentType, r'ZScannerResource', 'contentType');
    BuiltValueNullFieldError.checkNotNull(
        contentLength, r'ZScannerResource', 'contentLength');
    BuiltValueNullFieldError.checkNotNull(
        createdTs, r'ZScannerResource', 'createdTs');
    BuiltValueNullFieldError.checkNotNull(
        updatedTs, r'ZScannerResource', 'updatedTs');
  }

  @override
  ZScannerResource rebuild(void Function(ZScannerResourceBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerResourceBuilder toBuilder() =>
      new ZScannerResourceBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerResource &&
        resourceId == other.resourceId &&
        contentType == other.contentType &&
        contentLength == other.contentLength &&
        createdTs == other.createdTs &&
        updatedTs == other.updatedTs &&
        inlineData == other.inlineData;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, resourceId.hashCode);
    _$hash = $jc(_$hash, contentType.hashCode);
    _$hash = $jc(_$hash, contentLength.hashCode);
    _$hash = $jc(_$hash, createdTs.hashCode);
    _$hash = $jc(_$hash, updatedTs.hashCode);
    _$hash = $jc(_$hash, inlineData.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerResource')
          ..add('resourceId', resourceId)
          ..add('contentType', contentType)
          ..add('contentLength', contentLength)
          ..add('createdTs', createdTs)
          ..add('updatedTs', updatedTs)
          ..add('inlineData', inlineData))
        .toString();
  }
}

class ZScannerResourceBuilder
    implements Builder<ZScannerResource, ZScannerResourceBuilder> {
  _$ZScannerResource? _$v;

  String? _resourceId;
  String? get resourceId => _$this._resourceId;
  set resourceId(String? resourceId) => _$this._resourceId = resourceId;

  String? _contentType;
  String? get contentType => _$this._contentType;
  set contentType(String? contentType) => _$this._contentType = contentType;

  int? _contentLength;
  int? get contentLength => _$this._contentLength;
  set contentLength(int? contentLength) =>
      _$this._contentLength = contentLength;

  DateTime? _createdTs;
  DateTime? get createdTs => _$this._createdTs;
  set createdTs(DateTime? createdTs) => _$this._createdTs = createdTs;

  DateTime? _updatedTs;
  DateTime? get updatedTs => _$this._updatedTs;
  set updatedTs(DateTime? updatedTs) => _$this._updatedTs = updatedTs;

  String? _inlineData;
  String? get inlineData => _$this._inlineData;
  set inlineData(String? inlineData) => _$this._inlineData = inlineData;

  ZScannerResourceBuilder() {
    ZScannerResource._defaults(this);
  }

  ZScannerResourceBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _resourceId = $v.resourceId;
      _contentType = $v.contentType;
      _contentLength = $v.contentLength;
      _createdTs = $v.createdTs;
      _updatedTs = $v.updatedTs;
      _inlineData = $v.inlineData;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerResource other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerResource;
  }

  @override
  void update(void Function(ZScannerResourceBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerResource build() => _build();

  _$ZScannerResource _build() {
    final _$result = _$v ??
        new _$ZScannerResource._(
            resourceId: BuiltValueNullFieldError.checkNotNull(
                resourceId, r'ZScannerResource', 'resourceId'),
            contentType: BuiltValueNullFieldError.checkNotNull(
                contentType, r'ZScannerResource', 'contentType'),
            contentLength: BuiltValueNullFieldError.checkNotNull(
                contentLength, r'ZScannerResource', 'contentLength'),
            createdTs: BuiltValueNullFieldError.checkNotNull(
                createdTs, r'ZScannerResource', 'createdTs'),
            updatedTs: BuiltValueNullFieldError.checkNotNull(
                updatedTs, r'ZScannerResource', 'updatedTs'),
            inlineData: inlineData);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
