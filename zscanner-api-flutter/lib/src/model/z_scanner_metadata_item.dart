//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_metadata_value.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_metadata_item.g.dart';

/// ZScannerMetadataItem
///
/// Properties:
/// * [metaId] 
/// * [value] 
@BuiltValue()
abstract class ZScannerMetadataItem implements Built<ZScannerMetadataItem, ZScannerMetadataItemBuilder> {
  @BuiltValueField(wireName: r'metaId')
  String get metaId;

  @BuiltValueField(wireName: r'value')
  ZScannerMetadataValue get value;

  ZScannerMetadataItem._();

  factory ZScannerMetadataItem([void updates(ZScannerMetadataItemBuilder b)]) = _$ZScannerMetadataItem;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerMetadataItemBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerMetadataItem> get serializer => _$ZScannerMetadataItemSerializer();
}

class _$ZScannerMetadataItemSerializer implements PrimitiveSerializer<ZScannerMetadataItem> {
  @override
  final Iterable<Type> types = const [ZScannerMetadataItem, _$ZScannerMetadataItem];

  @override
  final String wireName = r'ZScannerMetadataItem';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerMetadataItem object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'metaId';
    yield serializers.serialize(
      object.metaId,
      specifiedType: const FullType(String),
    );
    yield r'value';
    yield serializers.serialize(
      object.value,
      specifiedType: const FullType(ZScannerMetadataValue),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerMetadataItem object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerMetadataItemBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'metaId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.metaId = valueDes;
          break;
        case r'value':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerMetadataValue),
          ) as ZScannerMetadataValue;
          result.value.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerMetadataItem deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerMetadataItemBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

