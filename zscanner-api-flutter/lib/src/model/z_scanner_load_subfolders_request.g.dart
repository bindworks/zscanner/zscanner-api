// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_load_subfolders_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerLoadSubfoldersRequest extends ZScannerLoadSubfoldersRequest {
  @override
  final BuiltList<ZScannerMetadataItem> metadata;
  @override
  final String? typeId;

  factory _$ZScannerLoadSubfoldersRequest(
          [void Function(ZScannerLoadSubfoldersRequestBuilder)? updates]) =>
      (new ZScannerLoadSubfoldersRequestBuilder()..update(updates))._build();

  _$ZScannerLoadSubfoldersRequest._({required this.metadata, this.typeId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        metadata, r'ZScannerLoadSubfoldersRequest', 'metadata');
  }

  @override
  ZScannerLoadSubfoldersRequest rebuild(
          void Function(ZScannerLoadSubfoldersRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerLoadSubfoldersRequestBuilder toBuilder() =>
      new ZScannerLoadSubfoldersRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerLoadSubfoldersRequest &&
        metadata == other.metadata &&
        typeId == other.typeId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, metadata.hashCode);
    _$hash = $jc(_$hash, typeId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerLoadSubfoldersRequest')
          ..add('metadata', metadata)
          ..add('typeId', typeId))
        .toString();
  }
}

class ZScannerLoadSubfoldersRequestBuilder
    implements
        Builder<ZScannerLoadSubfoldersRequest,
            ZScannerLoadSubfoldersRequestBuilder> {
  _$ZScannerLoadSubfoldersRequest? _$v;

  ListBuilder<ZScannerMetadataItem>? _metadata;
  ListBuilder<ZScannerMetadataItem> get metadata =>
      _$this._metadata ??= new ListBuilder<ZScannerMetadataItem>();
  set metadata(ListBuilder<ZScannerMetadataItem>? metadata) =>
      _$this._metadata = metadata;

  String? _typeId;
  String? get typeId => _$this._typeId;
  set typeId(String? typeId) => _$this._typeId = typeId;

  ZScannerLoadSubfoldersRequestBuilder() {
    ZScannerLoadSubfoldersRequest._defaults(this);
  }

  ZScannerLoadSubfoldersRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _metadata = $v.metadata.toBuilder();
      _typeId = $v.typeId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerLoadSubfoldersRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerLoadSubfoldersRequest;
  }

  @override
  void update(void Function(ZScannerLoadSubfoldersRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerLoadSubfoldersRequest build() => _build();

  _$ZScannerLoadSubfoldersRequest _build() {
    _$ZScannerLoadSubfoldersRequest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerLoadSubfoldersRequest._(
              metadata: metadata.build(), typeId: typeId);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metadata';
        metadata.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerLoadSubfoldersRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
