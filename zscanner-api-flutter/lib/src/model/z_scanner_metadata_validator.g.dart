// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_metadata_validator.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerMetadataValidator extends ZScannerMetadataValidator {
  @override
  final int? minLength;
  @override
  final int? maxLength;
  @override
  final int? maxDateRelative;
  @override
  final int? minDateRelative;
  @override
  final bool? czBirthNumber;

  factory _$ZScannerMetadataValidator(
          [void Function(ZScannerMetadataValidatorBuilder)? updates]) =>
      (new ZScannerMetadataValidatorBuilder()..update(updates))._build();

  _$ZScannerMetadataValidator._(
      {this.minLength,
      this.maxLength,
      this.maxDateRelative,
      this.minDateRelative,
      this.czBirthNumber})
      : super._();

  @override
  ZScannerMetadataValidator rebuild(
          void Function(ZScannerMetadataValidatorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerMetadataValidatorBuilder toBuilder() =>
      new ZScannerMetadataValidatorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerMetadataValidator &&
        minLength == other.minLength &&
        maxLength == other.maxLength &&
        maxDateRelative == other.maxDateRelative &&
        minDateRelative == other.minDateRelative &&
        czBirthNumber == other.czBirthNumber;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, minLength.hashCode);
    _$hash = $jc(_$hash, maxLength.hashCode);
    _$hash = $jc(_$hash, maxDateRelative.hashCode);
    _$hash = $jc(_$hash, minDateRelative.hashCode);
    _$hash = $jc(_$hash, czBirthNumber.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerMetadataValidator')
          ..add('minLength', minLength)
          ..add('maxLength', maxLength)
          ..add('maxDateRelative', maxDateRelative)
          ..add('minDateRelative', minDateRelative)
          ..add('czBirthNumber', czBirthNumber))
        .toString();
  }
}

class ZScannerMetadataValidatorBuilder
    implements
        Builder<ZScannerMetadataValidator, ZScannerMetadataValidatorBuilder> {
  _$ZScannerMetadataValidator? _$v;

  int? _minLength;
  int? get minLength => _$this._minLength;
  set minLength(int? minLength) => _$this._minLength = minLength;

  int? _maxLength;
  int? get maxLength => _$this._maxLength;
  set maxLength(int? maxLength) => _$this._maxLength = maxLength;

  int? _maxDateRelative;
  int? get maxDateRelative => _$this._maxDateRelative;
  set maxDateRelative(int? maxDateRelative) =>
      _$this._maxDateRelative = maxDateRelative;

  int? _minDateRelative;
  int? get minDateRelative => _$this._minDateRelative;
  set minDateRelative(int? minDateRelative) =>
      _$this._minDateRelative = minDateRelative;

  bool? _czBirthNumber;
  bool? get czBirthNumber => _$this._czBirthNumber;
  set czBirthNumber(bool? czBirthNumber) =>
      _$this._czBirthNumber = czBirthNumber;

  ZScannerMetadataValidatorBuilder() {
    ZScannerMetadataValidator._defaults(this);
  }

  ZScannerMetadataValidatorBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _minLength = $v.minLength;
      _maxLength = $v.maxLength;
      _maxDateRelative = $v.maxDateRelative;
      _minDateRelative = $v.minDateRelative;
      _czBirthNumber = $v.czBirthNumber;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerMetadataValidator other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerMetadataValidator;
  }

  @override
  void update(void Function(ZScannerMetadataValidatorBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerMetadataValidator build() => _build();

  _$ZScannerMetadataValidator _build() {
    final _$result = _$v ??
        new _$ZScannerMetadataValidator._(
            minLength: minLength,
            maxLength: maxLength,
            maxDateRelative: maxDateRelative,
            minDateRelative: minDateRelative,
            czBirthNumber: czBirthNumber);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
