//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_theme_manifest_dark.g.dart';

/// ZScannerThemeManifestDark
///
/// Properties:
/// * [colorSchemePrimaryColor] 
/// * [colorSchemeSecondaryColor] 
/// * [textScaleFactor] 
/// * [mainFontFamily] 
/// * [secondaryFontFamily] 
@BuiltValue()
abstract class ZScannerThemeManifestDark implements Built<ZScannerThemeManifestDark, ZScannerThemeManifestDarkBuilder> {
  @BuiltValueField(wireName: r'colorSchemePrimaryColor')
  String? get colorSchemePrimaryColor;

  @BuiltValueField(wireName: r'colorSchemeSecondaryColor')
  String? get colorSchemeSecondaryColor;

  @BuiltValueField(wireName: r'textScaleFactor')
  int? get textScaleFactor;

  @BuiltValueField(wireName: r'mainFontFamily')
  String? get mainFontFamily;

  @BuiltValueField(wireName: r'secondaryFontFamily')
  String? get secondaryFontFamily;

  ZScannerThemeManifestDark._();

  factory ZScannerThemeManifestDark([void updates(ZScannerThemeManifestDarkBuilder b)]) = _$ZScannerThemeManifestDark;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerThemeManifestDarkBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerThemeManifestDark> get serializer => _$ZScannerThemeManifestDarkSerializer();
}

class _$ZScannerThemeManifestDarkSerializer implements PrimitiveSerializer<ZScannerThemeManifestDark> {
  @override
  final Iterable<Type> types = const [ZScannerThemeManifestDark, _$ZScannerThemeManifestDark];

  @override
  final String wireName = r'ZScannerThemeManifestDark';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerThemeManifestDark object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.colorSchemePrimaryColor != null) {
      yield r'colorSchemePrimaryColor';
      yield serializers.serialize(
        object.colorSchemePrimaryColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.colorSchemeSecondaryColor != null) {
      yield r'colorSchemeSecondaryColor';
      yield serializers.serialize(
        object.colorSchemeSecondaryColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.textScaleFactor != null) {
      yield r'textScaleFactor';
      yield serializers.serialize(
        object.textScaleFactor,
        specifiedType: const FullType(int),
      );
    }
    if (object.mainFontFamily != null) {
      yield r'mainFontFamily';
      yield serializers.serialize(
        object.mainFontFamily,
        specifiedType: const FullType(String),
      );
    }
    if (object.secondaryFontFamily != null) {
      yield r'secondaryFontFamily';
      yield serializers.serialize(
        object.secondaryFontFamily,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerThemeManifestDark object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerThemeManifestDarkBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'colorSchemePrimaryColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.colorSchemePrimaryColor = valueDes;
          break;
        case r'colorSchemeSecondaryColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.colorSchemeSecondaryColor = valueDes;
          break;
        case r'textScaleFactor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.textScaleFactor = valueDes;
          break;
        case r'mainFontFamily':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.mainFontFamily = valueDes;
          break;
        case r'secondaryFontFamily':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.secondaryFontFamily = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerThemeManifestDark deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerThemeManifestDarkBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

