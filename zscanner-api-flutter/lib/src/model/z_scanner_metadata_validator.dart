//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_metadata_validator.g.dart';

/// ZScannerMetadataValidator
///
/// Properties:
/// * [minLength] 
/// * [maxLength] 
/// * [maxDateRelative] - Maximum date specified relative to today. I.e. TODAY + maxDateRelative is the maximum date valid
/// * [minDateRelative] - Minimum date specified relative to today. I.e. TODAY - minDateRelative is the maximum date valid
/// * [czBirthNumber] - String is a czech birth number
@BuiltValue()
abstract class ZScannerMetadataValidator implements Built<ZScannerMetadataValidator, ZScannerMetadataValidatorBuilder> {
  @BuiltValueField(wireName: r'minLength')
  int? get minLength;

  @BuiltValueField(wireName: r'maxLength')
  int? get maxLength;

  /// Maximum date specified relative to today. I.e. TODAY + maxDateRelative is the maximum date valid
  @BuiltValueField(wireName: r'maxDateRelative')
  int? get maxDateRelative;

  /// Minimum date specified relative to today. I.e. TODAY - minDateRelative is the maximum date valid
  @BuiltValueField(wireName: r'minDateRelative')
  int? get minDateRelative;

  /// String is a czech birth number
  @BuiltValueField(wireName: r'czBirthNumber')
  bool? get czBirthNumber;

  ZScannerMetadataValidator._();

  factory ZScannerMetadataValidator([void updates(ZScannerMetadataValidatorBuilder b)]) = _$ZScannerMetadataValidator;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerMetadataValidatorBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerMetadataValidator> get serializer => _$ZScannerMetadataValidatorSerializer();
}

class _$ZScannerMetadataValidatorSerializer implements PrimitiveSerializer<ZScannerMetadataValidator> {
  @override
  final Iterable<Type> types = const [ZScannerMetadataValidator, _$ZScannerMetadataValidator];

  @override
  final String wireName = r'ZScannerMetadataValidator';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerMetadataValidator object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.minLength != null) {
      yield r'minLength';
      yield serializers.serialize(
        object.minLength,
        specifiedType: const FullType(int),
      );
    }
    if (object.maxLength != null) {
      yield r'maxLength';
      yield serializers.serialize(
        object.maxLength,
        specifiedType: const FullType(int),
      );
    }
    if (object.maxDateRelative != null) {
      yield r'maxDateRelative';
      yield serializers.serialize(
        object.maxDateRelative,
        specifiedType: const FullType(int),
      );
    }
    if (object.minDateRelative != null) {
      yield r'minDateRelative';
      yield serializers.serialize(
        object.minDateRelative,
        specifiedType: const FullType(int),
      );
    }
    if (object.czBirthNumber != null) {
      yield r'czBirthNumber';
      yield serializers.serialize(
        object.czBirthNumber,
        specifiedType: const FullType(bool),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerMetadataValidator object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerMetadataValidatorBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'minLength':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.minLength = valueDes;
          break;
        case r'maxLength':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.maxLength = valueDes;
          break;
        case r'maxDateRelative':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.maxDateRelative = valueDes;
          break;
        case r'minDateRelative':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.minDateRelative = valueDes;
          break;
        case r'czBirthNumber':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.czBirthNumber = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerMetadataValidator deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerMetadataValidatorBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

