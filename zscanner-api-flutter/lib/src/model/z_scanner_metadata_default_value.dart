//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_metadata_default_value.g.dart';

/// ZScannerMetadataDefaultValue
///
/// Properties:
/// * [fromMeta] 
/// * [fromTemplate] 
/// * [dateValue] 
/// * [stringValue] 
/// * [numberValue] 
/// * [booleanValue] 
/// * [dateTimeValue] 
/// * [dateRelativeValue] 
/// * [multiStringValue] 
@BuiltValue()
abstract class ZScannerMetadataDefaultValue implements Built<ZScannerMetadataDefaultValue, ZScannerMetadataDefaultValueBuilder> {
  @BuiltValueField(wireName: r'fromMeta')
  String? get fromMeta;

  @BuiltValueField(wireName: r'fromTemplate')
  String? get fromTemplate;

  @BuiltValueField(wireName: r'dateValue')
  String? get dateValue;

  @BuiltValueField(wireName: r'stringValue')
  String? get stringValue;

  @BuiltValueField(wireName: r'numberValue')
  num? get numberValue;

  @BuiltValueField(wireName: r'booleanValue')
  bool? get booleanValue;

  @BuiltValueField(wireName: r'dateTimeValue')
  DateTime? get dateTimeValue;

  @BuiltValueField(wireName: r'dateRelativeValue')
  int? get dateRelativeValue;

  @BuiltValueField(wireName: r'multiStringValue')
  BuiltList<String>? get multiStringValue;

  ZScannerMetadataDefaultValue._();

  factory ZScannerMetadataDefaultValue([void updates(ZScannerMetadataDefaultValueBuilder b)]) = _$ZScannerMetadataDefaultValue;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerMetadataDefaultValueBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerMetadataDefaultValue> get serializer => _$ZScannerMetadataDefaultValueSerializer();
}

class _$ZScannerMetadataDefaultValueSerializer implements PrimitiveSerializer<ZScannerMetadataDefaultValue> {
  @override
  final Iterable<Type> types = const [ZScannerMetadataDefaultValue, _$ZScannerMetadataDefaultValue];

  @override
  final String wireName = r'ZScannerMetadataDefaultValue';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerMetadataDefaultValue object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.fromMeta != null) {
      yield r'fromMeta';
      yield serializers.serialize(
        object.fromMeta,
        specifiedType: const FullType(String),
      );
    }
    if (object.fromTemplate != null) {
      yield r'fromTemplate';
      yield serializers.serialize(
        object.fromTemplate,
        specifiedType: const FullType(String),
      );
    }
    if (object.dateValue != null) {
      yield r'dateValue';
      yield serializers.serialize(
        object.dateValue,
        specifiedType: const FullType(String),
      );
    }
    if (object.stringValue != null) {
      yield r'stringValue';
      yield serializers.serialize(
        object.stringValue,
        specifiedType: const FullType(String),
      );
    }
    if (object.numberValue != null) {
      yield r'numberValue';
      yield serializers.serialize(
        object.numberValue,
        specifiedType: const FullType(num),
      );
    }
    if (object.booleanValue != null) {
      yield r'booleanValue';
      yield serializers.serialize(
        object.booleanValue,
        specifiedType: const FullType(bool),
      );
    }
    if (object.dateTimeValue != null) {
      yield r'dateTimeValue';
      yield serializers.serialize(
        object.dateTimeValue,
        specifiedType: const FullType(DateTime),
      );
    }
    if (object.dateRelativeValue != null) {
      yield r'dateRelativeValue';
      yield serializers.serialize(
        object.dateRelativeValue,
        specifiedType: const FullType(int),
      );
    }
    if (object.multiStringValue != null) {
      yield r'multiStringValue';
      yield serializers.serialize(
        object.multiStringValue,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerMetadataDefaultValue object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerMetadataDefaultValueBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'fromMeta':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fromMeta = valueDes;
          break;
        case r'fromTemplate':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fromTemplate = valueDes;
          break;
        case r'dateValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.dateValue = valueDes;
          break;
        case r'stringValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.stringValue = valueDes;
          break;
        case r'numberValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.numberValue = valueDes;
          break;
        case r'booleanValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.booleanValue = valueDes;
          break;
        case r'dateTimeValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.dateTimeValue = valueDes;
          break;
        case r'dateRelativeValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.dateRelativeValue = valueDes;
          break;
        case r'multiStringValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.multiStringValue.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerMetadataDefaultValue deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerMetadataDefaultValueBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

