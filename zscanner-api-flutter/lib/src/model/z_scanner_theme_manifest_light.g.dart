// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_theme_manifest_light.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerThemeManifestLight extends ZScannerThemeManifestLight {
  @override
  final int? textScaleFactor;
  @override
  final String? mainFontFamily;
  @override
  final String? secondaryFontFamily;

  factory _$ZScannerThemeManifestLight(
          [void Function(ZScannerThemeManifestLightBuilder)? updates]) =>
      (new ZScannerThemeManifestLightBuilder()..update(updates))._build();

  _$ZScannerThemeManifestLight._(
      {this.textScaleFactor, this.mainFontFamily, this.secondaryFontFamily})
      : super._();

  @override
  ZScannerThemeManifestLight rebuild(
          void Function(ZScannerThemeManifestLightBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerThemeManifestLightBuilder toBuilder() =>
      new ZScannerThemeManifestLightBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerThemeManifestLight &&
        textScaleFactor == other.textScaleFactor &&
        mainFontFamily == other.mainFontFamily &&
        secondaryFontFamily == other.secondaryFontFamily;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, textScaleFactor.hashCode);
    _$hash = $jc(_$hash, mainFontFamily.hashCode);
    _$hash = $jc(_$hash, secondaryFontFamily.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerThemeManifestLight')
          ..add('textScaleFactor', textScaleFactor)
          ..add('mainFontFamily', mainFontFamily)
          ..add('secondaryFontFamily', secondaryFontFamily))
        .toString();
  }
}

class ZScannerThemeManifestLightBuilder
    implements
        Builder<ZScannerThemeManifestLight, ZScannerThemeManifestLightBuilder> {
  _$ZScannerThemeManifestLight? _$v;

  int? _textScaleFactor;
  int? get textScaleFactor => _$this._textScaleFactor;
  set textScaleFactor(int? textScaleFactor) =>
      _$this._textScaleFactor = textScaleFactor;

  String? _mainFontFamily;
  String? get mainFontFamily => _$this._mainFontFamily;
  set mainFontFamily(String? mainFontFamily) =>
      _$this._mainFontFamily = mainFontFamily;

  String? _secondaryFontFamily;
  String? get secondaryFontFamily => _$this._secondaryFontFamily;
  set secondaryFontFamily(String? secondaryFontFamily) =>
      _$this._secondaryFontFamily = secondaryFontFamily;

  ZScannerThemeManifestLightBuilder() {
    ZScannerThemeManifestLight._defaults(this);
  }

  ZScannerThemeManifestLightBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _textScaleFactor = $v.textScaleFactor;
      _mainFontFamily = $v.mainFontFamily;
      _secondaryFontFamily = $v.secondaryFontFamily;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerThemeManifestLight other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerThemeManifestLight;
  }

  @override
  void update(void Function(ZScannerThemeManifestLightBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerThemeManifestLight build() => _build();

  _$ZScannerThemeManifestLight _build() {
    final _$result = _$v ??
        new _$ZScannerThemeManifestLight._(
            textScaleFactor: textScaleFactor,
            mainFontFamily: mainFontFamily,
            secondaryFontFamily: secondaryFontFamily);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
