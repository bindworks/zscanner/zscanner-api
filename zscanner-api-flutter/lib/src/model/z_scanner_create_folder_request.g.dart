// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_create_folder_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerCreateFolderRequest extends ZScannerCreateFolderRequest {
  @override
  final BuiltList<ZScannerMetadataItem> metadata;
  @override
  final BuiltList<ZScannerMetadataItem> folderMetadata;

  factory _$ZScannerCreateFolderRequest(
          [void Function(ZScannerCreateFolderRequestBuilder)? updates]) =>
      (new ZScannerCreateFolderRequestBuilder()..update(updates))._build();

  _$ZScannerCreateFolderRequest._(
      {required this.metadata, required this.folderMetadata})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        metadata, r'ZScannerCreateFolderRequest', 'metadata');
    BuiltValueNullFieldError.checkNotNull(
        folderMetadata, r'ZScannerCreateFolderRequest', 'folderMetadata');
  }

  @override
  ZScannerCreateFolderRequest rebuild(
          void Function(ZScannerCreateFolderRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerCreateFolderRequestBuilder toBuilder() =>
      new ZScannerCreateFolderRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerCreateFolderRequest &&
        metadata == other.metadata &&
        folderMetadata == other.folderMetadata;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, metadata.hashCode);
    _$hash = $jc(_$hash, folderMetadata.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerCreateFolderRequest')
          ..add('metadata', metadata)
          ..add('folderMetadata', folderMetadata))
        .toString();
  }
}

class ZScannerCreateFolderRequestBuilder
    implements
        Builder<ZScannerCreateFolderRequest,
            ZScannerCreateFolderRequestBuilder> {
  _$ZScannerCreateFolderRequest? _$v;

  ListBuilder<ZScannerMetadataItem>? _metadata;
  ListBuilder<ZScannerMetadataItem> get metadata =>
      _$this._metadata ??= new ListBuilder<ZScannerMetadataItem>();
  set metadata(ListBuilder<ZScannerMetadataItem>? metadata) =>
      _$this._metadata = metadata;

  ListBuilder<ZScannerMetadataItem>? _folderMetadata;
  ListBuilder<ZScannerMetadataItem> get folderMetadata =>
      _$this._folderMetadata ??= new ListBuilder<ZScannerMetadataItem>();
  set folderMetadata(ListBuilder<ZScannerMetadataItem>? folderMetadata) =>
      _$this._folderMetadata = folderMetadata;

  ZScannerCreateFolderRequestBuilder() {
    ZScannerCreateFolderRequest._defaults(this);
  }

  ZScannerCreateFolderRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _metadata = $v.metadata.toBuilder();
      _folderMetadata = $v.folderMetadata.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerCreateFolderRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerCreateFolderRequest;
  }

  @override
  void update(void Function(ZScannerCreateFolderRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerCreateFolderRequest build() => _build();

  _$ZScannerCreateFolderRequest _build() {
    _$ZScannerCreateFolderRequest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerCreateFolderRequest._(
              metadata: metadata.build(),
              folderMetadata: folderMetadata.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metadata';
        metadata.build();
        _$failedField = 'folderMetadata';
        folderMetadata.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerCreateFolderRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
