//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_resource.g.dart';

/// ZScannerResource
///
/// Properties:
/// * [resourceId] 
/// * [contentType] 
/// * [contentLength] 
/// * [createdTs] 
/// * [updatedTs] 
/// * [inlineData] 
@BuiltValue()
abstract class ZScannerResource implements Built<ZScannerResource, ZScannerResourceBuilder> {
  @BuiltValueField(wireName: r'resourceId')
  String get resourceId;

  @BuiltValueField(wireName: r'contentType')
  String get contentType;

  @BuiltValueField(wireName: r'contentLength')
  int get contentLength;

  @BuiltValueField(wireName: r'createdTs')
  DateTime get createdTs;

  @BuiltValueField(wireName: r'updatedTs')
  DateTime get updatedTs;

  @BuiltValueField(wireName: r'inlineData')
  String? get inlineData;

  ZScannerResource._();

  factory ZScannerResource([void updates(ZScannerResourceBuilder b)]) = _$ZScannerResource;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerResourceBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerResource> get serializer => _$ZScannerResourceSerializer();
}

class _$ZScannerResourceSerializer implements PrimitiveSerializer<ZScannerResource> {
  @override
  final Iterable<Type> types = const [ZScannerResource, _$ZScannerResource];

  @override
  final String wireName = r'ZScannerResource';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerResource object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'resourceId';
    yield serializers.serialize(
      object.resourceId,
      specifiedType: const FullType(String),
    );
    yield r'contentType';
    yield serializers.serialize(
      object.contentType,
      specifiedType: const FullType(String),
    );
    yield r'contentLength';
    yield serializers.serialize(
      object.contentLength,
      specifiedType: const FullType(int),
    );
    yield r'createdTs';
    yield serializers.serialize(
      object.createdTs,
      specifiedType: const FullType(DateTime),
    );
    yield r'updatedTs';
    yield serializers.serialize(
      object.updatedTs,
      specifiedType: const FullType(DateTime),
    );
    if (object.inlineData != null) {
      yield r'inlineData';
      yield serializers.serialize(
        object.inlineData,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerResource object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerResourceBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'resourceId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.resourceId = valueDes;
          break;
        case r'contentType':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.contentType = valueDes;
          break;
        case r'contentLength':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.contentLength = valueDes;
          break;
        case r'createdTs':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdTs = valueDes;
          break;
        case r'updatedTs':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.updatedTs = valueDes;
          break;
        case r'inlineData':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.inlineData = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerResource deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerResourceBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

