//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_metadata_item.dart';
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_subfolder.dart';
import 'package:zscanner_api/src/model/z_scanner_upload_batch_media.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_upload_batch_request.g.dart';

/// ZScannerUploadBatchRequest
///
/// Properties:
/// * [uuid] 
/// * [folderId] 
/// * [flowId] 
/// * [moduleId] 
/// * [moduleVersion] 
/// * [createdTs] 
/// * [media] 
/// * [metadata] 
/// * [subfolders] 
@BuiltValue()
abstract class ZScannerUploadBatchRequest implements Built<ZScannerUploadBatchRequest, ZScannerUploadBatchRequestBuilder> {
  @BuiltValueField(wireName: r'uuid')
  String get uuid;

  @BuiltValueField(wireName: r'folderId')
  String get folderId;

  @BuiltValueField(wireName: r'flowId')
  String get flowId;

  @BuiltValueField(wireName: r'moduleId')
  String get moduleId;

  @BuiltValueField(wireName: r'moduleVersion')
  String get moduleVersion;

  @BuiltValueField(wireName: r'createdTs')
  DateTime get createdTs;

  @BuiltValueField(wireName: r'media')
  BuiltList<ZScannerUploadBatchMedia> get media;

  @BuiltValueField(wireName: r'metadata')
  BuiltList<ZScannerMetadataItem> get metadata;

  @BuiltValueField(wireName: r'subfolders')
  BuiltList<ZScannerSubfolder>? get subfolders;

  ZScannerUploadBatchRequest._();

  factory ZScannerUploadBatchRequest([void updates(ZScannerUploadBatchRequestBuilder b)]) = _$ZScannerUploadBatchRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerUploadBatchRequestBuilder b) => b
      ..moduleVersion = 'any';

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerUploadBatchRequest> get serializer => _$ZScannerUploadBatchRequestSerializer();
}

class _$ZScannerUploadBatchRequestSerializer implements PrimitiveSerializer<ZScannerUploadBatchRequest> {
  @override
  final Iterable<Type> types = const [ZScannerUploadBatchRequest, _$ZScannerUploadBatchRequest];

  @override
  final String wireName = r'ZScannerUploadBatchRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerUploadBatchRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'uuid';
    yield serializers.serialize(
      object.uuid,
      specifiedType: const FullType(String),
    );
    yield r'folderId';
    yield serializers.serialize(
      object.folderId,
      specifiedType: const FullType(String),
    );
    yield r'flowId';
    yield serializers.serialize(
      object.flowId,
      specifiedType: const FullType(String),
    );
    yield r'moduleId';
    yield serializers.serialize(
      object.moduleId,
      specifiedType: const FullType(String),
    );
    yield r'moduleVersion';
    yield serializers.serialize(
      object.moduleVersion,
      specifiedType: const FullType(String),
    );
    yield r'createdTs';
    yield serializers.serialize(
      object.createdTs,
      specifiedType: const FullType(DateTime),
    );
    yield r'media';
    yield serializers.serialize(
      object.media,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerUploadBatchMedia)]),
    );
    yield r'metadata';
    yield serializers.serialize(
      object.metadata,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
    );
    if (object.subfolders != null) {
      yield r'subfolders';
      yield serializers.serialize(
        object.subfolders,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerSubfolder)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerUploadBatchRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerUploadBatchRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'uuid':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.uuid = valueDes;
          break;
        case r'folderId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.folderId = valueDes;
          break;
        case r'flowId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.flowId = valueDes;
          break;
        case r'moduleId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.moduleId = valueDes;
          break;
        case r'moduleVersion':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.moduleVersion = valueDes;
          break;
        case r'createdTs':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdTs = valueDes;
          break;
        case r'media':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerUploadBatchMedia)]),
          ) as BuiltList<ZScannerUploadBatchMedia>;
          result.media.replace(valueDes);
          break;
        case r'metadata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
          ) as BuiltList<ZScannerMetadataItem>;
          result.metadata.replace(valueDes);
          break;
        case r'subfolders':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerSubfolder)]),
          ) as BuiltList<ZScannerSubfolder>;
          result.subfolders.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerUploadBatchRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerUploadBatchRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

