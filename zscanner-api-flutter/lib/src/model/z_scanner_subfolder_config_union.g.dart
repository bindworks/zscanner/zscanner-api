// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_subfolder_config_union.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerSubfolderConfigUnion extends ZScannerSubfolderConfigUnion {
  @override
  final SpotMapConfig? spotMap;
  @override
  final Map2dConfig? map2d;
  @override
  final bool? naked;

  factory _$ZScannerSubfolderConfigUnion(
          [void Function(ZScannerSubfolderConfigUnionBuilder)? updates]) =>
      (new ZScannerSubfolderConfigUnionBuilder()..update(updates))._build();

  _$ZScannerSubfolderConfigUnion._({this.spotMap, this.map2d, this.naked})
      : super._();

  @override
  ZScannerSubfolderConfigUnion rebuild(
          void Function(ZScannerSubfolderConfigUnionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerSubfolderConfigUnionBuilder toBuilder() =>
      new ZScannerSubfolderConfigUnionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerSubfolderConfigUnion &&
        spotMap == other.spotMap &&
        map2d == other.map2d &&
        naked == other.naked;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, spotMap.hashCode);
    _$hash = $jc(_$hash, map2d.hashCode);
    _$hash = $jc(_$hash, naked.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerSubfolderConfigUnion')
          ..add('spotMap', spotMap)
          ..add('map2d', map2d)
          ..add('naked', naked))
        .toString();
  }
}

class ZScannerSubfolderConfigUnionBuilder
    implements
        Builder<ZScannerSubfolderConfigUnion,
            ZScannerSubfolderConfigUnionBuilder> {
  _$ZScannerSubfolderConfigUnion? _$v;

  SpotMapConfigBuilder? _spotMap;
  SpotMapConfigBuilder get spotMap =>
      _$this._spotMap ??= new SpotMapConfigBuilder();
  set spotMap(SpotMapConfigBuilder? spotMap) => _$this._spotMap = spotMap;

  Map2dConfigBuilder? _map2d;
  Map2dConfigBuilder get map2d => _$this._map2d ??= new Map2dConfigBuilder();
  set map2d(Map2dConfigBuilder? map2d) => _$this._map2d = map2d;

  bool? _naked;
  bool? get naked => _$this._naked;
  set naked(bool? naked) => _$this._naked = naked;

  ZScannerSubfolderConfigUnionBuilder() {
    ZScannerSubfolderConfigUnion._defaults(this);
  }

  ZScannerSubfolderConfigUnionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _spotMap = $v.spotMap?.toBuilder();
      _map2d = $v.map2d?.toBuilder();
      _naked = $v.naked;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerSubfolderConfigUnion other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerSubfolderConfigUnion;
  }

  @override
  void update(void Function(ZScannerSubfolderConfigUnionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerSubfolderConfigUnion build() => _build();

  _$ZScannerSubfolderConfigUnion _build() {
    _$ZScannerSubfolderConfigUnion _$result;
    try {
      _$result = _$v ??
          new _$ZScannerSubfolderConfigUnion._(
              spotMap: _spotMap?.build(), map2d: _map2d?.build(), naked: naked);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'spotMap';
        _spotMap?.build();
        _$failedField = 'map2d';
        _map2d?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerSubfolderConfigUnion', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
