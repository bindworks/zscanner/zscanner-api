//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_client_manifest.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_global_manifest.g.dart';

/// ZScannerGlobalManifest
///
/// Properties:
/// * [client] 
@BuiltValue()
abstract class ZScannerGlobalManifest implements Built<ZScannerGlobalManifest, ZScannerGlobalManifestBuilder> {
  @BuiltValueField(wireName: r'client')
  ZScannerClientManifest get client;

  ZScannerGlobalManifest._();

  factory ZScannerGlobalManifest([void updates(ZScannerGlobalManifestBuilder b)]) = _$ZScannerGlobalManifest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerGlobalManifestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerGlobalManifest> get serializer => _$ZScannerGlobalManifestSerializer();
}

class _$ZScannerGlobalManifestSerializer implements PrimitiveSerializer<ZScannerGlobalManifest> {
  @override
  final Iterable<Type> types = const [ZScannerGlobalManifest, _$ZScannerGlobalManifest];

  @override
  final String wireName = r'ZScannerGlobalManifest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerGlobalManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'client';
    yield serializers.serialize(
      object.client,
      specifiedType: const FullType(ZScannerClientManifest),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerGlobalManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerGlobalManifestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'client':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerClientManifest),
          ) as ZScannerClientManifest;
          result.client.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerGlobalManifest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerGlobalManifestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

