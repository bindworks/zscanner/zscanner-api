// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_server_manifest.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerServerManifest extends ZScannerServerManifest {
  @override
  final String rootApiUrl;
  @override
  final BuiltList<String>? supportedCapabilities;

  factory _$ZScannerServerManifest(
          [void Function(ZScannerServerManifestBuilder)? updates]) =>
      (new ZScannerServerManifestBuilder()..update(updates))._build();

  _$ZScannerServerManifest._(
      {required this.rootApiUrl, this.supportedCapabilities})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        rootApiUrl, r'ZScannerServerManifest', 'rootApiUrl');
  }

  @override
  ZScannerServerManifest rebuild(
          void Function(ZScannerServerManifestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerServerManifestBuilder toBuilder() =>
      new ZScannerServerManifestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerServerManifest &&
        rootApiUrl == other.rootApiUrl &&
        supportedCapabilities == other.supportedCapabilities;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, rootApiUrl.hashCode);
    _$hash = $jc(_$hash, supportedCapabilities.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerServerManifest')
          ..add('rootApiUrl', rootApiUrl)
          ..add('supportedCapabilities', supportedCapabilities))
        .toString();
  }
}

class ZScannerServerManifestBuilder
    implements Builder<ZScannerServerManifest, ZScannerServerManifestBuilder> {
  _$ZScannerServerManifest? _$v;

  String? _rootApiUrl;
  String? get rootApiUrl => _$this._rootApiUrl;
  set rootApiUrl(String? rootApiUrl) => _$this._rootApiUrl = rootApiUrl;

  ListBuilder<String>? _supportedCapabilities;
  ListBuilder<String> get supportedCapabilities =>
      _$this._supportedCapabilities ??= new ListBuilder<String>();
  set supportedCapabilities(ListBuilder<String>? supportedCapabilities) =>
      _$this._supportedCapabilities = supportedCapabilities;

  ZScannerServerManifestBuilder() {
    ZScannerServerManifest._defaults(this);
  }

  ZScannerServerManifestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _rootApiUrl = $v.rootApiUrl;
      _supportedCapabilities = $v.supportedCapabilities?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerServerManifest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerServerManifest;
  }

  @override
  void update(void Function(ZScannerServerManifestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerServerManifest build() => _build();

  _$ZScannerServerManifest _build() {
    _$ZScannerServerManifest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerServerManifest._(
              rootApiUrl: BuiltValueNullFieldError.checkNotNull(
                  rootApiUrl, r'ZScannerServerManifest', 'rootApiUrl'),
              supportedCapabilities: _supportedCapabilities?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'supportedCapabilities';
        _supportedCapabilities?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerServerManifest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
