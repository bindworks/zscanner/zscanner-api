//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_sticker_config_font_face.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_sticker_config.g.dart';

/// ZScannerStickerConfig
///
/// Properties:
/// * [fontFace] 
/// * [textStickers] 
@BuiltValue()
abstract class ZScannerStickerConfig implements Built<ZScannerStickerConfig, ZScannerStickerConfigBuilder> {
  @BuiltValueField(wireName: r'fontFace')
  ZScannerStickerConfigFontFace? get fontFace;

  @BuiltValueField(wireName: r'textStickers')
  BuiltList<String>? get textStickers;

  ZScannerStickerConfig._();

  factory ZScannerStickerConfig([void updates(ZScannerStickerConfigBuilder b)]) = _$ZScannerStickerConfig;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerStickerConfigBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerStickerConfig> get serializer => _$ZScannerStickerConfigSerializer();
}

class _$ZScannerStickerConfigSerializer implements PrimitiveSerializer<ZScannerStickerConfig> {
  @override
  final Iterable<Type> types = const [ZScannerStickerConfig, _$ZScannerStickerConfig];

  @override
  final String wireName = r'ZScannerStickerConfig';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerStickerConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.fontFace != null) {
      yield r'fontFace';
      yield serializers.serialize(
        object.fontFace,
        specifiedType: const FullType(ZScannerStickerConfigFontFace),
      );
    }
    if (object.textStickers != null) {
      yield r'textStickers';
      yield serializers.serialize(
        object.textStickers,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerStickerConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerStickerConfigBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'fontFace':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerStickerConfigFontFace),
          ) as ZScannerStickerConfigFontFace;
          result.fontFace.replace(valueDes);
          break;
        case r'textStickers':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.textStickers.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerStickerConfig deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerStickerConfigBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

