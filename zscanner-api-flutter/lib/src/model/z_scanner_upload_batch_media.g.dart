// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_upload_batch_media.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerUploadBatchMedia extends ZScannerUploadBatchMedia {
  @override
  final String mediaId;
  @override
  final String contentType;
  @override
  final int contentLength;
  @override
  final DateTime? photoTs;
  @override
  final BuiltList<ZScannerMetadataItem>? metadata;
  @override
  final String? subfolderTypeId;
  @override
  final String? subfolderId;
  @override
  final BuiltList<ZScannerUploadBatchMediaRelation>? relations;
  @override
  final ZScannerModuleDataUnion? moduleData;

  factory _$ZScannerUploadBatchMedia(
          [void Function(ZScannerUploadBatchMediaBuilder)? updates]) =>
      (new ZScannerUploadBatchMediaBuilder()..update(updates))._build();

  _$ZScannerUploadBatchMedia._(
      {required this.mediaId,
      required this.contentType,
      required this.contentLength,
      this.photoTs,
      this.metadata,
      this.subfolderTypeId,
      this.subfolderId,
      this.relations,
      this.moduleData})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        mediaId, r'ZScannerUploadBatchMedia', 'mediaId');
    BuiltValueNullFieldError.checkNotNull(
        contentType, r'ZScannerUploadBatchMedia', 'contentType');
    BuiltValueNullFieldError.checkNotNull(
        contentLength, r'ZScannerUploadBatchMedia', 'contentLength');
  }

  @override
  ZScannerUploadBatchMedia rebuild(
          void Function(ZScannerUploadBatchMediaBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerUploadBatchMediaBuilder toBuilder() =>
      new ZScannerUploadBatchMediaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerUploadBatchMedia &&
        mediaId == other.mediaId &&
        contentType == other.contentType &&
        contentLength == other.contentLength &&
        photoTs == other.photoTs &&
        metadata == other.metadata &&
        subfolderTypeId == other.subfolderTypeId &&
        subfolderId == other.subfolderId &&
        relations == other.relations &&
        moduleData == other.moduleData;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, mediaId.hashCode);
    _$hash = $jc(_$hash, contentType.hashCode);
    _$hash = $jc(_$hash, contentLength.hashCode);
    _$hash = $jc(_$hash, photoTs.hashCode);
    _$hash = $jc(_$hash, metadata.hashCode);
    _$hash = $jc(_$hash, subfolderTypeId.hashCode);
    _$hash = $jc(_$hash, subfolderId.hashCode);
    _$hash = $jc(_$hash, relations.hashCode);
    _$hash = $jc(_$hash, moduleData.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerUploadBatchMedia')
          ..add('mediaId', mediaId)
          ..add('contentType', contentType)
          ..add('contentLength', contentLength)
          ..add('photoTs', photoTs)
          ..add('metadata', metadata)
          ..add('subfolderTypeId', subfolderTypeId)
          ..add('subfolderId', subfolderId)
          ..add('relations', relations)
          ..add('moduleData', moduleData))
        .toString();
  }
}

class ZScannerUploadBatchMediaBuilder
    implements
        Builder<ZScannerUploadBatchMedia, ZScannerUploadBatchMediaBuilder> {
  _$ZScannerUploadBatchMedia? _$v;

  String? _mediaId;
  String? get mediaId => _$this._mediaId;
  set mediaId(String? mediaId) => _$this._mediaId = mediaId;

  String? _contentType;
  String? get contentType => _$this._contentType;
  set contentType(String? contentType) => _$this._contentType = contentType;

  int? _contentLength;
  int? get contentLength => _$this._contentLength;
  set contentLength(int? contentLength) =>
      _$this._contentLength = contentLength;

  DateTime? _photoTs;
  DateTime? get photoTs => _$this._photoTs;
  set photoTs(DateTime? photoTs) => _$this._photoTs = photoTs;

  ListBuilder<ZScannerMetadataItem>? _metadata;
  ListBuilder<ZScannerMetadataItem> get metadata =>
      _$this._metadata ??= new ListBuilder<ZScannerMetadataItem>();
  set metadata(ListBuilder<ZScannerMetadataItem>? metadata) =>
      _$this._metadata = metadata;

  String? _subfolderTypeId;
  String? get subfolderTypeId => _$this._subfolderTypeId;
  set subfolderTypeId(String? subfolderTypeId) =>
      _$this._subfolderTypeId = subfolderTypeId;

  String? _subfolderId;
  String? get subfolderId => _$this._subfolderId;
  set subfolderId(String? subfolderId) => _$this._subfolderId = subfolderId;

  ListBuilder<ZScannerUploadBatchMediaRelation>? _relations;
  ListBuilder<ZScannerUploadBatchMediaRelation> get relations =>
      _$this._relations ??= new ListBuilder<ZScannerUploadBatchMediaRelation>();
  set relations(ListBuilder<ZScannerUploadBatchMediaRelation>? relations) =>
      _$this._relations = relations;

  ZScannerModuleDataUnionBuilder? _moduleData;
  ZScannerModuleDataUnionBuilder get moduleData =>
      _$this._moduleData ??= new ZScannerModuleDataUnionBuilder();
  set moduleData(ZScannerModuleDataUnionBuilder? moduleData) =>
      _$this._moduleData = moduleData;

  ZScannerUploadBatchMediaBuilder() {
    ZScannerUploadBatchMedia._defaults(this);
  }

  ZScannerUploadBatchMediaBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _mediaId = $v.mediaId;
      _contentType = $v.contentType;
      _contentLength = $v.contentLength;
      _photoTs = $v.photoTs;
      _metadata = $v.metadata?.toBuilder();
      _subfolderTypeId = $v.subfolderTypeId;
      _subfolderId = $v.subfolderId;
      _relations = $v.relations?.toBuilder();
      _moduleData = $v.moduleData?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerUploadBatchMedia other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerUploadBatchMedia;
  }

  @override
  void update(void Function(ZScannerUploadBatchMediaBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerUploadBatchMedia build() => _build();

  _$ZScannerUploadBatchMedia _build() {
    _$ZScannerUploadBatchMedia _$result;
    try {
      _$result = _$v ??
          new _$ZScannerUploadBatchMedia._(
              mediaId: BuiltValueNullFieldError.checkNotNull(
                  mediaId, r'ZScannerUploadBatchMedia', 'mediaId'),
              contentType: BuiltValueNullFieldError.checkNotNull(
                  contentType, r'ZScannerUploadBatchMedia', 'contentType'),
              contentLength: BuiltValueNullFieldError.checkNotNull(
                  contentLength, r'ZScannerUploadBatchMedia', 'contentLength'),
              photoTs: photoTs,
              metadata: _metadata?.build(),
              subfolderTypeId: subfolderTypeId,
              subfolderId: subfolderId,
              relations: _relations?.build(),
              moduleData: _moduleData?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metadata';
        _metadata?.build();

        _$failedField = 'relations';
        _relations?.build();
        _$failedField = 'moduleData';
        _moduleData?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerUploadBatchMedia', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
