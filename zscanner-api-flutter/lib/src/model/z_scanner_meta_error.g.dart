// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_meta_error.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerMetaError extends ZScannerMetaError {
  @override
  final String? metaId;
  @override
  final String? exception;
  @override
  final String? error;

  factory _$ZScannerMetaError(
          [void Function(ZScannerMetaErrorBuilder)? updates]) =>
      (new ZScannerMetaErrorBuilder()..update(updates))._build();

  _$ZScannerMetaError._({this.metaId, this.exception, this.error}) : super._();

  @override
  ZScannerMetaError rebuild(void Function(ZScannerMetaErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerMetaErrorBuilder toBuilder() =>
      new ZScannerMetaErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerMetaError &&
        metaId == other.metaId &&
        exception == other.exception &&
        error == other.error;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, metaId.hashCode);
    _$hash = $jc(_$hash, exception.hashCode);
    _$hash = $jc(_$hash, error.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerMetaError')
          ..add('metaId', metaId)
          ..add('exception', exception)
          ..add('error', error))
        .toString();
  }
}

class ZScannerMetaErrorBuilder
    implements Builder<ZScannerMetaError, ZScannerMetaErrorBuilder> {
  _$ZScannerMetaError? _$v;

  String? _metaId;
  String? get metaId => _$this._metaId;
  set metaId(String? metaId) => _$this._metaId = metaId;

  String? _exception;
  String? get exception => _$this._exception;
  set exception(String? exception) => _$this._exception = exception;

  String? _error;
  String? get error => _$this._error;
  set error(String? error) => _$this._error = error;

  ZScannerMetaErrorBuilder() {
    ZScannerMetaError._defaults(this);
  }

  ZScannerMetaErrorBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _metaId = $v.metaId;
      _exception = $v.exception;
      _error = $v.error;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerMetaError other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerMetaError;
  }

  @override
  void update(void Function(ZScannerMetaErrorBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerMetaError build() => _build();

  _$ZScannerMetaError _build() {
    final _$result = _$v ??
        new _$ZScannerMetaError._(
            metaId: metaId, exception: exception, error: error);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
