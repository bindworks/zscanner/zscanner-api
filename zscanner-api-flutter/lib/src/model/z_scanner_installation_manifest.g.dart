// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_installation_manifest.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ZScannerInstallationManifestEnvironmentEnum
    _$zScannerInstallationManifestEnvironmentEnum_development =
    const ZScannerInstallationManifestEnvironmentEnum._('development');
const ZScannerInstallationManifestEnvironmentEnum
    _$zScannerInstallationManifestEnvironmentEnum_test =
    const ZScannerInstallationManifestEnvironmentEnum._('test');
const ZScannerInstallationManifestEnvironmentEnum
    _$zScannerInstallationManifestEnvironmentEnum_staging =
    const ZScannerInstallationManifestEnvironmentEnum._('staging');
const ZScannerInstallationManifestEnvironmentEnum
    _$zScannerInstallationManifestEnvironmentEnum_production =
    const ZScannerInstallationManifestEnvironmentEnum._('production');
const ZScannerInstallationManifestEnvironmentEnum
    _$zScannerInstallationManifestEnvironmentEnum_$unknown =
    const ZScannerInstallationManifestEnvironmentEnum._('unknown');

ZScannerInstallationManifestEnvironmentEnum
    _$zScannerInstallationManifestEnvironmentEnumValueOf(String name) {
  switch (name) {
    case 'development':
      return _$zScannerInstallationManifestEnvironmentEnum_development;
    case 'test':
      return _$zScannerInstallationManifestEnvironmentEnum_test;
    case 'staging':
      return _$zScannerInstallationManifestEnvironmentEnum_staging;
    case 'production':
      return _$zScannerInstallationManifestEnvironmentEnum_production;
    case 'unknown':
      return _$zScannerInstallationManifestEnvironmentEnum_$unknown;
    default:
      return _$zScannerInstallationManifestEnvironmentEnum_$unknown;
  }
}

final BuiltSet<ZScannerInstallationManifestEnvironmentEnum>
    _$zScannerInstallationManifestEnvironmentEnumValues = new BuiltSet<
        ZScannerInstallationManifestEnvironmentEnum>(const <ZScannerInstallationManifestEnvironmentEnum>[
  _$zScannerInstallationManifestEnvironmentEnum_development,
  _$zScannerInstallationManifestEnvironmentEnum_test,
  _$zScannerInstallationManifestEnvironmentEnum_staging,
  _$zScannerInstallationManifestEnvironmentEnum_production,
  _$zScannerInstallationManifestEnvironmentEnum_$unknown,
]);

Serializer<ZScannerInstallationManifestEnvironmentEnum>
    _$zScannerInstallationManifestEnvironmentEnumSerializer =
    new _$ZScannerInstallationManifestEnvironmentEnumSerializer();

class _$ZScannerInstallationManifestEnvironmentEnumSerializer
    implements
        PrimitiveSerializer<ZScannerInstallationManifestEnvironmentEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'development': 'development',
    'test': 'test',
    'staging': 'staging',
    'production': 'production',
    'unknown': '____unknown____',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'development': 'development',
    'test': 'test',
    'staging': 'staging',
    'production': 'production',
    '____unknown____': 'unknown',
  };

  @override
  final Iterable<Type> types = const <Type>[
    ZScannerInstallationManifestEnvironmentEnum
  ];
  @override
  final String wireName = 'ZScannerInstallationManifestEnvironmentEnum';

  @override
  Object serialize(Serializers serializers,
          ZScannerInstallationManifestEnvironmentEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ZScannerInstallationManifestEnvironmentEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ZScannerInstallationManifestEnvironmentEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$ZScannerInstallationManifest extends ZScannerInstallationManifest {
  @override
  final String installationId;
  @override
  final String name;
  @override
  final String? iconId;
  @override
  final ZScannerInstallationManifestEnvironmentEnum environment;
  @override
  final ZScannerAuthenticationManifest authentication;
  @override
  final ZScannerServerManifest server;
  @override
  final ZScannerClientManifest client;
  @override
  final ZScannerApplicationManifest? application;
  @override
  final ZScannerThemeManifest? theme;

  factory _$ZScannerInstallationManifest(
          [void Function(ZScannerInstallationManifestBuilder)? updates]) =>
      (new ZScannerInstallationManifestBuilder()..update(updates))._build();

  _$ZScannerInstallationManifest._(
      {required this.installationId,
      required this.name,
      this.iconId,
      required this.environment,
      required this.authentication,
      required this.server,
      required this.client,
      this.application,
      this.theme})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        installationId, r'ZScannerInstallationManifest', 'installationId');
    BuiltValueNullFieldError.checkNotNull(
        name, r'ZScannerInstallationManifest', 'name');
    BuiltValueNullFieldError.checkNotNull(
        environment, r'ZScannerInstallationManifest', 'environment');
    BuiltValueNullFieldError.checkNotNull(
        authentication, r'ZScannerInstallationManifest', 'authentication');
    BuiltValueNullFieldError.checkNotNull(
        server, r'ZScannerInstallationManifest', 'server');
    BuiltValueNullFieldError.checkNotNull(
        client, r'ZScannerInstallationManifest', 'client');
  }

  @override
  ZScannerInstallationManifest rebuild(
          void Function(ZScannerInstallationManifestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerInstallationManifestBuilder toBuilder() =>
      new ZScannerInstallationManifestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerInstallationManifest &&
        installationId == other.installationId &&
        name == other.name &&
        iconId == other.iconId &&
        environment == other.environment &&
        authentication == other.authentication &&
        server == other.server &&
        client == other.client &&
        application == other.application &&
        theme == other.theme;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, installationId.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, iconId.hashCode);
    _$hash = $jc(_$hash, environment.hashCode);
    _$hash = $jc(_$hash, authentication.hashCode);
    _$hash = $jc(_$hash, server.hashCode);
    _$hash = $jc(_$hash, client.hashCode);
    _$hash = $jc(_$hash, application.hashCode);
    _$hash = $jc(_$hash, theme.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerInstallationManifest')
          ..add('installationId', installationId)
          ..add('name', name)
          ..add('iconId', iconId)
          ..add('environment', environment)
          ..add('authentication', authentication)
          ..add('server', server)
          ..add('client', client)
          ..add('application', application)
          ..add('theme', theme))
        .toString();
  }
}

class ZScannerInstallationManifestBuilder
    implements
        Builder<ZScannerInstallationManifest,
            ZScannerInstallationManifestBuilder> {
  _$ZScannerInstallationManifest? _$v;

  String? _installationId;
  String? get installationId => _$this._installationId;
  set installationId(String? installationId) =>
      _$this._installationId = installationId;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _iconId;
  String? get iconId => _$this._iconId;
  set iconId(String? iconId) => _$this._iconId = iconId;

  ZScannerInstallationManifestEnvironmentEnum? _environment;
  ZScannerInstallationManifestEnvironmentEnum? get environment =>
      _$this._environment;
  set environment(ZScannerInstallationManifestEnvironmentEnum? environment) =>
      _$this._environment = environment;

  ZScannerAuthenticationManifestBuilder? _authentication;
  ZScannerAuthenticationManifestBuilder get authentication =>
      _$this._authentication ??= new ZScannerAuthenticationManifestBuilder();
  set authentication(ZScannerAuthenticationManifestBuilder? authentication) =>
      _$this._authentication = authentication;

  ZScannerServerManifestBuilder? _server;
  ZScannerServerManifestBuilder get server =>
      _$this._server ??= new ZScannerServerManifestBuilder();
  set server(ZScannerServerManifestBuilder? server) => _$this._server = server;

  ZScannerClientManifestBuilder? _client;
  ZScannerClientManifestBuilder get client =>
      _$this._client ??= new ZScannerClientManifestBuilder();
  set client(ZScannerClientManifestBuilder? client) => _$this._client = client;

  ZScannerApplicationManifestBuilder? _application;
  ZScannerApplicationManifestBuilder get application =>
      _$this._application ??= new ZScannerApplicationManifestBuilder();
  set application(ZScannerApplicationManifestBuilder? application) =>
      _$this._application = application;

  ZScannerThemeManifestBuilder? _theme;
  ZScannerThemeManifestBuilder get theme =>
      _$this._theme ??= new ZScannerThemeManifestBuilder();
  set theme(ZScannerThemeManifestBuilder? theme) => _$this._theme = theme;

  ZScannerInstallationManifestBuilder() {
    ZScannerInstallationManifest._defaults(this);
  }

  ZScannerInstallationManifestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _installationId = $v.installationId;
      _name = $v.name;
      _iconId = $v.iconId;
      _environment = $v.environment;
      _authentication = $v.authentication.toBuilder();
      _server = $v.server.toBuilder();
      _client = $v.client.toBuilder();
      _application = $v.application?.toBuilder();
      _theme = $v.theme?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerInstallationManifest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerInstallationManifest;
  }

  @override
  void update(void Function(ZScannerInstallationManifestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerInstallationManifest build() => _build();

  _$ZScannerInstallationManifest _build() {
    _$ZScannerInstallationManifest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerInstallationManifest._(
              installationId: BuiltValueNullFieldError.checkNotNull(
                  installationId,
                  r'ZScannerInstallationManifest',
                  'installationId'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'ZScannerInstallationManifest', 'name'),
              iconId: iconId,
              environment: BuiltValueNullFieldError.checkNotNull(
                  environment, r'ZScannerInstallationManifest', 'environment'),
              authentication: authentication.build(),
              server: server.build(),
              client: client.build(),
              application: _application?.build(),
              theme: _theme?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'authentication';
        authentication.build();
        _$failedField = 'server';
        server.build();
        _$failedField = 'client';
        client.build();
        _$failedField = 'application';
        _application?.build();
        _$failedField = 'theme';
        _theme?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerInstallationManifest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
