// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_open_id_authentication_manifest.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerOpenIDAuthenticationManifest
    extends ZScannerOpenIDAuthenticationManifest {
  @override
  final String manifestUrl;
  @override
  final String clientId;
  @override
  final String scopes;

  factory _$ZScannerOpenIDAuthenticationManifest(
          [void Function(ZScannerOpenIDAuthenticationManifestBuilder)?
              updates]) =>
      (new ZScannerOpenIDAuthenticationManifestBuilder()..update(updates))
          ._build();

  _$ZScannerOpenIDAuthenticationManifest._(
      {required this.manifestUrl, required this.clientId, required this.scopes})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        manifestUrl, r'ZScannerOpenIDAuthenticationManifest', 'manifestUrl');
    BuiltValueNullFieldError.checkNotNull(
        clientId, r'ZScannerOpenIDAuthenticationManifest', 'clientId');
    BuiltValueNullFieldError.checkNotNull(
        scopes, r'ZScannerOpenIDAuthenticationManifest', 'scopes');
  }

  @override
  ZScannerOpenIDAuthenticationManifest rebuild(
          void Function(ZScannerOpenIDAuthenticationManifestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerOpenIDAuthenticationManifestBuilder toBuilder() =>
      new ZScannerOpenIDAuthenticationManifestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerOpenIDAuthenticationManifest &&
        manifestUrl == other.manifestUrl &&
        clientId == other.clientId &&
        scopes == other.scopes;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, manifestUrl.hashCode);
    _$hash = $jc(_$hash, clientId.hashCode);
    _$hash = $jc(_$hash, scopes.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerOpenIDAuthenticationManifest')
          ..add('manifestUrl', manifestUrl)
          ..add('clientId', clientId)
          ..add('scopes', scopes))
        .toString();
  }
}

class ZScannerOpenIDAuthenticationManifestBuilder
    implements
        Builder<ZScannerOpenIDAuthenticationManifest,
            ZScannerOpenIDAuthenticationManifestBuilder> {
  _$ZScannerOpenIDAuthenticationManifest? _$v;

  String? _manifestUrl;
  String? get manifestUrl => _$this._manifestUrl;
  set manifestUrl(String? manifestUrl) => _$this._manifestUrl = manifestUrl;

  String? _clientId;
  String? get clientId => _$this._clientId;
  set clientId(String? clientId) => _$this._clientId = clientId;

  String? _scopes;
  String? get scopes => _$this._scopes;
  set scopes(String? scopes) => _$this._scopes = scopes;

  ZScannerOpenIDAuthenticationManifestBuilder() {
    ZScannerOpenIDAuthenticationManifest._defaults(this);
  }

  ZScannerOpenIDAuthenticationManifestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _manifestUrl = $v.manifestUrl;
      _clientId = $v.clientId;
      _scopes = $v.scopes;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerOpenIDAuthenticationManifest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerOpenIDAuthenticationManifest;
  }

  @override
  void update(
      void Function(ZScannerOpenIDAuthenticationManifestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerOpenIDAuthenticationManifest build() => _build();

  _$ZScannerOpenIDAuthenticationManifest _build() {
    final _$result = _$v ??
        new _$ZScannerOpenIDAuthenticationManifest._(
            manifestUrl: BuiltValueNullFieldError.checkNotNull(manifestUrl,
                r'ZScannerOpenIDAuthenticationManifest', 'manifestUrl'),
            clientId: BuiltValueNullFieldError.checkNotNull(
                clientId, r'ZScannerOpenIDAuthenticationManifest', 'clientId'),
            scopes: BuiltValueNullFieldError.checkNotNull(
                scopes, r'ZScannerOpenIDAuthenticationManifest', 'scopes'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
