// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_user.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerUser extends ZScannerUser {
  @override
  final String id;
  @override
  final String? login;
  @override
  final String displayName;
  @override
  final String? emailAddress;

  factory _$ZScannerUser([void Function(ZScannerUserBuilder)? updates]) =>
      (new ZScannerUserBuilder()..update(updates))._build();

  _$ZScannerUser._(
      {required this.id,
      this.login,
      required this.displayName,
      this.emailAddress})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'ZScannerUser', 'id');
    BuiltValueNullFieldError.checkNotNull(
        displayName, r'ZScannerUser', 'displayName');
  }

  @override
  ZScannerUser rebuild(void Function(ZScannerUserBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerUserBuilder toBuilder() => new ZScannerUserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerUser &&
        id == other.id &&
        login == other.login &&
        displayName == other.displayName &&
        emailAddress == other.emailAddress;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, login.hashCode);
    _$hash = $jc(_$hash, displayName.hashCode);
    _$hash = $jc(_$hash, emailAddress.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerUser')
          ..add('id', id)
          ..add('login', login)
          ..add('displayName', displayName)
          ..add('emailAddress', emailAddress))
        .toString();
  }
}

class ZScannerUserBuilder
    implements Builder<ZScannerUser, ZScannerUserBuilder> {
  _$ZScannerUser? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _login;
  String? get login => _$this._login;
  set login(String? login) => _$this._login = login;

  String? _displayName;
  String? get displayName => _$this._displayName;
  set displayName(String? displayName) => _$this._displayName = displayName;

  String? _emailAddress;
  String? get emailAddress => _$this._emailAddress;
  set emailAddress(String? emailAddress) => _$this._emailAddress = emailAddress;

  ZScannerUserBuilder() {
    ZScannerUser._defaults(this);
  }

  ZScannerUserBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _login = $v.login;
      _displayName = $v.displayName;
      _emailAddress = $v.emailAddress;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerUser other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerUser;
  }

  @override
  void update(void Function(ZScannerUserBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerUser build() => _build();

  _$ZScannerUser _build() {
    final _$result = _$v ??
        new _$ZScannerUser._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'ZScannerUser', 'id'),
            login: login,
            displayName: BuiltValueNullFieldError.checkNotNull(
                displayName, r'ZScannerUser', 'displayName'),
            emailAddress: emailAddress);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
