//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_open_id_authentication_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_sea_cat3_authentication_manifest.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_authentication_manifest.g.dart';

/// ZScannerAuthenticationManifest
///
/// Properties:
/// * [type] 
/// * [openid] 
/// * [seacat3] 
@BuiltValue()
abstract class ZScannerAuthenticationManifest implements Built<ZScannerAuthenticationManifest, ZScannerAuthenticationManifestBuilder> {
  @BuiltValueField(wireName: r'type')
  ZScannerAuthenticationManifestTypeEnum get type;
  // enum typeEnum {  openid,  seacat3,  };

  @BuiltValueField(wireName: r'openid')
  ZScannerOpenIDAuthenticationManifest? get openid;

  @BuiltValueField(wireName: r'seacat3')
  ZScannerSeaCat3AuthenticationManifest? get seacat3;

  ZScannerAuthenticationManifest._();

  factory ZScannerAuthenticationManifest([void updates(ZScannerAuthenticationManifestBuilder b)]) = _$ZScannerAuthenticationManifest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerAuthenticationManifestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerAuthenticationManifest> get serializer => _$ZScannerAuthenticationManifestSerializer();
}

class _$ZScannerAuthenticationManifestSerializer implements PrimitiveSerializer<ZScannerAuthenticationManifest> {
  @override
  final Iterable<Type> types = const [ZScannerAuthenticationManifest, _$ZScannerAuthenticationManifest];

  @override
  final String wireName = r'ZScannerAuthenticationManifest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerAuthenticationManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(ZScannerAuthenticationManifestTypeEnum),
    );
    if (object.openid != null) {
      yield r'openid';
      yield serializers.serialize(
        object.openid,
        specifiedType: const FullType(ZScannerOpenIDAuthenticationManifest),
      );
    }
    if (object.seacat3 != null) {
      yield r'seacat3';
      yield serializers.serialize(
        object.seacat3,
        specifiedType: const FullType(ZScannerSeaCat3AuthenticationManifest),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerAuthenticationManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerAuthenticationManifestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerAuthenticationManifestTypeEnum),
          ) as ZScannerAuthenticationManifestTypeEnum;
          result.type = valueDes;
          break;
        case r'openid':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerOpenIDAuthenticationManifest),
          ) as ZScannerOpenIDAuthenticationManifest;
          result.openid.replace(valueDes);
          break;
        case r'seacat3':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerSeaCat3AuthenticationManifest),
          ) as ZScannerSeaCat3AuthenticationManifest;
          result.seacat3.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerAuthenticationManifest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerAuthenticationManifestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class ZScannerAuthenticationManifestTypeEnum extends EnumClass {

  @BuiltValueEnumConst(wireName: r'openid')
  static const ZScannerAuthenticationManifestTypeEnum openid = _$zScannerAuthenticationManifestTypeEnum_openid;
  @BuiltValueEnumConst(wireName: r'seacat3')
  static const ZScannerAuthenticationManifestTypeEnum seacat3 = _$zScannerAuthenticationManifestTypeEnum_seacat3;

  @BuiltValueEnumConst(wireName: r'____unknown____', fallback: true)
  static const ZScannerAuthenticationManifestTypeEnum unknown = _$zScannerAuthenticationManifestTypeEnum_$unknown;

  static Serializer<ZScannerAuthenticationManifestTypeEnum> get serializer => _$zScannerAuthenticationManifestTypeEnumSerializer;

  const ZScannerAuthenticationManifestTypeEnum._(String name): super(name);

  static BuiltSet<ZScannerAuthenticationManifestTypeEnum> get values => _$zScannerAuthenticationManifestTypeEnumValues;
  static ZScannerAuthenticationManifestTypeEnum valueOf(String name) => _$zScannerAuthenticationManifestTypeEnumValueOf(name);
}


