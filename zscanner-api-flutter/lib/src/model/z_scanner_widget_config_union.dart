//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_widget_config_union.g.dart';

/// ZScannerWidgetConfigUnion
///
/// Properties:
/// * [minLines] 
/// * [maxLines] 
/// * [searchable] 
/// * [allowOther] 
/// * [maxDecimalPlaces] 
@BuiltValue()
abstract class ZScannerWidgetConfigUnion implements Built<ZScannerWidgetConfigUnion, ZScannerWidgetConfigUnionBuilder> {
  @BuiltValueField(wireName: r'minLines')
  int? get minLines;

  @BuiltValueField(wireName: r'maxLines')
  int? get maxLines;

  @BuiltValueField(wireName: r'searchable')
  bool? get searchable;

  @BuiltValueField(wireName: r'allowOther')
  bool? get allowOther;

  @BuiltValueField(wireName: r'maxDecimalPlaces')
  int? get maxDecimalPlaces;

  ZScannerWidgetConfigUnion._();

  factory ZScannerWidgetConfigUnion([void updates(ZScannerWidgetConfigUnionBuilder b)]) = _$ZScannerWidgetConfigUnion;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerWidgetConfigUnionBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerWidgetConfigUnion> get serializer => _$ZScannerWidgetConfigUnionSerializer();
}

class _$ZScannerWidgetConfigUnionSerializer implements PrimitiveSerializer<ZScannerWidgetConfigUnion> {
  @override
  final Iterable<Type> types = const [ZScannerWidgetConfigUnion, _$ZScannerWidgetConfigUnion];

  @override
  final String wireName = r'ZScannerWidgetConfigUnion';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerWidgetConfigUnion object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.minLines != null) {
      yield r'minLines';
      yield serializers.serialize(
        object.minLines,
        specifiedType: const FullType(int),
      );
    }
    if (object.maxLines != null) {
      yield r'maxLines';
      yield serializers.serialize(
        object.maxLines,
        specifiedType: const FullType(int),
      );
    }
    if (object.searchable != null) {
      yield r'searchable';
      yield serializers.serialize(
        object.searchable,
        specifiedType: const FullType(bool),
      );
    }
    if (object.allowOther != null) {
      yield r'allowOther';
      yield serializers.serialize(
        object.allowOther,
        specifiedType: const FullType(bool),
      );
    }
    if (object.maxDecimalPlaces != null) {
      yield r'maxDecimalPlaces';
      yield serializers.serialize(
        object.maxDecimalPlaces,
        specifiedType: const FullType(int),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerWidgetConfigUnion object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerWidgetConfigUnionBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'minLines':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.minLines = valueDes;
          break;
        case r'maxLines':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.maxLines = valueDes;
          break;
        case r'searchable':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.searchable = valueDes;
          break;
        case r'allowOther':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.allowOther = valueDes;
          break;
        case r'maxDecimalPlaces':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.maxDecimalPlaces = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerWidgetConfigUnion deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerWidgetConfigUnionBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

