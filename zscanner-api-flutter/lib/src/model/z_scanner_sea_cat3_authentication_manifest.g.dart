// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_sea_cat3_authentication_manifest.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerSeaCat3AuthenticationManifest
    extends ZScannerSeaCat3AuthenticationManifest {
  @override
  final String authUrl;
  @override
  final String apiUrl;
  @override
  final String seacatUrl;

  factory _$ZScannerSeaCat3AuthenticationManifest(
          [void Function(ZScannerSeaCat3AuthenticationManifestBuilder)?
              updates]) =>
      (new ZScannerSeaCat3AuthenticationManifestBuilder()..update(updates))
          ._build();

  _$ZScannerSeaCat3AuthenticationManifest._(
      {required this.authUrl, required this.apiUrl, required this.seacatUrl})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        authUrl, r'ZScannerSeaCat3AuthenticationManifest', 'authUrl');
    BuiltValueNullFieldError.checkNotNull(
        apiUrl, r'ZScannerSeaCat3AuthenticationManifest', 'apiUrl');
    BuiltValueNullFieldError.checkNotNull(
        seacatUrl, r'ZScannerSeaCat3AuthenticationManifest', 'seacatUrl');
  }

  @override
  ZScannerSeaCat3AuthenticationManifest rebuild(
          void Function(ZScannerSeaCat3AuthenticationManifestBuilder)
              updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerSeaCat3AuthenticationManifestBuilder toBuilder() =>
      new ZScannerSeaCat3AuthenticationManifestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerSeaCat3AuthenticationManifest &&
        authUrl == other.authUrl &&
        apiUrl == other.apiUrl &&
        seacatUrl == other.seacatUrl;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, authUrl.hashCode);
    _$hash = $jc(_$hash, apiUrl.hashCode);
    _$hash = $jc(_$hash, seacatUrl.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(
            r'ZScannerSeaCat3AuthenticationManifest')
          ..add('authUrl', authUrl)
          ..add('apiUrl', apiUrl)
          ..add('seacatUrl', seacatUrl))
        .toString();
  }
}

class ZScannerSeaCat3AuthenticationManifestBuilder
    implements
        Builder<ZScannerSeaCat3AuthenticationManifest,
            ZScannerSeaCat3AuthenticationManifestBuilder> {
  _$ZScannerSeaCat3AuthenticationManifest? _$v;

  String? _authUrl;
  String? get authUrl => _$this._authUrl;
  set authUrl(String? authUrl) => _$this._authUrl = authUrl;

  String? _apiUrl;
  String? get apiUrl => _$this._apiUrl;
  set apiUrl(String? apiUrl) => _$this._apiUrl = apiUrl;

  String? _seacatUrl;
  String? get seacatUrl => _$this._seacatUrl;
  set seacatUrl(String? seacatUrl) => _$this._seacatUrl = seacatUrl;

  ZScannerSeaCat3AuthenticationManifestBuilder() {
    ZScannerSeaCat3AuthenticationManifest._defaults(this);
  }

  ZScannerSeaCat3AuthenticationManifestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _authUrl = $v.authUrl;
      _apiUrl = $v.apiUrl;
      _seacatUrl = $v.seacatUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerSeaCat3AuthenticationManifest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerSeaCat3AuthenticationManifest;
  }

  @override
  void update(
      void Function(ZScannerSeaCat3AuthenticationManifestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerSeaCat3AuthenticationManifest build() => _build();

  _$ZScannerSeaCat3AuthenticationManifest _build() {
    final _$result = _$v ??
        new _$ZScannerSeaCat3AuthenticationManifest._(
            authUrl: BuiltValueNullFieldError.checkNotNull(
                authUrl, r'ZScannerSeaCat3AuthenticationManifest', 'authUrl'),
            apiUrl: BuiltValueNullFieldError.checkNotNull(
                apiUrl, r'ZScannerSeaCat3AuthenticationManifest', 'apiUrl'),
            seacatUrl: BuiltValueNullFieldError.checkNotNull(seacatUrl,
                r'ZScannerSeaCat3AuthenticationManifest', 'seacatUrl'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
