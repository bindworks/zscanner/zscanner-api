// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_subfolder_schema.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerSubfolderSchema extends ZScannerSubfolderSchema {
  @override
  final String typeId;
  @override
  final String label;
  @override
  final String? moduleId;
  @override
  final String? moduleVersion;
  @override
  final ZScannerSubfolderConfigUnion? config;
  @override
  final BuiltList<ZScannerMetadataSchemaItem> displaySchema;
  @override
  final BuiltList<ZScannerMetadataSchemaItem> createSchema;
  @override
  final BuiltList<ZScannerMetadataSchemaItem> updateSchema;

  factory _$ZScannerSubfolderSchema(
          [void Function(ZScannerSubfolderSchemaBuilder)? updates]) =>
      (new ZScannerSubfolderSchemaBuilder()..update(updates))._build();

  _$ZScannerSubfolderSchema._(
      {required this.typeId,
      required this.label,
      this.moduleId,
      this.moduleVersion,
      this.config,
      required this.displaySchema,
      required this.createSchema,
      required this.updateSchema})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        typeId, r'ZScannerSubfolderSchema', 'typeId');
    BuiltValueNullFieldError.checkNotNull(
        label, r'ZScannerSubfolderSchema', 'label');
    BuiltValueNullFieldError.checkNotNull(
        displaySchema, r'ZScannerSubfolderSchema', 'displaySchema');
    BuiltValueNullFieldError.checkNotNull(
        createSchema, r'ZScannerSubfolderSchema', 'createSchema');
    BuiltValueNullFieldError.checkNotNull(
        updateSchema, r'ZScannerSubfolderSchema', 'updateSchema');
  }

  @override
  ZScannerSubfolderSchema rebuild(
          void Function(ZScannerSubfolderSchemaBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerSubfolderSchemaBuilder toBuilder() =>
      new ZScannerSubfolderSchemaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerSubfolderSchema &&
        typeId == other.typeId &&
        label == other.label &&
        moduleId == other.moduleId &&
        moduleVersion == other.moduleVersion &&
        config == other.config &&
        displaySchema == other.displaySchema &&
        createSchema == other.createSchema &&
        updateSchema == other.updateSchema;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, typeId.hashCode);
    _$hash = $jc(_$hash, label.hashCode);
    _$hash = $jc(_$hash, moduleId.hashCode);
    _$hash = $jc(_$hash, moduleVersion.hashCode);
    _$hash = $jc(_$hash, config.hashCode);
    _$hash = $jc(_$hash, displaySchema.hashCode);
    _$hash = $jc(_$hash, createSchema.hashCode);
    _$hash = $jc(_$hash, updateSchema.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerSubfolderSchema')
          ..add('typeId', typeId)
          ..add('label', label)
          ..add('moduleId', moduleId)
          ..add('moduleVersion', moduleVersion)
          ..add('config', config)
          ..add('displaySchema', displaySchema)
          ..add('createSchema', createSchema)
          ..add('updateSchema', updateSchema))
        .toString();
  }
}

class ZScannerSubfolderSchemaBuilder
    implements
        Builder<ZScannerSubfolderSchema, ZScannerSubfolderSchemaBuilder> {
  _$ZScannerSubfolderSchema? _$v;

  String? _typeId;
  String? get typeId => _$this._typeId;
  set typeId(String? typeId) => _$this._typeId = typeId;

  String? _label;
  String? get label => _$this._label;
  set label(String? label) => _$this._label = label;

  String? _moduleId;
  String? get moduleId => _$this._moduleId;
  set moduleId(String? moduleId) => _$this._moduleId = moduleId;

  String? _moduleVersion;
  String? get moduleVersion => _$this._moduleVersion;
  set moduleVersion(String? moduleVersion) =>
      _$this._moduleVersion = moduleVersion;

  ZScannerSubfolderConfigUnionBuilder? _config;
  ZScannerSubfolderConfigUnionBuilder get config =>
      _$this._config ??= new ZScannerSubfolderConfigUnionBuilder();
  set config(ZScannerSubfolderConfigUnionBuilder? config) =>
      _$this._config = config;

  ListBuilder<ZScannerMetadataSchemaItem>? _displaySchema;
  ListBuilder<ZScannerMetadataSchemaItem> get displaySchema =>
      _$this._displaySchema ??= new ListBuilder<ZScannerMetadataSchemaItem>();
  set displaySchema(ListBuilder<ZScannerMetadataSchemaItem>? displaySchema) =>
      _$this._displaySchema = displaySchema;

  ListBuilder<ZScannerMetadataSchemaItem>? _createSchema;
  ListBuilder<ZScannerMetadataSchemaItem> get createSchema =>
      _$this._createSchema ??= new ListBuilder<ZScannerMetadataSchemaItem>();
  set createSchema(ListBuilder<ZScannerMetadataSchemaItem>? createSchema) =>
      _$this._createSchema = createSchema;

  ListBuilder<ZScannerMetadataSchemaItem>? _updateSchema;
  ListBuilder<ZScannerMetadataSchemaItem> get updateSchema =>
      _$this._updateSchema ??= new ListBuilder<ZScannerMetadataSchemaItem>();
  set updateSchema(ListBuilder<ZScannerMetadataSchemaItem>? updateSchema) =>
      _$this._updateSchema = updateSchema;

  ZScannerSubfolderSchemaBuilder() {
    ZScannerSubfolderSchema._defaults(this);
  }

  ZScannerSubfolderSchemaBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _typeId = $v.typeId;
      _label = $v.label;
      _moduleId = $v.moduleId;
      _moduleVersion = $v.moduleVersion;
      _config = $v.config?.toBuilder();
      _displaySchema = $v.displaySchema.toBuilder();
      _createSchema = $v.createSchema.toBuilder();
      _updateSchema = $v.updateSchema.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerSubfolderSchema other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerSubfolderSchema;
  }

  @override
  void update(void Function(ZScannerSubfolderSchemaBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerSubfolderSchema build() => _build();

  _$ZScannerSubfolderSchema _build() {
    _$ZScannerSubfolderSchema _$result;
    try {
      _$result = _$v ??
          new _$ZScannerSubfolderSchema._(
              typeId: BuiltValueNullFieldError.checkNotNull(
                  typeId, r'ZScannerSubfolderSchema', 'typeId'),
              label: BuiltValueNullFieldError.checkNotNull(
                  label, r'ZScannerSubfolderSchema', 'label'),
              moduleId: moduleId,
              moduleVersion: moduleVersion,
              config: _config?.build(),
              displaySchema: displaySchema.build(),
              createSchema: createSchema.build(),
              updateSchema: updateSchema.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'config';
        _config?.build();
        _$failedField = 'displaySchema';
        displaySchema.build();
        _$failedField = 'createSchema';
        createSchema.build();
        _$failedField = 'updateSchema';
        updateSchema.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerSubfolderSchema', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
