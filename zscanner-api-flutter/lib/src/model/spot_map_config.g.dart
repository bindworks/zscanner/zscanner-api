// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'spot_map_config.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SpotMapConfig extends SpotMapConfig {
  @override
  final DateTime createdTs;
  @override
  final DateTime updatedTs;
  @override
  final String spotMetaId;
  @override
  final BuiltList<SpotMapViewConfig> views;
  @override
  final BuiltList<SpotMapSpotConfig> spots;

  factory _$SpotMapConfig([void Function(SpotMapConfigBuilder)? updates]) =>
      (new SpotMapConfigBuilder()..update(updates))._build();

  _$SpotMapConfig._(
      {required this.createdTs,
      required this.updatedTs,
      required this.spotMetaId,
      required this.views,
      required this.spots})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        createdTs, r'SpotMapConfig', 'createdTs');
    BuiltValueNullFieldError.checkNotNull(
        updatedTs, r'SpotMapConfig', 'updatedTs');
    BuiltValueNullFieldError.checkNotNull(
        spotMetaId, r'SpotMapConfig', 'spotMetaId');
    BuiltValueNullFieldError.checkNotNull(views, r'SpotMapConfig', 'views');
    BuiltValueNullFieldError.checkNotNull(spots, r'SpotMapConfig', 'spots');
  }

  @override
  SpotMapConfig rebuild(void Function(SpotMapConfigBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SpotMapConfigBuilder toBuilder() => new SpotMapConfigBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SpotMapConfig &&
        createdTs == other.createdTs &&
        updatedTs == other.updatedTs &&
        spotMetaId == other.spotMetaId &&
        views == other.views &&
        spots == other.spots;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, createdTs.hashCode);
    _$hash = $jc(_$hash, updatedTs.hashCode);
    _$hash = $jc(_$hash, spotMetaId.hashCode);
    _$hash = $jc(_$hash, views.hashCode);
    _$hash = $jc(_$hash, spots.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SpotMapConfig')
          ..add('createdTs', createdTs)
          ..add('updatedTs', updatedTs)
          ..add('spotMetaId', spotMetaId)
          ..add('views', views)
          ..add('spots', spots))
        .toString();
  }
}

class SpotMapConfigBuilder
    implements Builder<SpotMapConfig, SpotMapConfigBuilder> {
  _$SpotMapConfig? _$v;

  DateTime? _createdTs;
  DateTime? get createdTs => _$this._createdTs;
  set createdTs(DateTime? createdTs) => _$this._createdTs = createdTs;

  DateTime? _updatedTs;
  DateTime? get updatedTs => _$this._updatedTs;
  set updatedTs(DateTime? updatedTs) => _$this._updatedTs = updatedTs;

  String? _spotMetaId;
  String? get spotMetaId => _$this._spotMetaId;
  set spotMetaId(String? spotMetaId) => _$this._spotMetaId = spotMetaId;

  ListBuilder<SpotMapViewConfig>? _views;
  ListBuilder<SpotMapViewConfig> get views =>
      _$this._views ??= new ListBuilder<SpotMapViewConfig>();
  set views(ListBuilder<SpotMapViewConfig>? views) => _$this._views = views;

  ListBuilder<SpotMapSpotConfig>? _spots;
  ListBuilder<SpotMapSpotConfig> get spots =>
      _$this._spots ??= new ListBuilder<SpotMapSpotConfig>();
  set spots(ListBuilder<SpotMapSpotConfig>? spots) => _$this._spots = spots;

  SpotMapConfigBuilder() {
    SpotMapConfig._defaults(this);
  }

  SpotMapConfigBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _createdTs = $v.createdTs;
      _updatedTs = $v.updatedTs;
      _spotMetaId = $v.spotMetaId;
      _views = $v.views.toBuilder();
      _spots = $v.spots.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SpotMapConfig other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SpotMapConfig;
  }

  @override
  void update(void Function(SpotMapConfigBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SpotMapConfig build() => _build();

  _$SpotMapConfig _build() {
    _$SpotMapConfig _$result;
    try {
      _$result = _$v ??
          new _$SpotMapConfig._(
              createdTs: BuiltValueNullFieldError.checkNotNull(
                  createdTs, r'SpotMapConfig', 'createdTs'),
              updatedTs: BuiltValueNullFieldError.checkNotNull(
                  updatedTs, r'SpotMapConfig', 'updatedTs'),
              spotMetaId: BuiltValueNullFieldError.checkNotNull(
                  spotMetaId, r'SpotMapConfig', 'spotMetaId'),
              views: views.build(),
              spots: spots.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'views';
        views.build();
        _$failedField = 'spots';
        spots.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'SpotMapConfig', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
