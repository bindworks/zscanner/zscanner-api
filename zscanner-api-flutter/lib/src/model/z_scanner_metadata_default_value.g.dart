// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_metadata_default_value.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerMetadataDefaultValue extends ZScannerMetadataDefaultValue {
  @override
  final String? fromMeta;
  @override
  final String? fromTemplate;
  @override
  final String? dateValue;
  @override
  final String? stringValue;
  @override
  final num? numberValue;
  @override
  final bool? booleanValue;
  @override
  final DateTime? dateTimeValue;
  @override
  final int? dateRelativeValue;
  @override
  final BuiltList<String>? multiStringValue;

  factory _$ZScannerMetadataDefaultValue(
          [void Function(ZScannerMetadataDefaultValueBuilder)? updates]) =>
      (new ZScannerMetadataDefaultValueBuilder()..update(updates))._build();

  _$ZScannerMetadataDefaultValue._(
      {this.fromMeta,
      this.fromTemplate,
      this.dateValue,
      this.stringValue,
      this.numberValue,
      this.booleanValue,
      this.dateTimeValue,
      this.dateRelativeValue,
      this.multiStringValue})
      : super._();

  @override
  ZScannerMetadataDefaultValue rebuild(
          void Function(ZScannerMetadataDefaultValueBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerMetadataDefaultValueBuilder toBuilder() =>
      new ZScannerMetadataDefaultValueBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerMetadataDefaultValue &&
        fromMeta == other.fromMeta &&
        fromTemplate == other.fromTemplate &&
        dateValue == other.dateValue &&
        stringValue == other.stringValue &&
        numberValue == other.numberValue &&
        booleanValue == other.booleanValue &&
        dateTimeValue == other.dateTimeValue &&
        dateRelativeValue == other.dateRelativeValue &&
        multiStringValue == other.multiStringValue;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, fromMeta.hashCode);
    _$hash = $jc(_$hash, fromTemplate.hashCode);
    _$hash = $jc(_$hash, dateValue.hashCode);
    _$hash = $jc(_$hash, stringValue.hashCode);
    _$hash = $jc(_$hash, numberValue.hashCode);
    _$hash = $jc(_$hash, booleanValue.hashCode);
    _$hash = $jc(_$hash, dateTimeValue.hashCode);
    _$hash = $jc(_$hash, dateRelativeValue.hashCode);
    _$hash = $jc(_$hash, multiStringValue.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerMetadataDefaultValue')
          ..add('fromMeta', fromMeta)
          ..add('fromTemplate', fromTemplate)
          ..add('dateValue', dateValue)
          ..add('stringValue', stringValue)
          ..add('numberValue', numberValue)
          ..add('booleanValue', booleanValue)
          ..add('dateTimeValue', dateTimeValue)
          ..add('dateRelativeValue', dateRelativeValue)
          ..add('multiStringValue', multiStringValue))
        .toString();
  }
}

class ZScannerMetadataDefaultValueBuilder
    implements
        Builder<ZScannerMetadataDefaultValue,
            ZScannerMetadataDefaultValueBuilder> {
  _$ZScannerMetadataDefaultValue? _$v;

  String? _fromMeta;
  String? get fromMeta => _$this._fromMeta;
  set fromMeta(String? fromMeta) => _$this._fromMeta = fromMeta;

  String? _fromTemplate;
  String? get fromTemplate => _$this._fromTemplate;
  set fromTemplate(String? fromTemplate) => _$this._fromTemplate = fromTemplate;

  String? _dateValue;
  String? get dateValue => _$this._dateValue;
  set dateValue(String? dateValue) => _$this._dateValue = dateValue;

  String? _stringValue;
  String? get stringValue => _$this._stringValue;
  set stringValue(String? stringValue) => _$this._stringValue = stringValue;

  num? _numberValue;
  num? get numberValue => _$this._numberValue;
  set numberValue(num? numberValue) => _$this._numberValue = numberValue;

  bool? _booleanValue;
  bool? get booleanValue => _$this._booleanValue;
  set booleanValue(bool? booleanValue) => _$this._booleanValue = booleanValue;

  DateTime? _dateTimeValue;
  DateTime? get dateTimeValue => _$this._dateTimeValue;
  set dateTimeValue(DateTime? dateTimeValue) =>
      _$this._dateTimeValue = dateTimeValue;

  int? _dateRelativeValue;
  int? get dateRelativeValue => _$this._dateRelativeValue;
  set dateRelativeValue(int? dateRelativeValue) =>
      _$this._dateRelativeValue = dateRelativeValue;

  ListBuilder<String>? _multiStringValue;
  ListBuilder<String> get multiStringValue =>
      _$this._multiStringValue ??= new ListBuilder<String>();
  set multiStringValue(ListBuilder<String>? multiStringValue) =>
      _$this._multiStringValue = multiStringValue;

  ZScannerMetadataDefaultValueBuilder() {
    ZScannerMetadataDefaultValue._defaults(this);
  }

  ZScannerMetadataDefaultValueBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _fromMeta = $v.fromMeta;
      _fromTemplate = $v.fromTemplate;
      _dateValue = $v.dateValue;
      _stringValue = $v.stringValue;
      _numberValue = $v.numberValue;
      _booleanValue = $v.booleanValue;
      _dateTimeValue = $v.dateTimeValue;
      _dateRelativeValue = $v.dateRelativeValue;
      _multiStringValue = $v.multiStringValue?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerMetadataDefaultValue other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerMetadataDefaultValue;
  }

  @override
  void update(void Function(ZScannerMetadataDefaultValueBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerMetadataDefaultValue build() => _build();

  _$ZScannerMetadataDefaultValue _build() {
    _$ZScannerMetadataDefaultValue _$result;
    try {
      _$result = _$v ??
          new _$ZScannerMetadataDefaultValue._(
              fromMeta: fromMeta,
              fromTemplate: fromTemplate,
              dateValue: dateValue,
              stringValue: stringValue,
              numberValue: numberValue,
              booleanValue: booleanValue,
              dateTimeValue: dateTimeValue,
              dateRelativeValue: dateRelativeValue,
              multiStringValue: _multiStringValue?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'multiStringValue';
        _multiStringValue?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerMetadataDefaultValue', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
