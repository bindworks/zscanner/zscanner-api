//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_metadata_item.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_synchronization_request.g.dart';

/// ZScannerSynchronizationRequest
///
/// Properties:
/// * [requiredCaps] - DEPRECATED, pass it in the X-Required-Caps header
/// * [supportedCaps] - DEPRECATED, pass it in the X-Supported-Caps header
/// * [metadata] 
/// * [state] 
@BuiltValue()
abstract class ZScannerSynchronizationRequest implements Built<ZScannerSynchronizationRequest, ZScannerSynchronizationRequestBuilder> {
  /// DEPRECATED, pass it in the X-Required-Caps header
  @BuiltValueField(wireName: r'requiredCaps')
  BuiltList<String>? get requiredCaps;

  /// DEPRECATED, pass it in the X-Supported-Caps header
  @BuiltValueField(wireName: r'supportedCaps')
  BuiltList<String>? get supportedCaps;

  @BuiltValueField(wireName: r'metadata')
  BuiltList<ZScannerMetadataItem> get metadata;

  @BuiltValueField(wireName: r'state')
  String? get state;

  ZScannerSynchronizationRequest._();

  factory ZScannerSynchronizationRequest([void updates(ZScannerSynchronizationRequestBuilder b)]) = _$ZScannerSynchronizationRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerSynchronizationRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerSynchronizationRequest> get serializer => _$ZScannerSynchronizationRequestSerializer();
}

class _$ZScannerSynchronizationRequestSerializer implements PrimitiveSerializer<ZScannerSynchronizationRequest> {
  @override
  final Iterable<Type> types = const [ZScannerSynchronizationRequest, _$ZScannerSynchronizationRequest];

  @override
  final String wireName = r'ZScannerSynchronizationRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerSynchronizationRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.requiredCaps != null) {
      yield r'requiredCaps';
      yield serializers.serialize(
        object.requiredCaps,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
    if (object.supportedCaps != null) {
      yield r'supportedCaps';
      yield serializers.serialize(
        object.supportedCaps,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
    yield r'metadata';
    yield serializers.serialize(
      object.metadata,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
    );
    if (object.state != null) {
      yield r'state';
      yield serializers.serialize(
        object.state,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerSynchronizationRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerSynchronizationRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'requiredCaps':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.requiredCaps.replace(valueDes);
          break;
        case r'supportedCaps':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.supportedCaps.replace(valueDes);
          break;
        case r'metadata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
          ) as BuiltList<ZScannerMetadataItem>;
          result.metadata.replace(valueDes);
          break;
        case r'state':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.state = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerSynchronizationRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerSynchronizationRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

