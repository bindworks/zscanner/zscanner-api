// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_authentication_manifest.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ZScannerAuthenticationManifestTypeEnum
    _$zScannerAuthenticationManifestTypeEnum_openid =
    const ZScannerAuthenticationManifestTypeEnum._('openid');
const ZScannerAuthenticationManifestTypeEnum
    _$zScannerAuthenticationManifestTypeEnum_seacat3 =
    const ZScannerAuthenticationManifestTypeEnum._('seacat3');
const ZScannerAuthenticationManifestTypeEnum
    _$zScannerAuthenticationManifestTypeEnum_$unknown =
    const ZScannerAuthenticationManifestTypeEnum._('unknown');

ZScannerAuthenticationManifestTypeEnum
    _$zScannerAuthenticationManifestTypeEnumValueOf(String name) {
  switch (name) {
    case 'openid':
      return _$zScannerAuthenticationManifestTypeEnum_openid;
    case 'seacat3':
      return _$zScannerAuthenticationManifestTypeEnum_seacat3;
    case 'unknown':
      return _$zScannerAuthenticationManifestTypeEnum_$unknown;
    default:
      return _$zScannerAuthenticationManifestTypeEnum_$unknown;
  }
}

final BuiltSet<ZScannerAuthenticationManifestTypeEnum>
    _$zScannerAuthenticationManifestTypeEnumValues = new BuiltSet<
        ZScannerAuthenticationManifestTypeEnum>(const <ZScannerAuthenticationManifestTypeEnum>[
  _$zScannerAuthenticationManifestTypeEnum_openid,
  _$zScannerAuthenticationManifestTypeEnum_seacat3,
  _$zScannerAuthenticationManifestTypeEnum_$unknown,
]);

Serializer<ZScannerAuthenticationManifestTypeEnum>
    _$zScannerAuthenticationManifestTypeEnumSerializer =
    new _$ZScannerAuthenticationManifestTypeEnumSerializer();

class _$ZScannerAuthenticationManifestTypeEnumSerializer
    implements PrimitiveSerializer<ZScannerAuthenticationManifestTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'openid': 'openid',
    'seacat3': 'seacat3',
    'unknown': '____unknown____',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'openid': 'openid',
    'seacat3': 'seacat3',
    '____unknown____': 'unknown',
  };

  @override
  final Iterable<Type> types = const <Type>[
    ZScannerAuthenticationManifestTypeEnum
  ];
  @override
  final String wireName = 'ZScannerAuthenticationManifestTypeEnum';

  @override
  Object serialize(Serializers serializers,
          ZScannerAuthenticationManifestTypeEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ZScannerAuthenticationManifestTypeEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ZScannerAuthenticationManifestTypeEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$ZScannerAuthenticationManifest extends ZScannerAuthenticationManifest {
  @override
  final ZScannerAuthenticationManifestTypeEnum type;
  @override
  final ZScannerOpenIDAuthenticationManifest? openid;
  @override
  final ZScannerSeaCat3AuthenticationManifest? seacat3;

  factory _$ZScannerAuthenticationManifest(
          [void Function(ZScannerAuthenticationManifestBuilder)? updates]) =>
      (new ZScannerAuthenticationManifestBuilder()..update(updates))._build();

  _$ZScannerAuthenticationManifest._(
      {required this.type, this.openid, this.seacat3})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        type, r'ZScannerAuthenticationManifest', 'type');
  }

  @override
  ZScannerAuthenticationManifest rebuild(
          void Function(ZScannerAuthenticationManifestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerAuthenticationManifestBuilder toBuilder() =>
      new ZScannerAuthenticationManifestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerAuthenticationManifest &&
        type == other.type &&
        openid == other.openid &&
        seacat3 == other.seacat3;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, openid.hashCode);
    _$hash = $jc(_$hash, seacat3.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerAuthenticationManifest')
          ..add('type', type)
          ..add('openid', openid)
          ..add('seacat3', seacat3))
        .toString();
  }
}

class ZScannerAuthenticationManifestBuilder
    implements
        Builder<ZScannerAuthenticationManifest,
            ZScannerAuthenticationManifestBuilder> {
  _$ZScannerAuthenticationManifest? _$v;

  ZScannerAuthenticationManifestTypeEnum? _type;
  ZScannerAuthenticationManifestTypeEnum? get type => _$this._type;
  set type(ZScannerAuthenticationManifestTypeEnum? type) => _$this._type = type;

  ZScannerOpenIDAuthenticationManifestBuilder? _openid;
  ZScannerOpenIDAuthenticationManifestBuilder get openid =>
      _$this._openid ??= new ZScannerOpenIDAuthenticationManifestBuilder();
  set openid(ZScannerOpenIDAuthenticationManifestBuilder? openid) =>
      _$this._openid = openid;

  ZScannerSeaCat3AuthenticationManifestBuilder? _seacat3;
  ZScannerSeaCat3AuthenticationManifestBuilder get seacat3 =>
      _$this._seacat3 ??= new ZScannerSeaCat3AuthenticationManifestBuilder();
  set seacat3(ZScannerSeaCat3AuthenticationManifestBuilder? seacat3) =>
      _$this._seacat3 = seacat3;

  ZScannerAuthenticationManifestBuilder() {
    ZScannerAuthenticationManifest._defaults(this);
  }

  ZScannerAuthenticationManifestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _openid = $v.openid?.toBuilder();
      _seacat3 = $v.seacat3?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerAuthenticationManifest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerAuthenticationManifest;
  }

  @override
  void update(void Function(ZScannerAuthenticationManifestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerAuthenticationManifest build() => _build();

  _$ZScannerAuthenticationManifest _build() {
    _$ZScannerAuthenticationManifest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerAuthenticationManifest._(
              type: BuiltValueNullFieldError.checkNotNull(
                  type, r'ZScannerAuthenticationManifest', 'type'),
              openid: _openid?.build(),
              seacat3: _seacat3?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'openid';
        _openid?.build();
        _$failedField = 'seacat3';
        _seacat3?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerAuthenticationManifest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
