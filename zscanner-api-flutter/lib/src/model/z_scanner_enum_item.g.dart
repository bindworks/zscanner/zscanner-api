// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_enum_item.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerEnumItem extends ZScannerEnumItem {
  @override
  final String itemId;
  @override
  final String? code;
  @override
  final String? icon;
  @override
  final String label;
  @override
  final String? help;
  @override
  final String? parentItemId;
  @override
  final bool? selectable;

  factory _$ZScannerEnumItem(
          [void Function(ZScannerEnumItemBuilder)? updates]) =>
      (new ZScannerEnumItemBuilder()..update(updates))._build();

  _$ZScannerEnumItem._(
      {required this.itemId,
      this.code,
      this.icon,
      required this.label,
      this.help,
      this.parentItemId,
      this.selectable})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        itemId, r'ZScannerEnumItem', 'itemId');
    BuiltValueNullFieldError.checkNotNull(label, r'ZScannerEnumItem', 'label');
  }

  @override
  ZScannerEnumItem rebuild(void Function(ZScannerEnumItemBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerEnumItemBuilder toBuilder() =>
      new ZScannerEnumItemBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerEnumItem &&
        itemId == other.itemId &&
        code == other.code &&
        icon == other.icon &&
        label == other.label &&
        help == other.help &&
        parentItemId == other.parentItemId &&
        selectable == other.selectable;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, itemId.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, icon.hashCode);
    _$hash = $jc(_$hash, label.hashCode);
    _$hash = $jc(_$hash, help.hashCode);
    _$hash = $jc(_$hash, parentItemId.hashCode);
    _$hash = $jc(_$hash, selectable.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerEnumItem')
          ..add('itemId', itemId)
          ..add('code', code)
          ..add('icon', icon)
          ..add('label', label)
          ..add('help', help)
          ..add('parentItemId', parentItemId)
          ..add('selectable', selectable))
        .toString();
  }
}

class ZScannerEnumItemBuilder
    implements Builder<ZScannerEnumItem, ZScannerEnumItemBuilder> {
  _$ZScannerEnumItem? _$v;

  String? _itemId;
  String? get itemId => _$this._itemId;
  set itemId(String? itemId) => _$this._itemId = itemId;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  String? _icon;
  String? get icon => _$this._icon;
  set icon(String? icon) => _$this._icon = icon;

  String? _label;
  String? get label => _$this._label;
  set label(String? label) => _$this._label = label;

  String? _help;
  String? get help => _$this._help;
  set help(String? help) => _$this._help = help;

  String? _parentItemId;
  String? get parentItemId => _$this._parentItemId;
  set parentItemId(String? parentItemId) => _$this._parentItemId = parentItemId;

  bool? _selectable;
  bool? get selectable => _$this._selectable;
  set selectable(bool? selectable) => _$this._selectable = selectable;

  ZScannerEnumItemBuilder() {
    ZScannerEnumItem._defaults(this);
  }

  ZScannerEnumItemBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _itemId = $v.itemId;
      _code = $v.code;
      _icon = $v.icon;
      _label = $v.label;
      _help = $v.help;
      _parentItemId = $v.parentItemId;
      _selectable = $v.selectable;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerEnumItem other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerEnumItem;
  }

  @override
  void update(void Function(ZScannerEnumItemBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerEnumItem build() => _build();

  _$ZScannerEnumItem _build() {
    final _$result = _$v ??
        new _$ZScannerEnumItem._(
            itemId: BuiltValueNullFieldError.checkNotNull(
                itemId, r'ZScannerEnumItem', 'itemId'),
            code: code,
            icon: icon,
            label: BuiltValueNullFieldError.checkNotNull(
                label, r'ZScannerEnumItem', 'label'),
            help: help,
            parentItemId: parentItemId,
            selectable: selectable);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
