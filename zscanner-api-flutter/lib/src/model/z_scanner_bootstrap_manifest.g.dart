// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_bootstrap_manifest.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerBootstrapManifest extends ZScannerBootstrapManifest {
  @override
  final ZScannerGlobalManifest global;
  @override
  final BuiltList<ZScannerInstallationManifest> installations;

  factory _$ZScannerBootstrapManifest(
          [void Function(ZScannerBootstrapManifestBuilder)? updates]) =>
      (new ZScannerBootstrapManifestBuilder()..update(updates))._build();

  _$ZScannerBootstrapManifest._(
      {required this.global, required this.installations})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        global, r'ZScannerBootstrapManifest', 'global');
    BuiltValueNullFieldError.checkNotNull(
        installations, r'ZScannerBootstrapManifest', 'installations');
  }

  @override
  ZScannerBootstrapManifest rebuild(
          void Function(ZScannerBootstrapManifestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerBootstrapManifestBuilder toBuilder() =>
      new ZScannerBootstrapManifestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerBootstrapManifest &&
        global == other.global &&
        installations == other.installations;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, global.hashCode);
    _$hash = $jc(_$hash, installations.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerBootstrapManifest')
          ..add('global', global)
          ..add('installations', installations))
        .toString();
  }
}

class ZScannerBootstrapManifestBuilder
    implements
        Builder<ZScannerBootstrapManifest, ZScannerBootstrapManifestBuilder> {
  _$ZScannerBootstrapManifest? _$v;

  ZScannerGlobalManifestBuilder? _global;
  ZScannerGlobalManifestBuilder get global =>
      _$this._global ??= new ZScannerGlobalManifestBuilder();
  set global(ZScannerGlobalManifestBuilder? global) => _$this._global = global;

  ListBuilder<ZScannerInstallationManifest>? _installations;
  ListBuilder<ZScannerInstallationManifest> get installations =>
      _$this._installations ??= new ListBuilder<ZScannerInstallationManifest>();
  set installations(ListBuilder<ZScannerInstallationManifest>? installations) =>
      _$this._installations = installations;

  ZScannerBootstrapManifestBuilder() {
    ZScannerBootstrapManifest._defaults(this);
  }

  ZScannerBootstrapManifestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _global = $v.global.toBuilder();
      _installations = $v.installations.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerBootstrapManifest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerBootstrapManifest;
  }

  @override
  void update(void Function(ZScannerBootstrapManifestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerBootstrapManifest build() => _build();

  _$ZScannerBootstrapManifest _build() {
    _$ZScannerBootstrapManifest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerBootstrapManifest._(
              global: global.build(), installations: installations.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'global';
        global.build();
        _$failedField = 'installations';
        installations.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerBootstrapManifest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
