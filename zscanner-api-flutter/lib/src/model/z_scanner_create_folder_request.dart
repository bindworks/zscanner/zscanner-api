//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_metadata_item.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_create_folder_request.g.dart';

/// ZScannerCreateFolderRequest
///
/// Properties:
/// * [metadata] 
/// * [folderMetadata] 
@BuiltValue()
abstract class ZScannerCreateFolderRequest implements Built<ZScannerCreateFolderRequest, ZScannerCreateFolderRequestBuilder> {
  @BuiltValueField(wireName: r'metadata')
  BuiltList<ZScannerMetadataItem> get metadata;

  @BuiltValueField(wireName: r'folderMetadata')
  BuiltList<ZScannerMetadataItem> get folderMetadata;

  ZScannerCreateFolderRequest._();

  factory ZScannerCreateFolderRequest([void updates(ZScannerCreateFolderRequestBuilder b)]) = _$ZScannerCreateFolderRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerCreateFolderRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerCreateFolderRequest> get serializer => _$ZScannerCreateFolderRequestSerializer();
}

class _$ZScannerCreateFolderRequestSerializer implements PrimitiveSerializer<ZScannerCreateFolderRequest> {
  @override
  final Iterable<Type> types = const [ZScannerCreateFolderRequest, _$ZScannerCreateFolderRequest];

  @override
  final String wireName = r'ZScannerCreateFolderRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerCreateFolderRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'metadata';
    yield serializers.serialize(
      object.metadata,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
    );
    yield r'folderMetadata';
    yield serializers.serialize(
      object.folderMetadata,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerCreateFolderRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerCreateFolderRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'metadata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
          ) as BuiltList<ZScannerMetadataItem>;
          result.metadata.replace(valueDes);
          break;
        case r'folderMetadata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
          ) as BuiltList<ZScannerMetadataItem>;
          result.folderMetadata.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerCreateFolderRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerCreateFolderRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

