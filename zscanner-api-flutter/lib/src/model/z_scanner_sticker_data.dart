//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_sticker_data.g.dart';

/// ZScannerStickerData
///
/// Properties:
/// * [fontFace] 
/// * [text] 
/// * [textColor] 
/// * [boundingBox] 
@BuiltValue()
abstract class ZScannerStickerData implements Built<ZScannerStickerData, ZScannerStickerDataBuilder> {
  @BuiltValueField(wireName: r'fontFace')
  String? get fontFace;

  @BuiltValueField(wireName: r'text')
  String? get text;

  @BuiltValueField(wireName: r'textColor')
  String? get textColor;

  @BuiltValueField(wireName: r'boundingBox')
  BuiltList<int> get boundingBox;

  ZScannerStickerData._();

  factory ZScannerStickerData([void updates(ZScannerStickerDataBuilder b)]) = _$ZScannerStickerData;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerStickerDataBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerStickerData> get serializer => _$ZScannerStickerDataSerializer();
}

class _$ZScannerStickerDataSerializer implements PrimitiveSerializer<ZScannerStickerData> {
  @override
  final Iterable<Type> types = const [ZScannerStickerData, _$ZScannerStickerData];

  @override
  final String wireName = r'ZScannerStickerData';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerStickerData object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.fontFace != null) {
      yield r'fontFace';
      yield serializers.serialize(
        object.fontFace,
        specifiedType: const FullType(String),
      );
    }
    if (object.text != null) {
      yield r'text';
      yield serializers.serialize(
        object.text,
        specifiedType: const FullType(String),
      );
    }
    if (object.textColor != null) {
      yield r'textColor';
      yield serializers.serialize(
        object.textColor,
        specifiedType: const FullType(String),
      );
    }
    yield r'boundingBox';
    yield serializers.serialize(
      object.boundingBox,
      specifiedType: const FullType(BuiltList, [FullType(int)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerStickerData object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerStickerDataBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'fontFace':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fontFace = valueDes;
          break;
        case r'text':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.text = valueDes;
          break;
        case r'textColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.textColor = valueDes;
          break;
        case r'boundingBox':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(int)]),
          ) as BuiltList<int>;
          result.boundingBox.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerStickerData deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerStickerDataBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

