// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_application_manifest.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ZScannerApplicationManifestSystemProtectionEnum
    _$zScannerApplicationManifestSystemProtectionEnum_required_ =
    const ZScannerApplicationManifestSystemProtectionEnum._('required_');
const ZScannerApplicationManifestSystemProtectionEnum
    _$zScannerApplicationManifestSystemProtectionEnum_optional =
    const ZScannerApplicationManifestSystemProtectionEnum._('optional');
const ZScannerApplicationManifestSystemProtectionEnum
    _$zScannerApplicationManifestSystemProtectionEnum_$unknown =
    const ZScannerApplicationManifestSystemProtectionEnum._('unknown');

ZScannerApplicationManifestSystemProtectionEnum
    _$zScannerApplicationManifestSystemProtectionEnumValueOf(String name) {
  switch (name) {
    case 'required_':
      return _$zScannerApplicationManifestSystemProtectionEnum_required_;
    case 'optional':
      return _$zScannerApplicationManifestSystemProtectionEnum_optional;
    case 'unknown':
      return _$zScannerApplicationManifestSystemProtectionEnum_$unknown;
    default:
      return _$zScannerApplicationManifestSystemProtectionEnum_$unknown;
  }
}

final BuiltSet<ZScannerApplicationManifestSystemProtectionEnum>
    _$zScannerApplicationManifestSystemProtectionEnumValues = new BuiltSet<
        ZScannerApplicationManifestSystemProtectionEnum>(const <ZScannerApplicationManifestSystemProtectionEnum>[
  _$zScannerApplicationManifestSystemProtectionEnum_required_,
  _$zScannerApplicationManifestSystemProtectionEnum_optional,
  _$zScannerApplicationManifestSystemProtectionEnum_$unknown,
]);

const ZScannerApplicationManifestApplicationProtectionEnum
    _$zScannerApplicationManifestApplicationProtectionEnum_required_ =
    const ZScannerApplicationManifestApplicationProtectionEnum._('required_');
const ZScannerApplicationManifestApplicationProtectionEnum
    _$zScannerApplicationManifestApplicationProtectionEnum_optional =
    const ZScannerApplicationManifestApplicationProtectionEnum._('optional');
const ZScannerApplicationManifestApplicationProtectionEnum
    _$zScannerApplicationManifestApplicationProtectionEnum_none =
    const ZScannerApplicationManifestApplicationProtectionEnum._('none');
const ZScannerApplicationManifestApplicationProtectionEnum
    _$zScannerApplicationManifestApplicationProtectionEnum_$unknown =
    const ZScannerApplicationManifestApplicationProtectionEnum._('unknown');

ZScannerApplicationManifestApplicationProtectionEnum
    _$zScannerApplicationManifestApplicationProtectionEnumValueOf(String name) {
  switch (name) {
    case 'required_':
      return _$zScannerApplicationManifestApplicationProtectionEnum_required_;
    case 'optional':
      return _$zScannerApplicationManifestApplicationProtectionEnum_optional;
    case 'none':
      return _$zScannerApplicationManifestApplicationProtectionEnum_none;
    case 'unknown':
      return _$zScannerApplicationManifestApplicationProtectionEnum_$unknown;
    default:
      return _$zScannerApplicationManifestApplicationProtectionEnum_$unknown;
  }
}

final BuiltSet<ZScannerApplicationManifestApplicationProtectionEnum>
    _$zScannerApplicationManifestApplicationProtectionEnumValues = new BuiltSet<
        ZScannerApplicationManifestApplicationProtectionEnum>(const <ZScannerApplicationManifestApplicationProtectionEnum>[
  _$zScannerApplicationManifestApplicationProtectionEnum_required_,
  _$zScannerApplicationManifestApplicationProtectionEnum_optional,
  _$zScannerApplicationManifestApplicationProtectionEnum_none,
  _$zScannerApplicationManifestApplicationProtectionEnum_$unknown,
]);

Serializer<ZScannerApplicationManifestSystemProtectionEnum>
    _$zScannerApplicationManifestSystemProtectionEnumSerializer =
    new _$ZScannerApplicationManifestSystemProtectionEnumSerializer();
Serializer<ZScannerApplicationManifestApplicationProtectionEnum>
    _$zScannerApplicationManifestApplicationProtectionEnumSerializer =
    new _$ZScannerApplicationManifestApplicationProtectionEnumSerializer();

class _$ZScannerApplicationManifestSystemProtectionEnumSerializer
    implements
        PrimitiveSerializer<ZScannerApplicationManifestSystemProtectionEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'required_': 'required',
    'optional': 'optional',
    'unknown': '____unknown____',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'required': 'required_',
    'optional': 'optional',
    '____unknown____': 'unknown',
  };

  @override
  final Iterable<Type> types = const <Type>[
    ZScannerApplicationManifestSystemProtectionEnum
  ];
  @override
  final String wireName = 'ZScannerApplicationManifestSystemProtectionEnum';

  @override
  Object serialize(Serializers serializers,
          ZScannerApplicationManifestSystemProtectionEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ZScannerApplicationManifestSystemProtectionEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ZScannerApplicationManifestSystemProtectionEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$ZScannerApplicationManifestApplicationProtectionEnumSerializer
    implements
        PrimitiveSerializer<
            ZScannerApplicationManifestApplicationProtectionEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'required_': 'required',
    'optional': 'optional',
    'none': 'none',
    'unknown': '____unknown____',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'required': 'required_',
    'optional': 'optional',
    'none': 'none',
    '____unknown____': 'unknown',
  };

  @override
  final Iterable<Type> types = const <Type>[
    ZScannerApplicationManifestApplicationProtectionEnum
  ];
  @override
  final String wireName =
      'ZScannerApplicationManifestApplicationProtectionEnum';

  @override
  Object serialize(Serializers serializers,
          ZScannerApplicationManifestApplicationProtectionEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ZScannerApplicationManifestApplicationProtectionEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ZScannerApplicationManifestApplicationProtectionEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$ZScannerApplicationManifest extends ZScannerApplicationManifest {
  @override
  final ZScannerApplicationManifestSystemProtectionEnum? systemProtection;
  @override
  final ZScannerApplicationManifestApplicationProtectionEnum?
      applicationProtection;
  @override
  final bool? skipJailBreakCheck;
  @override
  final bool? hasCustomVibrations;
  @override
  final bool? showCrashButton;
  @override
  final bool? showServerUrl;
  @override
  final BuiltMap<String, String>? localization;

  factory _$ZScannerApplicationManifest(
          [void Function(ZScannerApplicationManifestBuilder)? updates]) =>
      (new ZScannerApplicationManifestBuilder()..update(updates))._build();

  _$ZScannerApplicationManifest._(
      {this.systemProtection,
      this.applicationProtection,
      this.skipJailBreakCheck,
      this.hasCustomVibrations,
      this.showCrashButton,
      this.showServerUrl,
      this.localization})
      : super._();

  @override
  ZScannerApplicationManifest rebuild(
          void Function(ZScannerApplicationManifestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerApplicationManifestBuilder toBuilder() =>
      new ZScannerApplicationManifestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerApplicationManifest &&
        systemProtection == other.systemProtection &&
        applicationProtection == other.applicationProtection &&
        skipJailBreakCheck == other.skipJailBreakCheck &&
        hasCustomVibrations == other.hasCustomVibrations &&
        showCrashButton == other.showCrashButton &&
        showServerUrl == other.showServerUrl &&
        localization == other.localization;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, systemProtection.hashCode);
    _$hash = $jc(_$hash, applicationProtection.hashCode);
    _$hash = $jc(_$hash, skipJailBreakCheck.hashCode);
    _$hash = $jc(_$hash, hasCustomVibrations.hashCode);
    _$hash = $jc(_$hash, showCrashButton.hashCode);
    _$hash = $jc(_$hash, showServerUrl.hashCode);
    _$hash = $jc(_$hash, localization.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerApplicationManifest')
          ..add('systemProtection', systemProtection)
          ..add('applicationProtection', applicationProtection)
          ..add('skipJailBreakCheck', skipJailBreakCheck)
          ..add('hasCustomVibrations', hasCustomVibrations)
          ..add('showCrashButton', showCrashButton)
          ..add('showServerUrl', showServerUrl)
          ..add('localization', localization))
        .toString();
  }
}

class ZScannerApplicationManifestBuilder
    implements
        Builder<ZScannerApplicationManifest,
            ZScannerApplicationManifestBuilder> {
  _$ZScannerApplicationManifest? _$v;

  ZScannerApplicationManifestSystemProtectionEnum? _systemProtection;
  ZScannerApplicationManifestSystemProtectionEnum? get systemProtection =>
      _$this._systemProtection;
  set systemProtection(
          ZScannerApplicationManifestSystemProtectionEnum? systemProtection) =>
      _$this._systemProtection = systemProtection;

  ZScannerApplicationManifestApplicationProtectionEnum? _applicationProtection;
  ZScannerApplicationManifestApplicationProtectionEnum?
      get applicationProtection => _$this._applicationProtection;
  set applicationProtection(
          ZScannerApplicationManifestApplicationProtectionEnum?
              applicationProtection) =>
      _$this._applicationProtection = applicationProtection;

  bool? _skipJailBreakCheck;
  bool? get skipJailBreakCheck => _$this._skipJailBreakCheck;
  set skipJailBreakCheck(bool? skipJailBreakCheck) =>
      _$this._skipJailBreakCheck = skipJailBreakCheck;

  bool? _hasCustomVibrations;
  bool? get hasCustomVibrations => _$this._hasCustomVibrations;
  set hasCustomVibrations(bool? hasCustomVibrations) =>
      _$this._hasCustomVibrations = hasCustomVibrations;

  bool? _showCrashButton;
  bool? get showCrashButton => _$this._showCrashButton;
  set showCrashButton(bool? showCrashButton) =>
      _$this._showCrashButton = showCrashButton;

  bool? _showServerUrl;
  bool? get showServerUrl => _$this._showServerUrl;
  set showServerUrl(bool? showServerUrl) =>
      _$this._showServerUrl = showServerUrl;

  MapBuilder<String, String>? _localization;
  MapBuilder<String, String> get localization =>
      _$this._localization ??= new MapBuilder<String, String>();
  set localization(MapBuilder<String, String>? localization) =>
      _$this._localization = localization;

  ZScannerApplicationManifestBuilder() {
    ZScannerApplicationManifest._defaults(this);
  }

  ZScannerApplicationManifestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _systemProtection = $v.systemProtection;
      _applicationProtection = $v.applicationProtection;
      _skipJailBreakCheck = $v.skipJailBreakCheck;
      _hasCustomVibrations = $v.hasCustomVibrations;
      _showCrashButton = $v.showCrashButton;
      _showServerUrl = $v.showServerUrl;
      _localization = $v.localization?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerApplicationManifest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerApplicationManifest;
  }

  @override
  void update(void Function(ZScannerApplicationManifestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerApplicationManifest build() => _build();

  _$ZScannerApplicationManifest _build() {
    _$ZScannerApplicationManifest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerApplicationManifest._(
              systemProtection: systemProtection,
              applicationProtection: applicationProtection,
              skipJailBreakCheck: skipJailBreakCheck,
              hasCustomVibrations: hasCustomVibrations,
              showCrashButton: showCrashButton,
              showServerUrl: showServerUrl,
              localization: _localization?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'localization';
        _localization?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerApplicationManifest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
