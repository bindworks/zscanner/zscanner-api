//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_application_manifest.g.dart';

/// ZScannerApplicationManifest
///
/// Properties:
/// * [systemProtection] 
/// * [applicationProtection] 
/// * [skipJailBreakCheck] 
/// * [hasCustomVibrations] 
/// * [showCrashButton] 
/// * [showServerUrl] 
/// * [localization] 
@BuiltValue()
abstract class ZScannerApplicationManifest implements Built<ZScannerApplicationManifest, ZScannerApplicationManifestBuilder> {
  @BuiltValueField(wireName: r'systemProtection')
  ZScannerApplicationManifestSystemProtectionEnum? get systemProtection;
  // enum systemProtectionEnum {  required,  optional,  };

  @BuiltValueField(wireName: r'applicationProtection')
  ZScannerApplicationManifestApplicationProtectionEnum? get applicationProtection;
  // enum applicationProtectionEnum {  required,  optional,  none,  };

  @BuiltValueField(wireName: r'skipJailBreakCheck')
  bool? get skipJailBreakCheck;

  @BuiltValueField(wireName: r'hasCustomVibrations')
  bool? get hasCustomVibrations;

  @BuiltValueField(wireName: r'showCrashButton')
  bool? get showCrashButton;

  @BuiltValueField(wireName: r'showServerUrl')
  bool? get showServerUrl;

  @BuiltValueField(wireName: r'localization')
  BuiltMap<String, String>? get localization;

  ZScannerApplicationManifest._();

  factory ZScannerApplicationManifest([void updates(ZScannerApplicationManifestBuilder b)]) = _$ZScannerApplicationManifest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerApplicationManifestBuilder b) => b
      ..systemProtection = const ZScannerApplicationManifestSystemProtectionEnum._('optional')
      ..applicationProtection = const ZScannerApplicationManifestApplicationProtectionEnum._('optional')
      ..skipJailBreakCheck = false
      ..hasCustomVibrations = false
      ..showCrashButton = false
      ..showServerUrl = false;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerApplicationManifest> get serializer => _$ZScannerApplicationManifestSerializer();
}

class _$ZScannerApplicationManifestSerializer implements PrimitiveSerializer<ZScannerApplicationManifest> {
  @override
  final Iterable<Type> types = const [ZScannerApplicationManifest, _$ZScannerApplicationManifest];

  @override
  final String wireName = r'ZScannerApplicationManifest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerApplicationManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.systemProtection != null) {
      yield r'systemProtection';
      yield serializers.serialize(
        object.systemProtection,
        specifiedType: const FullType(ZScannerApplicationManifestSystemProtectionEnum),
      );
    }
    if (object.applicationProtection != null) {
      yield r'applicationProtection';
      yield serializers.serialize(
        object.applicationProtection,
        specifiedType: const FullType(ZScannerApplicationManifestApplicationProtectionEnum),
      );
    }
    if (object.skipJailBreakCheck != null) {
      yield r'skipJailBreakCheck';
      yield serializers.serialize(
        object.skipJailBreakCheck,
        specifiedType: const FullType(bool),
      );
    }
    if (object.hasCustomVibrations != null) {
      yield r'hasCustomVibrations';
      yield serializers.serialize(
        object.hasCustomVibrations,
        specifiedType: const FullType(bool),
      );
    }
    if (object.showCrashButton != null) {
      yield r'showCrashButton';
      yield serializers.serialize(
        object.showCrashButton,
        specifiedType: const FullType(bool),
      );
    }
    if (object.showServerUrl != null) {
      yield r'showServerUrl';
      yield serializers.serialize(
        object.showServerUrl,
        specifiedType: const FullType(bool),
      );
    }
    if (object.localization != null) {
      yield r'localization';
      yield serializers.serialize(
        object.localization,
        specifiedType: const FullType(BuiltMap, [FullType(String), FullType(String)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerApplicationManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerApplicationManifestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'systemProtection':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerApplicationManifestSystemProtectionEnum),
          ) as ZScannerApplicationManifestSystemProtectionEnum;
          result.systemProtection = valueDes;
          break;
        case r'applicationProtection':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerApplicationManifestApplicationProtectionEnum),
          ) as ZScannerApplicationManifestApplicationProtectionEnum;
          result.applicationProtection = valueDes;
          break;
        case r'skipJailBreakCheck':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.skipJailBreakCheck = valueDes;
          break;
        case r'hasCustomVibrations':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.hasCustomVibrations = valueDes;
          break;
        case r'showCrashButton':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.showCrashButton = valueDes;
          break;
        case r'showServerUrl':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.showServerUrl = valueDes;
          break;
        case r'localization':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltMap, [FullType(String), FullType(String)]),
          ) as BuiltMap<String, String>;
          result.localization.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerApplicationManifest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerApplicationManifestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class ZScannerApplicationManifestSystemProtectionEnum extends EnumClass {

  @BuiltValueEnumConst(wireName: r'required')
  static const ZScannerApplicationManifestSystemProtectionEnum required_ = _$zScannerApplicationManifestSystemProtectionEnum_required_;
  @BuiltValueEnumConst(wireName: r'optional')
  static const ZScannerApplicationManifestSystemProtectionEnum optional = _$zScannerApplicationManifestSystemProtectionEnum_optional;

  @BuiltValueEnumConst(wireName: r'____unknown____', fallback: true)
  static const ZScannerApplicationManifestSystemProtectionEnum unknown = _$zScannerApplicationManifestSystemProtectionEnum_$unknown;

  static Serializer<ZScannerApplicationManifestSystemProtectionEnum> get serializer => _$zScannerApplicationManifestSystemProtectionEnumSerializer;

  const ZScannerApplicationManifestSystemProtectionEnum._(String name): super(name);

  static BuiltSet<ZScannerApplicationManifestSystemProtectionEnum> get values => _$zScannerApplicationManifestSystemProtectionEnumValues;
  static ZScannerApplicationManifestSystemProtectionEnum valueOf(String name) => _$zScannerApplicationManifestSystemProtectionEnumValueOf(name);
}


class ZScannerApplicationManifestApplicationProtectionEnum extends EnumClass {

  @BuiltValueEnumConst(wireName: r'required')
  static const ZScannerApplicationManifestApplicationProtectionEnum required_ = _$zScannerApplicationManifestApplicationProtectionEnum_required_;
  @BuiltValueEnumConst(wireName: r'optional')
  static const ZScannerApplicationManifestApplicationProtectionEnum optional = _$zScannerApplicationManifestApplicationProtectionEnum_optional;
  @BuiltValueEnumConst(wireName: r'none')
  static const ZScannerApplicationManifestApplicationProtectionEnum none = _$zScannerApplicationManifestApplicationProtectionEnum_none;

  @BuiltValueEnumConst(wireName: r'____unknown____', fallback: true)
  static const ZScannerApplicationManifestApplicationProtectionEnum unknown = _$zScannerApplicationManifestApplicationProtectionEnum_$unknown;

  static Serializer<ZScannerApplicationManifestApplicationProtectionEnum> get serializer => _$zScannerApplicationManifestApplicationProtectionEnumSerializer;

  const ZScannerApplicationManifestApplicationProtectionEnum._(String name): super(name);

  static BuiltSet<ZScannerApplicationManifestApplicationProtectionEnum> get values => _$zScannerApplicationManifestApplicationProtectionEnumValues;
  static ZScannerApplicationManifestApplicationProtectionEnum valueOf(String name) => _$zScannerApplicationManifestApplicationProtectionEnumValueOf(name);
}


