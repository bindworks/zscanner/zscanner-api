//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/map2d_config.dart';
import 'package:zscanner_api/src/model/spot_map_config.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_subfolder_config_union.g.dart';

/// ZScannerSubfolderConfigUnion
///
/// Properties:
/// * [spotMap] 
/// * [map2d] 
/// * [naked] 
@BuiltValue()
abstract class ZScannerSubfolderConfigUnion implements Built<ZScannerSubfolderConfigUnion, ZScannerSubfolderConfigUnionBuilder> {
  @BuiltValueField(wireName: r'spotMap')
  SpotMapConfig? get spotMap;

  @BuiltValueField(wireName: r'map2d')
  Map2dConfig? get map2d;

  @BuiltValueField(wireName: r'naked')
  bool? get naked;

  ZScannerSubfolderConfigUnion._();

  factory ZScannerSubfolderConfigUnion([void updates(ZScannerSubfolderConfigUnionBuilder b)]) = _$ZScannerSubfolderConfigUnion;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerSubfolderConfigUnionBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerSubfolderConfigUnion> get serializer => _$ZScannerSubfolderConfigUnionSerializer();
}

class _$ZScannerSubfolderConfigUnionSerializer implements PrimitiveSerializer<ZScannerSubfolderConfigUnion> {
  @override
  final Iterable<Type> types = const [ZScannerSubfolderConfigUnion, _$ZScannerSubfolderConfigUnion];

  @override
  final String wireName = r'ZScannerSubfolderConfigUnion';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerSubfolderConfigUnion object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.spotMap != null) {
      yield r'spotMap';
      yield serializers.serialize(
        object.spotMap,
        specifiedType: const FullType(SpotMapConfig),
      );
    }
    if (object.map2d != null) {
      yield r'map2d';
      yield serializers.serialize(
        object.map2d,
        specifiedType: const FullType(Map2dConfig),
      );
    }
    if (object.naked != null) {
      yield r'naked';
      yield serializers.serialize(
        object.naked,
        specifiedType: const FullType(bool),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerSubfolderConfigUnion object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerSubfolderConfigUnionBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'spotMap':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(SpotMapConfig),
          ) as SpotMapConfig;
          result.spotMap.replace(valueDes);
          break;
        case r'map2d':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(Map2dConfig),
          ) as Map2dConfig;
          result.map2d.replace(valueDes);
          break;
        case r'naked':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.naked = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerSubfolderConfigUnion deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerSubfolderConfigUnionBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

