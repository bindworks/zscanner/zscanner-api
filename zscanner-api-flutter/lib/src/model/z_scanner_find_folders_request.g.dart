// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_find_folders_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerFindFoldersRequest extends ZScannerFindFoldersRequest {
  @override
  final BuiltList<ZScannerMetadataItem> metadata;
  @override
  final String? internalId;
  @override
  final String? fullText;
  @override
  final bool? suggestions;
  @override
  final String? barCodeType;
  @override
  final String? barCodeValue;

  factory _$ZScannerFindFoldersRequest(
          [void Function(ZScannerFindFoldersRequestBuilder)? updates]) =>
      (new ZScannerFindFoldersRequestBuilder()..update(updates))._build();

  _$ZScannerFindFoldersRequest._(
      {required this.metadata,
      this.internalId,
      this.fullText,
      this.suggestions,
      this.barCodeType,
      this.barCodeValue})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        metadata, r'ZScannerFindFoldersRequest', 'metadata');
  }

  @override
  ZScannerFindFoldersRequest rebuild(
          void Function(ZScannerFindFoldersRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerFindFoldersRequestBuilder toBuilder() =>
      new ZScannerFindFoldersRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerFindFoldersRequest &&
        metadata == other.metadata &&
        internalId == other.internalId &&
        fullText == other.fullText &&
        suggestions == other.suggestions &&
        barCodeType == other.barCodeType &&
        barCodeValue == other.barCodeValue;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, metadata.hashCode);
    _$hash = $jc(_$hash, internalId.hashCode);
    _$hash = $jc(_$hash, fullText.hashCode);
    _$hash = $jc(_$hash, suggestions.hashCode);
    _$hash = $jc(_$hash, barCodeType.hashCode);
    _$hash = $jc(_$hash, barCodeValue.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerFindFoldersRequest')
          ..add('metadata', metadata)
          ..add('internalId', internalId)
          ..add('fullText', fullText)
          ..add('suggestions', suggestions)
          ..add('barCodeType', barCodeType)
          ..add('barCodeValue', barCodeValue))
        .toString();
  }
}

class ZScannerFindFoldersRequestBuilder
    implements
        Builder<ZScannerFindFoldersRequest, ZScannerFindFoldersRequestBuilder> {
  _$ZScannerFindFoldersRequest? _$v;

  ListBuilder<ZScannerMetadataItem>? _metadata;
  ListBuilder<ZScannerMetadataItem> get metadata =>
      _$this._metadata ??= new ListBuilder<ZScannerMetadataItem>();
  set metadata(ListBuilder<ZScannerMetadataItem>? metadata) =>
      _$this._metadata = metadata;

  String? _internalId;
  String? get internalId => _$this._internalId;
  set internalId(String? internalId) => _$this._internalId = internalId;

  String? _fullText;
  String? get fullText => _$this._fullText;
  set fullText(String? fullText) => _$this._fullText = fullText;

  bool? _suggestions;
  bool? get suggestions => _$this._suggestions;
  set suggestions(bool? suggestions) => _$this._suggestions = suggestions;

  String? _barCodeType;
  String? get barCodeType => _$this._barCodeType;
  set barCodeType(String? barCodeType) => _$this._barCodeType = barCodeType;

  String? _barCodeValue;
  String? get barCodeValue => _$this._barCodeValue;
  set barCodeValue(String? barCodeValue) => _$this._barCodeValue = barCodeValue;

  ZScannerFindFoldersRequestBuilder() {
    ZScannerFindFoldersRequest._defaults(this);
  }

  ZScannerFindFoldersRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _metadata = $v.metadata.toBuilder();
      _internalId = $v.internalId;
      _fullText = $v.fullText;
      _suggestions = $v.suggestions;
      _barCodeType = $v.barCodeType;
      _barCodeValue = $v.barCodeValue;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerFindFoldersRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerFindFoldersRequest;
  }

  @override
  void update(void Function(ZScannerFindFoldersRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerFindFoldersRequest build() => _build();

  _$ZScannerFindFoldersRequest _build() {
    _$ZScannerFindFoldersRequest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerFindFoldersRequest._(
              metadata: metadata.build(),
              internalId: internalId,
              fullText: fullText,
              suggestions: suggestions,
              barCodeType: barCodeType,
              barCodeValue: barCodeValue);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metadata';
        metadata.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerFindFoldersRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
