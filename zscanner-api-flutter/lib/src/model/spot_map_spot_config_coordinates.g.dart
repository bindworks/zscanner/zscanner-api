// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'spot_map_spot_config_coordinates.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SpotMapSpotConfigCoordinates extends SpotMapSpotConfigCoordinates {
  @override
  final double x;
  @override
  final double y;

  factory _$SpotMapSpotConfigCoordinates(
          [void Function(SpotMapSpotConfigCoordinatesBuilder)? updates]) =>
      (new SpotMapSpotConfigCoordinatesBuilder()..update(updates))._build();

  _$SpotMapSpotConfigCoordinates._({required this.x, required this.y})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        x, r'SpotMapSpotConfigCoordinates', 'x');
    BuiltValueNullFieldError.checkNotNull(
        y, r'SpotMapSpotConfigCoordinates', 'y');
  }

  @override
  SpotMapSpotConfigCoordinates rebuild(
          void Function(SpotMapSpotConfigCoordinatesBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SpotMapSpotConfigCoordinatesBuilder toBuilder() =>
      new SpotMapSpotConfigCoordinatesBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SpotMapSpotConfigCoordinates &&
        x == other.x &&
        y == other.y;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, x.hashCode);
    _$hash = $jc(_$hash, y.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SpotMapSpotConfigCoordinates')
          ..add('x', x)
          ..add('y', y))
        .toString();
  }
}

class SpotMapSpotConfigCoordinatesBuilder
    implements
        Builder<SpotMapSpotConfigCoordinates,
            SpotMapSpotConfigCoordinatesBuilder> {
  _$SpotMapSpotConfigCoordinates? _$v;

  double? _x;
  double? get x => _$this._x;
  set x(double? x) => _$this._x = x;

  double? _y;
  double? get y => _$this._y;
  set y(double? y) => _$this._y = y;

  SpotMapSpotConfigCoordinatesBuilder() {
    SpotMapSpotConfigCoordinates._defaults(this);
  }

  SpotMapSpotConfigCoordinatesBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _x = $v.x;
      _y = $v.y;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SpotMapSpotConfigCoordinates other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SpotMapSpotConfigCoordinates;
  }

  @override
  void update(void Function(SpotMapSpotConfigCoordinatesBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SpotMapSpotConfigCoordinates build() => _build();

  _$SpotMapSpotConfigCoordinates _build() {
    final _$result = _$v ??
        new _$SpotMapSpotConfigCoordinates._(
            x: BuiltValueNullFieldError.checkNotNull(
                x, r'SpotMapSpotConfigCoordinates', 'x'),
            y: BuiltValueNullFieldError.checkNotNull(
                y, r'SpotMapSpotConfigCoordinates', 'y'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
