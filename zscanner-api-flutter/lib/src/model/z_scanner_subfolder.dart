//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_metadata_item.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_subfolder.g.dart';

/// ZScannerSubfolder
///
/// Properties:
/// * [subfolderId] 
/// * [typeId] 
/// * [metadata] 
@BuiltValue()
abstract class ZScannerSubfolder implements Built<ZScannerSubfolder, ZScannerSubfolderBuilder> {
  @BuiltValueField(wireName: r'subfolderId')
  String get subfolderId;

  @BuiltValueField(wireName: r'typeId')
  String get typeId;

  @BuiltValueField(wireName: r'metadata')
  BuiltList<ZScannerMetadataItem> get metadata;

  ZScannerSubfolder._();

  factory ZScannerSubfolder([void updates(ZScannerSubfolderBuilder b)]) = _$ZScannerSubfolder;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerSubfolderBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerSubfolder> get serializer => _$ZScannerSubfolderSerializer();
}

class _$ZScannerSubfolderSerializer implements PrimitiveSerializer<ZScannerSubfolder> {
  @override
  final Iterable<Type> types = const [ZScannerSubfolder, _$ZScannerSubfolder];

  @override
  final String wireName = r'ZScannerSubfolder';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerSubfolder object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'subfolderId';
    yield serializers.serialize(
      object.subfolderId,
      specifiedType: const FullType(String),
    );
    yield r'typeId';
    yield serializers.serialize(
      object.typeId,
      specifiedType: const FullType(String),
    );
    yield r'metadata';
    yield serializers.serialize(
      object.metadata,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerSubfolder object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerSubfolderBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'subfolderId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.subfolderId = valueDes;
          break;
        case r'typeId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.typeId = valueDes;
          break;
        case r'metadata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
          ) as BuiltList<ZScannerMetadataItem>;
          result.metadata.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerSubfolder deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerSubfolderBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

