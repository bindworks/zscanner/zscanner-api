//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_metadata_item.dart';
import 'package:zscanner_api/src/model/z_scanner_folder.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_found_folder.g.dart';

/// ZScannerFoundFolder
///
/// Properties:
/// * [flowId] 
/// * [metadata] 
/// * [folder] 
/// * [subfolderId] 
@BuiltValue()
abstract class ZScannerFoundFolder implements Built<ZScannerFoundFolder, ZScannerFoundFolderBuilder> {
  @BuiltValueField(wireName: r'flowId')
  String? get flowId;

  @BuiltValueField(wireName: r'metadata')
  BuiltList<ZScannerMetadataItem>? get metadata;

  @BuiltValueField(wireName: r'folder')
  ZScannerFolder get folder;

  @BuiltValueField(wireName: r'subfolderId')
  String? get subfolderId;

  ZScannerFoundFolder._();

  factory ZScannerFoundFolder([void updates(ZScannerFoundFolderBuilder b)]) = _$ZScannerFoundFolder;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerFoundFolderBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerFoundFolder> get serializer => _$ZScannerFoundFolderSerializer();
}

class _$ZScannerFoundFolderSerializer implements PrimitiveSerializer<ZScannerFoundFolder> {
  @override
  final Iterable<Type> types = const [ZScannerFoundFolder, _$ZScannerFoundFolder];

  @override
  final String wireName = r'ZScannerFoundFolder';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerFoundFolder object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.flowId != null) {
      yield r'flowId';
      yield serializers.serialize(
        object.flowId,
        specifiedType: const FullType(String),
      );
    }
    if (object.metadata != null) {
      yield r'metadata';
      yield serializers.serialize(
        object.metadata,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
      );
    }
    yield r'folder';
    yield serializers.serialize(
      object.folder,
      specifiedType: const FullType(ZScannerFolder),
    );
    if (object.subfolderId != null) {
      yield r'subfolderId';
      yield serializers.serialize(
        object.subfolderId,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerFoundFolder object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerFoundFolderBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'flowId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.flowId = valueDes;
          break;
        case r'metadata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
          ) as BuiltList<ZScannerMetadataItem>;
          result.metadata.replace(valueDes);
          break;
        case r'folder':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerFolder),
          ) as ZScannerFolder;
          result.folder.replace(valueDes);
          break;
        case r'subfolderId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.subfolderId = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerFoundFolder deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerFoundFolderBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

