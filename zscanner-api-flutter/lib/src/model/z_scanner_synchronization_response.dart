//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_flow.dart';
import 'package:zscanner_api/src/model/z_scanner_error.dart';
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_enum.dart';
import 'package:zscanner_api/src/model/z_scanner_config.dart';
import 'package:zscanner_api/src/model/z_scanner_subfolder_schema.dart';
import 'package:zscanner_api/src/model/z_scanner_resource.dart';
import 'package:zscanner_api/src/model/z_scanner_metadata_schema_item.dart';
import 'package:zscanner_api/src/model/z_scanner_user.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_synchronization_response.g.dart';

/// ZScannerSynchronizationResponse
///
/// Properties:
/// * [nextState] 
/// * [metadataValid] 
/// * [error] 
/// * [blockApplication] 
/// * [supportedCaps] - DEPRECATED, use the value from the X-Supported-Caps: header
/// * [config] 
/// * [me] 
/// * [metadataSchema] - Schema of metadata. If not empty, the whole list is replaced 
/// * [folderSchema] - Schema of folder metadata. If not empty, the whole list is replaced 
/// * [createFolderSchema] - Schema of folder metadata. If not empty, the whole list is replaced 
/// * [subfolderSchemata] - Schema of metadata of various types of subfolders 
/// * [flows] - List of flows. If not empty, the whole list is replaced 
/// * [updatedEnums] - List of flows. If not empty, enums are upserted to the ones currently on the client 
/// * [updatedResources] - List of resources. If not empty, resources are upserted to the ones currently on the client 
@BuiltValue()
abstract class ZScannerSynchronizationResponse implements Built<ZScannerSynchronizationResponse, ZScannerSynchronizationResponseBuilder> {
  @BuiltValueField(wireName: r'nextState')
  String get nextState;

  @BuiltValueField(wireName: r'metadataValid')
  bool get metadataValid;

  @BuiltValueField(wireName: r'error')
  ZScannerError? get error;

  @BuiltValueField(wireName: r'blockApplication')
  bool? get blockApplication;

  /// DEPRECATED, use the value from the X-Supported-Caps: header
  @BuiltValueField(wireName: r'supportedCaps')
  BuiltList<String>? get supportedCaps;

  @BuiltValueField(wireName: r'config')
  ZScannerConfig? get config;

  @BuiltValueField(wireName: r'me')
  ZScannerUser? get me;

  /// Schema of metadata. If not empty, the whole list is replaced 
  @BuiltValueField(wireName: r'metadataSchema')
  BuiltList<ZScannerMetadataSchemaItem>? get metadataSchema;

  /// Schema of folder metadata. If not empty, the whole list is replaced 
  @BuiltValueField(wireName: r'folderSchema')
  BuiltList<ZScannerMetadataSchemaItem>? get folderSchema;

  /// Schema of folder metadata. If not empty, the whole list is replaced 
  @BuiltValueField(wireName: r'createFolderSchema')
  BuiltList<ZScannerMetadataSchemaItem>? get createFolderSchema;

  /// Schema of metadata of various types of subfolders 
  @BuiltValueField(wireName: r'subfolderSchemata')
  BuiltList<ZScannerSubfolderSchema>? get subfolderSchemata;

  /// List of flows. If not empty, the whole list is replaced 
  @BuiltValueField(wireName: r'flows')
  BuiltList<ZScannerFlow>? get flows;

  /// List of flows. If not empty, enums are upserted to the ones currently on the client 
  @BuiltValueField(wireName: r'updatedEnums')
  BuiltList<ZScannerEnum>? get updatedEnums;

  /// List of resources. If not empty, resources are upserted to the ones currently on the client 
  @BuiltValueField(wireName: r'updatedResources')
  BuiltList<ZScannerResource>? get updatedResources;

  ZScannerSynchronizationResponse._();

  factory ZScannerSynchronizationResponse([void updates(ZScannerSynchronizationResponseBuilder b)]) = _$ZScannerSynchronizationResponse;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerSynchronizationResponseBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerSynchronizationResponse> get serializer => _$ZScannerSynchronizationResponseSerializer();
}

class _$ZScannerSynchronizationResponseSerializer implements PrimitiveSerializer<ZScannerSynchronizationResponse> {
  @override
  final Iterable<Type> types = const [ZScannerSynchronizationResponse, _$ZScannerSynchronizationResponse];

  @override
  final String wireName = r'ZScannerSynchronizationResponse';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerSynchronizationResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'nextState';
    yield serializers.serialize(
      object.nextState,
      specifiedType: const FullType(String),
    );
    yield r'metadataValid';
    yield serializers.serialize(
      object.metadataValid,
      specifiedType: const FullType(bool),
    );
    if (object.error != null) {
      yield r'error';
      yield serializers.serialize(
        object.error,
        specifiedType: const FullType(ZScannerError),
      );
    }
    if (object.blockApplication != null) {
      yield r'blockApplication';
      yield serializers.serialize(
        object.blockApplication,
        specifiedType: const FullType(bool),
      );
    }
    if (object.supportedCaps != null) {
      yield r'supportedCaps';
      yield serializers.serialize(
        object.supportedCaps,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
    if (object.config != null) {
      yield r'config';
      yield serializers.serialize(
        object.config,
        specifiedType: const FullType(ZScannerConfig),
      );
    }
    if (object.me != null) {
      yield r'me';
      yield serializers.serialize(
        object.me,
        specifiedType: const FullType(ZScannerUser),
      );
    }
    if (object.metadataSchema != null) {
      yield r'metadataSchema';
      yield serializers.serialize(
        object.metadataSchema,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
      );
    }
    if (object.folderSchema != null) {
      yield r'folderSchema';
      yield serializers.serialize(
        object.folderSchema,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
      );
    }
    if (object.createFolderSchema != null) {
      yield r'createFolderSchema';
      yield serializers.serialize(
        object.createFolderSchema,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
      );
    }
    if (object.subfolderSchemata != null) {
      yield r'subfolderSchemata';
      yield serializers.serialize(
        object.subfolderSchemata,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerSubfolderSchema)]),
      );
    }
    if (object.flows != null) {
      yield r'flows';
      yield serializers.serialize(
        object.flows,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerFlow)]),
      );
    }
    if (object.updatedEnums != null) {
      yield r'updatedEnums';
      yield serializers.serialize(
        object.updatedEnums,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerEnum)]),
      );
    }
    if (object.updatedResources != null) {
      yield r'updatedResources';
      yield serializers.serialize(
        object.updatedResources,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerResource)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerSynchronizationResponse object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerSynchronizationResponseBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'nextState':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.nextState = valueDes;
          break;
        case r'metadataValid':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.metadataValid = valueDes;
          break;
        case r'error':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerError),
          ) as ZScannerError;
          result.error.replace(valueDes);
          break;
        case r'blockApplication':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.blockApplication = valueDes;
          break;
        case r'supportedCaps':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.supportedCaps.replace(valueDes);
          break;
        case r'config':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerConfig),
          ) as ZScannerConfig;
          result.config.replace(valueDes);
          break;
        case r'me':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerUser),
          ) as ZScannerUser;
          result.me.replace(valueDes);
          break;
        case r'metadataSchema':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
          ) as BuiltList<ZScannerMetadataSchemaItem>;
          result.metadataSchema.replace(valueDes);
          break;
        case r'folderSchema':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
          ) as BuiltList<ZScannerMetadataSchemaItem>;
          result.folderSchema.replace(valueDes);
          break;
        case r'createFolderSchema':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
          ) as BuiltList<ZScannerMetadataSchemaItem>;
          result.createFolderSchema.replace(valueDes);
          break;
        case r'subfolderSchemata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerSubfolderSchema)]),
          ) as BuiltList<ZScannerSubfolderSchema>;
          result.subfolderSchemata.replace(valueDes);
          break;
        case r'flows':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerFlow)]),
          ) as BuiltList<ZScannerFlow>;
          result.flows.replace(valueDes);
          break;
        case r'updatedEnums':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerEnum)]),
          ) as BuiltList<ZScannerEnum>;
          result.updatedEnums.replace(valueDes);
          break;
        case r'updatedResources':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerResource)]),
          ) as BuiltList<ZScannerResource>;
          result.updatedResources.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerSynchronizationResponse deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerSynchronizationResponseBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

