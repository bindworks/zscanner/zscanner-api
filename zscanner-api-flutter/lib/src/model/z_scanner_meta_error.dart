//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_meta_error.g.dart';

/// ZScannerMetaError
///
/// Properties:
/// * [metaId] 
/// * [exception] 
/// * [error] 
@BuiltValue()
abstract class ZScannerMetaError implements Built<ZScannerMetaError, ZScannerMetaErrorBuilder> {
  @BuiltValueField(wireName: r'metaId')
  String? get metaId;

  @BuiltValueField(wireName: r'exception')
  String? get exception;

  @BuiltValueField(wireName: r'error')
  String? get error;

  ZScannerMetaError._();

  factory ZScannerMetaError([void updates(ZScannerMetaErrorBuilder b)]) = _$ZScannerMetaError;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerMetaErrorBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerMetaError> get serializer => _$ZScannerMetaErrorSerializer();
}

class _$ZScannerMetaErrorSerializer implements PrimitiveSerializer<ZScannerMetaError> {
  @override
  final Iterable<Type> types = const [ZScannerMetaError, _$ZScannerMetaError];

  @override
  final String wireName = r'ZScannerMetaError';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerMetaError object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.metaId != null) {
      yield r'metaId';
      yield serializers.serialize(
        object.metaId,
        specifiedType: const FullType(String),
      );
    }
    if (object.exception != null) {
      yield r'exception';
      yield serializers.serialize(
        object.exception,
        specifiedType: const FullType(String),
      );
    }
    if (object.error != null) {
      yield r'error';
      yield serializers.serialize(
        object.error,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerMetaError object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerMetaErrorBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'metaId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.metaId = valueDes;
          break;
        case r'exception':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.exception = valueDes;
          break;
        case r'error':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.error = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerMetaError deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerMetaErrorBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

