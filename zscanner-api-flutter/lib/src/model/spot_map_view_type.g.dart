// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'spot_map_view_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const SpotMapViewType _$IMAGE = const SpotMapViewType._('IMAGE');
const SpotMapViewType _$LIST = const SpotMapViewType._('LIST');
const SpotMapViewType _$unknown = const SpotMapViewType._('unknown');

SpotMapViewType _$valueOf(String name) {
  switch (name) {
    case 'IMAGE':
      return _$IMAGE;
    case 'LIST':
      return _$LIST;
    case 'unknown':
      return _$unknown;
    default:
      return _$unknown;
  }
}

final BuiltSet<SpotMapViewType> _$values =
    new BuiltSet<SpotMapViewType>(const <SpotMapViewType>[
  _$IMAGE,
  _$LIST,
  _$unknown,
]);

class _$SpotMapViewTypeMeta {
  const _$SpotMapViewTypeMeta();
  SpotMapViewType get IMAGE => _$IMAGE;
  SpotMapViewType get LIST => _$LIST;
  SpotMapViewType get unknown => _$unknown;
  SpotMapViewType valueOf(String name) => _$valueOf(name);
  BuiltSet<SpotMapViewType> get values => _$values;
}

abstract class _$SpotMapViewTypeMixin {
  // ignore: non_constant_identifier_names
  _$SpotMapViewTypeMeta get SpotMapViewType => const _$SpotMapViewTypeMeta();
}

Serializer<SpotMapViewType> _$spotMapViewTypeSerializer =
    new _$SpotMapViewTypeSerializer();

class _$SpotMapViewTypeSerializer
    implements PrimitiveSerializer<SpotMapViewType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'IMAGE': 'IMAGE',
    'LIST': 'LIST',
    'unknown': '____unknown____',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'IMAGE': 'IMAGE',
    'LIST': 'LIST',
    '____unknown____': 'unknown',
  };

  @override
  final Iterable<Type> types = const <Type>[SpotMapViewType];
  @override
  final String wireName = 'SpotMapViewType';

  @override
  Object serialize(Serializers serializers, SpotMapViewType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  SpotMapViewType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      SpotMapViewType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
