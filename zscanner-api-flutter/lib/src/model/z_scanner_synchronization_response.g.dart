// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_synchronization_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerSynchronizationResponse
    extends ZScannerSynchronizationResponse {
  @override
  final String nextState;
  @override
  final bool metadataValid;
  @override
  final ZScannerError? error;
  @override
  final bool? blockApplication;
  @override
  final BuiltList<String>? supportedCaps;
  @override
  final ZScannerConfig? config;
  @override
  final ZScannerUser? me;
  @override
  final BuiltList<ZScannerMetadataSchemaItem>? metadataSchema;
  @override
  final BuiltList<ZScannerMetadataSchemaItem>? folderSchema;
  @override
  final BuiltList<ZScannerMetadataSchemaItem>? createFolderSchema;
  @override
  final BuiltList<ZScannerSubfolderSchema>? subfolderSchemata;
  @override
  final BuiltList<ZScannerFlow>? flows;
  @override
  final BuiltList<ZScannerEnum>? updatedEnums;
  @override
  final BuiltList<ZScannerResource>? updatedResources;

  factory _$ZScannerSynchronizationResponse(
          [void Function(ZScannerSynchronizationResponseBuilder)? updates]) =>
      (new ZScannerSynchronizationResponseBuilder()..update(updates))._build();

  _$ZScannerSynchronizationResponse._(
      {required this.nextState,
      required this.metadataValid,
      this.error,
      this.blockApplication,
      this.supportedCaps,
      this.config,
      this.me,
      this.metadataSchema,
      this.folderSchema,
      this.createFolderSchema,
      this.subfolderSchemata,
      this.flows,
      this.updatedEnums,
      this.updatedResources})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        nextState, r'ZScannerSynchronizationResponse', 'nextState');
    BuiltValueNullFieldError.checkNotNull(
        metadataValid, r'ZScannerSynchronizationResponse', 'metadataValid');
  }

  @override
  ZScannerSynchronizationResponse rebuild(
          void Function(ZScannerSynchronizationResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerSynchronizationResponseBuilder toBuilder() =>
      new ZScannerSynchronizationResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerSynchronizationResponse &&
        nextState == other.nextState &&
        metadataValid == other.metadataValid &&
        error == other.error &&
        blockApplication == other.blockApplication &&
        supportedCaps == other.supportedCaps &&
        config == other.config &&
        me == other.me &&
        metadataSchema == other.metadataSchema &&
        folderSchema == other.folderSchema &&
        createFolderSchema == other.createFolderSchema &&
        subfolderSchemata == other.subfolderSchemata &&
        flows == other.flows &&
        updatedEnums == other.updatedEnums &&
        updatedResources == other.updatedResources;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, nextState.hashCode);
    _$hash = $jc(_$hash, metadataValid.hashCode);
    _$hash = $jc(_$hash, error.hashCode);
    _$hash = $jc(_$hash, blockApplication.hashCode);
    _$hash = $jc(_$hash, supportedCaps.hashCode);
    _$hash = $jc(_$hash, config.hashCode);
    _$hash = $jc(_$hash, me.hashCode);
    _$hash = $jc(_$hash, metadataSchema.hashCode);
    _$hash = $jc(_$hash, folderSchema.hashCode);
    _$hash = $jc(_$hash, createFolderSchema.hashCode);
    _$hash = $jc(_$hash, subfolderSchemata.hashCode);
    _$hash = $jc(_$hash, flows.hashCode);
    _$hash = $jc(_$hash, updatedEnums.hashCode);
    _$hash = $jc(_$hash, updatedResources.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerSynchronizationResponse')
          ..add('nextState', nextState)
          ..add('metadataValid', metadataValid)
          ..add('error', error)
          ..add('blockApplication', blockApplication)
          ..add('supportedCaps', supportedCaps)
          ..add('config', config)
          ..add('me', me)
          ..add('metadataSchema', metadataSchema)
          ..add('folderSchema', folderSchema)
          ..add('createFolderSchema', createFolderSchema)
          ..add('subfolderSchemata', subfolderSchemata)
          ..add('flows', flows)
          ..add('updatedEnums', updatedEnums)
          ..add('updatedResources', updatedResources))
        .toString();
  }
}

class ZScannerSynchronizationResponseBuilder
    implements
        Builder<ZScannerSynchronizationResponse,
            ZScannerSynchronizationResponseBuilder> {
  _$ZScannerSynchronizationResponse? _$v;

  String? _nextState;
  String? get nextState => _$this._nextState;
  set nextState(String? nextState) => _$this._nextState = nextState;

  bool? _metadataValid;
  bool? get metadataValid => _$this._metadataValid;
  set metadataValid(bool? metadataValid) =>
      _$this._metadataValid = metadataValid;

  ZScannerErrorBuilder? _error;
  ZScannerErrorBuilder get error =>
      _$this._error ??= new ZScannerErrorBuilder();
  set error(ZScannerErrorBuilder? error) => _$this._error = error;

  bool? _blockApplication;
  bool? get blockApplication => _$this._blockApplication;
  set blockApplication(bool? blockApplication) =>
      _$this._blockApplication = blockApplication;

  ListBuilder<String>? _supportedCaps;
  ListBuilder<String> get supportedCaps =>
      _$this._supportedCaps ??= new ListBuilder<String>();
  set supportedCaps(ListBuilder<String>? supportedCaps) =>
      _$this._supportedCaps = supportedCaps;

  ZScannerConfigBuilder? _config;
  ZScannerConfigBuilder get config =>
      _$this._config ??= new ZScannerConfigBuilder();
  set config(ZScannerConfigBuilder? config) => _$this._config = config;

  ZScannerUserBuilder? _me;
  ZScannerUserBuilder get me => _$this._me ??= new ZScannerUserBuilder();
  set me(ZScannerUserBuilder? me) => _$this._me = me;

  ListBuilder<ZScannerMetadataSchemaItem>? _metadataSchema;
  ListBuilder<ZScannerMetadataSchemaItem> get metadataSchema =>
      _$this._metadataSchema ??= new ListBuilder<ZScannerMetadataSchemaItem>();
  set metadataSchema(ListBuilder<ZScannerMetadataSchemaItem>? metadataSchema) =>
      _$this._metadataSchema = metadataSchema;

  ListBuilder<ZScannerMetadataSchemaItem>? _folderSchema;
  ListBuilder<ZScannerMetadataSchemaItem> get folderSchema =>
      _$this._folderSchema ??= new ListBuilder<ZScannerMetadataSchemaItem>();
  set folderSchema(ListBuilder<ZScannerMetadataSchemaItem>? folderSchema) =>
      _$this._folderSchema = folderSchema;

  ListBuilder<ZScannerMetadataSchemaItem>? _createFolderSchema;
  ListBuilder<ZScannerMetadataSchemaItem> get createFolderSchema =>
      _$this._createFolderSchema ??=
          new ListBuilder<ZScannerMetadataSchemaItem>();
  set createFolderSchema(
          ListBuilder<ZScannerMetadataSchemaItem>? createFolderSchema) =>
      _$this._createFolderSchema = createFolderSchema;

  ListBuilder<ZScannerSubfolderSchema>? _subfolderSchemata;
  ListBuilder<ZScannerSubfolderSchema> get subfolderSchemata =>
      _$this._subfolderSchemata ??= new ListBuilder<ZScannerSubfolderSchema>();
  set subfolderSchemata(
          ListBuilder<ZScannerSubfolderSchema>? subfolderSchemata) =>
      _$this._subfolderSchemata = subfolderSchemata;

  ListBuilder<ZScannerFlow>? _flows;
  ListBuilder<ZScannerFlow> get flows =>
      _$this._flows ??= new ListBuilder<ZScannerFlow>();
  set flows(ListBuilder<ZScannerFlow>? flows) => _$this._flows = flows;

  ListBuilder<ZScannerEnum>? _updatedEnums;
  ListBuilder<ZScannerEnum> get updatedEnums =>
      _$this._updatedEnums ??= new ListBuilder<ZScannerEnum>();
  set updatedEnums(ListBuilder<ZScannerEnum>? updatedEnums) =>
      _$this._updatedEnums = updatedEnums;

  ListBuilder<ZScannerResource>? _updatedResources;
  ListBuilder<ZScannerResource> get updatedResources =>
      _$this._updatedResources ??= new ListBuilder<ZScannerResource>();
  set updatedResources(ListBuilder<ZScannerResource>? updatedResources) =>
      _$this._updatedResources = updatedResources;

  ZScannerSynchronizationResponseBuilder() {
    ZScannerSynchronizationResponse._defaults(this);
  }

  ZScannerSynchronizationResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _nextState = $v.nextState;
      _metadataValid = $v.metadataValid;
      _error = $v.error?.toBuilder();
      _blockApplication = $v.blockApplication;
      _supportedCaps = $v.supportedCaps?.toBuilder();
      _config = $v.config?.toBuilder();
      _me = $v.me?.toBuilder();
      _metadataSchema = $v.metadataSchema?.toBuilder();
      _folderSchema = $v.folderSchema?.toBuilder();
      _createFolderSchema = $v.createFolderSchema?.toBuilder();
      _subfolderSchemata = $v.subfolderSchemata?.toBuilder();
      _flows = $v.flows?.toBuilder();
      _updatedEnums = $v.updatedEnums?.toBuilder();
      _updatedResources = $v.updatedResources?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerSynchronizationResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerSynchronizationResponse;
  }

  @override
  void update(void Function(ZScannerSynchronizationResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerSynchronizationResponse build() => _build();

  _$ZScannerSynchronizationResponse _build() {
    _$ZScannerSynchronizationResponse _$result;
    try {
      _$result = _$v ??
          new _$ZScannerSynchronizationResponse._(
              nextState: BuiltValueNullFieldError.checkNotNull(
                  nextState, r'ZScannerSynchronizationResponse', 'nextState'),
              metadataValid: BuiltValueNullFieldError.checkNotNull(
                  metadataValid,
                  r'ZScannerSynchronizationResponse',
                  'metadataValid'),
              error: _error?.build(),
              blockApplication: blockApplication,
              supportedCaps: _supportedCaps?.build(),
              config: _config?.build(),
              me: _me?.build(),
              metadataSchema: _metadataSchema?.build(),
              folderSchema: _folderSchema?.build(),
              createFolderSchema: _createFolderSchema?.build(),
              subfolderSchemata: _subfolderSchemata?.build(),
              flows: _flows?.build(),
              updatedEnums: _updatedEnums?.build(),
              updatedResources: _updatedResources?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'error';
        _error?.build();

        _$failedField = 'supportedCaps';
        _supportedCaps?.build();
        _$failedField = 'config';
        _config?.build();
        _$failedField = 'me';
        _me?.build();
        _$failedField = 'metadataSchema';
        _metadataSchema?.build();
        _$failedField = 'folderSchema';
        _folderSchema?.build();
        _$failedField = 'createFolderSchema';
        _createFolderSchema?.build();
        _$failedField = 'subfolderSchemata';
        _subfolderSchemata?.build();
        _$failedField = 'flows';
        _flows?.build();
        _$failedField = 'updatedEnums';
        _updatedEnums?.build();
        _$failedField = 'updatedResources';
        _updatedResources?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerSynchronizationResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
