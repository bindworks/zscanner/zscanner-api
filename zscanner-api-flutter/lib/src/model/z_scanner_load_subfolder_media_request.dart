//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_metadata_item.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_load_subfolder_media_request.g.dart';

/// ZScannerLoadSubfolderMediaRequest
///
/// Properties:
/// * [metadata] 
@BuiltValue()
abstract class ZScannerLoadSubfolderMediaRequest implements Built<ZScannerLoadSubfolderMediaRequest, ZScannerLoadSubfolderMediaRequestBuilder> {
  @BuiltValueField(wireName: r'metadata')
  BuiltList<ZScannerMetadataItem> get metadata;

  ZScannerLoadSubfolderMediaRequest._();

  factory ZScannerLoadSubfolderMediaRequest([void updates(ZScannerLoadSubfolderMediaRequestBuilder b)]) = _$ZScannerLoadSubfolderMediaRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerLoadSubfolderMediaRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerLoadSubfolderMediaRequest> get serializer => _$ZScannerLoadSubfolderMediaRequestSerializer();
}

class _$ZScannerLoadSubfolderMediaRequestSerializer implements PrimitiveSerializer<ZScannerLoadSubfolderMediaRequest> {
  @override
  final Iterable<Type> types = const [ZScannerLoadSubfolderMediaRequest, _$ZScannerLoadSubfolderMediaRequest];

  @override
  final String wireName = r'ZScannerLoadSubfolderMediaRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerLoadSubfolderMediaRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'metadata';
    yield serializers.serialize(
      object.metadata,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerLoadSubfolderMediaRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerLoadSubfolderMediaRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'metadata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
          ) as BuiltList<ZScannerMetadataItem>;
          result.metadata.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerLoadSubfolderMediaRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerLoadSubfolderMediaRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

