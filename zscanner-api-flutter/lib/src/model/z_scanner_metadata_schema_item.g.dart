// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_metadata_schema_item.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerMetadataSchemaItem extends ZScannerMetadataSchemaItem {
  @override
  final String metaId;
  @override
  final String label;
  @override
  final bool? hidden;
  @override
  final String? hint;
  @override
  final String type;
  @override
  final bool? required_;
  @override
  final String? enum_;
  @override
  final ZScannerMetadataDefaultValue? default_;
  @override
  final String? widgetId;
  @override
  final ZScannerWidgetConfigUnion? widgetConfig;
  @override
  final BuiltList<ZScannerMetadataValidator>? validators;
  @override
  final bool? serverOnly;

  factory _$ZScannerMetadataSchemaItem(
          [void Function(ZScannerMetadataSchemaItemBuilder)? updates]) =>
      (new ZScannerMetadataSchemaItemBuilder()..update(updates))._build();

  _$ZScannerMetadataSchemaItem._(
      {required this.metaId,
      required this.label,
      this.hidden,
      this.hint,
      required this.type,
      this.required_,
      this.enum_,
      this.default_,
      this.widgetId,
      this.widgetConfig,
      this.validators,
      this.serverOnly})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        metaId, r'ZScannerMetadataSchemaItem', 'metaId');
    BuiltValueNullFieldError.checkNotNull(
        label, r'ZScannerMetadataSchemaItem', 'label');
    BuiltValueNullFieldError.checkNotNull(
        type, r'ZScannerMetadataSchemaItem', 'type');
  }

  @override
  ZScannerMetadataSchemaItem rebuild(
          void Function(ZScannerMetadataSchemaItemBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerMetadataSchemaItemBuilder toBuilder() =>
      new ZScannerMetadataSchemaItemBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerMetadataSchemaItem &&
        metaId == other.metaId &&
        label == other.label &&
        hidden == other.hidden &&
        hint == other.hint &&
        type == other.type &&
        required_ == other.required_ &&
        enum_ == other.enum_ &&
        default_ == other.default_ &&
        widgetId == other.widgetId &&
        widgetConfig == other.widgetConfig &&
        validators == other.validators &&
        serverOnly == other.serverOnly;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, metaId.hashCode);
    _$hash = $jc(_$hash, label.hashCode);
    _$hash = $jc(_$hash, hidden.hashCode);
    _$hash = $jc(_$hash, hint.hashCode);
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, required_.hashCode);
    _$hash = $jc(_$hash, enum_.hashCode);
    _$hash = $jc(_$hash, default_.hashCode);
    _$hash = $jc(_$hash, widgetId.hashCode);
    _$hash = $jc(_$hash, widgetConfig.hashCode);
    _$hash = $jc(_$hash, validators.hashCode);
    _$hash = $jc(_$hash, serverOnly.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerMetadataSchemaItem')
          ..add('metaId', metaId)
          ..add('label', label)
          ..add('hidden', hidden)
          ..add('hint', hint)
          ..add('type', type)
          ..add('required_', required_)
          ..add('enum_', enum_)
          ..add('default_', default_)
          ..add('widgetId', widgetId)
          ..add('widgetConfig', widgetConfig)
          ..add('validators', validators)
          ..add('serverOnly', serverOnly))
        .toString();
  }
}

class ZScannerMetadataSchemaItemBuilder
    implements
        Builder<ZScannerMetadataSchemaItem, ZScannerMetadataSchemaItemBuilder> {
  _$ZScannerMetadataSchemaItem? _$v;

  String? _metaId;
  String? get metaId => _$this._metaId;
  set metaId(String? metaId) => _$this._metaId = metaId;

  String? _label;
  String? get label => _$this._label;
  set label(String? label) => _$this._label = label;

  bool? _hidden;
  bool? get hidden => _$this._hidden;
  set hidden(bool? hidden) => _$this._hidden = hidden;

  String? _hint;
  String? get hint => _$this._hint;
  set hint(String? hint) => _$this._hint = hint;

  String? _type;
  String? get type => _$this._type;
  set type(String? type) => _$this._type = type;

  bool? _required_;
  bool? get required_ => _$this._required_;
  set required_(bool? required_) => _$this._required_ = required_;

  String? _enum_;
  String? get enum_ => _$this._enum_;
  set enum_(String? enum_) => _$this._enum_ = enum_;

  ZScannerMetadataDefaultValueBuilder? _default_;
  ZScannerMetadataDefaultValueBuilder get default_ =>
      _$this._default_ ??= new ZScannerMetadataDefaultValueBuilder();
  set default_(ZScannerMetadataDefaultValueBuilder? default_) =>
      _$this._default_ = default_;

  String? _widgetId;
  String? get widgetId => _$this._widgetId;
  set widgetId(String? widgetId) => _$this._widgetId = widgetId;

  ZScannerWidgetConfigUnionBuilder? _widgetConfig;
  ZScannerWidgetConfigUnionBuilder get widgetConfig =>
      _$this._widgetConfig ??= new ZScannerWidgetConfigUnionBuilder();
  set widgetConfig(ZScannerWidgetConfigUnionBuilder? widgetConfig) =>
      _$this._widgetConfig = widgetConfig;

  ListBuilder<ZScannerMetadataValidator>? _validators;
  ListBuilder<ZScannerMetadataValidator> get validators =>
      _$this._validators ??= new ListBuilder<ZScannerMetadataValidator>();
  set validators(ListBuilder<ZScannerMetadataValidator>? validators) =>
      _$this._validators = validators;

  bool? _serverOnly;
  bool? get serverOnly => _$this._serverOnly;
  set serverOnly(bool? serverOnly) => _$this._serverOnly = serverOnly;

  ZScannerMetadataSchemaItemBuilder() {
    ZScannerMetadataSchemaItem._defaults(this);
  }

  ZScannerMetadataSchemaItemBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _metaId = $v.metaId;
      _label = $v.label;
      _hidden = $v.hidden;
      _hint = $v.hint;
      _type = $v.type;
      _required_ = $v.required_;
      _enum_ = $v.enum_;
      _default_ = $v.default_?.toBuilder();
      _widgetId = $v.widgetId;
      _widgetConfig = $v.widgetConfig?.toBuilder();
      _validators = $v.validators?.toBuilder();
      _serverOnly = $v.serverOnly;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerMetadataSchemaItem other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerMetadataSchemaItem;
  }

  @override
  void update(void Function(ZScannerMetadataSchemaItemBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerMetadataSchemaItem build() => _build();

  _$ZScannerMetadataSchemaItem _build() {
    _$ZScannerMetadataSchemaItem _$result;
    try {
      _$result = _$v ??
          new _$ZScannerMetadataSchemaItem._(
              metaId: BuiltValueNullFieldError.checkNotNull(
                  metaId, r'ZScannerMetadataSchemaItem', 'metaId'),
              label: BuiltValueNullFieldError.checkNotNull(
                  label, r'ZScannerMetadataSchemaItem', 'label'),
              hidden: hidden,
              hint: hint,
              type: BuiltValueNullFieldError.checkNotNull(
                  type, r'ZScannerMetadataSchemaItem', 'type'),
              required_: required_,
              enum_: enum_,
              default_: _default_?.build(),
              widgetId: widgetId,
              widgetConfig: _widgetConfig?.build(),
              validators: _validators?.build(),
              serverOnly: serverOnly);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'default_';
        _default_?.build();

        _$failedField = 'widgetConfig';
        _widgetConfig?.build();
        _$failedField = 'validators';
        _validators?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerMetadataSchemaItem', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
