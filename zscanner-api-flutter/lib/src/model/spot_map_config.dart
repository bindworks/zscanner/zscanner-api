//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/spot_map_spot_config.dart';
import 'package:zscanner_api/src/model/spot_map_view_config.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'spot_map_config.g.dart';

/// SpotMapConfig
///
/// Properties:
/// * [createdTs] 
/// * [updatedTs] 
/// * [spotMetaId] 
/// * [views] 
/// * [spots] 
@BuiltValue()
abstract class SpotMapConfig implements Built<SpotMapConfig, SpotMapConfigBuilder> {
  @BuiltValueField(wireName: r'createdTs')
  DateTime get createdTs;

  @BuiltValueField(wireName: r'updatedTs')
  DateTime get updatedTs;

  @BuiltValueField(wireName: r'spotMetaId')
  String get spotMetaId;

  @BuiltValueField(wireName: r'views')
  BuiltList<SpotMapViewConfig> get views;

  @BuiltValueField(wireName: r'spots')
  BuiltList<SpotMapSpotConfig> get spots;

  SpotMapConfig._();

  factory SpotMapConfig([void updates(SpotMapConfigBuilder b)]) = _$SpotMapConfig;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(SpotMapConfigBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<SpotMapConfig> get serializer => _$SpotMapConfigSerializer();
}

class _$SpotMapConfigSerializer implements PrimitiveSerializer<SpotMapConfig> {
  @override
  final Iterable<Type> types = const [SpotMapConfig, _$SpotMapConfig];

  @override
  final String wireName = r'SpotMapConfig';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    SpotMapConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'createdTs';
    yield serializers.serialize(
      object.createdTs,
      specifiedType: const FullType(DateTime),
    );
    yield r'updatedTs';
    yield serializers.serialize(
      object.updatedTs,
      specifiedType: const FullType(DateTime),
    );
    yield r'spotMetaId';
    yield serializers.serialize(
      object.spotMetaId,
      specifiedType: const FullType(String),
    );
    yield r'views';
    yield serializers.serialize(
      object.views,
      specifiedType: const FullType(BuiltList, [FullType(SpotMapViewConfig)]),
    );
    yield r'spots';
    yield serializers.serialize(
      object.spots,
      specifiedType: const FullType(BuiltList, [FullType(SpotMapSpotConfig)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    SpotMapConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required SpotMapConfigBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'createdTs':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdTs = valueDes;
          break;
        case r'updatedTs':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.updatedTs = valueDes;
          break;
        case r'spotMetaId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.spotMetaId = valueDes;
          break;
        case r'views':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(SpotMapViewConfig)]),
          ) as BuiltList<SpotMapViewConfig>;
          result.views.replace(valueDes);
          break;
        case r'spots':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(SpotMapSpotConfig)]),
          ) as BuiltList<SpotMapSpotConfig>;
          result.spots.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  SpotMapConfig deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = SpotMapConfigBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

