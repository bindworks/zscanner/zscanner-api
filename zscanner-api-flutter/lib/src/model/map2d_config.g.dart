// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map2d_config.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Map2dConfig extends Map2dConfig {
  @override
  final DateTime createdTs;
  @override
  final DateTime updatedTs;
  @override
  final String spotMetaId;
  @override
  final BuiltList<SpotMapViewConfig> views;

  factory _$Map2dConfig([void Function(Map2dConfigBuilder)? updates]) =>
      (new Map2dConfigBuilder()..update(updates))._build();

  _$Map2dConfig._(
      {required this.createdTs,
      required this.updatedTs,
      required this.spotMetaId,
      required this.views})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        createdTs, r'Map2dConfig', 'createdTs');
    BuiltValueNullFieldError.checkNotNull(
        updatedTs, r'Map2dConfig', 'updatedTs');
    BuiltValueNullFieldError.checkNotNull(
        spotMetaId, r'Map2dConfig', 'spotMetaId');
    BuiltValueNullFieldError.checkNotNull(views, r'Map2dConfig', 'views');
  }

  @override
  Map2dConfig rebuild(void Function(Map2dConfigBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  Map2dConfigBuilder toBuilder() => new Map2dConfigBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Map2dConfig &&
        createdTs == other.createdTs &&
        updatedTs == other.updatedTs &&
        spotMetaId == other.spotMetaId &&
        views == other.views;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, createdTs.hashCode);
    _$hash = $jc(_$hash, updatedTs.hashCode);
    _$hash = $jc(_$hash, spotMetaId.hashCode);
    _$hash = $jc(_$hash, views.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Map2dConfig')
          ..add('createdTs', createdTs)
          ..add('updatedTs', updatedTs)
          ..add('spotMetaId', spotMetaId)
          ..add('views', views))
        .toString();
  }
}

class Map2dConfigBuilder implements Builder<Map2dConfig, Map2dConfigBuilder> {
  _$Map2dConfig? _$v;

  DateTime? _createdTs;
  DateTime? get createdTs => _$this._createdTs;
  set createdTs(DateTime? createdTs) => _$this._createdTs = createdTs;

  DateTime? _updatedTs;
  DateTime? get updatedTs => _$this._updatedTs;
  set updatedTs(DateTime? updatedTs) => _$this._updatedTs = updatedTs;

  String? _spotMetaId;
  String? get spotMetaId => _$this._spotMetaId;
  set spotMetaId(String? spotMetaId) => _$this._spotMetaId = spotMetaId;

  ListBuilder<SpotMapViewConfig>? _views;
  ListBuilder<SpotMapViewConfig> get views =>
      _$this._views ??= new ListBuilder<SpotMapViewConfig>();
  set views(ListBuilder<SpotMapViewConfig>? views) => _$this._views = views;

  Map2dConfigBuilder() {
    Map2dConfig._defaults(this);
  }

  Map2dConfigBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _createdTs = $v.createdTs;
      _updatedTs = $v.updatedTs;
      _spotMetaId = $v.spotMetaId;
      _views = $v.views.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Map2dConfig other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Map2dConfig;
  }

  @override
  void update(void Function(Map2dConfigBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Map2dConfig build() => _build();

  _$Map2dConfig _build() {
    _$Map2dConfig _$result;
    try {
      _$result = _$v ??
          new _$Map2dConfig._(
              createdTs: BuiltValueNullFieldError.checkNotNull(
                  createdTs, r'Map2dConfig', 'createdTs'),
              updatedTs: BuiltValueNullFieldError.checkNotNull(
                  updatedTs, r'Map2dConfig', 'updatedTs'),
              spotMetaId: BuiltValueNullFieldError.checkNotNull(
                  spotMetaId, r'Map2dConfig', 'spotMetaId'),
              views: views.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'views';
        views.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Map2dConfig', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
