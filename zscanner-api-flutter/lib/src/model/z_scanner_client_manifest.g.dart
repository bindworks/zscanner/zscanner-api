// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_client_manifest.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerClientManifest extends ZScannerClientManifest {
  @override
  final BuiltList<String>? requiredCapabilities;

  factory _$ZScannerClientManifest(
          [void Function(ZScannerClientManifestBuilder)? updates]) =>
      (new ZScannerClientManifestBuilder()..update(updates))._build();

  _$ZScannerClientManifest._({this.requiredCapabilities}) : super._();

  @override
  ZScannerClientManifest rebuild(
          void Function(ZScannerClientManifestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerClientManifestBuilder toBuilder() =>
      new ZScannerClientManifestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerClientManifest &&
        requiredCapabilities == other.requiredCapabilities;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, requiredCapabilities.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerClientManifest')
          ..add('requiredCapabilities', requiredCapabilities))
        .toString();
  }
}

class ZScannerClientManifestBuilder
    implements Builder<ZScannerClientManifest, ZScannerClientManifestBuilder> {
  _$ZScannerClientManifest? _$v;

  ListBuilder<String>? _requiredCapabilities;
  ListBuilder<String> get requiredCapabilities =>
      _$this._requiredCapabilities ??= new ListBuilder<String>();
  set requiredCapabilities(ListBuilder<String>? requiredCapabilities) =>
      _$this._requiredCapabilities = requiredCapabilities;

  ZScannerClientManifestBuilder() {
    ZScannerClientManifest._defaults(this);
  }

  ZScannerClientManifestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _requiredCapabilities = $v.requiredCapabilities?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerClientManifest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerClientManifest;
  }

  @override
  void update(void Function(ZScannerClientManifestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerClientManifest build() => _build();

  _$ZScannerClientManifest _build() {
    _$ZScannerClientManifest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerClientManifest._(
              requiredCapabilities: _requiredCapabilities?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'requiredCapabilities';
        _requiredCapabilities?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerClientManifest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
