// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'spot_map_spot_config.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SpotMapSpotConfig extends SpotMapSpotConfig {
  @override
  final String spotId;
  @override
  final String label;
  @override
  final String viewId;
  @override
  final SpotMapSpotConfigCoordinates coordinates;

  factory _$SpotMapSpotConfig(
          [void Function(SpotMapSpotConfigBuilder)? updates]) =>
      (new SpotMapSpotConfigBuilder()..update(updates))._build();

  _$SpotMapSpotConfig._(
      {required this.spotId,
      required this.label,
      required this.viewId,
      required this.coordinates})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        spotId, r'SpotMapSpotConfig', 'spotId');
    BuiltValueNullFieldError.checkNotNull(label, r'SpotMapSpotConfig', 'label');
    BuiltValueNullFieldError.checkNotNull(
        viewId, r'SpotMapSpotConfig', 'viewId');
    BuiltValueNullFieldError.checkNotNull(
        coordinates, r'SpotMapSpotConfig', 'coordinates');
  }

  @override
  SpotMapSpotConfig rebuild(void Function(SpotMapSpotConfigBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SpotMapSpotConfigBuilder toBuilder() =>
      new SpotMapSpotConfigBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SpotMapSpotConfig &&
        spotId == other.spotId &&
        label == other.label &&
        viewId == other.viewId &&
        coordinates == other.coordinates;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, spotId.hashCode);
    _$hash = $jc(_$hash, label.hashCode);
    _$hash = $jc(_$hash, viewId.hashCode);
    _$hash = $jc(_$hash, coordinates.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SpotMapSpotConfig')
          ..add('spotId', spotId)
          ..add('label', label)
          ..add('viewId', viewId)
          ..add('coordinates', coordinates))
        .toString();
  }
}

class SpotMapSpotConfigBuilder
    implements Builder<SpotMapSpotConfig, SpotMapSpotConfigBuilder> {
  _$SpotMapSpotConfig? _$v;

  String? _spotId;
  String? get spotId => _$this._spotId;
  set spotId(String? spotId) => _$this._spotId = spotId;

  String? _label;
  String? get label => _$this._label;
  set label(String? label) => _$this._label = label;

  String? _viewId;
  String? get viewId => _$this._viewId;
  set viewId(String? viewId) => _$this._viewId = viewId;

  SpotMapSpotConfigCoordinatesBuilder? _coordinates;
  SpotMapSpotConfigCoordinatesBuilder get coordinates =>
      _$this._coordinates ??= new SpotMapSpotConfigCoordinatesBuilder();
  set coordinates(SpotMapSpotConfigCoordinatesBuilder? coordinates) =>
      _$this._coordinates = coordinates;

  SpotMapSpotConfigBuilder() {
    SpotMapSpotConfig._defaults(this);
  }

  SpotMapSpotConfigBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _spotId = $v.spotId;
      _label = $v.label;
      _viewId = $v.viewId;
      _coordinates = $v.coordinates.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SpotMapSpotConfig other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SpotMapSpotConfig;
  }

  @override
  void update(void Function(SpotMapSpotConfigBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SpotMapSpotConfig build() => _build();

  _$SpotMapSpotConfig _build() {
    _$SpotMapSpotConfig _$result;
    try {
      _$result = _$v ??
          new _$SpotMapSpotConfig._(
              spotId: BuiltValueNullFieldError.checkNotNull(
                  spotId, r'SpotMapSpotConfig', 'spotId'),
              label: BuiltValueNullFieldError.checkNotNull(
                  label, r'SpotMapSpotConfig', 'label'),
              viewId: BuiltValueNullFieldError.checkNotNull(
                  viewId, r'SpotMapSpotConfig', 'viewId'),
              coordinates: coordinates.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'coordinates';
        coordinates.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'SpotMapSpotConfig', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
