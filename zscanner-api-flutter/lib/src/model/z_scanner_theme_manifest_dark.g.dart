// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_theme_manifest_dark.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerThemeManifestDark extends ZScannerThemeManifestDark {
  @override
  final String? colorSchemePrimaryColor;
  @override
  final String? colorSchemeSecondaryColor;
  @override
  final int? textScaleFactor;
  @override
  final String? mainFontFamily;
  @override
  final String? secondaryFontFamily;

  factory _$ZScannerThemeManifestDark(
          [void Function(ZScannerThemeManifestDarkBuilder)? updates]) =>
      (new ZScannerThemeManifestDarkBuilder()..update(updates))._build();

  _$ZScannerThemeManifestDark._(
      {this.colorSchemePrimaryColor,
      this.colorSchemeSecondaryColor,
      this.textScaleFactor,
      this.mainFontFamily,
      this.secondaryFontFamily})
      : super._();

  @override
  ZScannerThemeManifestDark rebuild(
          void Function(ZScannerThemeManifestDarkBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerThemeManifestDarkBuilder toBuilder() =>
      new ZScannerThemeManifestDarkBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerThemeManifestDark &&
        colorSchemePrimaryColor == other.colorSchemePrimaryColor &&
        colorSchemeSecondaryColor == other.colorSchemeSecondaryColor &&
        textScaleFactor == other.textScaleFactor &&
        mainFontFamily == other.mainFontFamily &&
        secondaryFontFamily == other.secondaryFontFamily;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, colorSchemePrimaryColor.hashCode);
    _$hash = $jc(_$hash, colorSchemeSecondaryColor.hashCode);
    _$hash = $jc(_$hash, textScaleFactor.hashCode);
    _$hash = $jc(_$hash, mainFontFamily.hashCode);
    _$hash = $jc(_$hash, secondaryFontFamily.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerThemeManifestDark')
          ..add('colorSchemePrimaryColor', colorSchemePrimaryColor)
          ..add('colorSchemeSecondaryColor', colorSchemeSecondaryColor)
          ..add('textScaleFactor', textScaleFactor)
          ..add('mainFontFamily', mainFontFamily)
          ..add('secondaryFontFamily', secondaryFontFamily))
        .toString();
  }
}

class ZScannerThemeManifestDarkBuilder
    implements
        Builder<ZScannerThemeManifestDark, ZScannerThemeManifestDarkBuilder> {
  _$ZScannerThemeManifestDark? _$v;

  String? _colorSchemePrimaryColor;
  String? get colorSchemePrimaryColor => _$this._colorSchemePrimaryColor;
  set colorSchemePrimaryColor(String? colorSchemePrimaryColor) =>
      _$this._colorSchemePrimaryColor = colorSchemePrimaryColor;

  String? _colorSchemeSecondaryColor;
  String? get colorSchemeSecondaryColor => _$this._colorSchemeSecondaryColor;
  set colorSchemeSecondaryColor(String? colorSchemeSecondaryColor) =>
      _$this._colorSchemeSecondaryColor = colorSchemeSecondaryColor;

  int? _textScaleFactor;
  int? get textScaleFactor => _$this._textScaleFactor;
  set textScaleFactor(int? textScaleFactor) =>
      _$this._textScaleFactor = textScaleFactor;

  String? _mainFontFamily;
  String? get mainFontFamily => _$this._mainFontFamily;
  set mainFontFamily(String? mainFontFamily) =>
      _$this._mainFontFamily = mainFontFamily;

  String? _secondaryFontFamily;
  String? get secondaryFontFamily => _$this._secondaryFontFamily;
  set secondaryFontFamily(String? secondaryFontFamily) =>
      _$this._secondaryFontFamily = secondaryFontFamily;

  ZScannerThemeManifestDarkBuilder() {
    ZScannerThemeManifestDark._defaults(this);
  }

  ZScannerThemeManifestDarkBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _colorSchemePrimaryColor = $v.colorSchemePrimaryColor;
      _colorSchemeSecondaryColor = $v.colorSchemeSecondaryColor;
      _textScaleFactor = $v.textScaleFactor;
      _mainFontFamily = $v.mainFontFamily;
      _secondaryFontFamily = $v.secondaryFontFamily;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerThemeManifestDark other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerThemeManifestDark;
  }

  @override
  void update(void Function(ZScannerThemeManifestDarkBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerThemeManifestDark build() => _build();

  _$ZScannerThemeManifestDark _build() {
    final _$result = _$v ??
        new _$ZScannerThemeManifestDark._(
            colorSchemePrimaryColor: colorSchemePrimaryColor,
            colorSchemeSecondaryColor: colorSchemeSecondaryColor,
            textScaleFactor: textScaleFactor,
            mainFontFamily: mainFontFamily,
            secondaryFontFamily: secondaryFontFamily);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
