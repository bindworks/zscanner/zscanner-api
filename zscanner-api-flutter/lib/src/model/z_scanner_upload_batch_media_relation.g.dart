// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_upload_batch_media_relation.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerUploadBatchMediaRelation
    extends ZScannerUploadBatchMediaRelation {
  @override
  final String relatedMediaRole;
  @override
  final String relatedMediaId;

  factory _$ZScannerUploadBatchMediaRelation(
          [void Function(ZScannerUploadBatchMediaRelationBuilder)? updates]) =>
      (new ZScannerUploadBatchMediaRelationBuilder()..update(updates))._build();

  _$ZScannerUploadBatchMediaRelation._(
      {required this.relatedMediaRole, required this.relatedMediaId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(relatedMediaRole,
        r'ZScannerUploadBatchMediaRelation', 'relatedMediaRole');
    BuiltValueNullFieldError.checkNotNull(
        relatedMediaId, r'ZScannerUploadBatchMediaRelation', 'relatedMediaId');
  }

  @override
  ZScannerUploadBatchMediaRelation rebuild(
          void Function(ZScannerUploadBatchMediaRelationBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerUploadBatchMediaRelationBuilder toBuilder() =>
      new ZScannerUploadBatchMediaRelationBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerUploadBatchMediaRelation &&
        relatedMediaRole == other.relatedMediaRole &&
        relatedMediaId == other.relatedMediaId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, relatedMediaRole.hashCode);
    _$hash = $jc(_$hash, relatedMediaId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerUploadBatchMediaRelation')
          ..add('relatedMediaRole', relatedMediaRole)
          ..add('relatedMediaId', relatedMediaId))
        .toString();
  }
}

class ZScannerUploadBatchMediaRelationBuilder
    implements
        Builder<ZScannerUploadBatchMediaRelation,
            ZScannerUploadBatchMediaRelationBuilder> {
  _$ZScannerUploadBatchMediaRelation? _$v;

  String? _relatedMediaRole;
  String? get relatedMediaRole => _$this._relatedMediaRole;
  set relatedMediaRole(String? relatedMediaRole) =>
      _$this._relatedMediaRole = relatedMediaRole;

  String? _relatedMediaId;
  String? get relatedMediaId => _$this._relatedMediaId;
  set relatedMediaId(String? relatedMediaId) =>
      _$this._relatedMediaId = relatedMediaId;

  ZScannerUploadBatchMediaRelationBuilder() {
    ZScannerUploadBatchMediaRelation._defaults(this);
  }

  ZScannerUploadBatchMediaRelationBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _relatedMediaRole = $v.relatedMediaRole;
      _relatedMediaId = $v.relatedMediaId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerUploadBatchMediaRelation other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerUploadBatchMediaRelation;
  }

  @override
  void update(void Function(ZScannerUploadBatchMediaRelationBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerUploadBatchMediaRelation build() => _build();

  _$ZScannerUploadBatchMediaRelation _build() {
    final _$result = _$v ??
        new _$ZScannerUploadBatchMediaRelation._(
            relatedMediaRole: BuiltValueNullFieldError.checkNotNull(
                relatedMediaRole,
                r'ZScannerUploadBatchMediaRelation',
                'relatedMediaRole'),
            relatedMediaId: BuiltValueNullFieldError.checkNotNull(
                relatedMediaId,
                r'ZScannerUploadBatchMediaRelation',
                'relatedMediaId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
