//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_theme_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_server_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_authentication_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_client_manifest.dart';
import 'package:zscanner_api/src/model/z_scanner_application_manifest.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_installation_manifest.g.dart';

/// ZScannerInstallationManifest
///
/// Properties:
/// * [installationId] 
/// * [name] 
/// * [iconId] 
/// * [environment] 
/// * [authentication] 
/// * [server] 
/// * [client] 
/// * [application] 
/// * [theme] 
@BuiltValue()
abstract class ZScannerInstallationManifest implements Built<ZScannerInstallationManifest, ZScannerInstallationManifestBuilder> {
  @BuiltValueField(wireName: r'installationId')
  String get installationId;

  @BuiltValueField(wireName: r'name')
  String get name;

  @BuiltValueField(wireName: r'iconId')
  String? get iconId;

  @BuiltValueField(wireName: r'environment')
  ZScannerInstallationManifestEnvironmentEnum get environment;
  // enum environmentEnum {  development,  test,  staging,  production,  };

  @BuiltValueField(wireName: r'authentication')
  ZScannerAuthenticationManifest get authentication;

  @BuiltValueField(wireName: r'server')
  ZScannerServerManifest get server;

  @BuiltValueField(wireName: r'client')
  ZScannerClientManifest get client;

  @BuiltValueField(wireName: r'application')
  ZScannerApplicationManifest? get application;

  @BuiltValueField(wireName: r'theme')
  ZScannerThemeManifest? get theme;

  ZScannerInstallationManifest._();

  factory ZScannerInstallationManifest([void updates(ZScannerInstallationManifestBuilder b)]) = _$ZScannerInstallationManifest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerInstallationManifestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerInstallationManifest> get serializer => _$ZScannerInstallationManifestSerializer();
}

class _$ZScannerInstallationManifestSerializer implements PrimitiveSerializer<ZScannerInstallationManifest> {
  @override
  final Iterable<Type> types = const [ZScannerInstallationManifest, _$ZScannerInstallationManifest];

  @override
  final String wireName = r'ZScannerInstallationManifest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerInstallationManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'installationId';
    yield serializers.serialize(
      object.installationId,
      specifiedType: const FullType(String),
    );
    yield r'name';
    yield serializers.serialize(
      object.name,
      specifiedType: const FullType(String),
    );
    if (object.iconId != null) {
      yield r'iconId';
      yield serializers.serialize(
        object.iconId,
        specifiedType: const FullType(String),
      );
    }
    yield r'environment';
    yield serializers.serialize(
      object.environment,
      specifiedType: const FullType(ZScannerInstallationManifestEnvironmentEnum),
    );
    yield r'authentication';
    yield serializers.serialize(
      object.authentication,
      specifiedType: const FullType(ZScannerAuthenticationManifest),
    );
    yield r'server';
    yield serializers.serialize(
      object.server,
      specifiedType: const FullType(ZScannerServerManifest),
    );
    yield r'client';
    yield serializers.serialize(
      object.client,
      specifiedType: const FullType(ZScannerClientManifest),
    );
    if (object.application != null) {
      yield r'application';
      yield serializers.serialize(
        object.application,
        specifiedType: const FullType(ZScannerApplicationManifest),
      );
    }
    if (object.theme != null) {
      yield r'theme';
      yield serializers.serialize(
        object.theme,
        specifiedType: const FullType(ZScannerThemeManifest),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerInstallationManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerInstallationManifestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'installationId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.installationId = valueDes;
          break;
        case r'name':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.name = valueDes;
          break;
        case r'iconId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.iconId = valueDes;
          break;
        case r'environment':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerInstallationManifestEnvironmentEnum),
          ) as ZScannerInstallationManifestEnvironmentEnum;
          result.environment = valueDes;
          break;
        case r'authentication':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerAuthenticationManifest),
          ) as ZScannerAuthenticationManifest;
          result.authentication.replace(valueDes);
          break;
        case r'server':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerServerManifest),
          ) as ZScannerServerManifest;
          result.server.replace(valueDes);
          break;
        case r'client':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerClientManifest),
          ) as ZScannerClientManifest;
          result.client.replace(valueDes);
          break;
        case r'application':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerApplicationManifest),
          ) as ZScannerApplicationManifest;
          result.application.replace(valueDes);
          break;
        case r'theme':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerThemeManifest),
          ) as ZScannerThemeManifest;
          result.theme.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerInstallationManifest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerInstallationManifestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class ZScannerInstallationManifestEnvironmentEnum extends EnumClass {

  @BuiltValueEnumConst(wireName: r'development')
  static const ZScannerInstallationManifestEnvironmentEnum development = _$zScannerInstallationManifestEnvironmentEnum_development;
  @BuiltValueEnumConst(wireName: r'test')
  static const ZScannerInstallationManifestEnvironmentEnum test = _$zScannerInstallationManifestEnvironmentEnum_test;
  @BuiltValueEnumConst(wireName: r'staging')
  static const ZScannerInstallationManifestEnvironmentEnum staging = _$zScannerInstallationManifestEnvironmentEnum_staging;
  @BuiltValueEnumConst(wireName: r'production')
  static const ZScannerInstallationManifestEnvironmentEnum production = _$zScannerInstallationManifestEnvironmentEnum_production;

  @BuiltValueEnumConst(wireName: r'____unknown____', fallback: true)
  static const ZScannerInstallationManifestEnvironmentEnum unknown = _$zScannerInstallationManifestEnvironmentEnum_$unknown;

  static Serializer<ZScannerInstallationManifestEnvironmentEnum> get serializer => _$zScannerInstallationManifestEnvironmentEnumSerializer;

  const ZScannerInstallationManifestEnvironmentEnum._(String name): super(name);

  static BuiltSet<ZScannerInstallationManifestEnvironmentEnum> get values => _$zScannerInstallationManifestEnvironmentEnumValues;
  static ZScannerInstallationManifestEnvironmentEnum valueOf(String name) => _$zScannerInstallationManifestEnvironmentEnumValueOf(name);
}


