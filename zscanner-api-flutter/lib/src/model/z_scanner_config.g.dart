// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_config.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerConfig extends ZScannerConfig {
  @override
  final bool? folderHistoryDisabled;
  @override
  final bool? scannerEnabled;
  @override
  final BuiltList<String>? scannerTypes;
  @override
  final bool? createFolderEnabled;
  @override
  final bool? submissionConfirmationEnabled;

  factory _$ZScannerConfig([void Function(ZScannerConfigBuilder)? updates]) =>
      (new ZScannerConfigBuilder()..update(updates))._build();

  _$ZScannerConfig._(
      {this.folderHistoryDisabled,
      this.scannerEnabled,
      this.scannerTypes,
      this.createFolderEnabled,
      this.submissionConfirmationEnabled})
      : super._();

  @override
  ZScannerConfig rebuild(void Function(ZScannerConfigBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerConfigBuilder toBuilder() =>
      new ZScannerConfigBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerConfig &&
        folderHistoryDisabled == other.folderHistoryDisabled &&
        scannerEnabled == other.scannerEnabled &&
        scannerTypes == other.scannerTypes &&
        createFolderEnabled == other.createFolderEnabled &&
        submissionConfirmationEnabled == other.submissionConfirmationEnabled;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, folderHistoryDisabled.hashCode);
    _$hash = $jc(_$hash, scannerEnabled.hashCode);
    _$hash = $jc(_$hash, scannerTypes.hashCode);
    _$hash = $jc(_$hash, createFolderEnabled.hashCode);
    _$hash = $jc(_$hash, submissionConfirmationEnabled.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerConfig')
          ..add('folderHistoryDisabled', folderHistoryDisabled)
          ..add('scannerEnabled', scannerEnabled)
          ..add('scannerTypes', scannerTypes)
          ..add('createFolderEnabled', createFolderEnabled)
          ..add('submissionConfirmationEnabled', submissionConfirmationEnabled))
        .toString();
  }
}

class ZScannerConfigBuilder
    implements Builder<ZScannerConfig, ZScannerConfigBuilder> {
  _$ZScannerConfig? _$v;

  bool? _folderHistoryDisabled;
  bool? get folderHistoryDisabled => _$this._folderHistoryDisabled;
  set folderHistoryDisabled(bool? folderHistoryDisabled) =>
      _$this._folderHistoryDisabled = folderHistoryDisabled;

  bool? _scannerEnabled;
  bool? get scannerEnabled => _$this._scannerEnabled;
  set scannerEnabled(bool? scannerEnabled) =>
      _$this._scannerEnabled = scannerEnabled;

  ListBuilder<String>? _scannerTypes;
  ListBuilder<String> get scannerTypes =>
      _$this._scannerTypes ??= new ListBuilder<String>();
  set scannerTypes(ListBuilder<String>? scannerTypes) =>
      _$this._scannerTypes = scannerTypes;

  bool? _createFolderEnabled;
  bool? get createFolderEnabled => _$this._createFolderEnabled;
  set createFolderEnabled(bool? createFolderEnabled) =>
      _$this._createFolderEnabled = createFolderEnabled;

  bool? _submissionConfirmationEnabled;
  bool? get submissionConfirmationEnabled =>
      _$this._submissionConfirmationEnabled;
  set submissionConfirmationEnabled(bool? submissionConfirmationEnabled) =>
      _$this._submissionConfirmationEnabled = submissionConfirmationEnabled;

  ZScannerConfigBuilder() {
    ZScannerConfig._defaults(this);
  }

  ZScannerConfigBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _folderHistoryDisabled = $v.folderHistoryDisabled;
      _scannerEnabled = $v.scannerEnabled;
      _scannerTypes = $v.scannerTypes?.toBuilder();
      _createFolderEnabled = $v.createFolderEnabled;
      _submissionConfirmationEnabled = $v.submissionConfirmationEnabled;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerConfig other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerConfig;
  }

  @override
  void update(void Function(ZScannerConfigBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerConfig build() => _build();

  _$ZScannerConfig _build() {
    _$ZScannerConfig _$result;
    try {
      _$result = _$v ??
          new _$ZScannerConfig._(
              folderHistoryDisabled: folderHistoryDisabled,
              scannerEnabled: scannerEnabled,
              scannerTypes: _scannerTypes?.build(),
              createFolderEnabled: createFolderEnabled,
              submissionConfirmationEnabled: submissionConfirmationEnabled);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'scannerTypes';
        _scannerTypes?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerConfig', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
