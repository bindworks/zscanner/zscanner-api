//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_open_id_authentication_manifest.g.dart';

/// ZScannerOpenIDAuthenticationManifest
///
/// Properties:
/// * [manifestUrl] 
/// * [clientId] 
/// * [scopes] 
@BuiltValue()
abstract class ZScannerOpenIDAuthenticationManifest implements Built<ZScannerOpenIDAuthenticationManifest, ZScannerOpenIDAuthenticationManifestBuilder> {
  @BuiltValueField(wireName: r'manifestUrl')
  String get manifestUrl;

  @BuiltValueField(wireName: r'clientId')
  String get clientId;

  @BuiltValueField(wireName: r'scopes')
  String get scopes;

  ZScannerOpenIDAuthenticationManifest._();

  factory ZScannerOpenIDAuthenticationManifest([void updates(ZScannerOpenIDAuthenticationManifestBuilder b)]) = _$ZScannerOpenIDAuthenticationManifest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerOpenIDAuthenticationManifestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerOpenIDAuthenticationManifest> get serializer => _$ZScannerOpenIDAuthenticationManifestSerializer();
}

class _$ZScannerOpenIDAuthenticationManifestSerializer implements PrimitiveSerializer<ZScannerOpenIDAuthenticationManifest> {
  @override
  final Iterable<Type> types = const [ZScannerOpenIDAuthenticationManifest, _$ZScannerOpenIDAuthenticationManifest];

  @override
  final String wireName = r'ZScannerOpenIDAuthenticationManifest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerOpenIDAuthenticationManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'manifestUrl';
    yield serializers.serialize(
      object.manifestUrl,
      specifiedType: const FullType(String),
    );
    yield r'clientId';
    yield serializers.serialize(
      object.clientId,
      specifiedType: const FullType(String),
    );
    yield r'scopes';
    yield serializers.serialize(
      object.scopes,
      specifiedType: const FullType(String),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerOpenIDAuthenticationManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerOpenIDAuthenticationManifestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'manifestUrl':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.manifestUrl = valueDes;
          break;
        case r'clientId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.clientId = valueDes;
          break;
        case r'scopes':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.scopes = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerOpenIDAuthenticationManifest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerOpenIDAuthenticationManifestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

