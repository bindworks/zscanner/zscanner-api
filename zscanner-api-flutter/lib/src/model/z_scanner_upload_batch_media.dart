//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_metadata_item.dart';
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_upload_batch_media_relation.dart';
import 'package:zscanner_api/src/model/z_scanner_module_data_union.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_upload_batch_media.g.dart';

/// ZScannerUploadBatchMedia
///
/// Properties:
/// * [mediaId] 
/// * [contentType] 
/// * [contentLength] 
/// * [photoTs] 
/// * [metadata] 
/// * [subfolderTypeId] 
/// * [subfolderId] 
/// * [relations] 
/// * [moduleData] 
@BuiltValue()
abstract class ZScannerUploadBatchMedia implements Built<ZScannerUploadBatchMedia, ZScannerUploadBatchMediaBuilder> {
  @BuiltValueField(wireName: r'mediaId')
  String get mediaId;

  @BuiltValueField(wireName: r'contentType')
  String get contentType;

  @BuiltValueField(wireName: r'contentLength')
  int get contentLength;

  @BuiltValueField(wireName: r'photoTs')
  DateTime? get photoTs;

  @BuiltValueField(wireName: r'metadata')
  BuiltList<ZScannerMetadataItem>? get metadata;

  @BuiltValueField(wireName: r'subfolderTypeId')
  String? get subfolderTypeId;

  @BuiltValueField(wireName: r'subfolderId')
  String? get subfolderId;

  @BuiltValueField(wireName: r'relations')
  BuiltList<ZScannerUploadBatchMediaRelation>? get relations;

  @BuiltValueField(wireName: r'moduleData')
  ZScannerModuleDataUnion? get moduleData;

  ZScannerUploadBatchMedia._();

  factory ZScannerUploadBatchMedia([void updates(ZScannerUploadBatchMediaBuilder b)]) = _$ZScannerUploadBatchMedia;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerUploadBatchMediaBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerUploadBatchMedia> get serializer => _$ZScannerUploadBatchMediaSerializer();
}

class _$ZScannerUploadBatchMediaSerializer implements PrimitiveSerializer<ZScannerUploadBatchMedia> {
  @override
  final Iterable<Type> types = const [ZScannerUploadBatchMedia, _$ZScannerUploadBatchMedia];

  @override
  final String wireName = r'ZScannerUploadBatchMedia';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerUploadBatchMedia object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'mediaId';
    yield serializers.serialize(
      object.mediaId,
      specifiedType: const FullType(String),
    );
    yield r'contentType';
    yield serializers.serialize(
      object.contentType,
      specifiedType: const FullType(String),
    );
    yield r'contentLength';
    yield serializers.serialize(
      object.contentLength,
      specifiedType: const FullType(int),
    );
    if (object.photoTs != null) {
      yield r'photoTs';
      yield serializers.serialize(
        object.photoTs,
        specifiedType: const FullType(DateTime),
      );
    }
    if (object.metadata != null) {
      yield r'metadata';
      yield serializers.serialize(
        object.metadata,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
      );
    }
    if (object.subfolderTypeId != null) {
      yield r'subfolderTypeId';
      yield serializers.serialize(
        object.subfolderTypeId,
        specifiedType: const FullType(String),
      );
    }
    if (object.subfolderId != null) {
      yield r'subfolderId';
      yield serializers.serialize(
        object.subfolderId,
        specifiedType: const FullType(String),
      );
    }
    if (object.relations != null) {
      yield r'relations';
      yield serializers.serialize(
        object.relations,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerUploadBatchMediaRelation)]),
      );
    }
    if (object.moduleData != null) {
      yield r'moduleData';
      yield serializers.serialize(
        object.moduleData,
        specifiedType: const FullType(ZScannerModuleDataUnion),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerUploadBatchMedia object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerUploadBatchMediaBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'mediaId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.mediaId = valueDes;
          break;
        case r'contentType':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.contentType = valueDes;
          break;
        case r'contentLength':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.contentLength = valueDes;
          break;
        case r'photoTs':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.photoTs = valueDes;
          break;
        case r'metadata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
          ) as BuiltList<ZScannerMetadataItem>;
          result.metadata.replace(valueDes);
          break;
        case r'subfolderTypeId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.subfolderTypeId = valueDes;
          break;
        case r'subfolderId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.subfolderId = valueDes;
          break;
        case r'relations':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerUploadBatchMediaRelation)]),
          ) as BuiltList<ZScannerUploadBatchMediaRelation>;
          result.relations.replace(valueDes);
          break;
        case r'moduleData':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerModuleDataUnion),
          ) as ZScannerModuleDataUnion;
          result.moduleData.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerUploadBatchMedia deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerUploadBatchMediaBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

