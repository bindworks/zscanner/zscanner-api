//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/spot_map_view_config.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map2d_config.g.dart';

/// Map2dConfig
///
/// Properties:
/// * [createdTs] 
/// * [updatedTs] 
/// * [spotMetaId] 
/// * [views] 
@BuiltValue()
abstract class Map2dConfig implements Built<Map2dConfig, Map2dConfigBuilder> {
  @BuiltValueField(wireName: r'createdTs')
  DateTime get createdTs;

  @BuiltValueField(wireName: r'updatedTs')
  DateTime get updatedTs;

  @BuiltValueField(wireName: r'spotMetaId')
  String get spotMetaId;

  @BuiltValueField(wireName: r'views')
  BuiltList<SpotMapViewConfig> get views;

  Map2dConfig._();

  factory Map2dConfig([void updates(Map2dConfigBuilder b)]) = _$Map2dConfig;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(Map2dConfigBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<Map2dConfig> get serializer => _$Map2dConfigSerializer();
}

class _$Map2dConfigSerializer implements PrimitiveSerializer<Map2dConfig> {
  @override
  final Iterable<Type> types = const [Map2dConfig, _$Map2dConfig];

  @override
  final String wireName = r'Map2dConfig';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    Map2dConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'createdTs';
    yield serializers.serialize(
      object.createdTs,
      specifiedType: const FullType(DateTime),
    );
    yield r'updatedTs';
    yield serializers.serialize(
      object.updatedTs,
      specifiedType: const FullType(DateTime),
    );
    yield r'spotMetaId';
    yield serializers.serialize(
      object.spotMetaId,
      specifiedType: const FullType(String),
    );
    yield r'views';
    yield serializers.serialize(
      object.views,
      specifiedType: const FullType(BuiltList, [FullType(SpotMapViewConfig)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    Map2dConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required Map2dConfigBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'createdTs':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdTs = valueDes;
          break;
        case r'updatedTs':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.updatedTs = valueDes;
          break;
        case r'spotMetaId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.spotMetaId = valueDes;
          break;
        case r'views':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(SpotMapViewConfig)]),
          ) as BuiltList<SpotMapViewConfig>;
          result.views.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  Map2dConfig deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = Map2dConfigBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

