//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'spot_map_spot_config_coordinates.g.dart';

/// SpotMapSpotConfigCoordinates
///
/// Properties:
/// * [x] 
/// * [y] 
@BuiltValue()
abstract class SpotMapSpotConfigCoordinates implements Built<SpotMapSpotConfigCoordinates, SpotMapSpotConfigCoordinatesBuilder> {
  @BuiltValueField(wireName: r'x')
  double get x;

  @BuiltValueField(wireName: r'y')
  double get y;

  SpotMapSpotConfigCoordinates._();

  factory SpotMapSpotConfigCoordinates([void updates(SpotMapSpotConfigCoordinatesBuilder b)]) = _$SpotMapSpotConfigCoordinates;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(SpotMapSpotConfigCoordinatesBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<SpotMapSpotConfigCoordinates> get serializer => _$SpotMapSpotConfigCoordinatesSerializer();
}

class _$SpotMapSpotConfigCoordinatesSerializer implements PrimitiveSerializer<SpotMapSpotConfigCoordinates> {
  @override
  final Iterable<Type> types = const [SpotMapSpotConfigCoordinates, _$SpotMapSpotConfigCoordinates];

  @override
  final String wireName = r'SpotMapSpotConfigCoordinates';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    SpotMapSpotConfigCoordinates object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'x';
    yield serializers.serialize(
      object.x,
      specifiedType: const FullType(double),
    );
    yield r'y';
    yield serializers.serialize(
      object.y,
      specifiedType: const FullType(double),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    SpotMapSpotConfigCoordinates object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required SpotMapSpotConfigCoordinatesBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'x':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(double),
          ) as double;
          result.x = valueDes;
          break;
        case r'y':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(double),
          ) as double;
          result.y = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  SpotMapSpotConfigCoordinates deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = SpotMapSpotConfigCoordinatesBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

