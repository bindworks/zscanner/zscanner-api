// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_flow.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerFlow extends ZScannerFlow {
  @override
  final String flowId;
  @override
  final String label;
  @override
  final bool? hidden;
  @override
  final String? icon;
  @override
  final String moduleId;
  @override
  final String moduleVersion;
  @override
  final ZScannerModuleConfigUnion? moduleConfig;
  @override
  final BuiltList<ZScannerMetadataSchemaItem> metadataSchema;
  @override
  final BuiltList<ZScannerMetadataSchemaItem> mediaMetadataSchema;

  factory _$ZScannerFlow([void Function(ZScannerFlowBuilder)? updates]) =>
      (new ZScannerFlowBuilder()..update(updates))._build();

  _$ZScannerFlow._(
      {required this.flowId,
      required this.label,
      this.hidden,
      this.icon,
      required this.moduleId,
      required this.moduleVersion,
      this.moduleConfig,
      required this.metadataSchema,
      required this.mediaMetadataSchema})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(flowId, r'ZScannerFlow', 'flowId');
    BuiltValueNullFieldError.checkNotNull(label, r'ZScannerFlow', 'label');
    BuiltValueNullFieldError.checkNotNull(
        moduleId, r'ZScannerFlow', 'moduleId');
    BuiltValueNullFieldError.checkNotNull(
        moduleVersion, r'ZScannerFlow', 'moduleVersion');
    BuiltValueNullFieldError.checkNotNull(
        metadataSchema, r'ZScannerFlow', 'metadataSchema');
    BuiltValueNullFieldError.checkNotNull(
        mediaMetadataSchema, r'ZScannerFlow', 'mediaMetadataSchema');
  }

  @override
  ZScannerFlow rebuild(void Function(ZScannerFlowBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerFlowBuilder toBuilder() => new ZScannerFlowBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerFlow &&
        flowId == other.flowId &&
        label == other.label &&
        hidden == other.hidden &&
        icon == other.icon &&
        moduleId == other.moduleId &&
        moduleVersion == other.moduleVersion &&
        moduleConfig == other.moduleConfig &&
        metadataSchema == other.metadataSchema &&
        mediaMetadataSchema == other.mediaMetadataSchema;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, flowId.hashCode);
    _$hash = $jc(_$hash, label.hashCode);
    _$hash = $jc(_$hash, hidden.hashCode);
    _$hash = $jc(_$hash, icon.hashCode);
    _$hash = $jc(_$hash, moduleId.hashCode);
    _$hash = $jc(_$hash, moduleVersion.hashCode);
    _$hash = $jc(_$hash, moduleConfig.hashCode);
    _$hash = $jc(_$hash, metadataSchema.hashCode);
    _$hash = $jc(_$hash, mediaMetadataSchema.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerFlow')
          ..add('flowId', flowId)
          ..add('label', label)
          ..add('hidden', hidden)
          ..add('icon', icon)
          ..add('moduleId', moduleId)
          ..add('moduleVersion', moduleVersion)
          ..add('moduleConfig', moduleConfig)
          ..add('metadataSchema', metadataSchema)
          ..add('mediaMetadataSchema', mediaMetadataSchema))
        .toString();
  }
}

class ZScannerFlowBuilder
    implements Builder<ZScannerFlow, ZScannerFlowBuilder> {
  _$ZScannerFlow? _$v;

  String? _flowId;
  String? get flowId => _$this._flowId;
  set flowId(String? flowId) => _$this._flowId = flowId;

  String? _label;
  String? get label => _$this._label;
  set label(String? label) => _$this._label = label;

  bool? _hidden;
  bool? get hidden => _$this._hidden;
  set hidden(bool? hidden) => _$this._hidden = hidden;

  String? _icon;
  String? get icon => _$this._icon;
  set icon(String? icon) => _$this._icon = icon;

  String? _moduleId;
  String? get moduleId => _$this._moduleId;
  set moduleId(String? moduleId) => _$this._moduleId = moduleId;

  String? _moduleVersion;
  String? get moduleVersion => _$this._moduleVersion;
  set moduleVersion(String? moduleVersion) =>
      _$this._moduleVersion = moduleVersion;

  ZScannerModuleConfigUnionBuilder? _moduleConfig;
  ZScannerModuleConfigUnionBuilder get moduleConfig =>
      _$this._moduleConfig ??= new ZScannerModuleConfigUnionBuilder();
  set moduleConfig(ZScannerModuleConfigUnionBuilder? moduleConfig) =>
      _$this._moduleConfig = moduleConfig;

  ListBuilder<ZScannerMetadataSchemaItem>? _metadataSchema;
  ListBuilder<ZScannerMetadataSchemaItem> get metadataSchema =>
      _$this._metadataSchema ??= new ListBuilder<ZScannerMetadataSchemaItem>();
  set metadataSchema(ListBuilder<ZScannerMetadataSchemaItem>? metadataSchema) =>
      _$this._metadataSchema = metadataSchema;

  ListBuilder<ZScannerMetadataSchemaItem>? _mediaMetadataSchema;
  ListBuilder<ZScannerMetadataSchemaItem> get mediaMetadataSchema =>
      _$this._mediaMetadataSchema ??=
          new ListBuilder<ZScannerMetadataSchemaItem>();
  set mediaMetadataSchema(
          ListBuilder<ZScannerMetadataSchemaItem>? mediaMetadataSchema) =>
      _$this._mediaMetadataSchema = mediaMetadataSchema;

  ZScannerFlowBuilder() {
    ZScannerFlow._defaults(this);
  }

  ZScannerFlowBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _flowId = $v.flowId;
      _label = $v.label;
      _hidden = $v.hidden;
      _icon = $v.icon;
      _moduleId = $v.moduleId;
      _moduleVersion = $v.moduleVersion;
      _moduleConfig = $v.moduleConfig?.toBuilder();
      _metadataSchema = $v.metadataSchema.toBuilder();
      _mediaMetadataSchema = $v.mediaMetadataSchema.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerFlow other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerFlow;
  }

  @override
  void update(void Function(ZScannerFlowBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerFlow build() => _build();

  _$ZScannerFlow _build() {
    _$ZScannerFlow _$result;
    try {
      _$result = _$v ??
          new _$ZScannerFlow._(
              flowId: BuiltValueNullFieldError.checkNotNull(
                  flowId, r'ZScannerFlow', 'flowId'),
              label: BuiltValueNullFieldError.checkNotNull(
                  label, r'ZScannerFlow', 'label'),
              hidden: hidden,
              icon: icon,
              moduleId: BuiltValueNullFieldError.checkNotNull(
                  moduleId, r'ZScannerFlow', 'moduleId'),
              moduleVersion: BuiltValueNullFieldError.checkNotNull(
                  moduleVersion, r'ZScannerFlow', 'moduleVersion'),
              moduleConfig: _moduleConfig?.build(),
              metadataSchema: metadataSchema.build(),
              mediaMetadataSchema: mediaMetadataSchema.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'moduleConfig';
        _moduleConfig?.build();
        _$failedField = 'metadataSchema';
        metadataSchema.build();
        _$failedField = 'mediaMetadataSchema';
        mediaMetadataSchema.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerFlow', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
