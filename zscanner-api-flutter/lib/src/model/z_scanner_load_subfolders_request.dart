//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_metadata_item.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_load_subfolders_request.g.dart';

/// ZScannerLoadSubfoldersRequest
///
/// Properties:
/// * [metadata] 
/// * [typeId] 
@BuiltValue()
abstract class ZScannerLoadSubfoldersRequest implements Built<ZScannerLoadSubfoldersRequest, ZScannerLoadSubfoldersRequestBuilder> {
  @BuiltValueField(wireName: r'metadata')
  BuiltList<ZScannerMetadataItem> get metadata;

  @BuiltValueField(wireName: r'typeId')
  String? get typeId;

  ZScannerLoadSubfoldersRequest._();

  factory ZScannerLoadSubfoldersRequest([void updates(ZScannerLoadSubfoldersRequestBuilder b)]) = _$ZScannerLoadSubfoldersRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerLoadSubfoldersRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerLoadSubfoldersRequest> get serializer => _$ZScannerLoadSubfoldersRequestSerializer();
}

class _$ZScannerLoadSubfoldersRequestSerializer implements PrimitiveSerializer<ZScannerLoadSubfoldersRequest> {
  @override
  final Iterable<Type> types = const [ZScannerLoadSubfoldersRequest, _$ZScannerLoadSubfoldersRequest];

  @override
  final String wireName = r'ZScannerLoadSubfoldersRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerLoadSubfoldersRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'metadata';
    yield serializers.serialize(
      object.metadata,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
    );
    if (object.typeId != null) {
      yield r'typeId';
      yield serializers.serialize(
        object.typeId,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerLoadSubfoldersRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerLoadSubfoldersRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'metadata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
          ) as BuiltList<ZScannerMetadataItem>;
          result.metadata.replace(valueDes);
          break;
        case r'typeId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.typeId = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerLoadSubfoldersRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerLoadSubfoldersRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

