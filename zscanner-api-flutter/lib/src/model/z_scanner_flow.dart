//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_module_config_union.dart';
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_metadata_schema_item.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_flow.g.dart';

/// ZScannerFlow
///
/// Properties:
/// * [flowId] 
/// * [label] 
/// * [hidden] 
/// * [icon] - Icon can take several forms: - `str:...` plain string (takes advantage of emojis) - `material:...` material icon (by Flutter library code) - `res:...` specific resource by UUID 
/// * [moduleId] 
/// * [moduleVersion] - Specification according to https://pub.dev/documentation/pub_semver/latest/pub_semver/VersionConstraint/VersionConstraint.parse.html 
/// * [moduleConfig] 
/// * [metadataSchema] 
/// * [mediaMetadataSchema] 
@BuiltValue()
abstract class ZScannerFlow implements Built<ZScannerFlow, ZScannerFlowBuilder> {
  @BuiltValueField(wireName: r'flowId')
  String get flowId;

  @BuiltValueField(wireName: r'label')
  String get label;

  @BuiltValueField(wireName: r'hidden')
  bool? get hidden;

  /// Icon can take several forms: - `str:...` plain string (takes advantage of emojis) - `material:...` material icon (by Flutter library code) - `res:...` specific resource by UUID 
  @BuiltValueField(wireName: r'icon')
  String? get icon;

  @BuiltValueField(wireName: r'moduleId')
  String get moduleId;

  /// Specification according to https://pub.dev/documentation/pub_semver/latest/pub_semver/VersionConstraint/VersionConstraint.parse.html 
  @BuiltValueField(wireName: r'moduleVersion')
  String get moduleVersion;

  @BuiltValueField(wireName: r'moduleConfig')
  ZScannerModuleConfigUnion? get moduleConfig;

  @BuiltValueField(wireName: r'metadataSchema')
  BuiltList<ZScannerMetadataSchemaItem> get metadataSchema;

  @BuiltValueField(wireName: r'mediaMetadataSchema')
  BuiltList<ZScannerMetadataSchemaItem> get mediaMetadataSchema;

  ZScannerFlow._();

  factory ZScannerFlow([void updates(ZScannerFlowBuilder b)]) = _$ZScannerFlow;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerFlowBuilder b) => b
      ..hidden = false
      ..moduleVersion = 'any';

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerFlow> get serializer => _$ZScannerFlowSerializer();
}

class _$ZScannerFlowSerializer implements PrimitiveSerializer<ZScannerFlow> {
  @override
  final Iterable<Type> types = const [ZScannerFlow, _$ZScannerFlow];

  @override
  final String wireName = r'ZScannerFlow';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerFlow object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'flowId';
    yield serializers.serialize(
      object.flowId,
      specifiedType: const FullType(String),
    );
    yield r'label';
    yield serializers.serialize(
      object.label,
      specifiedType: const FullType(String),
    );
    if (object.hidden != null) {
      yield r'hidden';
      yield serializers.serialize(
        object.hidden,
        specifiedType: const FullType(bool),
      );
    }
    if (object.icon != null) {
      yield r'icon';
      yield serializers.serialize(
        object.icon,
        specifiedType: const FullType(String),
      );
    }
    yield r'moduleId';
    yield serializers.serialize(
      object.moduleId,
      specifiedType: const FullType(String),
    );
    yield r'moduleVersion';
    yield serializers.serialize(
      object.moduleVersion,
      specifiedType: const FullType(String),
    );
    if (object.moduleConfig != null) {
      yield r'moduleConfig';
      yield serializers.serialize(
        object.moduleConfig,
        specifiedType: const FullType(ZScannerModuleConfigUnion),
      );
    }
    yield r'metadataSchema';
    yield serializers.serialize(
      object.metadataSchema,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
    );
    yield r'mediaMetadataSchema';
    yield serializers.serialize(
      object.mediaMetadataSchema,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerFlow object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerFlowBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'flowId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.flowId = valueDes;
          break;
        case r'label':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.label = valueDes;
          break;
        case r'hidden':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.hidden = valueDes;
          break;
        case r'icon':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.icon = valueDes;
          break;
        case r'moduleId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.moduleId = valueDes;
          break;
        case r'moduleVersion':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.moduleVersion = valueDes;
          break;
        case r'moduleConfig':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerModuleConfigUnion),
          ) as ZScannerModuleConfigUnion;
          result.moduleConfig.replace(valueDes);
          break;
        case r'metadataSchema':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
          ) as BuiltList<ZScannerMetadataSchemaItem>;
          result.metadataSchema.replace(valueDes);
          break;
        case r'mediaMetadataSchema':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataSchemaItem)]),
          ) as BuiltList<ZScannerMetadataSchemaItem>;
          result.mediaMetadataSchema.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerFlow deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerFlowBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

