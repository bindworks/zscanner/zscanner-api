//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/spot_map_view_type.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'spot_map_view_config.g.dart';

/// SpotMapViewConfig
///
/// Properties:
/// * [parentSpotId] 
/// * [viewId] 
/// * [label] 
/// * [resourceId] 
/// * [type] 
@BuiltValue()
abstract class SpotMapViewConfig implements Built<SpotMapViewConfig, SpotMapViewConfigBuilder> {
  @BuiltValueField(wireName: r'parentSpotId')
  String? get parentSpotId;

  @BuiltValueField(wireName: r'viewId')
  String get viewId;

  @BuiltValueField(wireName: r'label')
  String get label;

  @BuiltValueField(wireName: r'resourceId')
  String get resourceId;

  @BuiltValueField(wireName: r'type')
  SpotMapViewType? get type;
  // enum typeEnum {  IMAGE,  LIST,  };

  SpotMapViewConfig._();

  factory SpotMapViewConfig([void updates(SpotMapViewConfigBuilder b)]) = _$SpotMapViewConfig;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(SpotMapViewConfigBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<SpotMapViewConfig> get serializer => _$SpotMapViewConfigSerializer();
}

class _$SpotMapViewConfigSerializer implements PrimitiveSerializer<SpotMapViewConfig> {
  @override
  final Iterable<Type> types = const [SpotMapViewConfig, _$SpotMapViewConfig];

  @override
  final String wireName = r'SpotMapViewConfig';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    SpotMapViewConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.parentSpotId != null) {
      yield r'parentSpotId';
      yield serializers.serialize(
        object.parentSpotId,
        specifiedType: const FullType(String),
      );
    }
    yield r'viewId';
    yield serializers.serialize(
      object.viewId,
      specifiedType: const FullType(String),
    );
    yield r'label';
    yield serializers.serialize(
      object.label,
      specifiedType: const FullType(String),
    );
    yield r'resourceId';
    yield serializers.serialize(
      object.resourceId,
      specifiedType: const FullType(String),
    );
    if (object.type != null) {
      yield r'type';
      yield serializers.serialize(
        object.type,
        specifiedType: const FullType(SpotMapViewType),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    SpotMapViewConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required SpotMapViewConfigBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'parentSpotId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.parentSpotId = valueDes;
          break;
        case r'viewId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.viewId = valueDes;
          break;
        case r'label':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.label = valueDes;
          break;
        case r'resourceId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.resourceId = valueDes;
          break;
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(SpotMapViewType),
          ) as SpotMapViewType;
          result.type = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  SpotMapViewConfig deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = SpotMapViewConfigBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

