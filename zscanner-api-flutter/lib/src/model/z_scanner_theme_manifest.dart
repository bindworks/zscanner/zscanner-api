//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_theme_manifest_dark.dart';
import 'package:zscanner_api/src/model/z_scanner_theme_manifest_light.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_theme_manifest.g.dart';

/// ZScannerThemeManifest
///
/// Properties:
/// * [logoUrl] 
/// * [secondaryLogoUrl] 
/// * [mainColor] 
/// * [mainLightColor] 
/// * [secondaryColor] 
/// * [backgroundColor] 
/// * [additionalColor] 
/// * [textFieldFillColor] 
/// * [outlineColor] 
/// * [shadowColor] 
/// * [textColor] 
/// * [fabForegroundColor] 
/// * [fabBackgroundColor] 
/// * [light] 
/// * [dark] 
@BuiltValue()
abstract class ZScannerThemeManifest implements Built<ZScannerThemeManifest, ZScannerThemeManifestBuilder> {
  @BuiltValueField(wireName: r'logoUrl')
  String? get logoUrl;

  @BuiltValueField(wireName: r'secondaryLogoUrl')
  String? get secondaryLogoUrl;

  @BuiltValueField(wireName: r'mainColor')
  String? get mainColor;

  @BuiltValueField(wireName: r'mainLightColor')
  String? get mainLightColor;

  @BuiltValueField(wireName: r'secondaryColor')
  String? get secondaryColor;

  @BuiltValueField(wireName: r'backgroundColor')
  String? get backgroundColor;

  @BuiltValueField(wireName: r'additionalColor')
  String? get additionalColor;

  @BuiltValueField(wireName: r'textFieldFillColor')
  String? get textFieldFillColor;

  @BuiltValueField(wireName: r'outlineColor')
  String? get outlineColor;

  @BuiltValueField(wireName: r'shadowColor')
  String? get shadowColor;

  @BuiltValueField(wireName: r'textColor')
  String? get textColor;

  @BuiltValueField(wireName: r'fabForegroundColor')
  String? get fabForegroundColor;

  @BuiltValueField(wireName: r'fabBackgroundColor')
  String? get fabBackgroundColor;

  @BuiltValueField(wireName: r'light')
  ZScannerThemeManifestLight? get light;

  @BuiltValueField(wireName: r'dark')
  ZScannerThemeManifestDark? get dark;

  ZScannerThemeManifest._();

  factory ZScannerThemeManifest([void updates(ZScannerThemeManifestBuilder b)]) = _$ZScannerThemeManifest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerThemeManifestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerThemeManifest> get serializer => _$ZScannerThemeManifestSerializer();
}

class _$ZScannerThemeManifestSerializer implements PrimitiveSerializer<ZScannerThemeManifest> {
  @override
  final Iterable<Type> types = const [ZScannerThemeManifest, _$ZScannerThemeManifest];

  @override
  final String wireName = r'ZScannerThemeManifest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerThemeManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.logoUrl != null) {
      yield r'logoUrl';
      yield serializers.serialize(
        object.logoUrl,
        specifiedType: const FullType(String),
      );
    }
    if (object.secondaryLogoUrl != null) {
      yield r'secondaryLogoUrl';
      yield serializers.serialize(
        object.secondaryLogoUrl,
        specifiedType: const FullType(String),
      );
    }
    if (object.mainColor != null) {
      yield r'mainColor';
      yield serializers.serialize(
        object.mainColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.mainLightColor != null) {
      yield r'mainLightColor';
      yield serializers.serialize(
        object.mainLightColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.secondaryColor != null) {
      yield r'secondaryColor';
      yield serializers.serialize(
        object.secondaryColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.backgroundColor != null) {
      yield r'backgroundColor';
      yield serializers.serialize(
        object.backgroundColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.additionalColor != null) {
      yield r'additionalColor';
      yield serializers.serialize(
        object.additionalColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.textFieldFillColor != null) {
      yield r'textFieldFillColor';
      yield serializers.serialize(
        object.textFieldFillColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.outlineColor != null) {
      yield r'outlineColor';
      yield serializers.serialize(
        object.outlineColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.shadowColor != null) {
      yield r'shadowColor';
      yield serializers.serialize(
        object.shadowColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.textColor != null) {
      yield r'textColor';
      yield serializers.serialize(
        object.textColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.fabForegroundColor != null) {
      yield r'fabForegroundColor';
      yield serializers.serialize(
        object.fabForegroundColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.fabBackgroundColor != null) {
      yield r'fabBackgroundColor';
      yield serializers.serialize(
        object.fabBackgroundColor,
        specifiedType: const FullType(String),
      );
    }
    if (object.light != null) {
      yield r'light';
      yield serializers.serialize(
        object.light,
        specifiedType: const FullType(ZScannerThemeManifestLight),
      );
    }
    if (object.dark != null) {
      yield r'dark';
      yield serializers.serialize(
        object.dark,
        specifiedType: const FullType(ZScannerThemeManifestDark),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerThemeManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerThemeManifestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'logoUrl':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.logoUrl = valueDes;
          break;
        case r'secondaryLogoUrl':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.secondaryLogoUrl = valueDes;
          break;
        case r'mainColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.mainColor = valueDes;
          break;
        case r'mainLightColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.mainLightColor = valueDes;
          break;
        case r'secondaryColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.secondaryColor = valueDes;
          break;
        case r'backgroundColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.backgroundColor = valueDes;
          break;
        case r'additionalColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.additionalColor = valueDes;
          break;
        case r'textFieldFillColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.textFieldFillColor = valueDes;
          break;
        case r'outlineColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.outlineColor = valueDes;
          break;
        case r'shadowColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.shadowColor = valueDes;
          break;
        case r'textColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.textColor = valueDes;
          break;
        case r'fabForegroundColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fabForegroundColor = valueDes;
          break;
        case r'fabBackgroundColor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fabBackgroundColor = valueDes;
          break;
        case r'light':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerThemeManifestLight),
          ) as ZScannerThemeManifestLight;
          result.light.replace(valueDes);
          break;
        case r'dark':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerThemeManifestDark),
          ) as ZScannerThemeManifestDark;
          result.dark.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerThemeManifest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerThemeManifestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

