// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_folder.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ZScannerFolderTypeEnum _$zScannerFolderTypeEnum_SUGGESTION =
    const ZScannerFolderTypeEnum._('SUGGESTION');
const ZScannerFolderTypeEnum _$zScannerFolderTypeEnum_RESULT =
    const ZScannerFolderTypeEnum._('RESULT');
const ZScannerFolderTypeEnum _$zScannerFolderTypeEnum_CREATE_TEMPLATE =
    const ZScannerFolderTypeEnum._('CREATE_TEMPLATE');
const ZScannerFolderTypeEnum _$zScannerFolderTypeEnum_$unknown =
    const ZScannerFolderTypeEnum._('unknown');

ZScannerFolderTypeEnum _$zScannerFolderTypeEnumValueOf(String name) {
  switch (name) {
    case 'SUGGESTION':
      return _$zScannerFolderTypeEnum_SUGGESTION;
    case 'RESULT':
      return _$zScannerFolderTypeEnum_RESULT;
    case 'CREATE_TEMPLATE':
      return _$zScannerFolderTypeEnum_CREATE_TEMPLATE;
    case 'unknown':
      return _$zScannerFolderTypeEnum_$unknown;
    default:
      return _$zScannerFolderTypeEnum_$unknown;
  }
}

final BuiltSet<ZScannerFolderTypeEnum> _$zScannerFolderTypeEnumValues =
    new BuiltSet<ZScannerFolderTypeEnum>(const <ZScannerFolderTypeEnum>[
  _$zScannerFolderTypeEnum_SUGGESTION,
  _$zScannerFolderTypeEnum_RESULT,
  _$zScannerFolderTypeEnum_CREATE_TEMPLATE,
  _$zScannerFolderTypeEnum_$unknown,
]);

Serializer<ZScannerFolderTypeEnum> _$zScannerFolderTypeEnumSerializer =
    new _$ZScannerFolderTypeEnumSerializer();

class _$ZScannerFolderTypeEnumSerializer
    implements PrimitiveSerializer<ZScannerFolderTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'SUGGESTION': 'SUGGESTION',
    'RESULT': 'RESULT',
    'CREATE_TEMPLATE': 'CREATE_TEMPLATE',
    'unknown': '____unknown____',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'SUGGESTION': 'SUGGESTION',
    'RESULT': 'RESULT',
    'CREATE_TEMPLATE': 'CREATE_TEMPLATE',
    '____unknown____': 'unknown',
  };

  @override
  final Iterable<Type> types = const <Type>[ZScannerFolderTypeEnum];
  @override
  final String wireName = 'ZScannerFolderTypeEnum';

  @override
  Object serialize(Serializers serializers, ZScannerFolderTypeEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ZScannerFolderTypeEnum deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ZScannerFolderTypeEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$ZScannerFolder extends ZScannerFolder {
  @override
  final String internalId;
  @override
  final String externalId;
  @override
  final String name;
  @override
  final ZScannerFolderTypeEnum type;
  @override
  final BuiltList<ZScannerMetadataItem>? metadata;

  factory _$ZScannerFolder([void Function(ZScannerFolderBuilder)? updates]) =>
      (new ZScannerFolderBuilder()..update(updates))._build();

  _$ZScannerFolder._(
      {required this.internalId,
      required this.externalId,
      required this.name,
      required this.type,
      this.metadata})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        internalId, r'ZScannerFolder', 'internalId');
    BuiltValueNullFieldError.checkNotNull(
        externalId, r'ZScannerFolder', 'externalId');
    BuiltValueNullFieldError.checkNotNull(name, r'ZScannerFolder', 'name');
    BuiltValueNullFieldError.checkNotNull(type, r'ZScannerFolder', 'type');
  }

  @override
  ZScannerFolder rebuild(void Function(ZScannerFolderBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerFolderBuilder toBuilder() =>
      new ZScannerFolderBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerFolder &&
        internalId == other.internalId &&
        externalId == other.externalId &&
        name == other.name &&
        type == other.type &&
        metadata == other.metadata;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, internalId.hashCode);
    _$hash = $jc(_$hash, externalId.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, metadata.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerFolder')
          ..add('internalId', internalId)
          ..add('externalId', externalId)
          ..add('name', name)
          ..add('type', type)
          ..add('metadata', metadata))
        .toString();
  }
}

class ZScannerFolderBuilder
    implements Builder<ZScannerFolder, ZScannerFolderBuilder> {
  _$ZScannerFolder? _$v;

  String? _internalId;
  String? get internalId => _$this._internalId;
  set internalId(String? internalId) => _$this._internalId = internalId;

  String? _externalId;
  String? get externalId => _$this._externalId;
  set externalId(String? externalId) => _$this._externalId = externalId;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  ZScannerFolderTypeEnum? _type;
  ZScannerFolderTypeEnum? get type => _$this._type;
  set type(ZScannerFolderTypeEnum? type) => _$this._type = type;

  ListBuilder<ZScannerMetadataItem>? _metadata;
  ListBuilder<ZScannerMetadataItem> get metadata =>
      _$this._metadata ??= new ListBuilder<ZScannerMetadataItem>();
  set metadata(ListBuilder<ZScannerMetadataItem>? metadata) =>
      _$this._metadata = metadata;

  ZScannerFolderBuilder() {
    ZScannerFolder._defaults(this);
  }

  ZScannerFolderBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _internalId = $v.internalId;
      _externalId = $v.externalId;
      _name = $v.name;
      _type = $v.type;
      _metadata = $v.metadata?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerFolder other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerFolder;
  }

  @override
  void update(void Function(ZScannerFolderBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerFolder build() => _build();

  _$ZScannerFolder _build() {
    _$ZScannerFolder _$result;
    try {
      _$result = _$v ??
          new _$ZScannerFolder._(
              internalId: BuiltValueNullFieldError.checkNotNull(
                  internalId, r'ZScannerFolder', 'internalId'),
              externalId: BuiltValueNullFieldError.checkNotNull(
                  externalId, r'ZScannerFolder', 'externalId'),
              name: BuiltValueNullFieldError.checkNotNull(
                  name, r'ZScannerFolder', 'name'),
              type: BuiltValueNullFieldError.checkNotNull(
                  type, r'ZScannerFolder', 'type'),
              metadata: _metadata?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metadata';
        _metadata?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerFolder', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
