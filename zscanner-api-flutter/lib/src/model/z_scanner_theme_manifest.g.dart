// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_theme_manifest.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerThemeManifest extends ZScannerThemeManifest {
  @override
  final String? logoUrl;
  @override
  final String? secondaryLogoUrl;
  @override
  final String? mainColor;
  @override
  final String? mainLightColor;
  @override
  final String? secondaryColor;
  @override
  final String? backgroundColor;
  @override
  final String? additionalColor;
  @override
  final String? textFieldFillColor;
  @override
  final String? outlineColor;
  @override
  final String? shadowColor;
  @override
  final String? textColor;
  @override
  final String? fabForegroundColor;
  @override
  final String? fabBackgroundColor;
  @override
  final ZScannerThemeManifestLight? light;
  @override
  final ZScannerThemeManifestDark? dark;

  factory _$ZScannerThemeManifest(
          [void Function(ZScannerThemeManifestBuilder)? updates]) =>
      (new ZScannerThemeManifestBuilder()..update(updates))._build();

  _$ZScannerThemeManifest._(
      {this.logoUrl,
      this.secondaryLogoUrl,
      this.mainColor,
      this.mainLightColor,
      this.secondaryColor,
      this.backgroundColor,
      this.additionalColor,
      this.textFieldFillColor,
      this.outlineColor,
      this.shadowColor,
      this.textColor,
      this.fabForegroundColor,
      this.fabBackgroundColor,
      this.light,
      this.dark})
      : super._();

  @override
  ZScannerThemeManifest rebuild(
          void Function(ZScannerThemeManifestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerThemeManifestBuilder toBuilder() =>
      new ZScannerThemeManifestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerThemeManifest &&
        logoUrl == other.logoUrl &&
        secondaryLogoUrl == other.secondaryLogoUrl &&
        mainColor == other.mainColor &&
        mainLightColor == other.mainLightColor &&
        secondaryColor == other.secondaryColor &&
        backgroundColor == other.backgroundColor &&
        additionalColor == other.additionalColor &&
        textFieldFillColor == other.textFieldFillColor &&
        outlineColor == other.outlineColor &&
        shadowColor == other.shadowColor &&
        textColor == other.textColor &&
        fabForegroundColor == other.fabForegroundColor &&
        fabBackgroundColor == other.fabBackgroundColor &&
        light == other.light &&
        dark == other.dark;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, logoUrl.hashCode);
    _$hash = $jc(_$hash, secondaryLogoUrl.hashCode);
    _$hash = $jc(_$hash, mainColor.hashCode);
    _$hash = $jc(_$hash, mainLightColor.hashCode);
    _$hash = $jc(_$hash, secondaryColor.hashCode);
    _$hash = $jc(_$hash, backgroundColor.hashCode);
    _$hash = $jc(_$hash, additionalColor.hashCode);
    _$hash = $jc(_$hash, textFieldFillColor.hashCode);
    _$hash = $jc(_$hash, outlineColor.hashCode);
    _$hash = $jc(_$hash, shadowColor.hashCode);
    _$hash = $jc(_$hash, textColor.hashCode);
    _$hash = $jc(_$hash, fabForegroundColor.hashCode);
    _$hash = $jc(_$hash, fabBackgroundColor.hashCode);
    _$hash = $jc(_$hash, light.hashCode);
    _$hash = $jc(_$hash, dark.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerThemeManifest')
          ..add('logoUrl', logoUrl)
          ..add('secondaryLogoUrl', secondaryLogoUrl)
          ..add('mainColor', mainColor)
          ..add('mainLightColor', mainLightColor)
          ..add('secondaryColor', secondaryColor)
          ..add('backgroundColor', backgroundColor)
          ..add('additionalColor', additionalColor)
          ..add('textFieldFillColor', textFieldFillColor)
          ..add('outlineColor', outlineColor)
          ..add('shadowColor', shadowColor)
          ..add('textColor', textColor)
          ..add('fabForegroundColor', fabForegroundColor)
          ..add('fabBackgroundColor', fabBackgroundColor)
          ..add('light', light)
          ..add('dark', dark))
        .toString();
  }
}

class ZScannerThemeManifestBuilder
    implements Builder<ZScannerThemeManifest, ZScannerThemeManifestBuilder> {
  _$ZScannerThemeManifest? _$v;

  String? _logoUrl;
  String? get logoUrl => _$this._logoUrl;
  set logoUrl(String? logoUrl) => _$this._logoUrl = logoUrl;

  String? _secondaryLogoUrl;
  String? get secondaryLogoUrl => _$this._secondaryLogoUrl;
  set secondaryLogoUrl(String? secondaryLogoUrl) =>
      _$this._secondaryLogoUrl = secondaryLogoUrl;

  String? _mainColor;
  String? get mainColor => _$this._mainColor;
  set mainColor(String? mainColor) => _$this._mainColor = mainColor;

  String? _mainLightColor;
  String? get mainLightColor => _$this._mainLightColor;
  set mainLightColor(String? mainLightColor) =>
      _$this._mainLightColor = mainLightColor;

  String? _secondaryColor;
  String? get secondaryColor => _$this._secondaryColor;
  set secondaryColor(String? secondaryColor) =>
      _$this._secondaryColor = secondaryColor;

  String? _backgroundColor;
  String? get backgroundColor => _$this._backgroundColor;
  set backgroundColor(String? backgroundColor) =>
      _$this._backgroundColor = backgroundColor;

  String? _additionalColor;
  String? get additionalColor => _$this._additionalColor;
  set additionalColor(String? additionalColor) =>
      _$this._additionalColor = additionalColor;

  String? _textFieldFillColor;
  String? get textFieldFillColor => _$this._textFieldFillColor;
  set textFieldFillColor(String? textFieldFillColor) =>
      _$this._textFieldFillColor = textFieldFillColor;

  String? _outlineColor;
  String? get outlineColor => _$this._outlineColor;
  set outlineColor(String? outlineColor) => _$this._outlineColor = outlineColor;

  String? _shadowColor;
  String? get shadowColor => _$this._shadowColor;
  set shadowColor(String? shadowColor) => _$this._shadowColor = shadowColor;

  String? _textColor;
  String? get textColor => _$this._textColor;
  set textColor(String? textColor) => _$this._textColor = textColor;

  String? _fabForegroundColor;
  String? get fabForegroundColor => _$this._fabForegroundColor;
  set fabForegroundColor(String? fabForegroundColor) =>
      _$this._fabForegroundColor = fabForegroundColor;

  String? _fabBackgroundColor;
  String? get fabBackgroundColor => _$this._fabBackgroundColor;
  set fabBackgroundColor(String? fabBackgroundColor) =>
      _$this._fabBackgroundColor = fabBackgroundColor;

  ZScannerThemeManifestLightBuilder? _light;
  ZScannerThemeManifestLightBuilder get light =>
      _$this._light ??= new ZScannerThemeManifestLightBuilder();
  set light(ZScannerThemeManifestLightBuilder? light) => _$this._light = light;

  ZScannerThemeManifestDarkBuilder? _dark;
  ZScannerThemeManifestDarkBuilder get dark =>
      _$this._dark ??= new ZScannerThemeManifestDarkBuilder();
  set dark(ZScannerThemeManifestDarkBuilder? dark) => _$this._dark = dark;

  ZScannerThemeManifestBuilder() {
    ZScannerThemeManifest._defaults(this);
  }

  ZScannerThemeManifestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _logoUrl = $v.logoUrl;
      _secondaryLogoUrl = $v.secondaryLogoUrl;
      _mainColor = $v.mainColor;
      _mainLightColor = $v.mainLightColor;
      _secondaryColor = $v.secondaryColor;
      _backgroundColor = $v.backgroundColor;
      _additionalColor = $v.additionalColor;
      _textFieldFillColor = $v.textFieldFillColor;
      _outlineColor = $v.outlineColor;
      _shadowColor = $v.shadowColor;
      _textColor = $v.textColor;
      _fabForegroundColor = $v.fabForegroundColor;
      _fabBackgroundColor = $v.fabBackgroundColor;
      _light = $v.light?.toBuilder();
      _dark = $v.dark?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerThemeManifest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerThemeManifest;
  }

  @override
  void update(void Function(ZScannerThemeManifestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerThemeManifest build() => _build();

  _$ZScannerThemeManifest _build() {
    _$ZScannerThemeManifest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerThemeManifest._(
              logoUrl: logoUrl,
              secondaryLogoUrl: secondaryLogoUrl,
              mainColor: mainColor,
              mainLightColor: mainLightColor,
              secondaryColor: secondaryColor,
              backgroundColor: backgroundColor,
              additionalColor: additionalColor,
              textFieldFillColor: textFieldFillColor,
              outlineColor: outlineColor,
              shadowColor: shadowColor,
              textColor: textColor,
              fabForegroundColor: fabForegroundColor,
              fabBackgroundColor: fabBackgroundColor,
              light: _light?.build(),
              dark: _dark?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'light';
        _light?.build();
        _$failedField = 'dark';
        _dark?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerThemeManifest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
