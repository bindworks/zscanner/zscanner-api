// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_error.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerError extends ZScannerError {
  @override
  final String? exception;
  @override
  final String? error;
  @override
  final String? uuid;
  @override
  final BuiltList<ZScannerMetaError>? metaErrors;
  @override
  final BuiltList<String>? mediaIds;
  @override
  final String? folderId;
  @override
  final String? subfolderId;
  @override
  final String? resourceId;

  factory _$ZScannerError([void Function(ZScannerErrorBuilder)? updates]) =>
      (new ZScannerErrorBuilder()..update(updates))._build();

  _$ZScannerError._(
      {this.exception,
      this.error,
      this.uuid,
      this.metaErrors,
      this.mediaIds,
      this.folderId,
      this.subfolderId,
      this.resourceId})
      : super._();

  @override
  ZScannerError rebuild(void Function(ZScannerErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerErrorBuilder toBuilder() => new ZScannerErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerError &&
        exception == other.exception &&
        error == other.error &&
        uuid == other.uuid &&
        metaErrors == other.metaErrors &&
        mediaIds == other.mediaIds &&
        folderId == other.folderId &&
        subfolderId == other.subfolderId &&
        resourceId == other.resourceId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, exception.hashCode);
    _$hash = $jc(_$hash, error.hashCode);
    _$hash = $jc(_$hash, uuid.hashCode);
    _$hash = $jc(_$hash, metaErrors.hashCode);
    _$hash = $jc(_$hash, mediaIds.hashCode);
    _$hash = $jc(_$hash, folderId.hashCode);
    _$hash = $jc(_$hash, subfolderId.hashCode);
    _$hash = $jc(_$hash, resourceId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerError')
          ..add('exception', exception)
          ..add('error', error)
          ..add('uuid', uuid)
          ..add('metaErrors', metaErrors)
          ..add('mediaIds', mediaIds)
          ..add('folderId', folderId)
          ..add('subfolderId', subfolderId)
          ..add('resourceId', resourceId))
        .toString();
  }
}

class ZScannerErrorBuilder
    implements Builder<ZScannerError, ZScannerErrorBuilder> {
  _$ZScannerError? _$v;

  String? _exception;
  String? get exception => _$this._exception;
  set exception(String? exception) => _$this._exception = exception;

  String? _error;
  String? get error => _$this._error;
  set error(String? error) => _$this._error = error;

  String? _uuid;
  String? get uuid => _$this._uuid;
  set uuid(String? uuid) => _$this._uuid = uuid;

  ListBuilder<ZScannerMetaError>? _metaErrors;
  ListBuilder<ZScannerMetaError> get metaErrors =>
      _$this._metaErrors ??= new ListBuilder<ZScannerMetaError>();
  set metaErrors(ListBuilder<ZScannerMetaError>? metaErrors) =>
      _$this._metaErrors = metaErrors;

  ListBuilder<String>? _mediaIds;
  ListBuilder<String> get mediaIds =>
      _$this._mediaIds ??= new ListBuilder<String>();
  set mediaIds(ListBuilder<String>? mediaIds) => _$this._mediaIds = mediaIds;

  String? _folderId;
  String? get folderId => _$this._folderId;
  set folderId(String? folderId) => _$this._folderId = folderId;

  String? _subfolderId;
  String? get subfolderId => _$this._subfolderId;
  set subfolderId(String? subfolderId) => _$this._subfolderId = subfolderId;

  String? _resourceId;
  String? get resourceId => _$this._resourceId;
  set resourceId(String? resourceId) => _$this._resourceId = resourceId;

  ZScannerErrorBuilder() {
    ZScannerError._defaults(this);
  }

  ZScannerErrorBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _exception = $v.exception;
      _error = $v.error;
      _uuid = $v.uuid;
      _metaErrors = $v.metaErrors?.toBuilder();
      _mediaIds = $v.mediaIds?.toBuilder();
      _folderId = $v.folderId;
      _subfolderId = $v.subfolderId;
      _resourceId = $v.resourceId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerError other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerError;
  }

  @override
  void update(void Function(ZScannerErrorBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerError build() => _build();

  _$ZScannerError _build() {
    _$ZScannerError _$result;
    try {
      _$result = _$v ??
          new _$ZScannerError._(
              exception: exception,
              error: error,
              uuid: uuid,
              metaErrors: _metaErrors?.build(),
              mediaIds: _mediaIds?.build(),
              folderId: folderId,
              subfolderId: subfolderId,
              resourceId: resourceId);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metaErrors';
        _metaErrors?.build();
        _$failedField = 'mediaIds';
        _mediaIds?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerError', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
