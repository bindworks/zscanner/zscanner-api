//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_metadata_item.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_find_folders_request.g.dart';

/// ZScannerFindFoldersRequest
///
/// Properties:
/// * [metadata] 
/// * [internalId] 
/// * [fullText] 
/// * [suggestions] 
/// * [barCodeType] 
/// * [barCodeValue] 
@BuiltValue()
abstract class ZScannerFindFoldersRequest implements Built<ZScannerFindFoldersRequest, ZScannerFindFoldersRequestBuilder> {
  @BuiltValueField(wireName: r'metadata')
  BuiltList<ZScannerMetadataItem> get metadata;

  @BuiltValueField(wireName: r'internalId')
  String? get internalId;

  @BuiltValueField(wireName: r'fullText')
  String? get fullText;

  @BuiltValueField(wireName: r'suggestions')
  bool? get suggestions;

  @BuiltValueField(wireName: r'barCodeType')
  String? get barCodeType;

  @BuiltValueField(wireName: r'barCodeValue')
  String? get barCodeValue;

  ZScannerFindFoldersRequest._();

  factory ZScannerFindFoldersRequest([void updates(ZScannerFindFoldersRequestBuilder b)]) = _$ZScannerFindFoldersRequest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerFindFoldersRequestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerFindFoldersRequest> get serializer => _$ZScannerFindFoldersRequestSerializer();
}

class _$ZScannerFindFoldersRequestSerializer implements PrimitiveSerializer<ZScannerFindFoldersRequest> {
  @override
  final Iterable<Type> types = const [ZScannerFindFoldersRequest, _$ZScannerFindFoldersRequest];

  @override
  final String wireName = r'ZScannerFindFoldersRequest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerFindFoldersRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'metadata';
    yield serializers.serialize(
      object.metadata,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
    );
    if (object.internalId != null) {
      yield r'internalId';
      yield serializers.serialize(
        object.internalId,
        specifiedType: const FullType(String),
      );
    }
    if (object.fullText != null) {
      yield r'fullText';
      yield serializers.serialize(
        object.fullText,
        specifiedType: const FullType(String),
      );
    }
    if (object.suggestions != null) {
      yield r'suggestions';
      yield serializers.serialize(
        object.suggestions,
        specifiedType: const FullType(bool),
      );
    }
    if (object.barCodeType != null) {
      yield r'barCodeType';
      yield serializers.serialize(
        object.barCodeType,
        specifiedType: const FullType(String),
      );
    }
    if (object.barCodeValue != null) {
      yield r'barCodeValue';
      yield serializers.serialize(
        object.barCodeValue,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerFindFoldersRequest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerFindFoldersRequestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'metadata':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataItem)]),
          ) as BuiltList<ZScannerMetadataItem>;
          result.metadata.replace(valueDes);
          break;
        case r'internalId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.internalId = valueDes;
          break;
        case r'fullText':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.fullText = valueDes;
          break;
        case r'suggestions':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.suggestions = valueDes;
          break;
        case r'barCodeType':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.barCodeType = valueDes;
          break;
        case r'barCodeValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.barCodeValue = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerFindFoldersRequest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerFindFoldersRequestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

