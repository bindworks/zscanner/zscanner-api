//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_config.g.dart';

/// Configuration of application. If not empty, the whole configuration is replaced. It is sent only during initial configuration. 
///
/// Properties:
/// * [folderHistoryDisabled] 
/// * [scannerEnabled] 
/// * [scannerTypes] 
/// * [createFolderEnabled] 
/// * [submissionConfirmationEnabled] 
@BuiltValue()
abstract class ZScannerConfig implements Built<ZScannerConfig, ZScannerConfigBuilder> {
  @BuiltValueField(wireName: r'folderHistoryDisabled')
  bool? get folderHistoryDisabled;

  @BuiltValueField(wireName: r'scannerEnabled')
  bool? get scannerEnabled;

  @BuiltValueField(wireName: r'scannerTypes')
  BuiltList<String>? get scannerTypes;

  @BuiltValueField(wireName: r'createFolderEnabled')
  bool? get createFolderEnabled;

  @BuiltValueField(wireName: r'submissionConfirmationEnabled')
  bool? get submissionConfirmationEnabled;

  ZScannerConfig._();

  factory ZScannerConfig([void updates(ZScannerConfigBuilder b)]) = _$ZScannerConfig;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerConfigBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerConfig> get serializer => _$ZScannerConfigSerializer();
}

class _$ZScannerConfigSerializer implements PrimitiveSerializer<ZScannerConfig> {
  @override
  final Iterable<Type> types = const [ZScannerConfig, _$ZScannerConfig];

  @override
  final String wireName = r'ZScannerConfig';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.folderHistoryDisabled != null) {
      yield r'folderHistoryDisabled';
      yield serializers.serialize(
        object.folderHistoryDisabled,
        specifiedType: const FullType(bool),
      );
    }
    if (object.scannerEnabled != null) {
      yield r'scannerEnabled';
      yield serializers.serialize(
        object.scannerEnabled,
        specifiedType: const FullType(bool),
      );
    }
    if (object.scannerTypes != null) {
      yield r'scannerTypes';
      yield serializers.serialize(
        object.scannerTypes,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
    if (object.createFolderEnabled != null) {
      yield r'createFolderEnabled';
      yield serializers.serialize(
        object.createFolderEnabled,
        specifiedType: const FullType(bool),
      );
    }
    if (object.submissionConfirmationEnabled != null) {
      yield r'submissionConfirmationEnabled';
      yield serializers.serialize(
        object.submissionConfirmationEnabled,
        specifiedType: const FullType(bool),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerConfig object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerConfigBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'folderHistoryDisabled':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.folderHistoryDisabled = valueDes;
          break;
        case r'scannerEnabled':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.scannerEnabled = valueDes;
          break;
        case r'scannerTypes':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.scannerTypes.replace(valueDes);
          break;
        case r'createFolderEnabled':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.createFolderEnabled = valueDes;
          break;
        case r'submissionConfirmationEnabled':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.submissionConfirmationEnabled = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerConfig deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerConfigBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

