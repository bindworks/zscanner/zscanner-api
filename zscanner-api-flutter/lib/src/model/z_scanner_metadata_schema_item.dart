//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_widget_config_union.dart';
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_metadata_validator.dart';
import 'package:zscanner_api/src/model/z_scanner_metadata_default_value.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_metadata_schema_item.g.dart';

/// ZScannerMetadataSchemaItem
///
/// Properties:
/// * [metaId] 
/// * [label] 
/// * [hidden] 
/// * [hint] - Known hints: - `czBirthNumber` (this is a Czech birth number) - `birthDate` (this is a birth date) - `sex` (this is sex M or F) 
/// * [type] - Supported types: - string - number - boolean - date - date-time - multi-string 
/// * [required_] 
/// * [enum_] 
/// * [default_] 
/// * [widgetId] 
/// * [widgetConfig] 
/// * [validators] 
/// * [serverOnly] 
@BuiltValue()
abstract class ZScannerMetadataSchemaItem implements Built<ZScannerMetadataSchemaItem, ZScannerMetadataSchemaItemBuilder> {
  @BuiltValueField(wireName: r'metaId')
  String get metaId;

  @BuiltValueField(wireName: r'label')
  String get label;

  @BuiltValueField(wireName: r'hidden')
  bool? get hidden;

  /// Known hints: - `czBirthNumber` (this is a Czech birth number) - `birthDate` (this is a birth date) - `sex` (this is sex M or F) 
  @BuiltValueField(wireName: r'hint')
  String? get hint;

  /// Supported types: - string - number - boolean - date - date-time - multi-string 
  @BuiltValueField(wireName: r'type')
  String get type;

  @BuiltValueField(wireName: r'required')
  bool? get required_;

  @BuiltValueField(wireName: r'enum')
  String? get enum_;

  @BuiltValueField(wireName: r'default')
  ZScannerMetadataDefaultValue? get default_;

  @BuiltValueField(wireName: r'widgetId')
  String? get widgetId;

  @BuiltValueField(wireName: r'widgetConfig')
  ZScannerWidgetConfigUnion? get widgetConfig;

  @BuiltValueField(wireName: r'validators')
  BuiltList<ZScannerMetadataValidator>? get validators;

  @BuiltValueField(wireName: r'serverOnly')
  bool? get serverOnly;

  ZScannerMetadataSchemaItem._();

  factory ZScannerMetadataSchemaItem([void updates(ZScannerMetadataSchemaItemBuilder b)]) = _$ZScannerMetadataSchemaItem;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerMetadataSchemaItemBuilder b) => b
      ..hidden = false
      ..required_ = false
      ..validators = ListBuilder();

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerMetadataSchemaItem> get serializer => _$ZScannerMetadataSchemaItemSerializer();
}

class _$ZScannerMetadataSchemaItemSerializer implements PrimitiveSerializer<ZScannerMetadataSchemaItem> {
  @override
  final Iterable<Type> types = const [ZScannerMetadataSchemaItem, _$ZScannerMetadataSchemaItem];

  @override
  final String wireName = r'ZScannerMetadataSchemaItem';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerMetadataSchemaItem object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'metaId';
    yield serializers.serialize(
      object.metaId,
      specifiedType: const FullType(String),
    );
    yield r'label';
    yield serializers.serialize(
      object.label,
      specifiedType: const FullType(String),
    );
    if (object.hidden != null) {
      yield r'hidden';
      yield serializers.serialize(
        object.hidden,
        specifiedType: const FullType(bool),
      );
    }
    if (object.hint != null) {
      yield r'hint';
      yield serializers.serialize(
        object.hint,
        specifiedType: const FullType(String),
      );
    }
    yield r'type';
    yield serializers.serialize(
      object.type,
      specifiedType: const FullType(String),
    );
    if (object.required_ != null) {
      yield r'required';
      yield serializers.serialize(
        object.required_,
        specifiedType: const FullType(bool),
      );
    }
    if (object.enum_ != null) {
      yield r'enum';
      yield serializers.serialize(
        object.enum_,
        specifiedType: const FullType(String),
      );
    }
    if (object.default_ != null) {
      yield r'default';
      yield serializers.serialize(
        object.default_,
        specifiedType: const FullType(ZScannerMetadataDefaultValue),
      );
    }
    if (object.widgetId != null) {
      yield r'widgetId';
      yield serializers.serialize(
        object.widgetId,
        specifiedType: const FullType(String),
      );
    }
    if (object.widgetConfig != null) {
      yield r'widgetConfig';
      yield serializers.serialize(
        object.widgetConfig,
        specifiedType: const FullType(ZScannerWidgetConfigUnion),
      );
    }
    if (object.validators != null) {
      yield r'validators';
      yield serializers.serialize(
        object.validators,
        specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataValidator)]),
      );
    }
    if (object.serverOnly != null) {
      yield r'serverOnly';
      yield serializers.serialize(
        object.serverOnly,
        specifiedType: const FullType(bool),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerMetadataSchemaItem object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerMetadataSchemaItemBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'metaId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.metaId = valueDes;
          break;
        case r'label':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.label = valueDes;
          break;
        case r'hidden':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.hidden = valueDes;
          break;
        case r'hint':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.hint = valueDes;
          break;
        case r'type':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.type = valueDes;
          break;
        case r'required':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.required_ = valueDes;
          break;
        case r'enum':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.enum_ = valueDes;
          break;
        case r'default':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerMetadataDefaultValue),
          ) as ZScannerMetadataDefaultValue;
          result.default_.replace(valueDes);
          break;
        case r'widgetId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.widgetId = valueDes;
          break;
        case r'widgetConfig':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerWidgetConfigUnion),
          ) as ZScannerWidgetConfigUnion;
          result.widgetConfig.replace(valueDes);
          break;
        case r'validators':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerMetadataValidator)]),
          ) as BuiltList<ZScannerMetadataValidator>;
          result.validators.replace(valueDes);
          break;
        case r'serverOnly':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.serverOnly = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerMetadataSchemaItem deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerMetadataSchemaItemBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

