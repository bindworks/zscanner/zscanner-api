//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_enum_item.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_enum.g.dart';

/// ZScannerEnum
///
/// Properties:
/// * [enumId] 
/// * [enumVersion] 
/// * [enumType] 
/// * [createdTs] 
/// * [updatedTs] 
/// * [label] 
/// * [items] 
@BuiltValue()
abstract class ZScannerEnum implements Built<ZScannerEnum, ZScannerEnumBuilder> {
  @BuiltValueField(wireName: r'enumId')
  String get enumId;

  @BuiltValueField(wireName: r'enumVersion')
  String? get enumVersion;

  @BuiltValueField(wireName: r'enumType')
  ZScannerEnumEnumTypeEnum get enumType;
  // enum enumTypeEnum {  FLAT,  TREE,  };

  @BuiltValueField(wireName: r'createdTs')
  DateTime get createdTs;

  @BuiltValueField(wireName: r'updatedTs')
  DateTime get updatedTs;

  @BuiltValueField(wireName: r'label')
  String get label;

  @BuiltValueField(wireName: r'items')
  BuiltList<ZScannerEnumItem> get items;

  ZScannerEnum._();

  factory ZScannerEnum([void updates(ZScannerEnumBuilder b)]) = _$ZScannerEnum;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerEnumBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerEnum> get serializer => _$ZScannerEnumSerializer();
}

class _$ZScannerEnumSerializer implements PrimitiveSerializer<ZScannerEnum> {
  @override
  final Iterable<Type> types = const [ZScannerEnum, _$ZScannerEnum];

  @override
  final String wireName = r'ZScannerEnum';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerEnum object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'enumId';
    yield serializers.serialize(
      object.enumId,
      specifiedType: const FullType(String),
    );
    if (object.enumVersion != null) {
      yield r'enumVersion';
      yield serializers.serialize(
        object.enumVersion,
        specifiedType: const FullType(String),
      );
    }
    yield r'enumType';
    yield serializers.serialize(
      object.enumType,
      specifiedType: const FullType(ZScannerEnumEnumTypeEnum),
    );
    yield r'createdTs';
    yield serializers.serialize(
      object.createdTs,
      specifiedType: const FullType(DateTime),
    );
    yield r'updatedTs';
    yield serializers.serialize(
      object.updatedTs,
      specifiedType: const FullType(DateTime),
    );
    yield r'label';
    yield serializers.serialize(
      object.label,
      specifiedType: const FullType(String),
    );
    yield r'items';
    yield serializers.serialize(
      object.items,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerEnumItem)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerEnum object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerEnumBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'enumId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.enumId = valueDes;
          break;
        case r'enumVersion':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.enumVersion = valueDes;
          break;
        case r'enumType':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerEnumEnumTypeEnum),
          ) as ZScannerEnumEnumTypeEnum;
          result.enumType = valueDes;
          break;
        case r'createdTs':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.createdTs = valueDes;
          break;
        case r'updatedTs':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.updatedTs = valueDes;
          break;
        case r'label':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.label = valueDes;
          break;
        case r'items':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerEnumItem)]),
          ) as BuiltList<ZScannerEnumItem>;
          result.items.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerEnum deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerEnumBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class ZScannerEnumEnumTypeEnum extends EnumClass {

  @BuiltValueEnumConst(wireName: r'FLAT')
  static const ZScannerEnumEnumTypeEnum FLAT = _$zScannerEnumEnumTypeEnum_FLAT;
  @BuiltValueEnumConst(wireName: r'TREE')
  static const ZScannerEnumEnumTypeEnum TREE = _$zScannerEnumEnumTypeEnum_TREE;

  @BuiltValueEnumConst(wireName: r'____unknown____', fallback: true)
  static const ZScannerEnumEnumTypeEnum unknown = _$zScannerEnumEnumTypeEnum_$unknown;

  static Serializer<ZScannerEnumEnumTypeEnum> get serializer => _$zScannerEnumEnumTypeEnumSerializer;

  const ZScannerEnumEnumTypeEnum._(String name): super(name);

  static BuiltSet<ZScannerEnumEnumTypeEnum> get values => _$zScannerEnumEnumTypeEnumValues;
  static ZScannerEnumEnumTypeEnum valueOf(String name) => _$zScannerEnumEnumTypeEnumValueOf(name);
}


