// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_module_data_union.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerModuleDataUnion extends ZScannerModuleDataUnion {
  @override
  final BuiltList<ZScannerStickerData>? stickers;

  factory _$ZScannerModuleDataUnion(
          [void Function(ZScannerModuleDataUnionBuilder)? updates]) =>
      (new ZScannerModuleDataUnionBuilder()..update(updates))._build();

  _$ZScannerModuleDataUnion._({this.stickers}) : super._();

  @override
  ZScannerModuleDataUnion rebuild(
          void Function(ZScannerModuleDataUnionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerModuleDataUnionBuilder toBuilder() =>
      new ZScannerModuleDataUnionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerModuleDataUnion && stickers == other.stickers;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, stickers.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerModuleDataUnion')
          ..add('stickers', stickers))
        .toString();
  }
}

class ZScannerModuleDataUnionBuilder
    implements
        Builder<ZScannerModuleDataUnion, ZScannerModuleDataUnionBuilder> {
  _$ZScannerModuleDataUnion? _$v;

  ListBuilder<ZScannerStickerData>? _stickers;
  ListBuilder<ZScannerStickerData> get stickers =>
      _$this._stickers ??= new ListBuilder<ZScannerStickerData>();
  set stickers(ListBuilder<ZScannerStickerData>? stickers) =>
      _$this._stickers = stickers;

  ZScannerModuleDataUnionBuilder() {
    ZScannerModuleDataUnion._defaults(this);
  }

  ZScannerModuleDataUnionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _stickers = $v.stickers?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerModuleDataUnion other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerModuleDataUnion;
  }

  @override
  void update(void Function(ZScannerModuleDataUnionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerModuleDataUnion build() => _build();

  _$ZScannerModuleDataUnion _build() {
    _$ZScannerModuleDataUnion _$result;
    try {
      _$result =
          _$v ?? new _$ZScannerModuleDataUnion._(stickers: _stickers?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'stickers';
        _stickers?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerModuleDataUnion', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
