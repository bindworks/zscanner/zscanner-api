// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_enum.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ZScannerEnumEnumTypeEnum _$zScannerEnumEnumTypeEnum_FLAT =
    const ZScannerEnumEnumTypeEnum._('FLAT');
const ZScannerEnumEnumTypeEnum _$zScannerEnumEnumTypeEnum_TREE =
    const ZScannerEnumEnumTypeEnum._('TREE');
const ZScannerEnumEnumTypeEnum _$zScannerEnumEnumTypeEnum_$unknown =
    const ZScannerEnumEnumTypeEnum._('unknown');

ZScannerEnumEnumTypeEnum _$zScannerEnumEnumTypeEnumValueOf(String name) {
  switch (name) {
    case 'FLAT':
      return _$zScannerEnumEnumTypeEnum_FLAT;
    case 'TREE':
      return _$zScannerEnumEnumTypeEnum_TREE;
    case 'unknown':
      return _$zScannerEnumEnumTypeEnum_$unknown;
    default:
      return _$zScannerEnumEnumTypeEnum_$unknown;
  }
}

final BuiltSet<ZScannerEnumEnumTypeEnum> _$zScannerEnumEnumTypeEnumValues =
    new BuiltSet<ZScannerEnumEnumTypeEnum>(const <ZScannerEnumEnumTypeEnum>[
  _$zScannerEnumEnumTypeEnum_FLAT,
  _$zScannerEnumEnumTypeEnum_TREE,
  _$zScannerEnumEnumTypeEnum_$unknown,
]);

Serializer<ZScannerEnumEnumTypeEnum> _$zScannerEnumEnumTypeEnumSerializer =
    new _$ZScannerEnumEnumTypeEnumSerializer();

class _$ZScannerEnumEnumTypeEnumSerializer
    implements PrimitiveSerializer<ZScannerEnumEnumTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'FLAT': 'FLAT',
    'TREE': 'TREE',
    'unknown': '____unknown____',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'FLAT': 'FLAT',
    'TREE': 'TREE',
    '____unknown____': 'unknown',
  };

  @override
  final Iterable<Type> types = const <Type>[ZScannerEnumEnumTypeEnum];
  @override
  final String wireName = 'ZScannerEnumEnumTypeEnum';

  @override
  Object serialize(Serializers serializers, ZScannerEnumEnumTypeEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ZScannerEnumEnumTypeEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ZScannerEnumEnumTypeEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$ZScannerEnum extends ZScannerEnum {
  @override
  final String enumId;
  @override
  final String? enumVersion;
  @override
  final ZScannerEnumEnumTypeEnum enumType;
  @override
  final DateTime createdTs;
  @override
  final DateTime updatedTs;
  @override
  final String label;
  @override
  final BuiltList<ZScannerEnumItem> items;

  factory _$ZScannerEnum([void Function(ZScannerEnumBuilder)? updates]) =>
      (new ZScannerEnumBuilder()..update(updates))._build();

  _$ZScannerEnum._(
      {required this.enumId,
      this.enumVersion,
      required this.enumType,
      required this.createdTs,
      required this.updatedTs,
      required this.label,
      required this.items})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(enumId, r'ZScannerEnum', 'enumId');
    BuiltValueNullFieldError.checkNotNull(
        enumType, r'ZScannerEnum', 'enumType');
    BuiltValueNullFieldError.checkNotNull(
        createdTs, r'ZScannerEnum', 'createdTs');
    BuiltValueNullFieldError.checkNotNull(
        updatedTs, r'ZScannerEnum', 'updatedTs');
    BuiltValueNullFieldError.checkNotNull(label, r'ZScannerEnum', 'label');
    BuiltValueNullFieldError.checkNotNull(items, r'ZScannerEnum', 'items');
  }

  @override
  ZScannerEnum rebuild(void Function(ZScannerEnumBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerEnumBuilder toBuilder() => new ZScannerEnumBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerEnum &&
        enumId == other.enumId &&
        enumVersion == other.enumVersion &&
        enumType == other.enumType &&
        createdTs == other.createdTs &&
        updatedTs == other.updatedTs &&
        label == other.label &&
        items == other.items;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, enumId.hashCode);
    _$hash = $jc(_$hash, enumVersion.hashCode);
    _$hash = $jc(_$hash, enumType.hashCode);
    _$hash = $jc(_$hash, createdTs.hashCode);
    _$hash = $jc(_$hash, updatedTs.hashCode);
    _$hash = $jc(_$hash, label.hashCode);
    _$hash = $jc(_$hash, items.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerEnum')
          ..add('enumId', enumId)
          ..add('enumVersion', enumVersion)
          ..add('enumType', enumType)
          ..add('createdTs', createdTs)
          ..add('updatedTs', updatedTs)
          ..add('label', label)
          ..add('items', items))
        .toString();
  }
}

class ZScannerEnumBuilder
    implements Builder<ZScannerEnum, ZScannerEnumBuilder> {
  _$ZScannerEnum? _$v;

  String? _enumId;
  String? get enumId => _$this._enumId;
  set enumId(String? enumId) => _$this._enumId = enumId;

  String? _enumVersion;
  String? get enumVersion => _$this._enumVersion;
  set enumVersion(String? enumVersion) => _$this._enumVersion = enumVersion;

  ZScannerEnumEnumTypeEnum? _enumType;
  ZScannerEnumEnumTypeEnum? get enumType => _$this._enumType;
  set enumType(ZScannerEnumEnumTypeEnum? enumType) =>
      _$this._enumType = enumType;

  DateTime? _createdTs;
  DateTime? get createdTs => _$this._createdTs;
  set createdTs(DateTime? createdTs) => _$this._createdTs = createdTs;

  DateTime? _updatedTs;
  DateTime? get updatedTs => _$this._updatedTs;
  set updatedTs(DateTime? updatedTs) => _$this._updatedTs = updatedTs;

  String? _label;
  String? get label => _$this._label;
  set label(String? label) => _$this._label = label;

  ListBuilder<ZScannerEnumItem>? _items;
  ListBuilder<ZScannerEnumItem> get items =>
      _$this._items ??= new ListBuilder<ZScannerEnumItem>();
  set items(ListBuilder<ZScannerEnumItem>? items) => _$this._items = items;

  ZScannerEnumBuilder() {
    ZScannerEnum._defaults(this);
  }

  ZScannerEnumBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _enumId = $v.enumId;
      _enumVersion = $v.enumVersion;
      _enumType = $v.enumType;
      _createdTs = $v.createdTs;
      _updatedTs = $v.updatedTs;
      _label = $v.label;
      _items = $v.items.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerEnum other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerEnum;
  }

  @override
  void update(void Function(ZScannerEnumBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerEnum build() => _build();

  _$ZScannerEnum _build() {
    _$ZScannerEnum _$result;
    try {
      _$result = _$v ??
          new _$ZScannerEnum._(
              enumId: BuiltValueNullFieldError.checkNotNull(
                  enumId, r'ZScannerEnum', 'enumId'),
              enumVersion: enumVersion,
              enumType: BuiltValueNullFieldError.checkNotNull(
                  enumType, r'ZScannerEnum', 'enumType'),
              createdTs: BuiltValueNullFieldError.checkNotNull(
                  createdTs, r'ZScannerEnum', 'createdTs'),
              updatedTs: BuiltValueNullFieldError.checkNotNull(
                  updatedTs, r'ZScannerEnum', 'updatedTs'),
              label: BuiltValueNullFieldError.checkNotNull(
                  label, r'ZScannerEnum', 'label'),
              items: items.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'items';
        items.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerEnum', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
