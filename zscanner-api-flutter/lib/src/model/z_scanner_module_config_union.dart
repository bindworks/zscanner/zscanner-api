//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_sticker_config.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_module_config_union.g.dart';

/// ZScannerModuleConfigUnion
///
/// Properties:
/// * [subfolder] 
/// * [subfolderTypeId] 
/// * [maxVideoDuration] - seconds
/// * [recordAudio] 
/// * [allowMimeTypes] 
/// * [allowShareTo] 
/// * [allowGallery] 
/// * [allowMultiselect] 
/// * [skipToVictory] 
/// * [skipEdgeRecognition] 
/// * [skipStickerBurning] 
/// * [stickers] 
@BuiltValue()
abstract class ZScannerModuleConfigUnion implements Built<ZScannerModuleConfigUnion, ZScannerModuleConfigUnionBuilder> {
  @BuiltValueField(wireName: r'subfolder')
  ZScannerModuleConfigUnionSubfolderEnum? get subfolder;
  // enum subfolderEnum {  REQUIRED,  OPTIONAL,  };

  @BuiltValueField(wireName: r'subfolderTypeId')
  String? get subfolderTypeId;

  /// seconds
  @BuiltValueField(wireName: r'maxVideoDuration')
  int? get maxVideoDuration;

  @BuiltValueField(wireName: r'recordAudio')
  ZScannerModuleConfigUnionRecordAudioEnum? get recordAudio;
  // enum recordAudioEnum {  ALWAYS,  NEVER,  ALLOW-DEFAULT-YES,  ALLOW-DEFAULT-NO,  };

  @BuiltValueField(wireName: r'allowMimeTypes')
  BuiltList<String>? get allowMimeTypes;

  @BuiltValueField(wireName: r'allowShareTo')
  bool? get allowShareTo;

  @BuiltValueField(wireName: r'allowGallery')
  bool? get allowGallery;

  @BuiltValueField(wireName: r'allowMultiselect')
  bool? get allowMultiselect;

  @BuiltValueField(wireName: r'skipToVictory')
  bool? get skipToVictory;

  @BuiltValueField(wireName: r'skipEdgeRecognition')
  bool? get skipEdgeRecognition;

  @BuiltValueField(wireName: r'skipStickerBurning')
  bool? get skipStickerBurning;

  @BuiltValueField(wireName: r'stickers')
  ZScannerStickerConfig? get stickers;

  ZScannerModuleConfigUnion._();

  factory ZScannerModuleConfigUnion([void updates(ZScannerModuleConfigUnionBuilder b)]) = _$ZScannerModuleConfigUnion;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerModuleConfigUnionBuilder b) => b
      ..maxVideoDuration = 15
      ..recordAudio = const ZScannerModuleConfigUnionRecordAudioEnum._('ALLOW-DEFAULT-YES')
      ..allowMimeTypes = ListBuilder();

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerModuleConfigUnion> get serializer => _$ZScannerModuleConfigUnionSerializer();
}

class _$ZScannerModuleConfigUnionSerializer implements PrimitiveSerializer<ZScannerModuleConfigUnion> {
  @override
  final Iterable<Type> types = const [ZScannerModuleConfigUnion, _$ZScannerModuleConfigUnion];

  @override
  final String wireName = r'ZScannerModuleConfigUnion';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerModuleConfigUnion object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.subfolder != null) {
      yield r'subfolder';
      yield serializers.serialize(
        object.subfolder,
        specifiedType: const FullType(ZScannerModuleConfigUnionSubfolderEnum),
      );
    }
    if (object.subfolderTypeId != null) {
      yield r'subfolderTypeId';
      yield serializers.serialize(
        object.subfolderTypeId,
        specifiedType: const FullType(String),
      );
    }
    if (object.maxVideoDuration != null) {
      yield r'maxVideoDuration';
      yield serializers.serialize(
        object.maxVideoDuration,
        specifiedType: const FullType(int),
      );
    }
    if (object.recordAudio != null) {
      yield r'recordAudio';
      yield serializers.serialize(
        object.recordAudio,
        specifiedType: const FullType(ZScannerModuleConfigUnionRecordAudioEnum),
      );
    }
    if (object.allowMimeTypes != null) {
      yield r'allowMimeTypes';
      yield serializers.serialize(
        object.allowMimeTypes,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
    if (object.allowShareTo != null) {
      yield r'allowShareTo';
      yield serializers.serialize(
        object.allowShareTo,
        specifiedType: const FullType(bool),
      );
    }
    if (object.allowGallery != null) {
      yield r'allowGallery';
      yield serializers.serialize(
        object.allowGallery,
        specifiedType: const FullType(bool),
      );
    }
    if (object.allowMultiselect != null) {
      yield r'allowMultiselect';
      yield serializers.serialize(
        object.allowMultiselect,
        specifiedType: const FullType(bool),
      );
    }
    if (object.skipToVictory != null) {
      yield r'skipToVictory';
      yield serializers.serialize(
        object.skipToVictory,
        specifiedType: const FullType(bool),
      );
    }
    if (object.skipEdgeRecognition != null) {
      yield r'skipEdgeRecognition';
      yield serializers.serialize(
        object.skipEdgeRecognition,
        specifiedType: const FullType(bool),
      );
    }
    if (object.skipStickerBurning != null) {
      yield r'skipStickerBurning';
      yield serializers.serialize(
        object.skipStickerBurning,
        specifiedType: const FullType(bool),
      );
    }
    if (object.stickers != null) {
      yield r'stickers';
      yield serializers.serialize(
        object.stickers,
        specifiedType: const FullType(ZScannerStickerConfig),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerModuleConfigUnion object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerModuleConfigUnionBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'subfolder':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerModuleConfigUnionSubfolderEnum),
          ) as ZScannerModuleConfigUnionSubfolderEnum;
          result.subfolder = valueDes;
          break;
        case r'subfolderTypeId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.subfolderTypeId = valueDes;
          break;
        case r'maxVideoDuration':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.maxVideoDuration = valueDes;
          break;
        case r'recordAudio':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerModuleConfigUnionRecordAudioEnum),
          ) as ZScannerModuleConfigUnionRecordAudioEnum;
          result.recordAudio = valueDes;
          break;
        case r'allowMimeTypes':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.allowMimeTypes.replace(valueDes);
          break;
        case r'allowShareTo':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.allowShareTo = valueDes;
          break;
        case r'allowGallery':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.allowGallery = valueDes;
          break;
        case r'allowMultiselect':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.allowMultiselect = valueDes;
          break;
        case r'skipToVictory':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.skipToVictory = valueDes;
          break;
        case r'skipEdgeRecognition':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.skipEdgeRecognition = valueDes;
          break;
        case r'skipStickerBurning':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.skipStickerBurning = valueDes;
          break;
        case r'stickers':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerStickerConfig),
          ) as ZScannerStickerConfig;
          result.stickers.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerModuleConfigUnion deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerModuleConfigUnionBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

class ZScannerModuleConfigUnionSubfolderEnum extends EnumClass {

  @BuiltValueEnumConst(wireName: r'REQUIRED')
  static const ZScannerModuleConfigUnionSubfolderEnum REQUIRED = _$zScannerModuleConfigUnionSubfolderEnum_REQUIRED;
  @BuiltValueEnumConst(wireName: r'OPTIONAL')
  static const ZScannerModuleConfigUnionSubfolderEnum OPTIONAL = _$zScannerModuleConfigUnionSubfolderEnum_OPTIONAL;

  @BuiltValueEnumConst(wireName: r'____unknown____', fallback: true)
  static const ZScannerModuleConfigUnionSubfolderEnum unknown = _$zScannerModuleConfigUnionSubfolderEnum_$unknown;

  static Serializer<ZScannerModuleConfigUnionSubfolderEnum> get serializer => _$zScannerModuleConfigUnionSubfolderEnumSerializer;

  const ZScannerModuleConfigUnionSubfolderEnum._(String name): super(name);

  static BuiltSet<ZScannerModuleConfigUnionSubfolderEnum> get values => _$zScannerModuleConfigUnionSubfolderEnumValues;
  static ZScannerModuleConfigUnionSubfolderEnum valueOf(String name) => _$zScannerModuleConfigUnionSubfolderEnumValueOf(name);
}


class ZScannerModuleConfigUnionRecordAudioEnum extends EnumClass {

  @BuiltValueEnumConst(wireName: r'ALWAYS')
  static const ZScannerModuleConfigUnionRecordAudioEnum ALWAYS = _$zScannerModuleConfigUnionRecordAudioEnum_ALWAYS;
  @BuiltValueEnumConst(wireName: r'NEVER')
  static const ZScannerModuleConfigUnionRecordAudioEnum NEVER = _$zScannerModuleConfigUnionRecordAudioEnum_NEVER;
  @BuiltValueEnumConst(wireName: r'ALLOW-DEFAULT-YES')
  static const ZScannerModuleConfigUnionRecordAudioEnum ALLOW_DEFAULT_YES = _$zScannerModuleConfigUnionRecordAudioEnum_ALLOW_DEFAULT_YES;
  @BuiltValueEnumConst(wireName: r'ALLOW-DEFAULT-NO')
  static const ZScannerModuleConfigUnionRecordAudioEnum ALLOW_DEFAULT_NO = _$zScannerModuleConfigUnionRecordAudioEnum_ALLOW_DEFAULT_NO;

  @BuiltValueEnumConst(wireName: r'____unknown____', fallback: true)
  static const ZScannerModuleConfigUnionRecordAudioEnum unknown = _$zScannerModuleConfigUnionRecordAudioEnum_$unknown;

  static Serializer<ZScannerModuleConfigUnionRecordAudioEnum> get serializer => _$zScannerModuleConfigUnionRecordAudioEnumSerializer;

  const ZScannerModuleConfigUnionRecordAudioEnum._(String name): super(name);

  static BuiltSet<ZScannerModuleConfigUnionRecordAudioEnum> get values => _$zScannerModuleConfigUnionRecordAudioEnumValues;
  static ZScannerModuleConfigUnionRecordAudioEnum valueOf(String name) => _$zScannerModuleConfigUnionRecordAudioEnumValueOf(name);
}


