//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'spot_map_view_type.g.dart';

class SpotMapViewType extends EnumClass {

  @BuiltValueEnumConst(wireName: r'IMAGE')
  static const SpotMapViewType IMAGE = _$IMAGE;
  @BuiltValueEnumConst(wireName: r'LIST')
  static const SpotMapViewType LIST = _$LIST;

  @BuiltValueEnumConst(wireName: r'____unknown____', fallback: true)
  static const SpotMapViewType unknown = _$unknown;

  static Serializer<SpotMapViewType> get serializer => _$spotMapViewTypeSerializer;

  const SpotMapViewType._(String name): super(name);

  static BuiltSet<SpotMapViewType> get values => _$values;
  static SpotMapViewType valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class SpotMapViewTypeMixin = Object with _$SpotMapViewTypeMixin;

