//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_metadata_value.g.dart';

/// ZScannerMetadataValue
///
/// Properties:
/// * [dateValue] 
/// * [stringValue] 
/// * [numberValue] 
/// * [booleanValue] 
/// * [dateTimeValue] 
/// * [multiStringValue] 
@BuiltValue()
abstract class ZScannerMetadataValue implements Built<ZScannerMetadataValue, ZScannerMetadataValueBuilder> {
  @BuiltValueField(wireName: r'dateValue')
  String? get dateValue;

  @BuiltValueField(wireName: r'stringValue')
  String? get stringValue;

  @BuiltValueField(wireName: r'numberValue')
  num? get numberValue;

  @BuiltValueField(wireName: r'booleanValue')
  bool? get booleanValue;

  @BuiltValueField(wireName: r'dateTimeValue')
  DateTime? get dateTimeValue;

  @BuiltValueField(wireName: r'multiStringValue')
  BuiltList<String>? get multiStringValue;

  ZScannerMetadataValue._();

  factory ZScannerMetadataValue([void updates(ZScannerMetadataValueBuilder b)]) = _$ZScannerMetadataValue;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerMetadataValueBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerMetadataValue> get serializer => _$ZScannerMetadataValueSerializer();
}

class _$ZScannerMetadataValueSerializer implements PrimitiveSerializer<ZScannerMetadataValue> {
  @override
  final Iterable<Type> types = const [ZScannerMetadataValue, _$ZScannerMetadataValue];

  @override
  final String wireName = r'ZScannerMetadataValue';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerMetadataValue object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.dateValue != null) {
      yield r'dateValue';
      yield serializers.serialize(
        object.dateValue,
        specifiedType: const FullType(String),
      );
    }
    if (object.stringValue != null) {
      yield r'stringValue';
      yield serializers.serialize(
        object.stringValue,
        specifiedType: const FullType(String),
      );
    }
    if (object.numberValue != null) {
      yield r'numberValue';
      yield serializers.serialize(
        object.numberValue,
        specifiedType: const FullType(num),
      );
    }
    if (object.booleanValue != null) {
      yield r'booleanValue';
      yield serializers.serialize(
        object.booleanValue,
        specifiedType: const FullType(bool),
      );
    }
    if (object.dateTimeValue != null) {
      yield r'dateTimeValue';
      yield serializers.serialize(
        object.dateTimeValue,
        specifiedType: const FullType(DateTime),
      );
    }
    if (object.multiStringValue != null) {
      yield r'multiStringValue';
      yield serializers.serialize(
        object.multiStringValue,
        specifiedType: const FullType(BuiltList, [FullType(String)]),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerMetadataValue object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerMetadataValueBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'dateValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.dateValue = valueDes;
          break;
        case r'stringValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.stringValue = valueDes;
          break;
        case r'numberValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(num),
          ) as num;
          result.numberValue = valueDes;
          break;
        case r'booleanValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.booleanValue = valueDes;
          break;
        case r'dateTimeValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(DateTime),
          ) as DateTime;
          result.dateTimeValue = valueDes;
          break;
        case r'multiStringValue':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(String)]),
          ) as BuiltList<String>;
          result.multiStringValue.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerMetadataValue deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerMetadataValueBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

