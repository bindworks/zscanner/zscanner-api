// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_synchronization_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerSynchronizationRequest extends ZScannerSynchronizationRequest {
  @override
  final BuiltList<String>? requiredCaps;
  @override
  final BuiltList<String>? supportedCaps;
  @override
  final BuiltList<ZScannerMetadataItem> metadata;
  @override
  final String? state;

  factory _$ZScannerSynchronizationRequest(
          [void Function(ZScannerSynchronizationRequestBuilder)? updates]) =>
      (new ZScannerSynchronizationRequestBuilder()..update(updates))._build();

  _$ZScannerSynchronizationRequest._(
      {this.requiredCaps,
      this.supportedCaps,
      required this.metadata,
      this.state})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        metadata, r'ZScannerSynchronizationRequest', 'metadata');
  }

  @override
  ZScannerSynchronizationRequest rebuild(
          void Function(ZScannerSynchronizationRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerSynchronizationRequestBuilder toBuilder() =>
      new ZScannerSynchronizationRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerSynchronizationRequest &&
        requiredCaps == other.requiredCaps &&
        supportedCaps == other.supportedCaps &&
        metadata == other.metadata &&
        state == other.state;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, requiredCaps.hashCode);
    _$hash = $jc(_$hash, supportedCaps.hashCode);
    _$hash = $jc(_$hash, metadata.hashCode);
    _$hash = $jc(_$hash, state.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerSynchronizationRequest')
          ..add('requiredCaps', requiredCaps)
          ..add('supportedCaps', supportedCaps)
          ..add('metadata', metadata)
          ..add('state', state))
        .toString();
  }
}

class ZScannerSynchronizationRequestBuilder
    implements
        Builder<ZScannerSynchronizationRequest,
            ZScannerSynchronizationRequestBuilder> {
  _$ZScannerSynchronizationRequest? _$v;

  ListBuilder<String>? _requiredCaps;
  ListBuilder<String> get requiredCaps =>
      _$this._requiredCaps ??= new ListBuilder<String>();
  set requiredCaps(ListBuilder<String>? requiredCaps) =>
      _$this._requiredCaps = requiredCaps;

  ListBuilder<String>? _supportedCaps;
  ListBuilder<String> get supportedCaps =>
      _$this._supportedCaps ??= new ListBuilder<String>();
  set supportedCaps(ListBuilder<String>? supportedCaps) =>
      _$this._supportedCaps = supportedCaps;

  ListBuilder<ZScannerMetadataItem>? _metadata;
  ListBuilder<ZScannerMetadataItem> get metadata =>
      _$this._metadata ??= new ListBuilder<ZScannerMetadataItem>();
  set metadata(ListBuilder<ZScannerMetadataItem>? metadata) =>
      _$this._metadata = metadata;

  String? _state;
  String? get state => _$this._state;
  set state(String? state) => _$this._state = state;

  ZScannerSynchronizationRequestBuilder() {
    ZScannerSynchronizationRequest._defaults(this);
  }

  ZScannerSynchronizationRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _requiredCaps = $v.requiredCaps?.toBuilder();
      _supportedCaps = $v.supportedCaps?.toBuilder();
      _metadata = $v.metadata.toBuilder();
      _state = $v.state;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerSynchronizationRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerSynchronizationRequest;
  }

  @override
  void update(void Function(ZScannerSynchronizationRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerSynchronizationRequest build() => _build();

  _$ZScannerSynchronizationRequest _build() {
    _$ZScannerSynchronizationRequest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerSynchronizationRequest._(
              requiredCaps: _requiredCaps?.build(),
              supportedCaps: _supportedCaps?.build(),
              metadata: metadata.build(),
              state: state);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'requiredCaps';
        _requiredCaps?.build();
        _$failedField = 'supportedCaps';
        _supportedCaps?.build();
        _$failedField = 'metadata';
        metadata.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerSynchronizationRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
