//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:zscanner_api/src/model/z_scanner_global_manifest.dart';
import 'package:built_collection/built_collection.dart';
import 'package:zscanner_api/src/model/z_scanner_installation_manifest.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_bootstrap_manifest.g.dart';

/// ZScannerBootstrapManifest
///
/// Properties:
/// * [global] 
/// * [installations] 
@BuiltValue()
abstract class ZScannerBootstrapManifest implements Built<ZScannerBootstrapManifest, ZScannerBootstrapManifestBuilder> {
  @BuiltValueField(wireName: r'global')
  ZScannerGlobalManifest get global;

  @BuiltValueField(wireName: r'installations')
  BuiltList<ZScannerInstallationManifest> get installations;

  ZScannerBootstrapManifest._();

  factory ZScannerBootstrapManifest([void updates(ZScannerBootstrapManifestBuilder b)]) = _$ZScannerBootstrapManifest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerBootstrapManifestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerBootstrapManifest> get serializer => _$ZScannerBootstrapManifestSerializer();
}

class _$ZScannerBootstrapManifestSerializer implements PrimitiveSerializer<ZScannerBootstrapManifest> {
  @override
  final Iterable<Type> types = const [ZScannerBootstrapManifest, _$ZScannerBootstrapManifest];

  @override
  final String wireName = r'ZScannerBootstrapManifest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerBootstrapManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'global';
    yield serializers.serialize(
      object.global,
      specifiedType: const FullType(ZScannerGlobalManifest),
    );
    yield r'installations';
    yield serializers.serialize(
      object.installations,
      specifiedType: const FullType(BuiltList, [FullType(ZScannerInstallationManifest)]),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerBootstrapManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerBootstrapManifestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'global':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(ZScannerGlobalManifest),
          ) as ZScannerGlobalManifest;
          result.global.replace(valueDes);
          break;
        case r'installations':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(BuiltList, [FullType(ZScannerInstallationManifest)]),
          ) as BuiltList<ZScannerInstallationManifest>;
          result.installations.replace(valueDes);
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerBootstrapManifest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerBootstrapManifestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

