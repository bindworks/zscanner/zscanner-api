// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_upload_batch_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerUploadBatchRequest extends ZScannerUploadBatchRequest {
  @override
  final String uuid;
  @override
  final String folderId;
  @override
  final String flowId;
  @override
  final String moduleId;
  @override
  final String moduleVersion;
  @override
  final DateTime createdTs;
  @override
  final BuiltList<ZScannerUploadBatchMedia> media;
  @override
  final BuiltList<ZScannerMetadataItem> metadata;
  @override
  final BuiltList<ZScannerSubfolder>? subfolders;

  factory _$ZScannerUploadBatchRequest(
          [void Function(ZScannerUploadBatchRequestBuilder)? updates]) =>
      (new ZScannerUploadBatchRequestBuilder()..update(updates))._build();

  _$ZScannerUploadBatchRequest._(
      {required this.uuid,
      required this.folderId,
      required this.flowId,
      required this.moduleId,
      required this.moduleVersion,
      required this.createdTs,
      required this.media,
      required this.metadata,
      this.subfolders})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        uuid, r'ZScannerUploadBatchRequest', 'uuid');
    BuiltValueNullFieldError.checkNotNull(
        folderId, r'ZScannerUploadBatchRequest', 'folderId');
    BuiltValueNullFieldError.checkNotNull(
        flowId, r'ZScannerUploadBatchRequest', 'flowId');
    BuiltValueNullFieldError.checkNotNull(
        moduleId, r'ZScannerUploadBatchRequest', 'moduleId');
    BuiltValueNullFieldError.checkNotNull(
        moduleVersion, r'ZScannerUploadBatchRequest', 'moduleVersion');
    BuiltValueNullFieldError.checkNotNull(
        createdTs, r'ZScannerUploadBatchRequest', 'createdTs');
    BuiltValueNullFieldError.checkNotNull(
        media, r'ZScannerUploadBatchRequest', 'media');
    BuiltValueNullFieldError.checkNotNull(
        metadata, r'ZScannerUploadBatchRequest', 'metadata');
  }

  @override
  ZScannerUploadBatchRequest rebuild(
          void Function(ZScannerUploadBatchRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerUploadBatchRequestBuilder toBuilder() =>
      new ZScannerUploadBatchRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerUploadBatchRequest &&
        uuid == other.uuid &&
        folderId == other.folderId &&
        flowId == other.flowId &&
        moduleId == other.moduleId &&
        moduleVersion == other.moduleVersion &&
        createdTs == other.createdTs &&
        media == other.media &&
        metadata == other.metadata &&
        subfolders == other.subfolders;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, uuid.hashCode);
    _$hash = $jc(_$hash, folderId.hashCode);
    _$hash = $jc(_$hash, flowId.hashCode);
    _$hash = $jc(_$hash, moduleId.hashCode);
    _$hash = $jc(_$hash, moduleVersion.hashCode);
    _$hash = $jc(_$hash, createdTs.hashCode);
    _$hash = $jc(_$hash, media.hashCode);
    _$hash = $jc(_$hash, metadata.hashCode);
    _$hash = $jc(_$hash, subfolders.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerUploadBatchRequest')
          ..add('uuid', uuid)
          ..add('folderId', folderId)
          ..add('flowId', flowId)
          ..add('moduleId', moduleId)
          ..add('moduleVersion', moduleVersion)
          ..add('createdTs', createdTs)
          ..add('media', media)
          ..add('metadata', metadata)
          ..add('subfolders', subfolders))
        .toString();
  }
}

class ZScannerUploadBatchRequestBuilder
    implements
        Builder<ZScannerUploadBatchRequest, ZScannerUploadBatchRequestBuilder> {
  _$ZScannerUploadBatchRequest? _$v;

  String? _uuid;
  String? get uuid => _$this._uuid;
  set uuid(String? uuid) => _$this._uuid = uuid;

  String? _folderId;
  String? get folderId => _$this._folderId;
  set folderId(String? folderId) => _$this._folderId = folderId;

  String? _flowId;
  String? get flowId => _$this._flowId;
  set flowId(String? flowId) => _$this._flowId = flowId;

  String? _moduleId;
  String? get moduleId => _$this._moduleId;
  set moduleId(String? moduleId) => _$this._moduleId = moduleId;

  String? _moduleVersion;
  String? get moduleVersion => _$this._moduleVersion;
  set moduleVersion(String? moduleVersion) =>
      _$this._moduleVersion = moduleVersion;

  DateTime? _createdTs;
  DateTime? get createdTs => _$this._createdTs;
  set createdTs(DateTime? createdTs) => _$this._createdTs = createdTs;

  ListBuilder<ZScannerUploadBatchMedia>? _media;
  ListBuilder<ZScannerUploadBatchMedia> get media =>
      _$this._media ??= new ListBuilder<ZScannerUploadBatchMedia>();
  set media(ListBuilder<ZScannerUploadBatchMedia>? media) =>
      _$this._media = media;

  ListBuilder<ZScannerMetadataItem>? _metadata;
  ListBuilder<ZScannerMetadataItem> get metadata =>
      _$this._metadata ??= new ListBuilder<ZScannerMetadataItem>();
  set metadata(ListBuilder<ZScannerMetadataItem>? metadata) =>
      _$this._metadata = metadata;

  ListBuilder<ZScannerSubfolder>? _subfolders;
  ListBuilder<ZScannerSubfolder> get subfolders =>
      _$this._subfolders ??= new ListBuilder<ZScannerSubfolder>();
  set subfolders(ListBuilder<ZScannerSubfolder>? subfolders) =>
      _$this._subfolders = subfolders;

  ZScannerUploadBatchRequestBuilder() {
    ZScannerUploadBatchRequest._defaults(this);
  }

  ZScannerUploadBatchRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _uuid = $v.uuid;
      _folderId = $v.folderId;
      _flowId = $v.flowId;
      _moduleId = $v.moduleId;
      _moduleVersion = $v.moduleVersion;
      _createdTs = $v.createdTs;
      _media = $v.media.toBuilder();
      _metadata = $v.metadata.toBuilder();
      _subfolders = $v.subfolders?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerUploadBatchRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerUploadBatchRequest;
  }

  @override
  void update(void Function(ZScannerUploadBatchRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerUploadBatchRequest build() => _build();

  _$ZScannerUploadBatchRequest _build() {
    _$ZScannerUploadBatchRequest _$result;
    try {
      _$result = _$v ??
          new _$ZScannerUploadBatchRequest._(
              uuid: BuiltValueNullFieldError.checkNotNull(
                  uuid, r'ZScannerUploadBatchRequest', 'uuid'),
              folderId: BuiltValueNullFieldError.checkNotNull(
                  folderId, r'ZScannerUploadBatchRequest', 'folderId'),
              flowId: BuiltValueNullFieldError.checkNotNull(
                  flowId, r'ZScannerUploadBatchRequest', 'flowId'),
              moduleId: BuiltValueNullFieldError.checkNotNull(
                  moduleId, r'ZScannerUploadBatchRequest', 'moduleId'),
              moduleVersion: BuiltValueNullFieldError.checkNotNull(
                  moduleVersion,
                  r'ZScannerUploadBatchRequest',
                  'moduleVersion'),
              createdTs: BuiltValueNullFieldError.checkNotNull(
                  createdTs, r'ZScannerUploadBatchRequest', 'createdTs'),
              media: media.build(),
              metadata: metadata.build(),
              subfolders: _subfolders?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'media';
        media.build();
        _$failedField = 'metadata';
        metadata.build();
        _$failedField = 'subfolders';
        _subfolders?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerUploadBatchRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
