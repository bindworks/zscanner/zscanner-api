// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_module_config_union.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ZScannerModuleConfigUnionSubfolderEnum
    _$zScannerModuleConfigUnionSubfolderEnum_REQUIRED =
    const ZScannerModuleConfigUnionSubfolderEnum._('REQUIRED');
const ZScannerModuleConfigUnionSubfolderEnum
    _$zScannerModuleConfigUnionSubfolderEnum_OPTIONAL =
    const ZScannerModuleConfigUnionSubfolderEnum._('OPTIONAL');
const ZScannerModuleConfigUnionSubfolderEnum
    _$zScannerModuleConfigUnionSubfolderEnum_$unknown =
    const ZScannerModuleConfigUnionSubfolderEnum._('unknown');

ZScannerModuleConfigUnionSubfolderEnum
    _$zScannerModuleConfigUnionSubfolderEnumValueOf(String name) {
  switch (name) {
    case 'REQUIRED':
      return _$zScannerModuleConfigUnionSubfolderEnum_REQUIRED;
    case 'OPTIONAL':
      return _$zScannerModuleConfigUnionSubfolderEnum_OPTIONAL;
    case 'unknown':
      return _$zScannerModuleConfigUnionSubfolderEnum_$unknown;
    default:
      return _$zScannerModuleConfigUnionSubfolderEnum_$unknown;
  }
}

final BuiltSet<ZScannerModuleConfigUnionSubfolderEnum>
    _$zScannerModuleConfigUnionSubfolderEnumValues = new BuiltSet<
        ZScannerModuleConfigUnionSubfolderEnum>(const <ZScannerModuleConfigUnionSubfolderEnum>[
  _$zScannerModuleConfigUnionSubfolderEnum_REQUIRED,
  _$zScannerModuleConfigUnionSubfolderEnum_OPTIONAL,
  _$zScannerModuleConfigUnionSubfolderEnum_$unknown,
]);

const ZScannerModuleConfigUnionRecordAudioEnum
    _$zScannerModuleConfigUnionRecordAudioEnum_ALWAYS =
    const ZScannerModuleConfigUnionRecordAudioEnum._('ALWAYS');
const ZScannerModuleConfigUnionRecordAudioEnum
    _$zScannerModuleConfigUnionRecordAudioEnum_NEVER =
    const ZScannerModuleConfigUnionRecordAudioEnum._('NEVER');
const ZScannerModuleConfigUnionRecordAudioEnum
    _$zScannerModuleConfigUnionRecordAudioEnum_ALLOW_DEFAULT_YES =
    const ZScannerModuleConfigUnionRecordAudioEnum._('ALLOW_DEFAULT_YES');
const ZScannerModuleConfigUnionRecordAudioEnum
    _$zScannerModuleConfigUnionRecordAudioEnum_ALLOW_DEFAULT_NO =
    const ZScannerModuleConfigUnionRecordAudioEnum._('ALLOW_DEFAULT_NO');
const ZScannerModuleConfigUnionRecordAudioEnum
    _$zScannerModuleConfigUnionRecordAudioEnum_$unknown =
    const ZScannerModuleConfigUnionRecordAudioEnum._('unknown');

ZScannerModuleConfigUnionRecordAudioEnum
    _$zScannerModuleConfigUnionRecordAudioEnumValueOf(String name) {
  switch (name) {
    case 'ALWAYS':
      return _$zScannerModuleConfigUnionRecordAudioEnum_ALWAYS;
    case 'NEVER':
      return _$zScannerModuleConfigUnionRecordAudioEnum_NEVER;
    case 'ALLOW_DEFAULT_YES':
      return _$zScannerModuleConfigUnionRecordAudioEnum_ALLOW_DEFAULT_YES;
    case 'ALLOW_DEFAULT_NO':
      return _$zScannerModuleConfigUnionRecordAudioEnum_ALLOW_DEFAULT_NO;
    case 'unknown':
      return _$zScannerModuleConfigUnionRecordAudioEnum_$unknown;
    default:
      return _$zScannerModuleConfigUnionRecordAudioEnum_$unknown;
  }
}

final BuiltSet<ZScannerModuleConfigUnionRecordAudioEnum>
    _$zScannerModuleConfigUnionRecordAudioEnumValues = new BuiltSet<
        ZScannerModuleConfigUnionRecordAudioEnum>(const <ZScannerModuleConfigUnionRecordAudioEnum>[
  _$zScannerModuleConfigUnionRecordAudioEnum_ALWAYS,
  _$zScannerModuleConfigUnionRecordAudioEnum_NEVER,
  _$zScannerModuleConfigUnionRecordAudioEnum_ALLOW_DEFAULT_YES,
  _$zScannerModuleConfigUnionRecordAudioEnum_ALLOW_DEFAULT_NO,
  _$zScannerModuleConfigUnionRecordAudioEnum_$unknown,
]);

Serializer<ZScannerModuleConfigUnionSubfolderEnum>
    _$zScannerModuleConfigUnionSubfolderEnumSerializer =
    new _$ZScannerModuleConfigUnionSubfolderEnumSerializer();
Serializer<ZScannerModuleConfigUnionRecordAudioEnum>
    _$zScannerModuleConfigUnionRecordAudioEnumSerializer =
    new _$ZScannerModuleConfigUnionRecordAudioEnumSerializer();

class _$ZScannerModuleConfigUnionSubfolderEnumSerializer
    implements PrimitiveSerializer<ZScannerModuleConfigUnionSubfolderEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'REQUIRED': 'REQUIRED',
    'OPTIONAL': 'OPTIONAL',
    'unknown': '____unknown____',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'REQUIRED': 'REQUIRED',
    'OPTIONAL': 'OPTIONAL',
    '____unknown____': 'unknown',
  };

  @override
  final Iterable<Type> types = const <Type>[
    ZScannerModuleConfigUnionSubfolderEnum
  ];
  @override
  final String wireName = 'ZScannerModuleConfigUnionSubfolderEnum';

  @override
  Object serialize(Serializers serializers,
          ZScannerModuleConfigUnionSubfolderEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ZScannerModuleConfigUnionSubfolderEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ZScannerModuleConfigUnionSubfolderEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$ZScannerModuleConfigUnionRecordAudioEnumSerializer
    implements PrimitiveSerializer<ZScannerModuleConfigUnionRecordAudioEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'ALWAYS': 'ALWAYS',
    'NEVER': 'NEVER',
    'ALLOW_DEFAULT_YES': 'ALLOW-DEFAULT-YES',
    'ALLOW_DEFAULT_NO': 'ALLOW-DEFAULT-NO',
    'unknown': '____unknown____',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'ALWAYS': 'ALWAYS',
    'NEVER': 'NEVER',
    'ALLOW-DEFAULT-YES': 'ALLOW_DEFAULT_YES',
    'ALLOW-DEFAULT-NO': 'ALLOW_DEFAULT_NO',
    '____unknown____': 'unknown',
  };

  @override
  final Iterable<Type> types = const <Type>[
    ZScannerModuleConfigUnionRecordAudioEnum
  ];
  @override
  final String wireName = 'ZScannerModuleConfigUnionRecordAudioEnum';

  @override
  Object serialize(Serializers serializers,
          ZScannerModuleConfigUnionRecordAudioEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ZScannerModuleConfigUnionRecordAudioEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ZScannerModuleConfigUnionRecordAudioEnum.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$ZScannerModuleConfigUnion extends ZScannerModuleConfigUnion {
  @override
  final ZScannerModuleConfigUnionSubfolderEnum? subfolder;
  @override
  final String? subfolderTypeId;
  @override
  final int? maxVideoDuration;
  @override
  final ZScannerModuleConfigUnionRecordAudioEnum? recordAudio;
  @override
  final BuiltList<String>? allowMimeTypes;
  @override
  final bool? allowShareTo;
  @override
  final bool? allowGallery;
  @override
  final bool? allowMultiselect;
  @override
  final bool? skipToVictory;
  @override
  final bool? skipEdgeRecognition;
  @override
  final bool? skipStickerBurning;
  @override
  final ZScannerStickerConfig? stickers;

  factory _$ZScannerModuleConfigUnion(
          [void Function(ZScannerModuleConfigUnionBuilder)? updates]) =>
      (new ZScannerModuleConfigUnionBuilder()..update(updates))._build();

  _$ZScannerModuleConfigUnion._(
      {this.subfolder,
      this.subfolderTypeId,
      this.maxVideoDuration,
      this.recordAudio,
      this.allowMimeTypes,
      this.allowShareTo,
      this.allowGallery,
      this.allowMultiselect,
      this.skipToVictory,
      this.skipEdgeRecognition,
      this.skipStickerBurning,
      this.stickers})
      : super._();

  @override
  ZScannerModuleConfigUnion rebuild(
          void Function(ZScannerModuleConfigUnionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerModuleConfigUnionBuilder toBuilder() =>
      new ZScannerModuleConfigUnionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerModuleConfigUnion &&
        subfolder == other.subfolder &&
        subfolderTypeId == other.subfolderTypeId &&
        maxVideoDuration == other.maxVideoDuration &&
        recordAudio == other.recordAudio &&
        allowMimeTypes == other.allowMimeTypes &&
        allowShareTo == other.allowShareTo &&
        allowGallery == other.allowGallery &&
        allowMultiselect == other.allowMultiselect &&
        skipToVictory == other.skipToVictory &&
        skipEdgeRecognition == other.skipEdgeRecognition &&
        skipStickerBurning == other.skipStickerBurning &&
        stickers == other.stickers;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, subfolder.hashCode);
    _$hash = $jc(_$hash, subfolderTypeId.hashCode);
    _$hash = $jc(_$hash, maxVideoDuration.hashCode);
    _$hash = $jc(_$hash, recordAudio.hashCode);
    _$hash = $jc(_$hash, allowMimeTypes.hashCode);
    _$hash = $jc(_$hash, allowShareTo.hashCode);
    _$hash = $jc(_$hash, allowGallery.hashCode);
    _$hash = $jc(_$hash, allowMultiselect.hashCode);
    _$hash = $jc(_$hash, skipToVictory.hashCode);
    _$hash = $jc(_$hash, skipEdgeRecognition.hashCode);
    _$hash = $jc(_$hash, skipStickerBurning.hashCode);
    _$hash = $jc(_$hash, stickers.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerModuleConfigUnion')
          ..add('subfolder', subfolder)
          ..add('subfolderTypeId', subfolderTypeId)
          ..add('maxVideoDuration', maxVideoDuration)
          ..add('recordAudio', recordAudio)
          ..add('allowMimeTypes', allowMimeTypes)
          ..add('allowShareTo', allowShareTo)
          ..add('allowGallery', allowGallery)
          ..add('allowMultiselect', allowMultiselect)
          ..add('skipToVictory', skipToVictory)
          ..add('skipEdgeRecognition', skipEdgeRecognition)
          ..add('skipStickerBurning', skipStickerBurning)
          ..add('stickers', stickers))
        .toString();
  }
}

class ZScannerModuleConfigUnionBuilder
    implements
        Builder<ZScannerModuleConfigUnion, ZScannerModuleConfigUnionBuilder> {
  _$ZScannerModuleConfigUnion? _$v;

  ZScannerModuleConfigUnionSubfolderEnum? _subfolder;
  ZScannerModuleConfigUnionSubfolderEnum? get subfolder => _$this._subfolder;
  set subfolder(ZScannerModuleConfigUnionSubfolderEnum? subfolder) =>
      _$this._subfolder = subfolder;

  String? _subfolderTypeId;
  String? get subfolderTypeId => _$this._subfolderTypeId;
  set subfolderTypeId(String? subfolderTypeId) =>
      _$this._subfolderTypeId = subfolderTypeId;

  int? _maxVideoDuration;
  int? get maxVideoDuration => _$this._maxVideoDuration;
  set maxVideoDuration(int? maxVideoDuration) =>
      _$this._maxVideoDuration = maxVideoDuration;

  ZScannerModuleConfigUnionRecordAudioEnum? _recordAudio;
  ZScannerModuleConfigUnionRecordAudioEnum? get recordAudio =>
      _$this._recordAudio;
  set recordAudio(ZScannerModuleConfigUnionRecordAudioEnum? recordAudio) =>
      _$this._recordAudio = recordAudio;

  ListBuilder<String>? _allowMimeTypes;
  ListBuilder<String> get allowMimeTypes =>
      _$this._allowMimeTypes ??= new ListBuilder<String>();
  set allowMimeTypes(ListBuilder<String>? allowMimeTypes) =>
      _$this._allowMimeTypes = allowMimeTypes;

  bool? _allowShareTo;
  bool? get allowShareTo => _$this._allowShareTo;
  set allowShareTo(bool? allowShareTo) => _$this._allowShareTo = allowShareTo;

  bool? _allowGallery;
  bool? get allowGallery => _$this._allowGallery;
  set allowGallery(bool? allowGallery) => _$this._allowGallery = allowGallery;

  bool? _allowMultiselect;
  bool? get allowMultiselect => _$this._allowMultiselect;
  set allowMultiselect(bool? allowMultiselect) =>
      _$this._allowMultiselect = allowMultiselect;

  bool? _skipToVictory;
  bool? get skipToVictory => _$this._skipToVictory;
  set skipToVictory(bool? skipToVictory) =>
      _$this._skipToVictory = skipToVictory;

  bool? _skipEdgeRecognition;
  bool? get skipEdgeRecognition => _$this._skipEdgeRecognition;
  set skipEdgeRecognition(bool? skipEdgeRecognition) =>
      _$this._skipEdgeRecognition = skipEdgeRecognition;

  bool? _skipStickerBurning;
  bool? get skipStickerBurning => _$this._skipStickerBurning;
  set skipStickerBurning(bool? skipStickerBurning) =>
      _$this._skipStickerBurning = skipStickerBurning;

  ZScannerStickerConfigBuilder? _stickers;
  ZScannerStickerConfigBuilder get stickers =>
      _$this._stickers ??= new ZScannerStickerConfigBuilder();
  set stickers(ZScannerStickerConfigBuilder? stickers) =>
      _$this._stickers = stickers;

  ZScannerModuleConfigUnionBuilder() {
    ZScannerModuleConfigUnion._defaults(this);
  }

  ZScannerModuleConfigUnionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _subfolder = $v.subfolder;
      _subfolderTypeId = $v.subfolderTypeId;
      _maxVideoDuration = $v.maxVideoDuration;
      _recordAudio = $v.recordAudio;
      _allowMimeTypes = $v.allowMimeTypes?.toBuilder();
      _allowShareTo = $v.allowShareTo;
      _allowGallery = $v.allowGallery;
      _allowMultiselect = $v.allowMultiselect;
      _skipToVictory = $v.skipToVictory;
      _skipEdgeRecognition = $v.skipEdgeRecognition;
      _skipStickerBurning = $v.skipStickerBurning;
      _stickers = $v.stickers?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerModuleConfigUnion other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerModuleConfigUnion;
  }

  @override
  void update(void Function(ZScannerModuleConfigUnionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerModuleConfigUnion build() => _build();

  _$ZScannerModuleConfigUnion _build() {
    _$ZScannerModuleConfigUnion _$result;
    try {
      _$result = _$v ??
          new _$ZScannerModuleConfigUnion._(
              subfolder: subfolder,
              subfolderTypeId: subfolderTypeId,
              maxVideoDuration: maxVideoDuration,
              recordAudio: recordAudio,
              allowMimeTypes: _allowMimeTypes?.build(),
              allowShareTo: allowShareTo,
              allowGallery: allowGallery,
              allowMultiselect: allowMultiselect,
              skipToVictory: skipToVictory,
              skipEdgeRecognition: skipEdgeRecognition,
              skipStickerBurning: skipStickerBurning,
              stickers: _stickers?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'allowMimeTypes';
        _allowMimeTypes?.build();

        _$failedField = 'stickers';
        _stickers?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerModuleConfigUnion', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
