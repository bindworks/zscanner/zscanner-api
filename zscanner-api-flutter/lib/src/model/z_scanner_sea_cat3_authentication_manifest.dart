//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_sea_cat3_authentication_manifest.g.dart';

/// ZScannerSeaCat3AuthenticationManifest
///
/// Properties:
/// * [authUrl] 
/// * [apiUrl] 
/// * [seacatUrl] 
@BuiltValue()
abstract class ZScannerSeaCat3AuthenticationManifest implements Built<ZScannerSeaCat3AuthenticationManifest, ZScannerSeaCat3AuthenticationManifestBuilder> {
  @BuiltValueField(wireName: r'authUrl')
  String get authUrl;

  @BuiltValueField(wireName: r'apiUrl')
  String get apiUrl;

  @BuiltValueField(wireName: r'seacatUrl')
  String get seacatUrl;

  ZScannerSeaCat3AuthenticationManifest._();

  factory ZScannerSeaCat3AuthenticationManifest([void updates(ZScannerSeaCat3AuthenticationManifestBuilder b)]) = _$ZScannerSeaCat3AuthenticationManifest;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerSeaCat3AuthenticationManifestBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerSeaCat3AuthenticationManifest> get serializer => _$ZScannerSeaCat3AuthenticationManifestSerializer();
}

class _$ZScannerSeaCat3AuthenticationManifestSerializer implements PrimitiveSerializer<ZScannerSeaCat3AuthenticationManifest> {
  @override
  final Iterable<Type> types = const [ZScannerSeaCat3AuthenticationManifest, _$ZScannerSeaCat3AuthenticationManifest];

  @override
  final String wireName = r'ZScannerSeaCat3AuthenticationManifest';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerSeaCat3AuthenticationManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'authUrl';
    yield serializers.serialize(
      object.authUrl,
      specifiedType: const FullType(String),
    );
    yield r'apiUrl';
    yield serializers.serialize(
      object.apiUrl,
      specifiedType: const FullType(String),
    );
    yield r'seacatUrl';
    yield serializers.serialize(
      object.seacatUrl,
      specifiedType: const FullType(String),
    );
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerSeaCat3AuthenticationManifest object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerSeaCat3AuthenticationManifestBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'authUrl':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.authUrl = valueDes;
          break;
        case r'apiUrl':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.apiUrl = valueDes;
          break;
        case r'seacatUrl':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.seacatUrl = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerSeaCat3AuthenticationManifest deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerSeaCat3AuthenticationManifestBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

