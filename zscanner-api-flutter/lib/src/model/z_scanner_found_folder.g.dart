// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_found_folder.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerFoundFolder extends ZScannerFoundFolder {
  @override
  final String? flowId;
  @override
  final BuiltList<ZScannerMetadataItem>? metadata;
  @override
  final ZScannerFolder folder;
  @override
  final String? subfolderId;

  factory _$ZScannerFoundFolder(
          [void Function(ZScannerFoundFolderBuilder)? updates]) =>
      (new ZScannerFoundFolderBuilder()..update(updates))._build();

  _$ZScannerFoundFolder._(
      {this.flowId, this.metadata, required this.folder, this.subfolderId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        folder, r'ZScannerFoundFolder', 'folder');
  }

  @override
  ZScannerFoundFolder rebuild(
          void Function(ZScannerFoundFolderBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerFoundFolderBuilder toBuilder() =>
      new ZScannerFoundFolderBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerFoundFolder &&
        flowId == other.flowId &&
        metadata == other.metadata &&
        folder == other.folder &&
        subfolderId == other.subfolderId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, flowId.hashCode);
    _$hash = $jc(_$hash, metadata.hashCode);
    _$hash = $jc(_$hash, folder.hashCode);
    _$hash = $jc(_$hash, subfolderId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerFoundFolder')
          ..add('flowId', flowId)
          ..add('metadata', metadata)
          ..add('folder', folder)
          ..add('subfolderId', subfolderId))
        .toString();
  }
}

class ZScannerFoundFolderBuilder
    implements Builder<ZScannerFoundFolder, ZScannerFoundFolderBuilder> {
  _$ZScannerFoundFolder? _$v;

  String? _flowId;
  String? get flowId => _$this._flowId;
  set flowId(String? flowId) => _$this._flowId = flowId;

  ListBuilder<ZScannerMetadataItem>? _metadata;
  ListBuilder<ZScannerMetadataItem> get metadata =>
      _$this._metadata ??= new ListBuilder<ZScannerMetadataItem>();
  set metadata(ListBuilder<ZScannerMetadataItem>? metadata) =>
      _$this._metadata = metadata;

  ZScannerFolderBuilder? _folder;
  ZScannerFolderBuilder get folder =>
      _$this._folder ??= new ZScannerFolderBuilder();
  set folder(ZScannerFolderBuilder? folder) => _$this._folder = folder;

  String? _subfolderId;
  String? get subfolderId => _$this._subfolderId;
  set subfolderId(String? subfolderId) => _$this._subfolderId = subfolderId;

  ZScannerFoundFolderBuilder() {
    ZScannerFoundFolder._defaults(this);
  }

  ZScannerFoundFolderBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _flowId = $v.flowId;
      _metadata = $v.metadata?.toBuilder();
      _folder = $v.folder.toBuilder();
      _subfolderId = $v.subfolderId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerFoundFolder other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerFoundFolder;
  }

  @override
  void update(void Function(ZScannerFoundFolderBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerFoundFolder build() => _build();

  _$ZScannerFoundFolder _build() {
    _$ZScannerFoundFolder _$result;
    try {
      _$result = _$v ??
          new _$ZScannerFoundFolder._(
              flowId: flowId,
              metadata: _metadata?.build(),
              folder: folder.build(),
              subfolderId: subfolderId);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metadata';
        _metadata?.build();
        _$failedField = 'folder';
        folder.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerFoundFolder', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
