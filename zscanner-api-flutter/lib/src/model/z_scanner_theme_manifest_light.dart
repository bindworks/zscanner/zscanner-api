//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_theme_manifest_light.g.dart';

/// ZScannerThemeManifestLight
///
/// Properties:
/// * [textScaleFactor] 
/// * [mainFontFamily] 
/// * [secondaryFontFamily] 
@BuiltValue()
abstract class ZScannerThemeManifestLight implements Built<ZScannerThemeManifestLight, ZScannerThemeManifestLightBuilder> {
  @BuiltValueField(wireName: r'textScaleFactor')
  int? get textScaleFactor;

  @BuiltValueField(wireName: r'mainFontFamily')
  String? get mainFontFamily;

  @BuiltValueField(wireName: r'secondaryFontFamily')
  String? get secondaryFontFamily;

  ZScannerThemeManifestLight._();

  factory ZScannerThemeManifestLight([void updates(ZScannerThemeManifestLightBuilder b)]) = _$ZScannerThemeManifestLight;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerThemeManifestLightBuilder b) => b;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerThemeManifestLight> get serializer => _$ZScannerThemeManifestLightSerializer();
}

class _$ZScannerThemeManifestLightSerializer implements PrimitiveSerializer<ZScannerThemeManifestLight> {
  @override
  final Iterable<Type> types = const [ZScannerThemeManifestLight, _$ZScannerThemeManifestLight];

  @override
  final String wireName = r'ZScannerThemeManifestLight';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerThemeManifestLight object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    if (object.textScaleFactor != null) {
      yield r'textScaleFactor';
      yield serializers.serialize(
        object.textScaleFactor,
        specifiedType: const FullType(int),
      );
    }
    if (object.mainFontFamily != null) {
      yield r'mainFontFamily';
      yield serializers.serialize(
        object.mainFontFamily,
        specifiedType: const FullType(String),
      );
    }
    if (object.secondaryFontFamily != null) {
      yield r'secondaryFontFamily';
      yield serializers.serialize(
        object.secondaryFontFamily,
        specifiedType: const FullType(String),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerThemeManifestLight object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerThemeManifestLightBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'textScaleFactor':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(int),
          ) as int;
          result.textScaleFactor = valueDes;
          break;
        case r'mainFontFamily':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.mainFontFamily = valueDes;
          break;
        case r'secondaryFontFamily':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.secondaryFontFamily = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerThemeManifestLight deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerThemeManifestLightBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

