// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_sticker_config.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerStickerConfig extends ZScannerStickerConfig {
  @override
  final ZScannerStickerConfigFontFace? fontFace;
  @override
  final BuiltList<String>? textStickers;

  factory _$ZScannerStickerConfig(
          [void Function(ZScannerStickerConfigBuilder)? updates]) =>
      (new ZScannerStickerConfigBuilder()..update(updates))._build();

  _$ZScannerStickerConfig._({this.fontFace, this.textStickers}) : super._();

  @override
  ZScannerStickerConfig rebuild(
          void Function(ZScannerStickerConfigBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerStickerConfigBuilder toBuilder() =>
      new ZScannerStickerConfigBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerStickerConfig &&
        fontFace == other.fontFace &&
        textStickers == other.textStickers;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, fontFace.hashCode);
    _$hash = $jc(_$hash, textStickers.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerStickerConfig')
          ..add('fontFace', fontFace)
          ..add('textStickers', textStickers))
        .toString();
  }
}

class ZScannerStickerConfigBuilder
    implements Builder<ZScannerStickerConfig, ZScannerStickerConfigBuilder> {
  _$ZScannerStickerConfig? _$v;

  ZScannerStickerConfigFontFaceBuilder? _fontFace;
  ZScannerStickerConfigFontFaceBuilder get fontFace =>
      _$this._fontFace ??= new ZScannerStickerConfigFontFaceBuilder();
  set fontFace(ZScannerStickerConfigFontFaceBuilder? fontFace) =>
      _$this._fontFace = fontFace;

  ListBuilder<String>? _textStickers;
  ListBuilder<String> get textStickers =>
      _$this._textStickers ??= new ListBuilder<String>();
  set textStickers(ListBuilder<String>? textStickers) =>
      _$this._textStickers = textStickers;

  ZScannerStickerConfigBuilder() {
    ZScannerStickerConfig._defaults(this);
  }

  ZScannerStickerConfigBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _fontFace = $v.fontFace?.toBuilder();
      _textStickers = $v.textStickers?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerStickerConfig other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerStickerConfig;
  }

  @override
  void update(void Function(ZScannerStickerConfigBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerStickerConfig build() => _build();

  _$ZScannerStickerConfig _build() {
    _$ZScannerStickerConfig _$result;
    try {
      _$result = _$v ??
          new _$ZScannerStickerConfig._(
              fontFace: _fontFace?.build(),
              textStickers: _textStickers?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'fontFace';
        _fontFace?.build();
        _$failedField = 'textStickers';
        _textStickers?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerStickerConfig', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
