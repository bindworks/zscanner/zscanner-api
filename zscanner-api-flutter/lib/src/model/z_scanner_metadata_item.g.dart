// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_metadata_item.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerMetadataItem extends ZScannerMetadataItem {
  @override
  final String metaId;
  @override
  final ZScannerMetadataValue value;

  factory _$ZScannerMetadataItem(
          [void Function(ZScannerMetadataItemBuilder)? updates]) =>
      (new ZScannerMetadataItemBuilder()..update(updates))._build();

  _$ZScannerMetadataItem._({required this.metaId, required this.value})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        metaId, r'ZScannerMetadataItem', 'metaId');
    BuiltValueNullFieldError.checkNotNull(
        value, r'ZScannerMetadataItem', 'value');
  }

  @override
  ZScannerMetadataItem rebuild(
          void Function(ZScannerMetadataItemBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerMetadataItemBuilder toBuilder() =>
      new ZScannerMetadataItemBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerMetadataItem &&
        metaId == other.metaId &&
        value == other.value;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, metaId.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerMetadataItem')
          ..add('metaId', metaId)
          ..add('value', value))
        .toString();
  }
}

class ZScannerMetadataItemBuilder
    implements Builder<ZScannerMetadataItem, ZScannerMetadataItemBuilder> {
  _$ZScannerMetadataItem? _$v;

  String? _metaId;
  String? get metaId => _$this._metaId;
  set metaId(String? metaId) => _$this._metaId = metaId;

  ZScannerMetadataValueBuilder? _value;
  ZScannerMetadataValueBuilder get value =>
      _$this._value ??= new ZScannerMetadataValueBuilder();
  set value(ZScannerMetadataValueBuilder? value) => _$this._value = value;

  ZScannerMetadataItemBuilder() {
    ZScannerMetadataItem._defaults(this);
  }

  ZScannerMetadataItemBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _metaId = $v.metaId;
      _value = $v.value.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerMetadataItem other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerMetadataItem;
  }

  @override
  void update(void Function(ZScannerMetadataItemBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerMetadataItem build() => _build();

  _$ZScannerMetadataItem _build() {
    _$ZScannerMetadataItem _$result;
    try {
      _$result = _$v ??
          new _$ZScannerMetadataItem._(
              metaId: BuiltValueNullFieldError.checkNotNull(
                  metaId, r'ZScannerMetadataItem', 'metaId'),
              value: value.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'value';
        value.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerMetadataItem', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
