// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'z_scanner_sticker_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ZScannerStickerData extends ZScannerStickerData {
  @override
  final String? fontFace;
  @override
  final String? text;
  @override
  final String? textColor;
  @override
  final BuiltList<int> boundingBox;

  factory _$ZScannerStickerData(
          [void Function(ZScannerStickerDataBuilder)? updates]) =>
      (new ZScannerStickerDataBuilder()..update(updates))._build();

  _$ZScannerStickerData._(
      {this.fontFace, this.text, this.textColor, required this.boundingBox})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        boundingBox, r'ZScannerStickerData', 'boundingBox');
  }

  @override
  ZScannerStickerData rebuild(
          void Function(ZScannerStickerDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ZScannerStickerDataBuilder toBuilder() =>
      new ZScannerStickerDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ZScannerStickerData &&
        fontFace == other.fontFace &&
        text == other.text &&
        textColor == other.textColor &&
        boundingBox == other.boundingBox;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, fontFace.hashCode);
    _$hash = $jc(_$hash, text.hashCode);
    _$hash = $jc(_$hash, textColor.hashCode);
    _$hash = $jc(_$hash, boundingBox.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ZScannerStickerData')
          ..add('fontFace', fontFace)
          ..add('text', text)
          ..add('textColor', textColor)
          ..add('boundingBox', boundingBox))
        .toString();
  }
}

class ZScannerStickerDataBuilder
    implements Builder<ZScannerStickerData, ZScannerStickerDataBuilder> {
  _$ZScannerStickerData? _$v;

  String? _fontFace;
  String? get fontFace => _$this._fontFace;
  set fontFace(String? fontFace) => _$this._fontFace = fontFace;

  String? _text;
  String? get text => _$this._text;
  set text(String? text) => _$this._text = text;

  String? _textColor;
  String? get textColor => _$this._textColor;
  set textColor(String? textColor) => _$this._textColor = textColor;

  ListBuilder<int>? _boundingBox;
  ListBuilder<int> get boundingBox =>
      _$this._boundingBox ??= new ListBuilder<int>();
  set boundingBox(ListBuilder<int>? boundingBox) =>
      _$this._boundingBox = boundingBox;

  ZScannerStickerDataBuilder() {
    ZScannerStickerData._defaults(this);
  }

  ZScannerStickerDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _fontFace = $v.fontFace;
      _text = $v.text;
      _textColor = $v.textColor;
      _boundingBox = $v.boundingBox.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ZScannerStickerData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ZScannerStickerData;
  }

  @override
  void update(void Function(ZScannerStickerDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ZScannerStickerData build() => _build();

  _$ZScannerStickerData _build() {
    _$ZScannerStickerData _$result;
    try {
      _$result = _$v ??
          new _$ZScannerStickerData._(
              fontFace: fontFace,
              text: text,
              textColor: textColor,
              boundingBox: boundingBox.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'boundingBox';
        boundingBox.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ZScannerStickerData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
