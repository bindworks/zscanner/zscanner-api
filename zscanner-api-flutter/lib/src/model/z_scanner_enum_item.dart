//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'z_scanner_enum_item.g.dart';

/// ZScannerEnumItem
///
/// Properties:
/// * [itemId] 
/// * [code] 
/// * [icon] - Icon can take several forms: - `str:...` plain string (takes advantage of emojis) - `material:...` material icon (by Flutter library code) - `res:...` specific resource by UUID 
/// * [label] 
/// * [help] 
/// * [parentItemId] 
/// * [selectable] 
@BuiltValue()
abstract class ZScannerEnumItem implements Built<ZScannerEnumItem, ZScannerEnumItemBuilder> {
  @BuiltValueField(wireName: r'itemId')
  String get itemId;

  @BuiltValueField(wireName: r'code')
  String? get code;

  /// Icon can take several forms: - `str:...` plain string (takes advantage of emojis) - `material:...` material icon (by Flutter library code) - `res:...` specific resource by UUID 
  @BuiltValueField(wireName: r'icon')
  String? get icon;

  @BuiltValueField(wireName: r'label')
  String get label;

  @BuiltValueField(wireName: r'help')
  String? get help;

  @BuiltValueField(wireName: r'parentItemId')
  String? get parentItemId;

  @BuiltValueField(wireName: r'selectable')
  bool? get selectable;

  ZScannerEnumItem._();

  factory ZScannerEnumItem([void updates(ZScannerEnumItemBuilder b)]) = _$ZScannerEnumItem;

  @BuiltValueHook(initializeBuilder: true)
  static void _defaults(ZScannerEnumItemBuilder b) => b
      ..selectable = true;

  @BuiltValueSerializer(custom: true)
  static Serializer<ZScannerEnumItem> get serializer => _$ZScannerEnumItemSerializer();
}

class _$ZScannerEnumItemSerializer implements PrimitiveSerializer<ZScannerEnumItem> {
  @override
  final Iterable<Type> types = const [ZScannerEnumItem, _$ZScannerEnumItem];

  @override
  final String wireName = r'ZScannerEnumItem';

  Iterable<Object?> _serializeProperties(
    Serializers serializers,
    ZScannerEnumItem object, {
    FullType specifiedType = FullType.unspecified,
  }) sync* {
    yield r'itemId';
    yield serializers.serialize(
      object.itemId,
      specifiedType: const FullType(String),
    );
    if (object.code != null) {
      yield r'code';
      yield serializers.serialize(
        object.code,
        specifiedType: const FullType(String),
      );
    }
    if (object.icon != null) {
      yield r'icon';
      yield serializers.serialize(
        object.icon,
        specifiedType: const FullType(String),
      );
    }
    yield r'label';
    yield serializers.serialize(
      object.label,
      specifiedType: const FullType(String),
    );
    if (object.help != null) {
      yield r'help';
      yield serializers.serialize(
        object.help,
        specifiedType: const FullType(String),
      );
    }
    if (object.parentItemId != null) {
      yield r'parentItemId';
      yield serializers.serialize(
        object.parentItemId,
        specifiedType: const FullType(String),
      );
    }
    if (object.selectable != null) {
      yield r'selectable';
      yield serializers.serialize(
        object.selectable,
        specifiedType: const FullType(bool),
      );
    }
  }

  @override
  Object serialize(
    Serializers serializers,
    ZScannerEnumItem object, {
    FullType specifiedType = FullType.unspecified,
  }) {
    return _serializeProperties(serializers, object, specifiedType: specifiedType).toList();
  }

  void _deserializeProperties(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
    required List<Object?> serializedList,
    required ZScannerEnumItemBuilder result,
    required List<Object?> unhandled,
  }) {
    for (var i = 0; i < serializedList.length; i += 2) {
      final key = serializedList[i] as String;
      final value = serializedList[i + 1];
      switch (key) {
        case r'itemId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.itemId = valueDes;
          break;
        case r'code':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.code = valueDes;
          break;
        case r'icon':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.icon = valueDes;
          break;
        case r'label':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.label = valueDes;
          break;
        case r'help':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.help = valueDes;
          break;
        case r'parentItemId':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(String),
          ) as String;
          result.parentItemId = valueDes;
          break;
        case r'selectable':
          final valueDes = serializers.deserialize(
            value,
            specifiedType: const FullType(bool),
          ) as bool;
          result.selectable = valueDes;
          break;
        default:
          unhandled.add(key);
          unhandled.add(value);
          break;
      }
    }
  }

  @override
  ZScannerEnumItem deserialize(
    Serializers serializers,
    Object serialized, {
    FullType specifiedType = FullType.unspecified,
  }) {
    final result = ZScannerEnumItemBuilder();
    final serializedList = (serialized as Iterable<Object?>).toList();
    final unhandled = <Object?>[];
    _deserializeProperties(
      serializers,
      serialized,
      specifiedType: specifiedType,
      serializedList: serializedList,
      unhandled: unhandled,
      result: result,
    );
    return result.build();
  }
}

