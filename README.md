# zScanner API project

This project contains API of zScanner project and generated libraries out of the definitions.

You can browse the API specification in [openapi.yml](openapi.yml) and all defined metadata
definitions in [metadata.yml](metadata.yml).

## Headers

There are some headers that the server expects from application. They are:

- `x-supported-caps` list of capabilities supported by client (see [metadata.yml](metadata.yml))
- `x-required-caps` list of capabilities required from server by client (see [metadata.yml](metadata.yml))
- `x-tracking-id` ID of the device as reported by the platform (can be application-specific)

server in turn sends the same capability headers back with reciprocal meaning:

- `x-supported-caps` list of capabilities supported by server
- `x-required-caps` list of capabilities required from client by server

## Supported barcodes and other codes

The codes supported are guided by [dart qr code scanner](https://github.com/juliuscanute/qr_code_scanner/blob/master/lib/src/types/barcode_format.dart):

- `AZTEC`
- `CODABAR` (not supported on iOS)
- `CODE_39`
- `CODE_93`
- `CODE_128`
- `DATA_MATRIX`
- `EAN_8`
- `EAN_13`
- `ITF`
- `MAXICODE` (not supported on iOS)
- `PDF_417`
- `QR_CODE`
- `RSS14` (not supported on iOS)
- `RSS_EXPANDED` (not supported on iOS)
- `UPC_A` (same as ean-13 on iOS)
- `UPC_E`
- `UPC_EAN_EXTENSION`

## Regenerating the libraries

To generate new libraries, run

```shell
rm -rf $HOME/.m2/repository/org/yaml/snakeyaml $HOME/.groovy/grapes
grape install org.yaml snakeyaml 1.26
./mvnw install
```

in the root of the directory.

## Creating a release

Make sure you have your Maven authenticated for upload to Gitlab. If not, create a personal token [here](https://gitlab.com/-/profile/personal_access_tokens) with scope `api` then create file `~/.m2/settings.xml` to which add [this](https://pastebin.com/sqiWj00Y) and replace `TOKEN` with your new token (beware `TOKEN` is there twice)

To release a new version, you have to have all changes committed first and you have to be on the `master` branch. Then run:

```shell
./mvnw release:prepare
```

this will ask you several questions, where the important one is the first one:

```
What is the release version for "zscanner-api-multi-module"? (eu.bindworks.zscanner2:zscanner-api-multi-module) 0.9.27: :
```

please answer with version that is **one up from the suggested one**. Here, maven suggests `0.9.27`, so I would answer `0.9.28`.
All the other questions should be left unanswered, which will use their default value.

Maven will try to recompile and test everything and finish with no error if it is happy.

**If there was an error**, you can rollback whatever was performed during the release preparation using

```shell
./mvnw release:rollback
```

**Only if there was no error** during the preparation, you are allowed to run

```shell
./mvnw release:perform
```

this will actually perform the release and do all `git` magic as well. **After it is done, please run**:

```shell
git push --tags
```
