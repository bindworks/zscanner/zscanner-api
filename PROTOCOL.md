# ZScanner Mobile Protocol

## TOC

- [Introduction](#introduction)
- [Concepts](#concepts)
- [Communication](#communication)

## Introduction

ZScanner is an application for authoring media content and sending it directly to a backend system.
The application itself is highly customizable via configuration sent from the server. Thus,
via changing the data schema on the server, one can customize the flow of user's work quickly.

Upon start, the application **synchronizes** it's schema with the server. This includes
the schema for folders, subfolders, flows and various enumerations.

It then allows user to **find a folder** to upload to. In hospital environment, Folder usually corresponds
to a Patient in NIS.

The user is then **asked which flow** she would like to follow. This roughly corresponds to a process.
Flows are configurable from the server and can invoke various "modules" in the application. Modules
include ones for taking photos, for scanning documents, taking a video, etc...

Application guides the user thru the flow. User **authors some media** and **annotates them
with various metadata**. A package is created to be send to the server.

Application **submits the package** to the server, along with all the media.

Server **processes the package** and depending on the site customization submits the media and metadata.

## Concepts

### Metadata

Many times it is necessary to send over information of dynamic schema. Attributes of folders
(patients) differ from hospital to hospital. Metadata filled in for documents differ from metadata
filled in for photos of wounds.

To make the application easier, all these different instances of "dynamic classes" are represented
using the same abstraction - metadata.

Metadata is a set of `metaId` and their values (string, number, date, datetime, boolean). The meaning of
the individual attributes is not defined and is completely dynamic in each installation.
Each installation have **different schema of metadata** for different entities.

The following entities can contain metadata:

- Environment (global)
- Folders
- Subfolders
- Batch (defined by its Flow)
- Media (defined by the Batch it is in)

Example of metadata schema:

```json
[
  { 
    "metaId": "description",
    "label": "Document description",
    "type": "string",
    "required": false,
    "validators": [
      { "minLength": 2 },
      { "maxLength": 100 }
    ]
  },
  {
    "metaId": "documentType",
    "label": "Document type",
    "type": "string",
    "required": true,
    "enum": "documentTypes"
  }
]
```

The above schema defines two properties, `description` and `documentType`. This is probably
a potential schema of a document batch. One is a custom enterable string, the other one is
an entry from an enumeration (more on enumerations later).

Example of the actual metadata:

```json
[
  {
    "metaId": "description",
    "value": {
      "stringValue": "Blood test report"
    }
  },
  {
    "metaId": "documentType",
    "value": {
      "stringValue": "bloodtest"
    }
  }
]
```

### Global Metadata

There is a set of metadata (with appropriate schema), that is not assigned to a batch or folder, but the "session".
This can be used to select from multiple departments that the user is member of for example. This "form" is filled in
right after logging.

Schema for this metadata has the same shape as any other metadata. Global metadata (for example the selected department)
are used when calling synchronization or requesting folders.

### Folders and Subfolders

Each batch of media created must be assigned to a Folder. In Hospitals, Folder usually represents
a patient. But the concept is general. Folders can have metadata and therefore have their
own Folder metadata schema.

Each folder can have subfolders. For example in Hospitals, each Patient can have a list of
diagnoses that the patient is treated for in the hospital. Or it can be a list of wounds
that are being monitored.

These are represented as subfolders. A given installation can have multiple types of Subfolders
for one Folder. E.g. Patient can have both diagnoses and wounds. Each of these types
have specific metadata schemata.

The synchronization call contains all schemata, here is an example of a part of synchronization
call that deals with folder and subfolders schemata:

```json
{
  "folderSchema": [
    {
      "metaId": "fullName",
      "label": "Patient name",
      "type": "string"
    },
    {
      "metaId": "birthDate",
      "label": "Datum narození",
      "type": "date"
    }
  ],
  "subfolderSchemata": [
    {
      "typeId": "wounds",
      "label": "Wounds",
      "moduleId": "spotmap",
      "moduleVersion": "any",
      "config": {
        "spotMap": {
          "hereBeSpotmapConfiguration.....": true
        }
      },
      "displaySchema": [
        {
          "metaId": "label",
          "label": "Label",
          "type": "string"
        }
      ],
      "createSchema": [
        {
          "metaId": "label",
          "label": "Label",
          "type": "string"
        }
      ],
      "updateSchema": []
    }
  ]
}
```

As you can see, subfolders can be of many types in one installation. Also note, that subfolders
have multiple schemata. One for displaying them, one to fill in when being created via zscanner and
one to fill in when being edited via zscanner. This allows for easy creation of records of new wounds
directly from the application.

An example of a folder returned from one of the search functions:

```json
{
  "internalId": "123132123",
  "externalId": "1123456/112",
  "name": "Jana Veselá",
  "type": "RESULT",
  "metadata": [
    {
      "metaId": "fullName",
      "value": {
        "stringValue": "Jana Veselá"
      }
    },
    {
      "metaId": "birthDate",
      "value": {
        "dateValue": "1980-02-03"
      }
    }
  ]
}
```

An example of a subfolder:

```json
{
  "subfolderId": "def/12332/fdfjf",
  "typeId": "wounds",
  "metadata": [
    {
      "metaId": "label",
      "value": {
        "stringValue": "Bruised ankle"
      }
    }
  ]
}
```

Folders can be requested using `POST /v4/folders`. After selecting a folder, the respective
subfolders can be requested using `POST /v4/folders/{folderId}/subfolders` call.

### Flows and Batches

Users want to take photos or videos of various things. Documents, ID Cards, faces, wounds, etc. Each
should have a slightly different screen flow and user experience. ID cards can be searched for
data automatically, Documents can be cropped and enhanced for better readability, etc.
Moreover the internal processes of the organization may require, that Documents end up in a different
backend system than images of wounds for example.

ZScanner has a concept of Flows. Each flow represents a process (screenflow) in application that the user
can go thru to author some media. Each flow has a module in application that can execute it (image camera, document camera),
and a set of metadata that the user may or may not fill in.

After the flow is complete there is a Batch created in the application which is then submitted to the
server. Server verifies, that all the metadata and media in the batch conform to the
requirements of the flow and then submits it to the backend systems.

Example of a flow definition:

```json
{
  "flowId": "selfEmailWoundPhotoFlow",
  "label": "Wound documentation",
  "moduleId": "photo",
  "moduleVersion": "any",
  "moduleConfig": {
    "subfolder": "OPTIONAL",
    "subfolderTypeId": "wounds"
  },
  "metadataSchema": [
  ]
}
```

and an example of a corresponding batch created according to this flow:

```json
{
  "uuid": "7f3e9c27-3e2e-4205-84f3-700b16b7f68b",
  "folderId": "123132123",
  "flowId": "selfEmailWoundPhotoFlow",
  "moduleId": "photo",
  "moduleVersion": "1.0.0",
  "createdTs": "2022-03-12T23:11:23Z",
  "media": [
    {
      "mediaId": "58507f82-b85b-41de-8500-7263b020c58a",
      "contentType": "image/jpeg",
      "contentLength": 123123,
      "photoTs": "2022-03-12T23:11:23Z",
      "metadata": [],
      "subfolderTypeId": "wounds",
      "subfolderId": "579853f2-dd78-45c0-a3ab-deff6f9306e1"
    },
    {
      "mediaId": "542430ee-f200-40f5-8ce4-7bf81dc97d5d",
      "contentType": "image/jpeg",
      "contentLength": 343443,
      "photoTs": "2022-03-12T23:11:23Z",
      "metadata": [],
      "subfolderTypeId": "wounds",
      "subfolderId": "def/12332/fdfjf"
    }
  ],
  "metadata": [],
  "subfolders": [
    {
      "subfolderId": "579853f2-dd78-45c0-a3ab-deff6f9306e1",
      "typeId": "wounds",
      "metadata": [
        {
          "metaId": "label",
          "value": {
            "stringValue": "Broken left hand"
          }
        }
      ]
    }
  ]
}
```

Batches and the corresponding media are uploaded using the `POST /v4/batches`, `POST /v4/batches/{uuid}/media/{mediaId}` and
`POST /v4/batches/{uuid}/finish` endpoints.

### Enumerations, Resources

Enumerations and Resources are supporting objects. They are used when a custom image or an
enumeration are used on the installation. Resources include various spot maps, icons, logos, etc...
Enumeration can be the above-mentioned document type for example.

Example of an enumeration:

```json
{
  "enumId": "documentTypes",
  "enumVersion": "1",
  "enumType": "FLAT",
  "createdTs": "2022-03-01T12:23:12Z",
  "updatedTs": "2022-03-01T12:23:12Z",
  "label": "Document Types",
  "items": [
    {
      "itemId": "idcard",
      "code": "idcard",
      "icon": "res:idcard.png",
      "label": "ID Card"
    },
    {
      "itemId": "labtest",
      "code": "labtest",
      "icon": "str:🧪",
      "label": "Laboratory Results"
    },
    {
      "itemId": "examination",
      "code": "examination",
      "icon": "str:🔎",
      "label": "Examination"
    }
  ]
}
```

These enumerations can later be used in metadata to allow user to select an item from a preset list of items.

Resources are just images, videos and sounds that the application should download. They are then
used as icons, spot maps, etc. The amove example of enumeration references resource with resourceId `idcard.jpg`.
An example of resources:

```json
[
  {
    "resourceId": "idcard.png",
    "contentType": "image/png",
    "contentLength": 12332,
    "createdTs": "2022-03-01T21:23:56Z",
    "updatedTs": "2022-03-01T21:23:56Z"
  },
  {
    "resourceId": "dot.gif",
    "contentType": "image/gif",
    "contentLength": 101,
    "createdTs": "2022-03-01T21:23:56Z",
    "updatedTs": "2022-03-01T21:23:56Z",
    "inlineData": "R0lGODlhCgAKAMIFAABi/zhu/4yg/7XA/9DX/////////////yH+EUNyZWF0ZWQgd2l0aCBHSU1QACwAAAAACgAKAAADHVhFEiFElQGqHctqwLZtXuWEwENCZERtmMI4kJIAADs="
  }
]
```

As you can see, small resources can be transferred as inline base64 data, large resources are only referenced
and their contents can be downloaded using `GET /v4/resources/{resourceId}` call.

### Capabilities

To facilitate forward and backward compatibility of both server and client, there is a concept of server and client
capabilities. Client advertises its supported capabilities and capabilities required to be supported by server via headers 
in each request.

The server attempts to tailor the response to the capabilities of the client. In case it is unable to
do that (too old client), it returns an appropriate message in sync call and blocks the application
from accessing the server.

Current capabilities are all defined in [metadata.yml](metadata.yml). Application at
the time of writing this document supported the following capabilities:

- cc.std.1
- cc.subfolders.0
- cc.spotmap.0
- cm.document-scan.0
- cm.photo.0
- cm.video.0
- cm.spotmap.0

## Communication

### Synchronization

Synchronization call aims at transferring all the necessary information from server to the application
in one request incrementally. Let's assume the following installation configuration:

- there is a department to be selected after login
- user has access to one flow, document scan
- document scan flow requires the user to annotate 
  the document with description and a document type 

#### Step 1 - Initial Synchronization

First the application has no state from server and calls an empty synchronization request:

```json5
// POST /v4/sync
// -- some installation-specific authentication
// x-supported-caps: cc.std.1 cc.subfolders.0 cc.spotmap.0 cm.document-scan.0 cm.photo.0 cm.video.0 cm.spotmap.0
// x-required-caps: sc.std.0

{ 
  "metadata": []
}
```

Server responds with the information it can provide. It cannot give the user the list of
flows yet, as the department hasn't been selected. So the server gives the client enough
information to fill in the global metadata:

```json5
// 200 OK
// x-supported-caps: cc.std.1 cc.subfolders.0 cc.spotmap.0 cm.document-scan.0 cm.photo.0 cm.video.0 cm.spotmap.0

{
  "nextState": "jdlkjha8kufhskjhfjs",
  "metadataValid": false,
  "config": { "scannerEnabled": true, "scannerTypes": [ "AZTEC" ] },
  "me": { "id": "kjahd", "displayName": "John Doe" },
  
  "metadataSchema": [                     // <⸻ schema for global metadata
    {
      "metaId": "department",
      "label": "Department",
      "type": "string",
      "required": true,
      "enum": "departments"
    }
  ],
  
  "folderSchema": [                       // <⸻ schema for folder
    {
      "metaId": "fullName",
      "label": "Patient name",
      "type": "string"
    },
    {
      "metaId": "birthDate",
      "label": "Datum narození",
      "type": "date"
    }
  ],
  
  "updatedEnums": [                       // <⸻ enumerations to add to application
    {
      "enumId": "departments",
      "enumVersion": "1",
      "enumType": "FLAT",
      "createdTs": "2022-03-01T12:23:12Z",
      "updatedTs": "2022-03-01T12:23:12Z",
      "label": "Departments",
      "items": [
        {
          "itemId": "dep001",
          "label": "Sales"
        },
        {
          "itemId": "dep002",
          "label": "Accounting"
        }
      ]
    },
    {
      "enumId": "documentTypes",
      "enumVersion": "1",
      "enumType": "FLAT",
      "createdTs": "2022-03-01T12:23:12Z",
      "updatedTs": "2022-03-01T12:23:12Z",
      "label": "Document Types",
      "items": [
        {
          "itemId": "idcard",
          "code": "idcard",
          "icon": "res:idcard.png",
          "label": "ID Card"
        },
        {
          "itemId": "labtest",
          "code": "labtest",
          "icon": "str:🧪",
          "label": "Laboratory Results"
        },
        {
          "itemId": "examination",
          "code": "examination",
          "icon": "str:🔎",
          "label": "Examination"
        }
      ]
    }
  ]
}
```

#### Step 2 - Synchronization with metadata (if necessary)

As you have probably spotted, there are no flows yet and `metadataValid` is `false`. This indicates,
that the user is required to fill in the global metadata. Luckily the schema has arrived and
the application is able to let the user select a department. After that, the
synchronization is attempted again with `nextState` from previous request:

```json5
// POST /v4/sync
// -- some installation-specific authentication
// x-supported-caps: cc.std.1 cc.subfolders.0 cc.spotmap.0 cm.document-scan.0 cm.photo.0 cm.video.0 cm.spotmap.0
// x-required-caps: sc.std.0

{ 
  "state": "jdlkjha8kufhskjhfjs",         // <⸻ nextState from the previous response
  
  "metadata": [                           // <⸻ filled in metadata
    {
      "metaId": "departmentId",
      "value": {
        "stringValue": "dep001"
      }
    }
  ]
}
```

Now the server has all the information necessary and can return the rest of the state:

```json5
// 200 OK
// x-supported-caps: cc.std.1 cc.subfolders.0 cc.spotmap.0 cm.document-scan.0 cm.photo.0 cm.video.0 cm.spotmap.0

{
  "nextState": "pofsohfkjsadhflk",
  "metadataValid": true,
  "config": { "scannerEnabled": true, "scannerTypes": [ "AZTEC" ] },
  "me": { "id": "kjahd", "displayName": "John Doe" },

  // note that the enumerations and resources are not transferred again
  // as the state variable contains enough information to know that
  // the application has them already.
  
  "flows": [
    {
      "flowId": "documentFlow",
      "label": "Document Scan",
      "moduleId": "document-scan",
      "moduleVersion": "any",
      "metadataSchema": [
        {
          "metaId": "description",
          "label": "Document description",
          "type": "string",
          "required": false,
          "validators": [
            { "minLength": 2 },
            { "maxLength": 100 }
          ]
        },
        {
          "metaId": "documentType",
          "label": "Document type",
          "type": "string",
          "required": true,
          "enum": "documentTypes"
        }
      ]
    }
  ]
}
```

If there were any resources without inline data present in the synchronization, the application
would have to call `GET /v4/resources/{resourceId}` for each such resource
to get the contents.

### Folder selection

The user must now select a folder o which it wants to submit some documents. Application
allows for AZTEC barcode scanner (as per above configuration) or fulltext search and invokes
the `POST /v4/folders` endpoint as follows:

```json5
// POST /v4/folders
// -- some installation-specific authentication
// x-supported-caps: cc.std.1 cc.subfolders.0 cc.spotmap.0 cm.document-scan.0 cm.photo.0 cm.video.0 cm.spotmap.0
// x-required-caps: sc.std.0

{
  "metadata": [                           // <⸻ filled in global metadata
    {
      "metaId": "departmentId",
      "value": {
        "stringValue": "dep001"
      }
    }
  ],
  
  "fullText": "Clint"
}
```

the server responds with matching folders:

```json5
// 200 OK
// x-supported-caps: cc.std.1 cc.subfolders.0 cc.spotmap.0 cm.document-scan.0 cm.photo.0 cm.video.0 cm.spotmap.0

[
  {
    "internalId": "123132123",
    "externalId": "1123456/112",
    "name": "Bill Clinton",
    "type": "RESULT",
    "metadata": []
  }
]
```

### Batch Submission

After the user creates a batch a compact structure with all data about it is created and
submitted to the server. The media are only referenced here, the contents are not
transferred yet.

Each batch has a random UUID that is scoped to the logged in user.

```json5
// POST /v4/batches
// -- some installation-specific authentication
// x-supported-caps: cc.std.1 cc.subfolders.0 cc.spotmap.0 cm.document-scan.0 cm.photo.0 cm.video.0 cm.spotmap.0
// x-required-caps: sc.std.0

{ 
  "uuid": "7f3e9c27-3e2e-4205-84f3-700b16b7f68b",
  "folderId": "123132123",
  "flowId": "documentFlow",
  "moduleId": "document-scan",
  "moduleVersion": "1.0.0",
  "createdTs": "2022-03-12T23:11:23Z",
  "metadata": [
    {
      "metaId": "departmentId",
      "value": {
        "stringValue": "dep001"
      }
    },
    {
      "metaId": "documentType",
      "value": {
        "stringValue": "idcard"
      }
    },
    {
      "metaId": "description",
      "value": {
        "stringValue": "passport"
      }
    }
  ],
  "media": [
    {
      "mediaId": "58507f82-b85b-41de-8500-7263b020c58a",
      "contentType": "image/jpeg",
      "contentLength": 123123,
      "photoTs": "2022-03-12T23:11:23Z"
    }
  ],
}
```

server creates the batch and waits for application to submit each media:

```json5
// POST /v4/batches/7f3e9c27-3e2e-4205-84f3-700b16b7f68b/media/58507f82-b85b-41de-8500-7263b020c58a
// -- some installation-specific authentication
// x-supported-caps: cc.std.1 cc.subfolders.0 cc.spotmap.0 cm.document-scan.0 cm.photo.0 cm.video.0 cm.spotmap.0
// x-required-caps: sc.std.0
// content-type: multipart/form-data

// file upload here...

```

after each media is submitted, the application calls the finish batch call:

```json5
// POST /v4/batches/7f3e9c27-3e2e-4205-84f3-700b16b7f68b/finish
// -- some installation-specific authentication
// x-supported-caps: cc.std.1 cc.subfolders.0 cc.spotmap.0 cm.document-scan.0 cm.photo.0 cm.video.0 cm.spotmap.0
// x-required-caps: sc.std.0

```

