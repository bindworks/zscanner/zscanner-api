package eu.bindworks.zscanner.metadata

import eu.bindworks.zscanner.api.model.*
import org.springframework.stereotype.Component

class Capabilities(anonymous: Boolean, val isServer: Boolean, val name: String?, val impliedCapabilities: Set<Capabilities>? = null): Iterable<Capabilities> {
    init {
        if (anonymous && name != null)
            throw IllegalArgumentException()
        else if (!anonymous && name == null)
            throw IllegalArgumentException()
    }

    constructor(name: String, impliedCapabilities: Set<Capabilities>? = null): this(false, name[0] == 's', name, impliedCapabilities)

    fun supports(other: Capabilities): Boolean =
        other == this || impliedCapabilities?.any { it.supports(other) } == true

    fun missing(cs: Iterable<Capabilities>) = cs.filter { !supports(it) }

    override fun equals(other: Any?) =
        name != null && (other as? Capabilities)?.name == name

    override fun hashCode() = name?.hashCode() ?: 0

    override fun toString() = name ?: "anonymous"

    private fun collect(to: MutableSet<Capabilities>): MutableSet<Capabilities> {
        if (name != null)
            to.add(this)
        impliedCapabilities?.forEach { it.collect(to) }
        return to
    }

    override fun iterator(): Iterator<Capabilities> {
        return collect(mutableSetOf()).iterator()
    }

    companion object {
        fun anonymous(capabilities: Set<Capabilities>) =
            Capabilities(true, false, null, capabilities)
    }
}

@Component
class AllCapabilities(val allCapabilities: List<Capabilities>) {
    val capabilitiesByName = allCapabilities
        .fold(mutableMapOf<String, Capabilities>()) { acc, i ->
            if (i.name != null && acc.put(i.name, i) != null)
                throw IllegalArgumentException("There are two capabilities with name ${i.name} registered")
            acc
        }

    fun unknown(capabilityTags: List<String>?) =
        capabilityTags?.flatMap(::expand)?.filter { !capabilitiesByName.containsKey(it) } ?: listOf()

    fun parse(capabilityTags: List<String>?) =
        if (capabilityTags == null || capabilityTags.isEmpty())
            Capabilities.anonymous(setOf(
                StandardCapabilities.CLIENT_CAPABILITY_STANDARD_0,
                StandardCapabilities.CLIENT_MODULE_DOCUMENT_SCAN_0,
                StandardCapabilities.CLIENT_SECURITY_OIDC_0
            ))
        else
            Capabilities.anonymous(capabilityTags
                .map { parse(it) }
                .reduce(Set<Capabilities>::plus))

    fun parse(capabilityTag: String): Set<Capabilities> {
        return expand(capabilityTag)
            .mapNotNull { capabilitiesByName[it] }
            .toSet()
    }

    fun expand(capabilityTag: String): List<String> {
        val constituents = capabilityTag.split('.')
        if (constituents.size != 3) {
            return listOf(capabilityTag)
        }
        val versions = constituents[2].split('+')
        return versions.map { "${constituents[0]}.${constituents[1]}.${it}" }
    }

    fun serverCapabilityTags() =
        allCapabilities.filter { it.isServer }.mapNotNull { it.name }
}
