package eu.bindworks.zscanner.metadata

import eu.bindworks.zscanner.api.model.ZScannerFlow

interface IModule {
    fun supported(flow: ZScannerFlow, capabilities: Capabilities): Boolean
}

data class GenericModule(
    val moduleId: String,
    val supportedGetter: (flow: ZScannerFlow, caps: Capabilities) -> Boolean,
): IModule {
    override fun supported(flow: ZScannerFlow, capabilities: Capabilities) =
        flow.moduleId != moduleId || supportedGetter(flow, capabilities)
}
