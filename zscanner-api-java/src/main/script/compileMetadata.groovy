import groovy.text.StreamingTemplateEngine
@Grab(group = 'org.codehaus.groovy', module = 'groovy-yaml', version = '3.0.7')
import groovy.yaml.YamlSlurper

class Utility {
}

def standardModulesTemplateText='''\
package eu.bindworks.zscanner.metadata

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class StandardModules {
    companion object {
<% for (module in modules) { %>
        val <%= module.name %> = GenericModule(
            "<%= module.id %>",
            { _, c -> c.supports(StandardCapabilities.<%= module.capability.name %>) }
        ) 
<% } %>
    }

<% for (module in modules) { %>
    @Bean fun BEAN_<%= module.name %>() = <%= module.name %><% } %>
}
'''

def standardCapabilitiesTemplateText='''\
package eu.bindworks.zscanner.metadata

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class StandardCapabilities {
    companion object {
<% for (capability in capabilities.client + capabilities.server) { %>
        /**
         * <%= capability.description.replace("\\n", "\\n         * ") %>
         */
        val <%= capability.name %> = Capabilities("<%= capability.id %>"<% if (capability.impliedNames != null) { %>, setOf(<% for (name in capability.impliedNames) { %><%= name %>, <% } %>)<% } %>)
<% } %>
    }

<% for (capability in capabilities.client + capabilities.server) { %>
    @Bean fun BEAN_<%= capability.name %>() = <%= capability.name %><% } %>
}
'''

def sourceFile = new File((String) properties.getOrDefault("sourceFile", "../../../../metadata.yml"))
def targetDirectory = new File((String) properties.getOrDefault("targetDirectory", "."))
if (!targetDirectory.exists()) {
    targetDirectory.mkdirs()
}

def ys = new YamlSlurper()
def metadata = (Map) ys.parse(sourceFile)

metadata.util = new Utility()

def compile(Map metadata, File targetFile, String templateText) {
    if (binding.hasVariable("log")) {
        log.info("Writing ${targetFile.absolutePath}...")
    } else {
        println("Writing ${targetFile.absolutePath}...")
    }
    def template = new StreamingTemplateEngine().createTemplate(templateText)
    targetFile.write(template.make(metadata).toString())
}

def link(Map metadata) {
    def modules = (List) metadata.modules
    modules.forEach({ Map it ->
        def capabilityId = it.capability
        def capability = metadata.capabilities.client.find { q -> q.id == capabilityId }
        it.capability = capability
    })
    def capabilities = (List) metadata.capabilities.client
    capabilities.forEach({ Map capability ->
        if (capability.containsKey("implies")) {
            capability.impliedNames = new ArrayList()
            def implies = (List) capability.implies
            implies.forEach({ String impliedId ->
                def impliedCapability = metadata.capabilities.client.find { q -> q.id == impliedId }
                capability.impliedNames.add(impliedCapability.name)
            })
        }
    })
}

link(metadata)
compile(metadata, new File(targetDirectory, "StandardCapabilities.kt"), standardCapabilitiesTemplateText)
compile(metadata, new File(targetDirectory, "StandardModules.kt"), standardModulesTemplateText)
