package eu.bindworks.zscanner.metadata

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class CapabilitiesTests {

    @Test
    fun testAllCapabilities() {
        val allCapabilities = AllCapabilities(listOf(StandardCapabilities.CLIENT_CAPABILITY_STANDARD_0, StandardCapabilities.CLIENT_MODULE_DOCUMENT_SCAN_0, StandardCapabilities.CLIENT_SECURITY_OIDC_0))
        assertEquals(listOf("unknown"), allCapabilities.unknown(listOf("unknown", StandardCapabilities.CLIENT_CAPABILITY_STANDARD_0.name!!)))

        val clientCapabilities = allCapabilities.parse(listOf(StandardCapabilities.CLIENT_CAPABILITY_STANDARD_0.name!!))
        assertTrue(clientCapabilities.supports(StandardCapabilities.CLIENT_CAPABILITY_STANDARD_0))

        assertEquals(listOf(StandardCapabilities.CLIENT_SECURITY_OIDC_0), clientCapabilities.missing(listOf(StandardCapabilities.CLIENT_CAPABILITY_STANDARD_0, StandardCapabilities.CLIENT_SECURITY_OIDC_0)))
    }
}
